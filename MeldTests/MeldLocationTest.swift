//
//  MeldLocationTest.swift
//  MeldTests
//
//  Created by Blake Rogers on 6/6/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import XCTest
@testable import Meld
import MapKit
import CoreLocation
class MeldLocationTest: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    func testLatitudeDegreesToMiles() {
        let location = MeldLocation.random(miles: 10)
        let region = MKCoordinateRegion(center: MeldLocation.testCoordinate, latitudinalMeters: 10*1600, longitudinalMeters: 10*1600)
        let latDelta = region.span.latitudeDelta
        let longDelta = region.span.longitudeDelta
        let earthCircumference: Double = 24901.92
        let milePerLatitudeDegree = earthCircumference/360
        let latDeltaMiles = milePerLatitudeDegree * latDelta
        print("milePerLatitudeDegree:", milePerLatitudeDegree)
        print("latDeltaMiles:", latDeltaMiles)
        XCTAssert(latDeltaMiles > 9.9 && latDeltaMiles < 10.1)
    }
    func testCoordinateWithin10Miles() {
        let location = MeldLocation.random(miles: 10)
        let region = MKCoordinateRegion(center: MeldLocation.testCoordinate, latitudinalMeters: 10*1600, longitudinalMeters: 10*1600)
        let latDelta = region.span.latitudeDelta
        let longDelta = region.span.longitudeDelta
        let isWithinLatRange = ((region.center.latitude - latDelta)...(region.center.latitude + latDelta)).contains(location.coordinate.latitude)
        let isWithinLongRange = ((region.center.longitude - longDelta)...(region.center.longitude + longDelta)).contains(location.coordinate.longitude)
        print("location:", location.coordinate)
        print("latDelta:", latDelta)
        print("longDela:", longDelta)
        print("latMin:", region.center.latitude - latDelta)
        print("latMax:", region.center.latitude + latDelta)
        print("longMin:", region.center.longitude + longDelta)
        print("longMax:", region.center.longitude - longDelta)
        XCTAssert(isWithinLatRange && isWithinLongRange )
    }

    
    
}
