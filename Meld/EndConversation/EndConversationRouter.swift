//
//  EndConversationRouter.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UIKit
import UtilitiesPackage

final class EndConversationRouter {
    func dismiss() {
        currentController().dismiss(animated: true, completion: nil)
    }
}
extension EndConversationRouter {
    static func presentation(conversationsInteractor: ConversationsInteractorIdentity) -> UIViewController {
        EndConversationView(presenter: EndConversationPresenter(interactor: EndConversationInteractor(conversationsInteractor: conversationsInteractor)))
    }
}
