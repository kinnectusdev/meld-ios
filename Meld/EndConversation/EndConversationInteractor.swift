//
//  EndConversationInteractor.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift

protocol EndConversationInteractorIdentity {
    func didSelectEndConversation()
    func didSelectBlock()
    func didSelectReportAsAbusive()

}
final class EndConversationInteractor {
    private let conversationsInteractor: ConversationsInteractorIdentity
    init(conversationsInteractor: ConversationsInteractorIdentity) {
        self.conversationsInteractor = conversationsInteractor
    }
}

extension EndConversationInteractor: EndConversationInteractorIdentity {
    func didSelectEndConversation() {
        conversationsInteractor.didSelectEndConversation()
    }
    
    func didSelectBlock() {
        conversationsInteractor.didSelectBlock()
    }
    
    func didSelectReportAsAbusive() {
        conversationsInteractor.didSelectReportAsAbusive()
    }
}
