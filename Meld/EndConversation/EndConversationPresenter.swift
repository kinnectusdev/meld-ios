//
//  EndConversationPresenter.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import UtilitiesPackage
import UIKit

final class EndConversationPresenter {
    private let bag = DisposeBag()
    private let interactor: EndConversationInteractorIdentity
    private let router: EndConversationRouter
    
    let dismissViewButton = dismissButton("X", {})
    let endConversationButton = UIButton()
    let blockButton = UIButton()
    let keepTalkingButton = UIButton()
    let reportButton = UIButton()
    
    init(interactor: EndConversationInteractorIdentity) {
        self.interactor = interactor
        self.router = EndConversationRouter()
        
        bag.insert(
            endConversationButton.rx.tap.subscribe(onNext: { [weak self] _ in
                self?.interactor.didSelectEndConversation()
                self?.router.dismiss()
            }),
            blockButton.rx.tap.subscribe(onNext: { [weak self] _ in
                self?.interactor.didSelectBlock()
            }),
            reportButton.rx.tap.subscribe(onNext: { [weak self] _ in
                self?.interactor.didSelectReportAsAbusive()
            }),
            keepTalkingButton.rx.tap.subscribe(onNext: { [weak self] _ in
                self?.router.dismiss()
            }),
            dismissViewButton.rx.tap.subscribe(onNext: { [weak self] _ in
                self?.router.dismiss()
            })
        )
    }
}
