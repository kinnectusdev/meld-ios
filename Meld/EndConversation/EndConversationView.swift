//
//  EndConversationPage.swift
//  Meld
//
//  Created by blakerogers on 11/30/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import UtilitiesPackage

final class EndConversationView: UIViewController {
    
    private var presenter: EndConversationPresenter!
    
    let endButtonName: String = "end_button"
    let blockButtonName: String = "block_button"
    let notInterestedButtonName: String = "not_interested_button"
    let reportButtonName: String = "report_button"
    let continueButtonName: String = "continue_button"
    
    private func setViews() {
        let view = UIView()
            |> blurredWhiteBackground
            |> appendSubView(UILabel()
                        |> named("title")
                        |> alignToTop(50)
                        |> alignedToCenterX
                        |> styledTitle(formalBlackAttributedString("Are you sure you want to end this conversation?", 16)))
            >>> appendSubView(presenter.endConversationButton
                        |> named(endButtonName)
                        |> alignedToCenterX
                        |> alignToSibling("title", constraint: alignTopToSiblingBottom(150))
                        >>> styledTitle(darkGreyAttributedString("End Conversation", 15))
                        >>> roundedGreyShadowed(25)
                        >>> width_Height(150, 50))
            >>> appendSubView(presenter.blockButton
                        |> named(blockButtonName)
                        |> alignedToCenterX
                        |> alignToSibling(endButtonName, constraint: alignTopToSiblingBottom(20))
                        >>> styledTitle(darkGreyAttributedString("Block This User", 15))
                        >>> roundedGreyShadowed(25)
                        >>> width_Height(150, 50))
            >>> appendSubView(presenter.reportButton
                        |> named(reportButtonName)
                        |> alignedToCenterX
                        >>> alignToSibling(blockButtonName, constraint: alignTopToSiblingBottom(20))
                        >>> styledTitle(darkGreyAttributedString("Report Abusive Behavior", 15))
                        >>> roundedGreyShadowed(25)
                        >>> width_Height(250, 50))
            >>> appendSubView(presenter.keepTalkingButton
                                |> named(continueButtonName)
                                |> alignedToCenterX
                                |> alignToSibling(reportButtonName, constraint: alignTopToSiblingBottom(20))
                                >>> styledTitle(darkGreyAttributedString("Keep Talking", 15))
                                >>> roundedGreyShadowed(25)
                                >>> width_Height(150, 50))
        
        self.view = view
    }
}
extension EndConversationView {
    convenience init(presenter: EndConversationPresenter) {
        self.init()
        self.presenter = presenter
        self.setViews()
    }
}
