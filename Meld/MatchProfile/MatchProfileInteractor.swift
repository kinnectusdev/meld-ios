//
//  MatchProfileViewModel.swift
//  Meld
//
//  Created by blakerogers on 12/10/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import RxSwift
import RxCocoa
import FirebaseFirestore
import CoreLocation

enum MatchProfileAlert {
    case error(String)
    case noMatches
}
protocol MatchProfileInteractorIdentity: MatchUserInfoConversationDelegate, MatchProfileDelegate {
    
    // Input Actions
    func didSelectBack()
    func didSelectForward()
    func didSelectRetry()
    
    // Outputs
    func observeUsers() -> Observable<[MeldUser]>
    func observeIsLoading() -> Observable<Bool>
    func userImages() -> Observable<[String]>
    func alert() -> Observable<MatchProfileAlert>
}

final class MatchProfileInteractor {
    
    //MARK: Stored Properties
    private let bag = DisposeBag()
    private let homeInteractor: HomeInteractorIdentity
    
    //MARK: Observed Properties
    private let displayedUserListener = PublishSubject<MeldUser>()
    private let userImagesListener = PublishSubject<[String]>()
    private let currentRadiusListener = PublishSubject<Double>()
    private let alertListener = PublishSubject<MatchProfileAlert>()
    private let isLoadingInfoListener = PublishSubject<Void>()
    private let isLoading = BehaviorRelay<Bool>(value: true)
    
    //MARK: Internal Managed Properties
    private var currentUser: MeldUser = .empty
    private var currentLocation: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 0, longitude: 0)
    private var currentUserIndex: Int = 0
    private var matches: [MeldUser] = []
    private let matchesListener = BehaviorRelay<[MeldUser]>(value: [])
    private var numberOfMatches: Int = 0
    
    //MARK: Initializer
    init(homeInteractor: HomeInteractorIdentity) {
        self.homeInteractor = homeInteractor
        self.observeCurrentUser()
        self.setCurrentLocation()
        self.observeFilter()
    }
    private func observeFilter() {
        GlobalEventNotifier.shared.observeEvent().filter { $0 == .didUpdateFilter}.subscribe(onNext: { [weak self] _ in
            self?.searchForMatch()
        }).disposed(by: bag)
    }
    private func setCurrentLocation() {
        LocationManager()
            .fetchLocation()
            .flatMap{ result  -> Observable<LocationManager.LocationResult> in
                switch result {
                case .success(let location):
                    return UserService.updateUserLocation(latitude: location.coordinate.latitude,
                                                          longitude: location.coordinate.longitude)
                        .map { _ in result }
                default:
                    return .just(result)
                }
            }.subscribe(onNext: { [weak self] result in
                switch result {
                case .success(let location):
                    self?.currentLocation  = CLLocationCoordinate2D(latitude: location.coordinate.latitude,
                                                                    longitude: location.coordinate.longitude)
                    self?.searchForMatch()
                case .failure(let error):
                    self?.alertListener.onNext(.error(error.localizedDescription))
                }
            }).disposed(by: bag)
    }
    
    private func observeCurrentUser() {
        UserService.currentUser().map { $0.user }.compactMap { $0 }.subscribe(onNext: { [weak self] user in
            self?.currentUser = user
        }).disposed(by: bag)
    }
    
    private func searchForMatch() {
        let personality = currentUser.personalityType ?? .intj
        let location = currentLocation
        isLoading.accept(true)
        //MatchingService.getComplimentaryUsers(personality: personality,
//                                              location: location)
        MatchingService.testGetComplimentaryUser()
            .subscribe(onNext: { [weak self] result in
                switch result {
                case let .success(users):
                    self?.matches = users
                    self?.matchesListener.accept(users)
                    self?.numberOfMatches = users.count
                    self?.currentUserIndex = 0
                    self?.isLoading.accept(false)
                    self?.updateDisplayedUser(user: self?.matches.first)
                case .failure(let error):
                    switch error {
                    case "No Matches Found":
                        self?.alertListener.onNext(.noMatches)
                    default:
                        self?.alertListener.onNext(.error(error))
                    }
                    self?.isLoading.accept(false)
                }
            }).disposed(by: bag)
    }
    
    private func updateDisplayedUser(user: MeldUser?) {
        print("did update user")
        guard let user = user else {
            alertListener.onNext(.noMatches)
            return
        }
        displayedUserListener.onNext(user)
        userImagesListener.onNext(user.imageURLs)
        isLoadingInfoListener.onNext(())
        UserService.updateUserSeenStatus(id: user.id)
    }
}

extension MatchProfileInteractor: MatchProfileInteractorIdentity {
    func observeIsLoading() -> Observable<Bool> {
        isLoading.asObservable()
    }
    func observeUsers() -> Observable<[MeldUser]> {
        matchesListener.asObservable()
    }
    func didUpdateSearchRadius(radius: Double) {
        currentRadiusListener.onNext(radius)
        searchForMatch()
    }
    
    func didSelectBack() {
        currentUserIndex = max(0, currentUserIndex - 1)
        isLoadingInfoListener.onNext(())
        updateDisplayedUser(user: matches.itemAt(currentUserIndex))
    }
    
    func didSelectForward() {
        currentUserIndex = min(numberOfMatches, currentUserIndex + 1)
        isLoadingInfoListener.onNext(())
        updateDisplayedUser(user: matches.itemAt(currentUserIndex))
    }
    func didSelectRetry() {
        searchForMatch()
    }
    func userImages() -> Observable<[String]> {
        userImagesListener.asObservable()
    }
    
    func alert() -> Observable<MatchProfileAlert> {
        alertListener.asObservable()
    }

    func displayedUserInfo() -> Observable<MeldUser> {
        displayedUserListener.asObservable()
    }
    func isLoadingUserInfo() -> Observable<()> {
        isLoadingInfoListener.asObservable()
    }
}
