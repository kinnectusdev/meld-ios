//
//  MatchProfileController.swift
//  Meld
//
//  Created by blakerogers on 12/9/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import FirebaseStorage
import CHIPageControl
import UtilitiesPackage

final class MatchProfileView: UIView {
    private var presenter: MatchProfilePresenter!
    private func setViews() {
        let view = UIView()
            |> fill
            |> rounded(10)
            |> appendSubView(
                presenter.matchImages
                |> named("user_image")
                >>> alignedToLeftTopRight
                >>> alignToBottom(-250)
            )
            |> appendSubView(
                presenter.pageControl
                |> utilizeConstraints
                |> isInvisible
                >>> alignedToCenterX
                >>> alignToSibling("user_image", constraint: alignToSiblingBottom(-60))
            )
            |> appendSubView(
                presenter.matchUserInfoPanel
                |> named("info")
                |> alignedToLeft
                |> alignedToRight
                |> alignedToBottom
                |> viewHeight(320)
            )
            |> appendSubView(
                presenter.nextButton
                |> named("next_button")
                |> styledTitle(nextFontIcon(size: 16, color: appBlack))
                |> isInvisible
                >>> whiteBackground
                >>> width_Height(32, 32)
                >>> roundedGreyShadowed(16)
                >>> asButton
                >>> alignToRight(-10)
                >>> alignToSibling("info", constraint: alignBottomToSiblingTop(-20))
            )
            |> appendSubView(
                presenter.backButton
                |> named("back_button")
                |> styledTitle(backFontIcon(size: 16, color: appBlack))
                |> isInvisible
                >>> whiteBackground
                >>> width_Height(32, 32)
                >>> roundedGreyShadowed(16)
                >>> asButton
                >>> alignToLeft(10)
                >>> alignToSibling("info", constraint: alignBottomToSiblingTop(-20))
            )
            |> appendSubView(
                presenter.noMatchesView
                |> framed(appFrame)
                |> greenToBlueVerticalGradientBackground
                |> isInvisible
                |> appendSubView(
                    UILabel()
                    |> named("text")
                    |> alignToLeft(20)
                    |> alignToRight(-20)
                    |> alignedToCenterY
                    |> textCenterAligned
                    |> textFont(appFont(20))
                    |> textColored(appDarkGrey)
                    |> titled("Sorry...we couldn't find a match for you. \n Try different filtering criteria or come back later.")
                    |> unlimitedLines
                )
                |> appendSubView(
                    presenter.retryButton
                        |> named("retry")
                        |> styledTitle(whiteBoldAttributedString("Retry Search", 20))
                        |> width_Height(200, 50)
                        |> alignedToCenterX
                        |> alignToSibling("text", constraint: alignTopToSiblingBottom(20))
                )
                |> appendSubView(
                    presenter.filterButtton
                    |> styledTitle(whiteBoldAttributedString("Open Filter", 20))
                    |> width_Height(200, 50)
                    |> alignedToCenterX
                    |> alignToSibling("retry", constraint:  alignTopToSiblingBottom(20))
                )
            )
            |> appendSubView(
                presenter.searchingView
                |> framed(appFrame)
            )
        add(views: view)
    }
}
extension MatchProfileView {
    convenience init(presenter: MatchProfilePresenter) {
        self.init()
        self.presenter = presenter
        setViews()
    }
}
