//
//  MatchProfileRouter.swift
//  Meld
//
//  Created by Blake Rogers on 7/12/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UIKit
import UtilitiesPackage

final class MatchProfileRouter {
    func displayFilter() {
        let view = FilterRouter.presentation()
        currentController().present(view, animated: true, completion: nil)
    }
}
extension MatchProfileRouter {
    static func presentation(homeInteractor: HomeInteractorIdentity) -> UIView {
        let matchInteractor = MatchProfileInteractor(homeInteractor: homeInteractor)
        let matchUserInteractor = MatchUserInfoInteractor(conversationDelegate: matchInteractor, profileManager: matchInteractor)
        let matchUserPresenter = MatchUserInfoPresenter(interactor: matchUserInteractor)
        let matchUserInfoPanel = MatchUserInfoView(presenter: matchUserPresenter)
        let matchView = MatchProfileView(presenter: MatchProfilePresenter(interactor: matchInteractor, matchUserInfoPanel: matchUserInfoPanel))
        return matchView
    }
}
