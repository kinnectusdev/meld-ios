//
//  MatchProfilePresenter.swift
//  Meld
//
//  Created by Blake Rogers on 7/12/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import CHIPageControl
import MapKit
import SwiftyGif
import UtilitiesPackage

final class MatchProfilePresenter: NSObject {
    //MARK: Stored Properties
    private var interactor: MatchProfileInteractorIdentity!
    private var router: MatchProfileRouter!
    private let bag = DisposeBag()
    
    //MARK: UIElements
    let backButton = UIButton()
    let nextButton = UIButton()
    let pageControl = CHIPageControlPuya()
                            |> pageTint(.white)
                            >>> pageCurrentTint(appGrey)
                            >>> numberOfPages(4)
                            >>> pagePadding(6)
                            >>> pageRadius(4)
    var matchUserInfoPanel: UIView!
    var matchImages: UIView!
    let retryButton = UIButton()
    let filterButtton = UIButton()
    let noMatchesView = UIView()
    let searchingView: UIView = {
        let spotlightImage: UIView = {
            do {
                let gif = try UIImage(gifName: "searching.gif")
                return UIImageView(gifImage: gif, loopCount: .max)
                |> named("spotlight")
                |> clearBackground
                |> alignedToCenter
                |> width_Height(250, 250)
            } catch {
                print(error)
                return UIView()
                    |> named("searching")
                    |> backgroundColored(appPurple)
                    |> fill
                    |> rounded(25)
            }
        }()
        return UIView()
        |> framed(appFrame)
        |> whiteBackground
        |> appendSubView(
            spotlightImage
        )
    }()
    
    //MARK: Initializer
    init(interactor: MatchProfileInteractorIdentity, matchUserInfoPanel: UIView) {
        super.init()
        self.interactor = interactor
        self.router = MatchProfileRouter()
        let displayedUser = interactor.displayedUserInfo()
        let images = interactor.userImages()
        let alert = interactor.alert()
        self.matchUserInfoPanel = matchUserInfoPanel
        
        let didRequestUpdateUser = Observable.merge(nextButton.rx.tap.map { _ in ()}, backButton.rx.tap.map { _ in ()})
        self.matchImages = MatchImageView(images: images, onDidChangeUser: didRequestUpdateUser)

        bag.insert(
            retryButton.rx.tap.subscribe(onNext: { [weak self] _ in
                self?.interactor.didSelectRetry()
            }),
            filterButtton.rx.tap.subscribe(onNext: { [weak self] _ in
                self?.router.displayFilter()
            }),
            interactor.observeIsLoading().subscribe(onNext: { [weak self] isLoading in
                UIView.animate(withDuration: 0.25) {
                    self?.searchingView.alpha = isLoading ? 1.0 : 0.0
                }
            }),
            nextButton.rx.tap.subscribe(onNext: { [weak self] in
                self?.interactor.didSelectForward()
            }),
            backButton.rx.tap.subscribe(onNext: { [weak self] in
                self?.interactor.didSelectBack()
            }),
            images.subscribe(onNext: { [weak self] images in
                self?.pageControl.numberOfPages = images.count
            }),
            displayedUser.subscribe(onNext: { [weak self] _ in
                UIView.animate(withDuration: 0.25) {
                    self?.pageControl.alpha = 1.0
                    self?.nextButton.alpha = 1.0
                    self?.backButton.alpha = 1.0
                    self?.matchUserInfoPanel.alpha = 1.0
                    self?.noMatchesView.alpha = 0.0
                    self?.searchingView.alpha = 0.0
                }
            }),
            alert
                .asDriver(onErrorJustReturn: .error(.empty) )
                .drive(onNext: { [weak self] alert in
                    switch alert {
                    case .error(let error):
                        guard error.notEmpty() else { return }
                        currentWindow().addSubview(full_screen_modal(error))
                    case .noMatches:
                        UIView.animate(withDuration: 0.25) {
                            self?.noMatchesView.alpha = 1.0
                        }
                    }
            })
        )
    }
}
