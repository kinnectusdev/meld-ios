//
//  MatchingService.swift
//  Meld
//
//  Created by blakerogers on 12/7/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Firebase
import FirebaseFirestore
import CoreLocation
import MapKit
import UtilitiesPackage

public enum MatchingServiceResult  {
    case success([MeldUser])
    case failure(String)
}
public enum MatchingServiceError: Error {
    case noData
    case invalidURL
}
public class MatchingService {
    typealias UpdateQuery = (Query) -> Query
    static let matchURL: String = "https://us-central1-meld-dd5f8.cloudfunctions.net/findAMatchForUser"
    
    static func queryForSeenUsers(lastId: String?) -> UpdateQuery {
        return { query in
            if let lastSeenUser = lastId {
                return query.start(after: [lastSeenUser])
            }
            return query
        }
    }
    
    static func queryForMatchingIDs(ids: [String]) -> UpdateQuery {
        return { query in
            if ids.notEmpty() {
                return query.whereField("documentIDs", in: ids)
            } else {
                return query
            }
        }
    }
    static func matchQuery(query: Query) -> Observable<MatchingServiceResult> {
        Observable<MatchingServiceResult>.create { observer in
            query.getDocuments { (snapshot, error) in
                if let error = error {
                    observer.onNext(.failure(error.localizedDescription))
                } else {
                    if let documents = snapshot?.documents {
                        let users = documents
                            .compactMap { try? $0.data(as: WireMeldUser.self) }
                            .map { MeldUser(user: $0) }
                    
                        guard users.count > 0 else {
                            observer.onNext(.failure("No Matches Found"))
                            return
                        }
                        observer.onNext(.success(users))
                    } else {
                        observer.onNext(.failure(error?.localizedDescription ?? " Error "))
                    }
                }
            }
            return Disposables.create()
        }
    }
    static func testGetComplimentaryUser() -> Observable<MatchingServiceResult> {
        guard let user = UserService.immediateCurrentUser() else {
            return .empty()
        }
        
        var email: String {
            user.email == "Blake+3@mail.com" ? "Blake+4@mail.com" : "Blake+3@mail.com"
        }
        
        return UserService.fetchUserByEmail(email).map { result -> MatchingServiceResult in
            switch result {
            case .failure(let error):
                return .failure(error)
            case .success(let user):
                return .success([user])
            }
        }.asObservable()
    }
    static func getComplimentaryUsers(personality: PersonalityType,
                                      location: CLLocationCoordinate2D,
                                      queryLimit: Int = 1000,
                                      attempt: Int = 0) -> Observable<MatchingServiceResult> {
        
        let filterSettings = FilterService.fetchFilterSettings()
        guard filterSettings.isFilterApplied else {
            return matchQuery(query: Firestore.firestore().collection("Users").limit(to: 1000))
        }
        let radius = filterSettings.searchRadius.double()
        let region = MKCoordinateRegion(center: location, latitudinalMeters: radius*1600, longitudinalMeters: radius*1600)
        let latDelta = region.span.latitudeDelta
        let longDelta = region.span.longitudeDelta
        let latitudeRange: (lower: Double, upper: Double) = ((region.center.latitude - latDelta), (region.center.latitude + latDelta))
        let longitudeRange: (lower: Double, upper: Double)  = ((region.center.longitude - longDelta), (region.center.longitude + longDelta))
        let seenUsers = UserService.fetchSeenUserIds()
        let queryA = Firestore.firestore().collection("Users")
                .limit(to: 1000)
                .whereField("personalityType", in: personality.complimentaryTypes.map { $0.rawValue })
//                .whereField("id", notIn: seenUsers)
        let observeQueryA = matchQuery(query: queryA)
//        let observeQueryB = matchQuery(query: queryB)
            
        return observeQueryA.flatMap { queryAResult -> Observable<MatchingServiceResult> in
            switch queryAResult {
            case .success(let queryAUsers):
                let users = queryAUsers
                    |> uniqueElements
                    |> filter(FilterService.validate)
                    |> filter({ user in
                        guard let location = user.location else { return true }
                        return (latitudeRange.lower...latitudeRange.upper).contains(location.latitude) && (longitudeRange.lower...longitudeRange.upper).contains(location.longitude)
                    })
                
                guard users.count >= 50 || attempt == 3 else {
                    return getComplimentaryUsers(personality: personality, location: location, queryLimit: queryLimit + 1000, attempt: attempt + 1)
                }
                return .just(.success(users))
            default:
                return .just(.failure("No Matches Found"))
            }
        }
    }
    
    static func setLastUserSearchPosition(position: Int) {
        let lastPosition = UserDefaults.standard.integer(forKey: "lastUserSearchPosition")
        UserDefaults.standard.set(lastPosition + position, forKey: "lastUserSearchPosition")
    }
    static func observeLastUserSearchPosition() -> Observable<Int?> {
        return UserDefaults.standard.rx.observe(Int.self, "lastUserSearchPosition")
    }
    
    static func likedUsers() -> Observable<[MeldUser]> {
        return UserService.observeLikedUsers().flatMap { usersToFetch -> Observable<[MeldUser]> in
            guard usersToFetch.count > 0 else { return .just([]) }
            let userFetches = usersToFetch.map { UserService.fetchUser($0).map { $0.user }.compactMap { $0 }}
            return Observable.zip(userFetches)
        }
    }
    
    static func seenUsers() -> Observable<[MeldUser]> {
        return UserService.observeSeenUsers().take(1).flatMap { usersToFetch -> Observable<[MeldUser]> in
            guard usersToFetch.count > 0 else { return .just([]) }
            let userFetches = usersToFetch.map { UserService.fetchUser($0).map { $0.user }.compactMap { $0 }}
            return Observable.zip(userFetches)
        }
    }
}
