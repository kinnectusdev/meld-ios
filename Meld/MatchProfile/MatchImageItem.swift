//
//  MatchImageItem.swift
//  Meld
//
//  Created by Blake Rogers on 4/10/22.
//  Copyright © 2022 com.meld.kinnectus. All rights reserved.
//
import UIKit
import NVActivityIndicatorView
import RxSwift
import UtilitiesPackage

protocol ItemImageLoadingDelegate: AnyObject {
    func didLoadImage()
}
class MatchImageItem: UICollectionViewCell {
    static let identifier: String = "MatchImageItem"
    private let bag = DisposeBag()
    private let userImage = UIImageView()
    |> fill
    >>> scaledToFill
    >>> asImage
    private let loadingIndicator = NVActivityIndicatorView(frame: CGRect(origin: .zero, size: CGSize(width: 50, height: 50)),
                                                   type: .ballClipRotate,
                                                   color: appBlack,
                                                   padding: nil)
    func setImage(image: String, imageLoadingDelegate: ItemImageLoadingDelegate?) {
        contentView.add(views: userImage, loadingIndicator)
        loadingIndicator.constrainWidth_Height(width: 50, height: 50)
        loadingIndicator.constrainCenterToCenter(of: contentView)
        bag.insert(
            MediaImageService.fetchImageData(from: image)
                .do(onNext: { [weak self] url in
                    self?.userImage.image = nil
                    self?.loadingIndicator.isHidden = false
                    self?.loadingIndicator.startAnimating()
                })
                .delay(.seconds(1), scheduler: ConcurrentDispatchQueueScheduler.init(qos:.default))
                .asDriver(onErrorJustReturn: nil)
                .drive(onNext: { [weak self] data in
                    if let data = data {
                        self?.userImage.image = UIImage(data: data)
                    } else {
                        self?.userImage.image = defaultAccountImage()
                    }
                    imageLoadingDelegate?.didLoadImage()
                    self?.loadingIndicator.stopAnimating()
                    self?.loadingIndicator.isHidden = true
            })
        )
    }
    
    func resizeImage(size: CGSize) {
        if let image = userImage.image {
            let ratio: Double = size.width > size.height ? 16.0/9.0 : 1
            let resizedImage = (image |> imageWith(newSize: size |> multiplyHeight(dy: ratio)))
            userImage.image = resizedImage
        }
    }
}
