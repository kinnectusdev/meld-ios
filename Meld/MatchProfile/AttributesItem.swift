//
//  AttributesItem.swift
//  Meld
//
//  Created by blakerogers on 3/27/20.
//  Copyright © 2020 com.meld.kinnectus. All rights reserved.
//

import UIKit
import UtilitiesPackage

public class AttributesItem: UICollectionViewCell {
    static var identifier: String = String(describing: self)
    private let attributeTitleLabel = UILabel()
    |> named("title")
    >>> textFont(appFont(10))
    >>> textColored(appDarkGrey)
    >>> alignedToLeft
    >>> alignedToTop
    >>> alignToRight(-4)
    >>> asLabel
    private let attributeLabel = UILabel()
    |> textFont(appFont(18))
    >>> textColored(appDarkGrey)
    >>> alignedToLeft
    >>> alignToSibling("title", constraint: alignTopToSiblingBottom())
    >>> alignedToBottom
    >>> alignToRight(-4)
    >>> asLabel
    var sizeForItem: CGSize = .zero
    public override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.add(views: attributeTitleLabel, attributeLabel)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func setAttribute(title: String, attribute: String) {
        attributeTitleLabel.text = title
        attributeLabel.text = attribute
        sizeForItem = CGSize(width: max(80, attribute.rectForText(width: 1000, textSize: 18).width*1.5), height: 40)
    }
}
