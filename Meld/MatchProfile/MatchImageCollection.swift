//
//  MatchImageCollectioin.swift
//  Meld
//
//  Created by Blake Rogers on 4/10/22.
//  Copyright © 2022 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import UtilitiesPackage

typealias MatchImageCollectionView = MatchImageCollectionIdentity & UIView

protocol MatchImageCollectionIdentity {
    func loading()
    func configure(images: Observable<[String]>, onDidChangeUser: Observable<Void>, imageLoadingDelegate: ItemImageLoadingDelegate)
}

class MatchImageUICollectionView: UICollectionView, MatchImageCollectionIdentity {
    
    private var bag = DisposeBag()
    
    convenience init(isPaging: Bool) {
    
        let layout = UICollectionViewFlowLayout()
        if DeviceInfo.deviceType == .iPad {
            layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        }
        self.init(frame: CGRect(x: 0, y: 0, width: appFrame.width, height: appFrame.height), collectionViewLayout: layout)
        self.register(MatchImageItem.self, forCellWithReuseIdentifier: MatchImageItem.identifier)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.isPagingEnabled = isPaging
        self.delegate = self
    }
    
    func loading() {
        guard self.numberOfItems(inSection: 0) >= 1 else { return }
        self.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: false)
    }
    
    func configure(images: Observable<[String]>, onDidChangeUser: Observable<Void>, imageLoadingDelegate: ItemImageLoadingDelegate) {
        bag.insert(
            images.bind(to: self.rx.items(cellIdentifier: MatchImageItem.identifier, cellType: MatchImageItem.self))
            { _, url, cell in
                cell.setImage(image: url, imageLoadingDelegate: imageLoadingDelegate)
            }
        )
    }
}
extension MatchImageUICollectionView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if DeviceInfo.deviceType == .iPad {
            (collectionView.cellForItem(at: indexPath) as? MatchImageItem)?.resizeImage(size: collectionView.frame.size)
        }
        return collectionView.bounds.size

    }
}
