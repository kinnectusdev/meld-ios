//
//  MatchImageView.swift
//  Meld
//
//  Created by blakerogers on 4/29/20.
//  Copyright © 2020 com.meld.kinnectus. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import NVActivityIndicatorView
import UtilitiesPackage

class MatchImageView: UIView {
    private let bag = DisposeBag()
    
    private let loadingScreen = UIView()
        |> blurredWhiteBackground
        |> { view in
            let indicator = NVActivityIndicatorView(frame: CGRect(origin: .zero, size: CGSize(width: 50, height: 50)),
                                                     type: .ballClipRotate,
                                                     color: appBlack,
                                                     padding: nil)
            indicator.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(indicator)
            indicator.constrainCenterToCenter(of: view)
            indicator.constrainWidth_Height(width: 50, height: 50)
            indicator.startAnimating()
            return view
        }
        |> utilizeConstraints
        |> fill
    
    func loading() {
        collection.loading()
        UIView.animate(withDuration: 0.5) {
            self.loadingScreen.alpha = 1.0
        }
    }
    private func endLoading() {
        UIView.animate(withDuration: 1.0) {
            self.loadingScreen.alpha = 0.0
        }
    }
    let collection: MatchImageCollectionView = {
        MatchImageUICollectionView(isPaging: true)
    }()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setViews()
    }
}

extension MatchImageView {
    private func setViews() {
        add(views: collection, loadingScreen)
        collection.constrainInView(view: self, top: 0, left: 0, right: 0, bottom: 0)
    }
}

extension MatchImageView {
    convenience init(images: Observable<[String]>, onDidChangeUser: Observable<Void>) {
        self.init()
        setViews()
        collection.configure(images: images,
                             onDidChangeUser: onDidChangeUser,
                             imageLoadingDelegate: self)
        bag.insert(
            onDidChangeUser.subscribe(onNext: { [weak self] in
                self?.loading()
            })
        )
    }
}
extension MatchImageView: ItemImageLoadingDelegate {
    func didLoadImage() {
        endLoading()
    }
}
