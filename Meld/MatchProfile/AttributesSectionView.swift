//
//  AttributesSectionView.swift
//  Meld
//
//  Created by blakerogers on 3/27/20.
//  Copyright © 2020 com.meld.kinnectus. All rights reserved.
//

import UIKit
import RxSwift
import UtilitiesPackage

public class AttributesSectionView: UIView, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    private let bag = DisposeBag()
    private var attributes: [AttributeOption] = []
    let didSelectAttribute = PublishSubject<AttributeOption>()
    lazy var attributesCollection = UICollectionView
                                    .collectionView(width: 100, height: 50, direction: .horizontal)
                                    |> registerCell(AttributesItem.self, AttributesItem.identifier)
                                    >>> utilizeConstraints
                                    >>> collectionDelegate(self)
                                    >>> collectionDatasource(self)
                                    >>> asCollection
    static func view() -> AttributesSectionView {
        let view = AttributesSectionView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        add(views: attributesCollection)
        attributesCollection.constrainInView(view: self, top: 0, left: 0, right: 0, bottom: 0)
    }
  
    func setItems(_ items: [AttributeOption]) {
        attributes = items
        attributesCollection.reloadData()
    }
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let item = attributes.itemAt(indexPath.item) else { return .zero }
        return CGSize(width: max(80, item.description.rectForText(width: 1000, textSize: 18).width*1.5), height: 40)
    }
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    public func numberOfSections(in collectionView: UICollectionView) -> Int { return 1 }
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return attributes.count
    }
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AttributesItem.identifier, for: indexPath) as? AttributesItem
        let attribute = attributes.itemAt(indexPath.item)
        cell?.setAttribute(title: attribute?.category()?.description ?? .empty,
                           attribute: attribute?.description ?? .empty)
        return cell ?? UICollectionViewCell()
    }
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let attribute = attributes.itemAt(indexPath.item) else { return }
        didSelectAttribute.onNext(attribute)
    }
}
