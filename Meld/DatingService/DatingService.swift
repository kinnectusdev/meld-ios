//
//  DatingService.swift
//  Meld
//
//  Created by blakerogers on 11/19/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import Firebase
import UserNotificationsUI
import FirebaseFirestore
import UtilitiesPackage

enum DateScheduledServiceResult {
    case success
    case failure(String)
}
public struct MeldDate {
    var documentID: String
    let time: NSNumber
    let place: String
    let address: String
    let inviterID: String
    let inviteeID: String
    let inviteeFCMToken: String
    let inviterFCMToken: String
    let inviteeName: String
    let invitorName: String
    let description: String
    var date: Date {
        return Date(timeIntervalSince1970: time.doubleValue)
    }
}
class DatingService {
    /// Schedule date between users
    /// - Parameter date: MeldDate
    static func scheduleDate(date: MeldDate) -> Observable<DateScheduledServiceResult> {
        return Observable.create { observer in
            let date_document = Firestore.firestore().collection("Dates").document()
            date_document.setData(date |> prop(\.documentID)({ _ in date_document.documentID}) |> documentFromDate) { error in
                if let error = error {
                    observer.onNext(.failure(error.localizedDescription))
                } else {
                    observer.onNext(.success)
                }
            }
            return Disposables.create()
        }
    }
    /// Set Local Date
    /// - Parameter date: MeldDate
    static func setLocalDateReminder(date: MeldDate) -> Observable<DateScheduledServiceResult> {
        return UserService.currentUser().flatMap { result -> Observable<MeldUser> in
            switch result {
            case let .success(user):
                return .just(user)
            default: return .never()
            }
        }.flatMap { user -> Observable<DateScheduledServiceResult> in
            let userIsInvitor = date.invitorName == user.name
            let center = UNUserNotificationCenter.current()
            let content = UNMutableNotificationContent()
            content.sound = UNNotificationSound.default
            content.title = "Meld"
            content.subtitle = "Date with \(userIsInvitor ? date.inviteeName : date.invitorName)"
            content.body = date.description
            let trigger = UNCalendarNotificationTrigger(dateMatching: Calendar.current.dateComponents(in: .current, from: date.date), repeats: false)
            let request = UNNotificationRequest(identifier: AppNotificationKeys.scheduledDateLocalReminder.rawValue, content: content, trigger: trigger)
            return Observable<DateScheduledServiceResult>.create { observer in
                center.add(request) { (error) in
                    if let error = error {
                        observer.onNext(.failure(error.localizedDescription))
                    } else {
                        observer.onNext(.success)
                    }
                }
                return Disposables.create()
            }
        }
    }
}
public let documentFromDate: (MeldDate) -> [String: Any] = { date in
    return [String: Any]()
        |> prop(\[String: Any].["documentID"])({_ in date.documentID})
        |> prop(\[String: Any].["time"])({_ in date.time})
        |> prop(\[String: Any].["place"])({_ in date.place})
        |> prop(\[String: Any].["address"])({_ in date.address})
        |> prop(\[String: Any].["inviterID"])({_ in date.inviterID})
        |> prop(\[String: Any].["inviteeID"])({_ in date.inviteeID})
}

