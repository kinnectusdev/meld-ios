//
//  KeyboardView.swift
//  Meld
//
//  Created by blakerogers on 12/3/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import UtilitiesPackage
final class KeyboardRouter {
    func dismiss() {
        currentController().dismiss(animated: true , completion: nil)
    }
}
extension KeyboardRouter {
    static func presentation(manager: MessageManagerIdentity) -> UIViewController {
        KeyboardView(presenter: KeyboardPresenter(interactor: KeyboardInteractor(manager: manager, intent: .message)))
    }
}
protocol MessageManagerIdentity {
    func didCompleteMessage(message: String)
}
protocol KeyboardInteractorIdentity {
    func observePlaceholder() -> String
    func observeDoneButtonTitle() -> String
    
    func didCompleteKeyboard(text: String)
}
enum KeyboardIntent {
    case message
}
