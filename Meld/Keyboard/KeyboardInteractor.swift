//
//  KeyboardInteractor.swift
//  Meld
//
//  Created by Blake Rogers on 4/20/22.
//  Copyright © 2022 com.meld.kinnectus. All rights reserved.
//

import Foundation

final class KeyboardInteractor {
    private let messageManager: MessageManagerIdentity
    private let keyboardIntent: KeyboardIntent
    
    init(manager: MessageManagerIdentity, intent: KeyboardIntent) {
        self.messageManager = manager
        self.keyboardIntent = intent
    }
}

extension KeyboardInteractor: KeyboardInteractorIdentity {
    func observePlaceholder() -> String {
        switch keyboardIntent {
        case .message:
            return "Enter Your Message..."
        }
    }
    func observeDoneButtonTitle() -> String {
        switch keyboardIntent {
        case .message:
            return "Send"
        }
    }
    
    func didCompleteKeyboard(text: String) {
        messageManager.didCompleteMessage(message: text)
    }
}
