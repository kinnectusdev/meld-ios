//
//  KeyboardPresenter.swift
//  Meld
//
//  Created by Blake Rogers on 4/20/22.
//  Copyright © 2022 com.meld.kinnectus. All rights reserved.
//

import RxSwift
import UtilitiesPackage
import UIKit

final class KeyboardPresenter {
    private let bag = DisposeBag()
    private let interactor: KeyboardInteractorIdentity
    private let router: KeyboardRouter
    
    let screen = UIView()
    let messageField = UITextView()
    let dismissButton = UIButton()
    let sendButton = UIButton()
    
    init(interactor: KeyboardInteractorIdentity) {
        self.interactor = interactor
        self.messageField.returnKeyType = .send
        self.router = KeyboardRouter()
        
        messageField.text = interactor.observePlaceholder()
        sendButton.setAttributedTitle(darkGreyAttributedString(interactor.observeDoneButtonTitle(), 20), for: .normal)
        
        bag.insert(
            dismissButton.rx.tap.subscribe(onNext: { [weak self] _ in
                self?.messageField.text = ""
            }),
            Observable.merge(
                sendButton.rx.tap.debounce(.seconds(1), scheduler: MainScheduler.instance)
                .map { [weak self] _ in
                    self?.messageField.text ?? ""
                },
                messageField.rx.didEndEditing.asObservable()
                    .map { [weak self] _ in
                        self?.messageField.text ?? ""
                    }).distinctUntilChanged()
                .subscribe(onNext: { [weak self] text in
                self?.interactor.didCompleteKeyboard(text: text)
                self?.router.dismiss()
            }),
            messageField.rx.didChange.subscribe(onNext: { [weak self] _ in
                if (self?.messageField.text ?? "").contains("\n") {
                    self?.messageField.text = self?.messageField.text.replacingOccurrences(of: "\n", with: "")
                    self?.messageField.resignFirstResponder()
                }
            }),
            messageField.rx.didBeginEditing.asDriver().drive(onNext: { [weak self] _ in
                guard let this = self else { return }
                if this.messageField.text.contains(interactor.observePlaceholder()) {
                    self?.messageField.text = ""
                }
            })
        )
    }
}
