//
//  KeyboardView.swift
//  Meld
//
//  Created by Blake Rogers on 4/20/22.
//  Copyright © 2022 com.meld.kinnectus. All rights reserved.
//

import UIKit
import UtilitiesPackage

final class KeyboardView: UIViewController {
    private var presenter: KeyboardPresenter!
    
    private func setViews() {
        let view = presenter.screen
                |> whiteBackground
                >>> fill
            >>> appendSubView(presenter.messageField
                                    |> named("message_field")
                                    >>> alignToLeft(10)
                                    >>> tagged(999)
                                    >>> clearBackground
                                    >>> alignToRight(-10)
                                    >>> textFont(appFont(18))
                                    >>> textColored(appDarkGrey)
                                    >>> viewHeight(400)
                                    >>> alignToTop(100))
            >>> appendSubView(presenter.dismissButton
                                    |> alignToRight(-20)
                                    >>> alignToTop(80)
                                    >>> width_Height(40, 40)
                                    >>> styledTitle(darkGreyAttributedString("X", 20)))
            >>> appendSubView(presenter.sendButton
                                    |> alignedToCenterX
                                    >>> alignToBottom(-100)
                                    >>> width_Height(200, 60)
                                    >>> roundedGreyShadowed(30))
        self.view = view
    }
}
extension KeyboardView {
    convenience init(presenter: KeyboardPresenter) {
        self.init()
        self.presenter = presenter
        self.setViews()
    }
}
