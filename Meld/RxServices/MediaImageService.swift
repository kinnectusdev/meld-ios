//
//  MediaImageService.swift
//  Journey
//
//  Created by blakerogers on 11/3/18.
//  Copyright © 2018 Blake Rogers. All rights reserved.
//

import Foundation
import Photos
import UIKit
import RxSwift
import FirebaseStorage

private var cache: [String: Data] = [:]

// MARK: Image Service Relevant Functions
class MediaImageService {
    static func getImageAssets() -> Observable<[PHAsset]> {
            let status = PHPhotoLibrary.authorizationStatus()
            switch status {
            case .authorized:
                return Observable<[PHAsset]>.create { observer in
                    let results = PHAssetCollection.fetchAssetCollections(with: .smartAlbum, subtype: .albumSyncedAlbum, options: nil)
                    let set = IndexSet(integersIn: 0..<results.count)
                    let collections = results.objects(at: set)
                    var assets = Set<PHAsset>()
                    let options = PHFetchOptions()
                    options.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]
                    for collection in collections {
                        let collectionFetchResults = PHAsset.fetchAssets(in: collection, options: options)
                        let collectionIndexSet = IndexSet(integersIn: 0..<collectionFetchResults.count)
                        let collectionAssets = collectionFetchResults.objects(at: collectionIndexSet)
                        assets.formUnion(collectionAssets)
                    }
                    let sortedAssets = assets.filter { $0.mediaType == PHAssetMediaType.image}.sorted(by: { (first, second) -> Bool in
                        return (first.creationDate ?? Date()).timeIntervalSince1970 > (second.creationDate ?? Date()).timeIntervalSince1970
                    })
                    observer.onNext(sortedAssets)
                    return Disposables.create()
                }
            case .denied,.restricted,.notDetermined:
                return Observable<[PHAsset]>.create { observer in
                    PHPhotoLibrary.requestAuthorization { (authorization) in
                        switch authorization {
                        case .authorized:
                            _ = getImageAssets().bind(to: observer)
                        default:
                            observer.onNext([])
                        }
                    }
                    return Disposables.create()
                }
            @unknown default:
                return .just([])
            }
    }
    static func getImage(of asset: PHAsset, manager: PHImageManager) -> Observable<UIImage?> {
        return Observable.create { observer in
            let options = PHImageRequestOptions()
            options.deliveryMode = .highQualityFormat
            manager.requestImage(for: asset, targetSize: CGSize(width: 300, height: 300), contentMode: .aspectFit, options: options) { (image, nil) in
                if let photo = image {
                    observer.onNext(photo)
                } else {
                    observer.onNext(nil)
                }
            }
            return Disposables.create()
        }
    }
    
    static func fetchImageData(from path: String) -> Observable<Data?> {
        var url: String {
            guard path.contains(".png") || path.contains(".jpg") else {
                return path.appending(".jpg")
            }
            return path
        }
        if let data = cache[url] {
            print("got cached image")
            return Observable.just(data)
        }
        return Observable.create { observer in
            Storage.storage().reference(withPath: url).getData(maxSize: 1024 * 1000 * 1000) { (data, _) in
                if let data = data {
                    cache.updateValue(data, forKey: url)
                }
                observer.onNext(data)
            }
            return Disposables.create()
        }
    }
    static func fetchRemoteImageData(from url: String) -> Observable<Data?> {
        guard url.notEmpty() else { return .just(nil)}
        return Observable<Data?>.create { observer in
            let ref = Storage.storage().reference(withPath: url)
            ref.getData(maxSize: 1024 * 100 * 100) { (data, _) in
                observer.onNext(data)
            }
            return Disposables.create()
        }
    }
}
