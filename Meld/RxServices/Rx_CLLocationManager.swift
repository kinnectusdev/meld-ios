//
//  Rx_CLLocationManager.swift
//  Meld
//
//  Created by blakerogers on 4/30/20.
//  Copyright © 2020 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import CoreLocation
enum LocationManagerError: Error {
    case error(String)
}
class LocationManager: NSObject {
    typealias LocationResult = Result<CLLocation, LocationManagerError>
    let locationResult = PublishSubject<LocationResult>()
    lazy var manager: CLLocationManager = {
        let control = CLLocationManager()
        control.delegate = self
        return control
    }()
    func fetchLocation() -> Observable<LocationResult> {
        return CLLocationManager.authorized.flatMap { isAuthorized -> Observable<LocationResult> in
            if isAuthorized {
                self.manager.requestLocation()
                return self.locationResult.asObservable()
            } else {
                self.manager.requestWhenInUseAuthorization()
                return CLLocationManager.authorized.flatMap { isAuthorized -> Observable<LocationResult> in
                    if isAuthorized {
                        self.manager.requestLocation()
                        return self.locationResult.asObservable()
                    } else {
                        return .just(.failure(.error("Not Allowed")))
                    }
                }
            }
        }
    }
}
extension LocationManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else { return }
        locationResult.onNext(.success(location))
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationResult.onNext(.failure(.error(error.localizedDescription)))
    }
}
extension CLLocationManager {
    static var authorized: Observable<Bool> {
        return Observable.create { observer in
            DispatchQueue.main.async {
                if (authorizationStatus() == .authorizedAlways) || (authorizationStatus() == .authorizedWhenInUse) {
                    observer.onNext(true)
                    observer.onCompleted()
                } else {
                    observer.onNext(false)
                }
            }
            return Disposables.create()
        }
    }
}
