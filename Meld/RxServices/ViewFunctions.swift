//
//  ViewFunctions.swift
//  Meld
//
//  Created by blakerogers on 9/20/20.
//  Copyright © 2020 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
public func tapGestureEvent(view: UIView) -> ControlEvent<UITapGestureRecognizer> {
    let gesture = UITapGestureRecognizer()
    view.addGestureRecognizer(gesture)
    return gesture.rx.event
}
