//
//  Observable_Ext.swift
//  Meld
//
//  Created by Blake Rogers on 8/12/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import RxSwift


extension PrimitiveSequenceType where Trait == MaybeTrait {
    public func `print`()
        -> Maybe<Element> {
        self.do(onNext: { Swift.print("print: ", $0) })
    }
}
