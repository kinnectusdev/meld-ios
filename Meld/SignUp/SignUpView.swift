//
//  SignUpController.swift
//  Meld
//
//  Created by Blake Rogers on 5/2/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import UtilitiesPackage

final class SignUpView: UIViewController {
    
    private var presenter: SignUpPresenter!
    
    private func setViews() {
        let view = UIView()
            |> named("onboarding")
            >>>  whiteBackground
            >>> framed(view.frame)
            >>> appendSubView(UIImageView(image: UIImage(named: "logoDark"))
                |> width_Height(50, 50)
                >>> alignedToCenterX
                >>> alignToTop(50)
                >>> named("logo"))
            >>> appendSubView(presenter.backButton
                |> named("back_button")
                >>> setImage(UIImage(named: "BackButton"))
                >>> alignToLeft(10)
                >>> alignToTop(70))
            >>> appendSubView(presenter.instructionLabel
                |> named("instruction_label")
                >>> styledTitle(blackAttributedString("Create Account", 18))
                >>> alignToSibling("logo", constraint: alignTopToSiblingBottom(20))
                >>> textCenterAligned
                >>> width_Height(275, 60)
                >>> alignToCenterX())
            >>> appendSubView(UIView()
                |> bordered(appBlack, 1)
                |> named("email")
                >>> rounded(30)
                >>> viewHeight(60)
                >>> alignedToCenterX
                >>> alignToLeft(16)
                >>> alignToRight(-16)
                >>> alignToSibling("instruction_label", constraint: alignTopToSiblingBottom(20))
                >>> appendSubView(presenter.emailField
                    |> width_Height(300, 50)
                    >>> placeholder("Email Address")
                    >>> alignedToCenter
                    >>> textColored(appBlack)
                    >>> textFont(appFont(18)))
            )
            >>> appendSubView(UIView()
                |> bordered(appBlack, 1)
                |> named("name")
                >>> rounded(30)
                >>> viewHeight(60)
                >>> alignedToCenterX
                >>> alignToLeft(16)
                >>> alignToRight(-16)
                >>> alignToSibling("email", constraint: alignTopToSiblingBottom(20))
                >>> appendSubView(presenter.nameField
                    |> width_Height(300, 50)
                    >>> placeholder("First Name")
                    >>> alignedToCenter
                    >>> textColored(appBlack)
                    >>> textFont(appFont(18)))
            )
            >>> appendSubView(UIView()
                |> bordered(appBlack, 1)
                |> named("password")
                >>> rounded(30)
                >>> viewHeight(60)
                >>> alignedToCenterX
                >>> alignToLeft(16)
                >>> alignToRight(-16)
                >>> alignToSibling("name", constraint: alignTopToSiblingBottom(20))
                                >>> appendSubView(presenter.passwordField
                    |> width_Height(300, 50)
                    >>> placeholder("Password")
                    >>> isSecure
                    >>> alignedToCenter
                    >>> textColored(appBlack)
                    >>> textFont(appFont(18)))
            )
            >>> appendSubView(presenter.signUpButton
                |> named("sign_Up_button")
                >>> width_Height(200, 60)
                >>> backgroundColored(appBlack)
                >>> styledTitle(silverAttributedString("Sign Up", 18))
                >>> rounded(30)
                >>> alignedToCenterX
                >>> alignToSibling("password", constraint: alignTopToSiblingBottom(20)))
        
        self.view = view
        
    }
}
extension SignUpView {
    convenience init(presenter: SignUpPresenter) {
       self.init()
        self.modalPresentationStyle = ( DeviceInfo.deviceType == .iPad ? .overCurrentContext : .automatic)
       self.presenter = presenter
        self.setViews()
   }
}
