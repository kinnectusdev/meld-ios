//
//  SignUpViewModel.swift
//  Meld
//
//  Created by Blake Rogers on 5/2/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import UtilitiesPackage

protocol SignUpInteractorIdentity {
    func didUpdateEmail(email: String)
    func didUpdateName(name: String)
    func didUpdatePassword(password: String)
    func didSelectSignUp()
    func observeSignUpResultDidSucceed() -> Observable<Void>
    func observeAlert() -> Observable<String>
}

final class SignUpInteractor {
    
    private let bag = DisposeBag()
    private let testResult: PersonalityTestResult
    private var info = SignUpInfo.empty
    private var alert = PublishSubject<String>()
    private var signUpDidSucceed = PublishSubject<Void>()
    init(testResult: PersonalityTestResult) {
        self.testResult = testResult
    }
}
extension SignUpInteractor: SignUpInteractorIdentity {
    
    func didUpdateEmail(email: String) {
        self.info = (info |> prop(\.email)({ _ in email }))
    }
    
    func didUpdateName(name: String) {
        self.info = (info |> prop(\.name)({ _ in name }))
    }
    
    func didUpdatePassword(password: String) {
        self.info = (info |> prop(\.password)({ _ in password }))
    }
    func observeSignUpResultDidSucceed() -> Observable<Void> {
        signUpDidSucceed.asObservable()
    }
    func observeAlert() -> Observable<String> {
        alert.distinctUntilChanged()
    }
    func didSelectSignUp() {
        if info.isValid {
            SignUpService.signUp(email: info.email,
                                 name: info.name,
                                 password: info.password,
                                 testResult: testResult).take(1)
                .flatMap { result -> Observable<Bool> in
                    switch result {
                    case .success(let user):
                        return UserService.setCurrentUser(user: user)
                    default:
                        return .never()
                    }
                }.subscribe(onNext: { [weak self] result in
                    if result {
                        self?.signUpDidSucceed.onNext(())
                    } else {
                        self?.alert.onNext("Error Signing Up. Try Again")
                    }
                }).disposed(by: bag)
        } else {
            self.alert.onNext("Invalid Credentials")
        }
    }
}
