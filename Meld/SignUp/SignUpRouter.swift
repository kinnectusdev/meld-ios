//
//  SignUpRouter.swift
//  Meld
//
//  Created by Blake Rogers on 7/4/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UIKit
import UtilitiesPackage

final class SignUpRouter {
    
    func dismiss() {
        currentController().dismiss(animated: true, completion: nil)
    }
    
    func displayHome() {
        let view = HomeRouter.presentation()
        currentWindow().rootViewController = view
        currentWindow().makeKeyAndVisible()
    }
}
extension SignUpRouter {
    static func presentation(testResult: PersonalityTestResult) -> UIViewController {
        SignUpView(presenter: SignUpPresenter(interactor: SignUpInteractor(testResult: testResult)))
    }
}


