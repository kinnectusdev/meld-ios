//
//  SignUpService.swift
//  Meld
//
//  Created by Blake Rogers on 4/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import FirebaseFirestore
import FirebaseAuth
import FirebaseFirestoreSwift
import UtilitiesPackage

public enum SignUpServiceResult {
    case success(MeldUser)
    case failure(String)
}

public enum CreateAccountResult {
    case success
    case failure(String)
}
final class SignUpService {
    
    static func signUp(email: String, name: String, password: String, testResult: PersonalityTestResult) -> Observable<SignUpServiceResult> {
        Observable<CreateAccountResult>.create{ observer in
            Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
                if let error = error {
                    observer.onNext(CreateAccountResult.failure(error.localizedDescription))
                } else {
                    observer.onNext(CreateAccountResult.success)
                }
            }
            return Disposables.create()
        }.flatMap { result -> Observable<SignUpServiceResult> in
            switch result {
            case .success:
                return Observable<SignUpServiceResult>.create { observer in
                    let document = Firestore.firestore().collection("Users").document()
         
                    let currentUser = MeldUser.empty
                                        |> prop(\.email)({ _ in return email })
                                        |> prop(\.name)({ _ in return name })
                                        |> prop(\.id)({_ in return document.documentID })
                                        |> prop(\.personalityType)({_ in return Optional(testResult.personalityType())})
                                        |> prop(\.personalityTestResult)({_ in return Optional(testResult) })
                                        |> prop(\.lastSignIn)({_ in Date().timeIntervalSince1970 })
                    do {
                        let encoder = Firestore.Encoder()
                        try document.setData(from: WireMeldUser(user: currentUser), encoder: encoder) { (error) in
                            if let error = error {
                                observer.onNext(SignUpServiceResult.failure(error.localizedDescription))
                            } else {
                                observer.onNext(SignUpServiceResult.success(currentUser))
                            }
                        }
                    } catch {
                        print(error.localizedDescription)
                    }
                    return Disposables.create()
                }
            case .failure(let error):
                return .just(.failure(error))
            }
        }
    }
}
