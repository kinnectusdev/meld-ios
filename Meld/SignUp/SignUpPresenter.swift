//
//  SignUpPresenter.swift
//  Meld
//
//  Created by Blake Rogers on 7/11/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import UIKit
import RxSwift
import UtilitiesPackage

final class SignUpPresenter {
    
    //MARK: Stored Properties
    private let bag = DisposeBag()
    private let router: SignUpRouter
    private let interactor: SignUpInteractorIdentity
    
    //MARK: UIElements
    let backButton = UIButton()
    |> visibility( DeviceInfo.deviceType == .iPad ? 1.0 : 0.0 )
    |> asButton
    let instructionLabel = UILabel()
    let nameField = UITextField()
    let emailField = UITextField()
    let passwordField = UITextField()
    let signUpButton = UIButton()
    
    //MARK: Initialize
    init(interactor: SignUpInteractorIdentity) {
        self.interactor = interactor
        self.router = SignUpRouter()
    
        bag.insert(
            nameField.rx.value.compactMap { $0 }.subscribe(onNext: { [weak self] name in
                self?.interactor.didUpdateName(name: name)
            }),
            emailField.rx.value.compactMap { $0 }.subscribe(onNext: { [weak self] email in
                self?.interactor.didUpdateEmail(email: email.lowercased())
            }),
            passwordField.rx.value.compactMap { $0 }.subscribe(onNext: { [weak self] password in
                self?.interactor.didUpdatePassword(password: password)
            }),
            signUpButton.rx.tap.subscribe(onNext: { _ in
                interactor.didSelectSignUp()
            }),
            backButton.rx.tap.subscribe(onNext: { [weak self] _ in
                self?.router.dismiss()
            }),
            interactor.observeAlert().distinctUntilChanged().subscribe(onNext: { alert in
                let modal = full_screen_modal(alert)
                currentWindow().addSubview(modal)
            }),
            interactor.observeSignUpResultDidSucceed().subscribe(onNext: { [weak self] in
                self?.router.displayHome()
            })
        )
    }
}
