//
//  SignUpModels.swift
//  Meld
//
//  Created by Blake Rogers on 7/11/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation

enum SignUpError: Error {
    case invalidEmail
    var message: String {
        return "Invalid Email"
    }
}

struct SignUpInfo {
    var name: String
    var email: String
    var password: String
    var isValid: Bool {
        return name.notEmpty() && email.notEmpty() && password.notEmpty()
    }
    static let empty = SignUpInfo(name: .empty, email: .empty, password: .empty)
}
