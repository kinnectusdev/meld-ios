//
//  VideoChatController.swift
//  Meld
//
//  Created by blakerogers on 9/20/20.
//  Copyright © 2020 com.meld.kinnectus. All rights reserved.
//
import UIKit
import RxSwift
import RxCocoa
import TwilioVideo
import UtilitiesPackage

public let videoChatBag = DisposeBag()
public let micTitle: (Bool) -> NSAttributedString = { isEnabled in
    return microPhoneFontIcon(size: 18, color: isEnabled ? .white : appRed)
}
public func videoChatController(conversation: Conversation) -> UIViewController {
    let controller = UIViewController()
    let remoteView = VideoView()
    let previewView = VideoView()
    let endCallButton = UIButton()
    let micButton = UIButton()
    let isDeallocated = controller.rx.deallocated
           let didSelectFlipCamera = tapGestureEvent(view: previewView).map { _ in () }
           let (image,
               name,
               endCall,
               isAudioEnabled,
               localVideo,
               remoteVideo,
               shouldMirror) = videoChatViewModel(bag: videoChatBag,
                                                 conversation: conversation,
                                                 willEndVideoChat: isDeallocated,
                                                 didSelectFlipCamera: didSelectFlipCamera,
                                                 didSelectEndCall: endCallButton.rx.tap,
                                                 didToggleMute: micButton.rx.tap)
    if let stream = localVideo {
        previewView.contentMode = .scaleAspectFill
        stream.addRenderer(previewView)
    }
    videoChatBag.insert(
       shouldMirror.drive(onNext: { isMirrored in
           previewView.shouldMirror = isMirrored
       }),
       isAudioEnabled.map { micTitle($0) }.drive(micButton.rx.attributedTitle()),
       remoteVideo.subscribe(onNext: { video in
            remoteView.contentMode = .scaleAspectFill
            video.addRenderer(remoteView)
       }),
        endCall.subscribe(onNext: { _ in
            controller.dismiss(animated: true, completion: nil)
        })
    )
    
    let view = UIView()
        |> blurredWhiteBackground
        |> appendSubView(remoteView |> fill)
        |> appendSubView(previewView
            |> alignToTop(10)
            |> alignToRight(-10)
            |> rounded(10)
            |> width_Height(175, 175)
            |> appendSubView(micButton
                |> styledTitle(microPhoneFontIcon(size: 18))
                |> alignToBottom(-4)
                |> alignToRight(-4)
                |> width_Height(36, 36)))
        |> appendSubView(endCallButton
            |> styledTitle(phoneFontIcon(size: 25))
            |> alignedToCenterX
            |> alignToBottom(-50)
            |> width_Height(50, 50)
            |> backgroundColored(appRed)
            |> rounded(25))
    controller.view = view
    return controller
}
