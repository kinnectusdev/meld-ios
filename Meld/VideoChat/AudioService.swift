//
//  AudioService.swift
//  Meld
//
//  Created by blakerogers on 5/12/20.
//  Copyright © 2020 com.meld.kinnectus. All rights reserved.
//

import Foundation
import AVFoundation
import UtilitiesPackage

class AudioService {
    private var audio: AVAudioPlayer?
    private var isPlaying: Bool = false
}
extension AudioService {
    enum Sound: String {
        case ringTone1 = "new_phone_ring"
        case ringTone2 = "phone_d_ringtone"
    }
    convenience init(sound: Sound) {
        self.init()
        guard let file = Bundle.main.path(forResource: sound.rawValue, ofType: ".mp3"),
             let url = URL(string: file) else { return }
         do {
             audio = try AVAudioPlayer(contentsOf: url)
            } catch {
                printLog(error.localizedDescription)
            }
    }
}

extension AudioService {
    func play() {
        audio?.numberOfLoops = -1
        audio?.play()
    }
    func stop() {
        audio?.stop()
    }
}
