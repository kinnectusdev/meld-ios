//
//  IncomingCallController.swift
//  Meld
//
//  Created by blakerogers on 9/21/20.
//  Copyright © 2020 com.meld.kinnectus. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import UtilitiesPackage

public let incomingCallBag = DisposeBag()

public func incomingCallController(caller: String, conversationID: String) -> UIViewController {
    let controller = UIViewController()
    let conversantImage = UIImageView()
    let conversantName = UILabel()
    let answerButton = UIButton()
    let dismissButton = UIButton()

    let (name, image, conversation) = incomingCallViewModel(callerId: caller, conversationID: conversationID)
    
    incomingCallBag.insert(
        name.drive(conversantName.rx.text),
        image.flatMap { image  -> Observable<Data?> in
           return  MediaImageService.fetchImageData(from: image)
        }.asDriver(onErrorJustReturn: nil).drive(onNext: { data in
            if let data = data {
                conversantImage.image = UIImage(data: data)
            } else {
                conversantImage.image = defaultAccountImage()
            }
        }),
        answerButton.rx.tap.withLatestFrom(conversation).subscribe(onNext: { conversation in
            let conversation_controller = videoChatController(conversation: conversation)
            controller.present(conversation_controller, animated: true, completion: nil)
        })
    )
    let view = UIView()
        |> blurredDarkBackground
        >>> appendSubView(conversantImage
            |> named("conversantImage")
            |> scaledToFill
            |> width_Height(250, 250)
            |> rounded(125)
            |> alignedToCenter)
        >>> appendSubView(conversantName
            |> named("conversantName")
            |> alignToSibling("conversantImage", constraint: alignTopToSiblingBottom(10))
            |> alignedToCenterX)
        >>> appendSubView(answerButton
            |> styledTitle(phoneFontIcon(size: 20))
            |> backgroundColored(appForestGreen)
            |> rounded(25)
            |> alignToSibling("conversantName", constraint: alignTopToSiblingBottom(20))
            |> alignRightToCenter(-10)
            |> width_Height(50, 50))
        >>> appendSubView(dismissButton
            |> styledTitle(phoneFontIcon(size: 20))
            |> alignToSibling("conversantName", constraint: alignTopToSiblingBottom(20))
            |> rounded(25)
            |> alignLeftToCenter(10)
            |> width_Height(50, 50))

    controller.view = view
    return controller
}
public func incomingCallViewModel(callerId: String, conversationID: String) -> (name: Driver<String>, image: Observable<String>, conversation: Observable<Conversation>) {
    let conversant = UserService.fetchUser(callerId).compactMap { $0.user }.share()
    let conversation = ConversationService.fetchConversation(id: conversationID).compactMap { $0 }
    let name = conversant.map { $0.name }
    let image = conversant.compactMap { $0.imageURLs.first }

    
    return (name.asDriver(onErrorJustReturn: ""),
            image,
            conversation)
}
