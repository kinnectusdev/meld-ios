//
//  VideoChatViewModel.swift
//  Meld
//
//  Created by blakerogers on 6/10/20.
//  Copyright © 2020 com.meld.kinnectus. All rights reserved.
//
import Foundation
import RxSwift
import RxCocoa
import TwilioVideo
import UtilitiesPackage

func videoChatViewModel(bag: DisposeBag,
                        conversation: Conversation,
                        willEndVideoChat: Observable<Void>,
                        didSelectFlipCamera: Observable<Void>,
                        didSelectEndCall: ControlEvent<Void>,
                        didToggleMute: ControlEvent<Void>)
    ->
    (image: Observable<String>,
    name: Driver<String>,
    endCall: Observable<Void>,
    isAudioEnabled: Driver<Bool>,
    localVideo: LocalVideoTrack?,
    remoteVideo: Observable<VideoTrack>,
    shouldMirror: Driver<Bool>) {
    let service = VideoChatService()
    let liveConversation = ConversationService.observeConversation(id: conversation.id).compactMap { $0 }
    let userIsConversantA = liveConversation.map {
        $0.conversantA_id == UserService.immediateCurrentUser()?.id ?? ""
    }
    let initiateVideoChatResult = liveConversation.take(1)
        .withLatestFrom(userIsConversantA, resultSelector: {($0, $1)})
        .flatMap { conversation, userIsConversantA -> Observable<VideoChatService.InitiateVideoChatResult> in
        var userToken: String {
            if userIsConversantA {
                return conversation.access_tokenA
            } else {
                return conversation.access_tokenB
            }
        }
        if userToken.isEmpty {
            return VideoChatService.initiateVideoChat(conversation: conversation, localUserIsA: userIsConversantA)
        } else {
            return .just(VideoChatService.InitiateVideoChatResult.success(conversationID: conversation.id, accessToken: userToken))
        }
    }.share()
    let videoChatError = initiateVideoChatResult.map { result -> String? in
        switch result {
        case .failure(let error):
            return error
        default:
            return nil
        }
    }.compactMap { $0 }
    ///End Start Preview
    let serviceOutput = initiateVideoChatResult.take(1).flatMap { result -> Observable<VideoChatUpdate> in
        switch result {
        case let .success(conversationId, accessToken):
            return service.connect(conversation: conversationId, accessToken: accessToken)
        default: return .never()
        }
    }.share()
    let remoteVideo = serviceOutput
        .compactMap { $0 as? VideoChatService.VideoTrackUpdate }
        .map { update -> VideoTrack? in
        switch update {
        case .didRecieveVideo(_, _, let participant):
          let videoPublications = participant.remoteVideoTracks
          let track = videoPublications.filter { $0.isTrackSubscribed }.compactMap { $0.remoteTrack }.first
          return track
        }
    }.compactMap { $0 }
    let videoChatRoomUpdate = serviceOutput
        .compactMap { $0 as? VideoChatService.VideoChatRoomUpdate }
        .share()
    let currentVideoChatRoom = videoChatRoomUpdate.map { update -> Room? in
        switch update {
        case .didConnect(let room), .didReconnect(let room), .update(let room):
            return room
        default: return nil
        }
    }.compactMap { $0 }
    let isAudioEnabled = serviceOutput
        .compactMap { $0 as? VideoChatService.VideoAudioUpdate }
        .map { $0 == .enabled }
    let conversant = liveConversation.withLatestFrom(userIsConversantA) { (conversation, isUserA) -> String in
        return isUserA ? conversation.conversantB_id : conversation.conversantA_id
    }.flatMap { conversantID -> Observable<MeldUser> in
        return UserService.fetchUser(conversantID).compactMap { $0.user}
    }.share()
    let conversantImage = conversant.map { $0.imageURLs.first ?? "" }
    let conversantName = conversant.map { $0.name }
    let appWillTerminate = (UIApplication.shared.delegate as! AppDelegate).rx.methodInvoked(#selector(AppDelegate.applicationWillTerminate(_:))).map { _ in ()}
    let endCall = Observable.merge(appWillTerminate, willEndVideoChat, didSelectEndCall.asObservable().take(1))
    bag.insert(
        endCall.withLatestFrom(currentVideoChatRoom).do(onNext: { _ in
            service.endCall()
        })
        .flatMap { room -> Observable<ConversationServiceResult> in
            if room.remoteParticipants.count == 0 {
                let update = conversation
                    |> prop(\.access_tokenA)({_ in ""})
                    |> prop(\.access_tokenB)({_ in ""})
                return ConversationService.updateConversation(update)
            } else {
                return .empty()
            }
        }.subscribe(onNext: { result in
            switch result {
            case .success:
                printLog("did end conversation video chat")
            case .failure(let error):
                printLog(error)
            }
        }),
        didToggleMute.subscribe(onNext: { _ in
            service.toggleMute()
        }),
        didSelectFlipCamera.subscribe(onNext: {
            service.flipCamera()
        }),
        videoChatRoomUpdate.withLatestFrom(conversant, resultSelector: { ($0, $1)}).filter({ update, _ in
            switch update {
            case .didConnect:
                return true
            default:
                return false
            }
        }).take(1).flatMap({ update, conversant  -> Observable<PushNotificationService.NotificationResult> in
            switch update {
            case .didConnect(let room):
            if room.remoteParticipants.isEmpty {
                return PushNotificationService.notifyVideoCall(user: conversant, conversationID: conversation.id)
            } else {
                return .just(.failure("Did not send Notification"))
            }
            default:
                return .just(.failure("Did not send Notification"))
            }
        }).subscribe(onNext: { result in
            switch result {
            case .failure(let error):
                printLog("notification error: \(error)")
            case .success:
                printLog("did send notification")
            }
        })
    )
        return (conversantImage,
                conversantName.asDriver(onErrorJustReturn: ""),
                didSelectEndCall.asObservable(),
                isAudioEnabled.asDriver(onErrorJustReturn: true),
                service.localVideoTrack,
                remoteVideo,
                service.cameraShouldMirror.asDriver(onErrorJustReturn: false))
}
