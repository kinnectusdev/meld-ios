//
//  VideoChatService.swift
//  Meld
//
//  Created by blakerogers on 9/27/20.
//  Copyright © 2020 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import TwilioVideo
import UtilitiesPackage

protocol VideoChatUpdate {}
class VideoChatService: NSObject {
    enum VideoChatRoomUpdate: VideoChatUpdate {
        case didConnect(Room)
        case connectionError
        case mediaError
        case didReconnect(Room)
        case update(Room)
    }
    enum VideoChatParticipantUpdate: VideoChatUpdate {
        case didConnect(RemoteParticipant)
        case didDisconnect(RemoteParticipant)
    }
    enum VideoTrackUpdate: VideoChatUpdate {
        case didRecieveVideo(RemoteVideoTrack, RemoteVideoTrackPublication, RemoteParticipant)
    }
    enum VideoAudioUpdate: VideoChatUpdate {
        case enabled
        case disabled
    }
    enum VideoChatCameraUpdate: VideoChatUpdate {
        case interruptionEnded
        case wasInterrupted
        case didFail
    }
    private let frontCamera = CameraSource.captureDevice(position: .front)
    private let backCamera = CameraSource.captureDevice(position: .back)
    private var mainCamera: CameraSource?
    public let cameraShouldMirror = PublishSubject<Bool>()
    public var localVideoTrack: LocalVideoTrack?
    private var localAudioTrack: LocalAudioTrack?
    private var localDataTrack: LocalDataTrack?
    private let videoChatRelay = PublishRelay<VideoChatUpdate>()
    private var establishedRoom: Room?
    override init() {
        super.init()
        if (frontCamera != nil || backCamera != nil) {
            let options = CameraSourceOptions { (builder) in
                if #available(iOS 13.0, *) {
                    // Track UIWindowScene events for the key window's scene.
                    // The example app disables multi-window support in the .plist (see UIApplicationSceneManifestKey).
                    builder.orientationTracker = UserInterfaceTracker(scene: UIApplication.shared.keyWindow!.windowScene!)
                }
            }
            // Preview our local camera track in the local video preview view.
            mainCamera = CameraSource(options: options, delegate: self)
            localVideoTrack = LocalVideoTrack(source: mainCamera!, enabled: true, name: "Camera")
            localAudioTrack = LocalAudioTrack(options: nil, enabled: true, name: "Microphone")
            let validDevice = frontCamera ?? backCamera
            if let validDevice = validDevice {
                mainCamera?.startCapture(device: validDevice) { [weak self] (captureDevice, videoFormat, error) in
                    if let error = error {
                        printLog("Capture failed with error.\ncode = \((error as NSError).code) error = \(error.localizedDescription)")
                    } else {
                        self?.cameraShouldMirror.onNext(captureDevice.position == .front)
                    }
                }
            }
        }
        else {
            printLog("No front or back capture device found!")
        }
    }
}
extension VideoChatService {
    func connect(conversation: String, accessToken: String)
        -> Observable<VideoChatUpdate> {
        printLog("Joining room with access token \(accessToken)")
        let connectOptions = ConnectOptions(token: accessToken) { [weak self] (builder) in
            guard let this = self else { return }
            if let audioTrack = this.localAudioTrack {
                builder.audioTracks = [ audioTrack ]
            }
            if let dataTrack = this.localDataTrack { builder.dataTracks = [ dataTrack ] }
            if let videoTrack = this.localVideoTrack {
                builder.videoTracks = [ videoTrack ]
            }
            // Use the preferred audio codec
            if let preferredAudioCodec = Settings.shared.audioCodec {
                builder.preferredAudioCodecs = [preferredAudioCodec]
            }
            // Use the preferred video codec
            if let preferredVideoCodec = Settings.shared.videoCodec {
                builder.preferredVideoCodecs = [preferredVideoCodec]
            }
            // Use the preferred encoding parameters
            if let encodingParameters = Settings.shared.getEncodingParameters() {
                builder.encodingParameters = encodingParameters
            }
            // Use the preferred signaling region
            if let signalingRegion = Settings.shared.signalingRegion {
                builder.region = signalingRegion
            }
            builder.networkPrivacyPolicy = .allowAll
            // The name of the Room where the Client will attempt to connect to. Please note that if you pass an empty
            // Room `name`, the Client will create one for you. You can get the name or sid from any connected Room.
            builder.roomName = conversation
        }
        establishedRoom = TwilioVideoSDK.connect(options: connectOptions, delegate: self)
        return videoChatRelay.asObservable()
    }
    func endCall() {
        establishedRoom?.disconnect()
    }
    func toggleMute() {
        if let audioTrack = localAudioTrack {
            audioTrack.isEnabled = !audioTrack.isEnabled
            videoChatRelay.accept(audioTrack.isEnabled ? VideoAudioUpdate.enabled : .disabled)
        }
    }
    func flipCamera() {
        var newDevice: AVCaptureDevice?
        if let camera = mainCamera, let captureDevice = camera.device {
            if captureDevice.position == .front {
                newDevice = CameraSource.captureDevice(position: .back)
            } else {
                newDevice = CameraSource.captureDevice(position: .front)
            }
            if let newDevice = newDevice {
                camera.selectCaptureDevice(newDevice) { [weak self] (captureDevice, videoFormat, error) in
                    if let error = error {
                        printLog("Error selecting capture device.\ncode = \((error as NSError).code) error = \(error.localizedDescription)")
                    } else {
                        self?.cameraShouldMirror.onNext(captureDevice.position == .front)
                    }
                }
            }
        }
    }
}
extension VideoChatService {
    struct InitiateVideoChatResponse: Codable {
        let result: String
        let access_token: String
        let conversation: String
        enum CodingKeys: CodingKey {
            case result
            case access_token
            case conversation
        }
    }
    enum InitiateVideoChatResult {
        case success(conversationID: String, accessToken: String)
        case failure(String)
    }
    static func initiateVideoChat(conversation: Conversation, localUserIsA: Bool) -> Observable<InitiateVideoChatResult> {
        let session = URLSession.shared
        guard let currentUser = UserService.immediateCurrentUser(), let url = URL(string: "https://us-central1-meld-dd5f8.cloudfunctions.net/makeVideoAccessToken?conversationID=\(conversation.id)&userID=\(currentUser.id)&localUserIsA=\(localUserIsA)") else { return .just(.failure("Failed to process request"))}
        return Observable.create({ observer in
            let task = session.dataTask(with: url) { (data, response, error) in
                guard let data = data else {
                    observer.onNext(.failure("Failed to fetch data"))
                    return
                }
                do {
                    let result = try JSONDecoder().decode(InitiateVideoChatResponse.self, from: data)
                    if result.access_token.isEmpty {
                        observer.onNext(.failure("Video Chat not authorized"))
                    } else {
                        observer.onNext(.success(conversationID: conversation.id, accessToken: result.access_token))
                    }
                } catch {
                    observer.onNext(.failure("Failed to fetch data"))
                }
            }
            task.resume()
            return Disposables.create()
        })
    }
}
extension VideoChatService: RoomDelegate  {
    func roomDidConnect(room: Room) {
        if let localParticipant = room.localParticipant {
//            printLog("Local identity \(localParticipant.identity)")
            // Set the delegate of the local particiant to receive callbacks
            localParticipant.delegate = self
        }
        room.remoteParticipants.forEach {
//            printLog("Remote identity \($0.identity)")
            $0.delegate = self
        }
        videoChatRelay.accept(VideoChatRoomUpdate.didConnect(room))
    }
    func participantDidConnect(room: Room, participant: RemoteParticipant) {
        printLog("Remote Participant \(participant.identity) did connect")
        participant.delegate = self
        establishedRoom = room
        establishedRoom?.delegate = self
        videoChatRelay.accept(VideoChatParticipantUpdate.didConnect(participant))
        videoChatRelay.accept(VideoChatRoomUpdate.update(room))
    }
    func participantDidDisconnect(room: Room, participant: RemoteParticipant) {
        printLog("\(participant.identity) did disconnect")
        establishedRoom = room
        establishedRoom?.delegate = self
        videoChatRelay.accept(VideoChatParticipantUpdate.didDisconnect(participant))
        videoChatRelay.accept(VideoChatRoomUpdate.update(room))
    }
    // Error will be either TwilioVideoSDK.Error.signalingConnectionError or TwilioVideoSDK.Error.mediaConnectionError
    func roomIsReconnecting(room: Room, error: Error) {
        printLog("Reconnecting to room \(room.name), error = \(String(describing: error))")
        switch error.localizedDescription {
        case "\(TwilioVideoSDK.Error.signalingConnectionError)":
            videoChatRelay.accept(VideoChatRoomUpdate.connectionError)
        case "\(TwilioVideoSDK.Error.mediaConnectionError)":
            videoChatRelay.accept(VideoChatRoomUpdate.mediaError)
        default:
            break
        }
    }

    func roomDidReconnect(room: Room) {
        printLog("Reconnected to room \(room.name)")
        establishedRoom = room
        establishedRoom?.delegate = self
        videoChatRelay.accept(VideoChatRoomUpdate.didReconnect(room))
    }
}
extension VideoChatService: LocalParticipantDelegate {
    func localParticipantDidFailToPublishDataTrack(participant: LocalParticipant, dataTrack: LocalDataTrack, error: Error) {
        printLog("did fail to publish data track")
    }
    func localParticipantDidPublishDataTrack(participant: LocalParticipant, dataTrackPublication: LocalDataTrackPublication) {
        printLog(" did publish data track")
    }
    func localParticipantDidFailToPublishAudioTrack(participant: LocalParticipant, audioTrack: LocalAudioTrack, error: Error) {
        printLog(" did fail to publish audio track")
    }
    func localParticipantDidFailToPublishVideoTrack(participant: LocalParticipant, videoTrack: LocalVideoTrack, error: Error) {
        printLog("did fail to publish video track")
    }
    func localParticipantDidPublishAudioTrack(participant: LocalParticipant, audioTrackPublication: LocalAudioTrackPublication) {
        printLog(" did publish audio track")
    }
    func localParticipantDidPublishVideoTrack(participant: LocalParticipant, videoTrackPublication: LocalVideoTrackPublication) {
        printLog("did publish video track")
    }
}
extension VideoChatService: RemoteParticipantDelegate {
    func remoteParticipantSwitchedOnVideoTrack(participant: RemoteParticipant, track: RemoteVideoTrack) {
        printLog("remoteParticipantSwitchedOnVideoTrack")
    }
    func remoteParticipantSwitchedOffVideoTrack(participant: RemoteParticipant, track: RemoteVideoTrack) {
        printLog("remoteParticipantSwitchedOffVideoTrack")
    }
    func remoteParticipantDidChangeDataTrackPublishPriority(participant: RemoteParticipant, priority: Track.Priority, publication: RemoteDataTrackPublication) {
        printLog("remoteParticipantDidChangeDataTrackPublishPriority")
    }
    func remoteParticipantDidChangeAudioTrackPublishPriority(participant: RemoteParticipant, priority: Track.Priority, publication: RemoteAudioTrackPublication) {
        printLog("remoteParticipantDidChangeAudioTrackPublishPriority")
    }
    func remoteParticipantDidChangeVideoTrackPublishPriority(participant: RemoteParticipant, priority: Track.Priority, publication: RemoteVideoTrackPublication) {
        printLog("remoteParticipantDidChangeVideoTrackPublishPriority")
    }
    func remoteParticipantDidPublishDataTrack(participant: RemoteParticipant, publication: RemoteDataTrackPublication) {
        printLog("remoteParticipantDidPublishDataTrack")
    }
    func remoteParticipantDidEnableAudioTrack(participant: RemoteParticipant, publication: RemoteAudioTrackPublication) {
        printLog("remoteParticipantDidEnableAudioTrack")
    }
    func remoteParticipantDidEnableVideoTrack(participant: RemoteParticipant, publication: RemoteVideoTrackPublication) {
        printLog("remoteParticipantDidEnableVideoTrack")
    }
    func didFailToSubscribeToAudioTrack(publication: RemoteAudioTrackPublication, error: Error, participant: RemoteParticipant) {
        printLog(error.localizedDescription)
    }
    func remoteParticipantDidDisableAudioTrack(participant: RemoteParticipant, publication: RemoteAudioTrackPublication) {
        printLog("remoteParticipantDidDisableAudioTrack")
    }
    func remoteParticipantDidDisableVideoTrack(participant: RemoteParticipant, publication: RemoteVideoTrackPublication) {
        printLog("remoteParticipantDidDisableVideoTrack")
    }
    func remoteParticipantDidPublishAudioTrack(participant: RemoteParticipant, publication: RemoteAudioTrackPublication) {
        printLog("remoteParticipantDidPublishAudioTrack")
    }
    func remoteParticipantDidPublishVideoTrack(participant: RemoteParticipant, publication: RemoteVideoTrackPublication) {
        printLog("remoteParticipantDidPublishVideoTrack")
    }
    func remoteParticipantDidUnpublishDataTrack(participant: RemoteParticipant, publication: RemoteDataTrackPublication) {
        printLog("remoteParticipantDidUnpublishDataTrack")
    }
    func remoteParticipantDidUnpublishAudioTrack(participant: RemoteParticipant, publication: RemoteAudioTrackPublication) {
        printLog("remoteParticipantDidUnpublishAudioTrack")
    }
    func remoteParticipantDidUnpublishVideoTrack(participant: RemoteParticipant, publication: RemoteVideoTrackPublication) {
        printLog("remoteParticipantDidUnpublishVideoTrack")
    }
    func didSubscribeToVideoTrack(videoTrack: RemoteVideoTrack,
                                  publication: RemoteVideoTrackPublication,
                                  participant: RemoteParticipant) {
        printLog("Participant \(participant.identity) added a video track.")
        videoChatRelay.accept(VideoTrackUpdate.didRecieveVideo(videoTrack, publication, participant))
    }
    func didSubscribeToAudioTrack(audioTrack: RemoteAudioTrack, publication: RemoteAudioTrackPublication, participant: RemoteParticipant) {
           // We are subscribed to the remote Participant's audio Track. We will start receiving the
           // remote Participant's audio now.
          printLog("Subscribed to \(publication.trackName) audio track for Participant \(participant.identity)")
        do {
            try AVAudioSession.sharedInstance().setCategory(.playAndRecord, mode: .videoChat, options: .mixWithOthers)
            try AVAudioSession.sharedInstance().overrideOutputAudioPort(.speaker)
            try AVAudioSession.sharedInstance().setActive(true)
        } catch {
            printLog("audio not working")
        }
       }
       
       func didUnsubscribeFromAudioTrack(audioTrack: RemoteAudioTrack, publication: RemoteAudioTrackPublication, participant: RemoteParticipant) {
           // We are unsubscribed from the remote Participant's audio Track. We will no longer receive the
           // remote Participant's audio.
           printLog( "Unsubscribed from \(publication.trackName) audio track for Participant \(participant.identity)")
       }
}
extension VideoChatService: CameraSourceDelegate {
    func cameraSourceInterruptionEnded(source: CameraSource) {
        videoChatRelay.accept(VideoChatCameraUpdate.interruptionEnded)
    }
    func cameraSourceDidFail(source: CameraSource, error: Error) {
        videoChatRelay.accept(VideoChatCameraUpdate.didFail)
    }
    func cameraSourceWasInterrupted(source: CameraSource, reason: AVCaptureSession.InterruptionReason) {
        videoChatRelay.accept(VideoChatCameraUpdate.wasInterrupted)
    }
}
