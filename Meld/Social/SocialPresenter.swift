//
//  SocialPresentere.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import UtilitiesPackage
import UIKit

final class SocialPresenter {
    private var bag = DisposeBag()
    private let interactor: SocialInteractorIdentity
    private let router: SocialRouter
    
    let backButton = UIButton()
    |> visibility( DeviceInfo.deviceType == .iPad ? 1.0 : 0.0 )
    |> asButton
    let conversantsView: UIView
    let conversationsView: UIView
    let noConversationsView: UIView = noConversationPage()
    
    init(interactor: SocialInteractorIdentity, conversantsView: UIView, conversationsView: UIView) {
        
        self.interactor = interactor
        self.router = SocialRouter()
        
        self.conversantsView = conversantsView
        self.conversationsView = conversationsView
        
        bag.insert(
            interactor.observeIsConversationAvailable()
                .do(onNext: { print("conversation is Available: \($0)")})
                .map { $0 ? 0.0 : 1.0 }.subscribe(onNext: { alpha in
                    onMainThread {
                        UIView.animate(withDuration: 0.25) { [weak self] in
                            self?.noConversationsView.alpha = alpha
                        }
                    }
                }),
            backButton.rx.tap.subscribe(onNext: { [weak self] _ in
                    self?.router.dismiss()
                })
            )
    }
}

