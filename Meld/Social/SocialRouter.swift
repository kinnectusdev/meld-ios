//
//  ConversationRouter.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import UIKit
import UtilitiesPackage

final class SocialRouter {
    func dismiss() {
        currentController().dismiss(animated: true, completion: nil)
    }
}

extension SocialRouter {
    static func presentation() -> UIViewController {
        let conversationsInteractor = ConversationsInteractor()
        let interactor = SocialInteractor(conversationsInteractor: conversationsInteractor)
        let conversantsView = ConversantsViewRouter.presentation(conversantsManager: interactor)
        let conversationsView = ConversationsRouter.presentation(interactor: conversationsInteractor)
        let presenter = SocialPresenter(interactor: interactor,
                                              conversantsView: conversantsView,
                                              conversationsView: conversationsView)
        return SocialView(presenter: presenter)
    }
}
