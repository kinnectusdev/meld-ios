//
//  ConversationPage.swift
//  Meld
//
//  Created by blakerogers on 11/29/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import UIKit
import UtilitiesPackage

final class SocialView: UIViewController {
    
    private weak var presenter: SocialPresenter!
    
    private func setViews() {
        let view = UIView()
        |> whiteBackground
        >>> isClipping
        >>> appendSubView(presenter.conversantsView
            |> named("conversants")
            >>> utilizeConstraints
            >>> alignToLeft(40)
            >>> alignedToRight
            >>> alignToTop(40)
            >>> viewHeight(60))
        >>> appendSubView(presenter.conversationsView |> alignedToLeftBottomRight |> alignToSibling("conversants", constraint: alignTopToSiblingBottom(20)))
        >>> appendSubView(presenter.noConversationsView |> alignedToLeftBottomRight >>> alignToSibling("conversants", constraint: alignTopToSiblingBottom(10)))
        >>> appendSubView(
            presenter.backButton
            |> named("back_button")
            >>> setImage(UIImage(named: "BackButton"))
            >>> alignToLeft(10)
            >>> alignToTop(70)
        )
        self.view = view
    }
}

extension SocialView {
    convenience init(presenter: SocialPresenter) {
        self.init()
        self.presenter = presenter
        self.setViews()
        
        self.modalPresentationStyle = ( DeviceInfo.deviceType == .iPad ? .overCurrentContext : .automatic)
    }
}

