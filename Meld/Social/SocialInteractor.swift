//
//  SocialInteractor.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol SocialInteractorIdentity: ConversantsManagerIdentity {
    func observeIsConversationAvailable() -> Observable<Bool>
}

final class SocialInteractor {
    
    private weak var conversationsInteractor: ConversationsInteractorIdentity!
    
    init(conversationsInteractor: ConversationsInteractorIdentity) {
        self.conversationsInteractor = conversationsInteractor
    }
}

extension SocialInteractor: SocialInteractorIdentity {
    
    func didSelectStartConversation(user: MeldUser) {
        conversationsInteractor.didSelectStartConversation(conversant: user)
    }

    func observeConversants() -> Observable<[MeldUser]> {
        conversationsInteractor.observeConversants()
    }
    
    func observeCurrentConversantID() -> Observable<String> {
        conversationsInteractor.observeCurrentConversantID()
    }
    
    func didSelectConversant(conversant: MeldUser) {
        conversationsInteractor.didSelectConversant(conversant: conversant)
    }
    
    func observeIsConversationAvailable() -> Observable<Bool> {
        conversationsInteractor.observeIsConversationAvailable()
    }
}
