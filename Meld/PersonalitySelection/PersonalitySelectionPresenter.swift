//
//  PersonalitySelectionPresenter.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import UtilitiesPackage
import UIKit

final class PersonalitySelectionPresenter {
    
    private let interactor: PersonalitySelectionInteractorIdentity
    private let router: PersonalitySelectionRouter
    private let bag = DisposeBag()
    
    let backButton = UIButton()
    |> visibility( DeviceInfo.deviceType == .iPad ? 1.0 : 0.0 )
    |> asButton
    let list = UIScrollView()
        |> isPaging
        |> scrollSize(CGSize(width: PersonalityType.allCases.count.cgFloat() * appFrame.width,
                             height: appFrame.height - 200))
        |> asScrollView
    let selectTypeButton = UIButton()
    let types: [UIView]
    
    init(interactor: PersonalitySelectionInteractorIdentity) {
        
        self.interactor = interactor
        
        self.router = PersonalitySelectionRouter()
        
        self.types = PersonalityType.allCases.map { type -> UIView in
            
            let index = PersonalityType.allCases |> index(of:type)
            
            let view = PersonalityTypeSelectionItemRouter.presentation(personalityType: type)
            |> framed(CGRect(x: appFrame.width * index.cgFloat(), y: 0, width: appFrame.width, height: appFrame.height))
            return view
        }
        
        bag.insert(
            backButton.rx.tap.subscribe(onNext: { [weak self] _ in
            self?.router.dismiss()
            }),
            selectTypeButton.rx.tap.subscribe(onNext: { [weak self] _ in
                self?.interactor.didSelectPersonalityItem()
                self?.router.dismiss()
            }),
            list.rx.contentOffset
                .map { $0.x }
                .map { Int($0 / appFrame.width) }
                .map { PersonalityType.allCases.itemAt($0) }
                .compactMap { $0 }
                .subscribe(onNext: { [weak self] type in
                    self?.interactor.didSetPersonalityType(type: type)
            })
        )
    }
}
