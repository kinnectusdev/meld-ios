//
//  PersonalitySelectionRouter.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UIKit
import UtilitiesPackage

final class PersonalitySelectionRouter {
    func dismiss() {
        currentController().dismiss(animated: true, completion: nil)
    }
}
extension PersonalitySelectionRouter {
    static func presentation(manager: PersonalitySelectionManager) -> UIViewController {
        PersonalitySelectionView(presenter: PersonalitySelectionPresenter(interactor: PersonalitySelectionInteractor(manager: manager)))
    }
}
