//
//  PersonalitySelectionInteractor.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation

protocol PersonalitySelectionManager {
    func didSelectPersonalityType(type: PersonalityType)
}
protocol PersonalitySelectionInteractorIdentity {
    func didSetPersonalityType(type: PersonalityType)
    func didSelectPersonalityItem()
}

final class PersonalitySelectionInteractor {
    private let manager: PersonalitySelectionManager
    private var setPersonalityType: PersonalityType = PersonalityType.allCases.first!
    init(manager: PersonalitySelectionManager) {
        self.manager = manager
    }
}

extension PersonalitySelectionInteractor: PersonalitySelectionInteractorIdentity {
    func didSetPersonalityType(type: PersonalityType) {
        setPersonalityType = type
    }
    func didSelectPersonalityItem() {
        manager.didSelectPersonalityType(type: setPersonalityType)
    }
}
