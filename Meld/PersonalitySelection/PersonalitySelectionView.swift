//
//  PersonalitySelection.swift
//  Personality Selection
//
//  Created by blakerogers on 10/27/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import UIKit
import UtilitiesPackage

final class PersonalitySelectionView: UIViewController {
    
    private var presenter: PersonalitySelectionPresenter!
    
    private func setViews() {
        let view = UIView()
            |> whiteBackground
            >>> appendSubView(UILabel()
                |> named("title")
                >>> titled("Select Your Personality Type")
                >>> textAligned(.center)
                >>> textFont(appFont(18))
                >>> unlimitedLines
                >>> alignedToCenterX
                >>> alignToTop(20))
            >>> appendSubView(presenter.list
                            |> alignToSibling("title", constraint: alignTopToSiblingBottom(20))
                            |> alignedToLeftBottomRight
                            |> appendSubviews(presenter.types))
            >>> appendSubView(
                presenter.backButton
                |> named("back_button")
                >>> setImage(UIImage(named: "BackButton"))
                >>> alignToLeft(10)
                >>> alignToSibling("title", constraint: alignToSiblingCenterY())
            )
            |> appendSubView(presenter.selectTypeButton
                                |> whiteBackground
                                |> styledTitle(addFontIcon(size: 16, color: appDarkGrey))
                                |> alignToBottom(-40)
                                |> alignToRight(-20)
                                |> width_Height(40, 40)
                                |> greyShadowed)
        self.view = view
    }
}

extension PersonalitySelectionView {
    
    convenience init(presenter: PersonalitySelectionPresenter) {
        self.init()
        self.presenter = presenter
        self.modalPresentationStyle = ( DeviceInfo.deviceType == .iPad ? .overCurrentContext : .automatic)
        self.setViews()
    }
}

