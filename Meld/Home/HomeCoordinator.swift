//
//  HomeCoordinator.swift
//  Meld
//
//  Created by blakerogers on 11/26/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import RxSwift
import PushKit
public enum HomeCoordination {
    case displayHome
    case displayPricePlan
    case displayPrompts
    case displayPersonalitySelection
    case displayVideoChat(callerID: String)
    case logout
}

