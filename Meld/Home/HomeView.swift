//
//  HomeController.swift
//  Meld
//
//  Created by blakerogers on 11/26/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import UtilitiesPackage

final class HomeView: UIViewController {
    private var presenter: HomePresenter!
    private func setViews() {
        let view = UIView()
            |> backgroundColored(appDiamond)
            |> appendSubView(presenter.matchView
            |> alignedToSafeAreaTop
            >>> alignedToLeft
            >>> alignedToRight
            >>> alignedToBottom)
        >>> appendSubView(presenter.menuView
            |> alignedToLeft
            |> alignedToRight
            |> alignToBottom(10)
            |> viewHeight(100))
        self.view = view
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setViews()
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        GlobalEventNotifier.shared.emit(event: .didRotateScreen)
    }
}
extension HomeView {
    convenience init(presenter: HomePresenter) {
        self.init()
        self.modalPresentationStyle = .fullScreen
        self.presenter = presenter
    }
}
