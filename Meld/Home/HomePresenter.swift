//
//  HomePresenter.swift
//  Meld
//
//  Created by Blake Rogers on 7/12/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import UIKit

final class HomePresenter {
    let interactor: HomeInteractorIdentity
    let router: HomeRouter
    let matchView: UIView
    let menuView: UIView
    init(interactor: HomeInteractorIdentity, matchView: UIView, menuView: UIView) {
        self.interactor = interactor
        self.router = HomeRouter()
        self.matchView = matchView
        self.menuView = menuView
    }
}
