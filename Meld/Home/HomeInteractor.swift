//
//  HomeViewModel.swift
//  Meld
//
//  Created by blakerogers on 11/28/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift

protocol HomeInteractorIdentity {
    
}

final class HomeInteractor {
    
}
extension HomeInteractor: HomeInteractorIdentity {
    
}
