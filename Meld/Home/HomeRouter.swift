//
//  HomeRouter.swift
//  Meld
//
//  Created by Blake Rogers on 7/11/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

/*
 HomeRouter
    displayPricePlan
    displayPersonality
    displayVideoChat
    logout
     
    presentation
        
        homeInteractor
        homePresenter
        homeView
        
        matchInteractor : homeInteractor
        matchPresenter
        matchView

        menuInteractor : homeInteractor : matchInteractor
        menuPresenter
        menuView
        
            homeView add match then menuView
        
 */
/*
    HomeInteractor
        
 */
/*
    HomePresenter
        interactor: HomeInteractor
    
 */

/*
    HomeView
            presenter: HomePresenter
 */

import Foundation
import UIKit

final class HomeRouter {
    func displayVideoChat(callerID: String) {}
    func displayPersonalitySelection() {}
    func displayPricePlan() {}
}
extension HomeRouter {
    static func presentation() -> UIViewController {
        
        let homeInteractor = HomeInteractor()
        
        let matchView = MatchProfileRouter.presentation(homeInteractor: homeInteractor)
        
        let menuView = NavigationMenuRouter.presentation(homeInteractor: homeInteractor)
        
        let homePresenter = HomePresenter(interactor: homeInteractor,
                                          matchView: matchView,
                                          menuView: menuView)
        let homeView = HomeView(presenter: homePresenter)
        
        return homeView
    }
}
