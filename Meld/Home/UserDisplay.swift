//
//  UserDisplay.swift
//  Meld
//
//  Created by blakerogers on 11/12/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import UtilitiesPackage

class UserMosaicDisplay: UIView {
    private var heights: [CGFloat] = []
    lazy var collection: UICollectionView = {
        let layout = MosaicLayout()
        layout.delegate = self
        let collection = UICollectionView(frame: appFrame, collectionViewLayout: layout)
        collection.backgroundColor = .white
        collection.delegate = self
        collection.register(UserMosaicCell.self, forCellWithReuseIdentifier: UserMosaicCell.identifier)
        collection.translatesAutoresizingMaskIntoConstraints = false
        return collection
    }()
    public let didSelectUser = PublishSubject<Int>()
    override func layoutSubviews() {
        super.layoutSubviews()
        setViews()
    }
    private func setViews() {
        add(views: collection)
        backgroundColor = .clear
        collection.constrainInView(view: self, top: 0, left: 0, right: 0, bottom: 0)
        heights = heightsForImages()
    }
    private func heightsForImages() -> [CGFloat] {
        let small: CGFloat = 80
        let medium: CGFloat = 120
        let large: CGFloat = 180
        return (0...200).map { _ in
            let random = arc4random() % 10
            return random < 3 ? small : random < 6 ? medium : large
        }
    }
}
extension UserMosaicDisplay: MosaicLayoutDelegate {
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
        if let height = heights.itemAt(indexPath.item) {
            return height
        } else {
            let heightIndex = heights.count / indexPath.item
            return heights[heightIndex]
        }
    }
}
extension UserMosaicDisplay: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? UserMosaicCell, let image = cell.userImage.image?.pngData() else { fatalError()}
        UserDefaults.standard.setValue(image, forKey: "match_profile_image")
        didSelectUser.onNext(indexPath.item)
    }
}
import RxSwift
import UtilitiesPackage

class UserMosaicCell: UICollectionViewCell {
    static let identifier: String = "UserMosaicCell"
    override func layoutSubviews() {
        super.layoutSubviews()
        setViews()
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        contentView.subviews.forEach {$0.constraints.forEach { userImage.removeConstraint($0)} }
        userImage.alpha = 0.0
        userName.text = ""
        userImage.layer.cornerRadius = 0
        imageShadow.layer.cornerRadius = 0
        imageShadow.alpha = 0.0
    }
    let bag = DisposeBag()
    let userImage = UIImageView.image(mode: .scaleAspectFill, visible: false)
    let userName = UILabel.label(font: appFont(12), txtColor: appBlack, background: .clear)
    let imageShadow = UIView.containerView(alpha: 0.0)
    private func setViews() {
        imageShadow.layer.cornerRadius = 10
        contentView.add(views: imageShadow, userImage, userName)
        imageShadow.constrainCenterToCenter(of: contentView)
        imageShadow.constrainWidth_Height(width: contentView.frame.height*0.9, height: contentView.frame.height*0.9)
        imageShadow.layer.addShadow(4, dy: 4, color: appPurple, radius: 10, opacity: 0.9)
        userImage.constrainCenterToCenter(of: contentView)
        userImage.constrainWidth_Height(width: contentView.frame.height*0.9, height: contentView.frame.height*0.9)
        userImage.layer.cornerRadius = 10
        userName.constrainTopToBottom(of: userImage, constant: 6)
        userName.constrainCenterXTo(view: contentView, constant: 0)
    }
}
protocol MosaicLayoutDelegate: AnyObject {
  func collectionView(
    _ collectionView: UICollectionView,
    heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat
}

class MosaicLayout: UICollectionViewLayout {
    // 1
    weak var delegate: MosaicLayoutDelegate?

    // 2
    private let numberOfColumns = 2
    private let cellPadding: CGFloat = 6

    // 3
    private var cache: [UICollectionViewLayoutAttributes] = []

    // 4
    private var contentHeight: CGFloat = 0

    private var contentWidth: CGFloat {
      guard let collectionView = collectionView else {
        return 0
      }
      let insets = collectionView.contentInset
      return collectionView.bounds.width - (insets.left + insets.right)
    }
    // 5
    override var collectionViewContentSize: CGSize {
      return CGSize(width: contentWidth, height: contentHeight)
    }

    override func prepare() {
      // 1
      guard
        cache.isEmpty,
        let collectionView = collectionView
        else {
          return
      }
      // 2
      let columnWidth = contentWidth / CGFloat(numberOfColumns)
      var xOffset: [CGFloat] = []
      for column in 0..<numberOfColumns {
        xOffset.append(CGFloat(column) * columnWidth)
      }
      var column = 0
      var yOffset: [CGFloat] = .init(repeating: 0, count: numberOfColumns)
        
      // 3
      for item in 0..<collectionView.numberOfItems(inSection: 0) {
        let indexPath = IndexPath(item: item, section: 0)
          
        // 4
        let photoHeight = delegate?.collectionView(
          collectionView,
          heightForPhotoAtIndexPath: indexPath) ?? 180
        let height = cellPadding * 2 + photoHeight
        let frame = CGRect(x: xOffset[column],
                           y: yOffset[column],
                           width: columnWidth,
                           height: height)
        let insetFrame = frame.insetBy(dx: cellPadding, dy: cellPadding)
          
        // 5
        let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
        attributes.frame = insetFrame
        cache.append(attributes)

        // 6
        contentHeight = max(contentHeight, frame.maxY)
        yOffset[column] = yOffset[column] + height
        
        column = column < (numberOfColumns - 1) ? (column + 1) : 0
      }
    }
    override func layoutAttributesForElements(in rect: CGRect)
        -> [UICollectionViewLayoutAttributes]? {
      var visibleLayoutAttributes: [UICollectionViewLayoutAttributes] = []
      
      // Loop through the cache and look for items in the rect
      for attributes in cache {
        if attributes.frame.intersects(rect) {
          visibleLayoutAttributes.append(attributes)
        }
      }
      return visibleLayoutAttributes
    }
    override func layoutAttributesForItem(at indexPath: IndexPath)
        -> UICollectionViewLayoutAttributes? {
      return cache[indexPath.item]
    }
}
