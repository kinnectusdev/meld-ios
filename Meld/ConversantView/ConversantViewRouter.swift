//
//  ConversantViewRouter.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UIKit
import UtilitiesPackage

final class ConversantViewRouter {
    func dismiss() {
        currentController().dismiss(animated: true, completion: nil)
    }
}
extension ConversantViewRouter {
    static func presentation(conversant: MeldUser, conversantManager: ConversantManagerIdentity) -> UIViewController {
        ConversantView(presenter: ConversantPresenter(interactor: ConversantInteractor(user: conversant, manager: conversantManager)))
    }
}
