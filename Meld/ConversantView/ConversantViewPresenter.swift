//
//  ConversantViewPresenter.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import CHIPageControl
import UtilitiesPackage
import UIKit

final class ConversantPresenter {
    private let bag = DisposeBag()
    private let interactor: ConversantInteractorIdentity
    private let router: ConversantViewRouter
    
    
    let matchImages = MatchImageView()
    let pageControl = CHIPageControlPuya()
                            |> pageTint(.white)
                            >>> pageCurrentTint(appGrey)
                            >>> numberOfPages(4)
                            >>> pagePadding(6)
                            >>> pageRadius(4)
    
    let nameLabel = UILabel()
    let likeButton = UIButton()
    let backButton = UIButton()
    |> visibility( DeviceInfo.deviceType == .iPad ? 1.0 : 0.0 )
    |> asButton
    let personalityTypeLabel = UILabel()
    let attributesCollection = AttributesSectionView.view()
    
    init(interactor: ConversantInteractorIdentity) {
        self.interactor = interactor
        self.router = ConversantViewRouter()
       
        matchImages.collection.configure(images: interactor.observeImages().distinctUntilChanged(),
                                         onDidChangeUser: Observable.empty(),
                                         imageLoadingDelegate: matchImages)
        bag.insert(
            backButton.rx.tap.subscribe(onNext: { [weak self] _ in
                self?.router.dismiss()
            }),
            likeButton.rx.tap.subscribe(onNext: { [weak self] _ in
                self?.interactor.didToggleIsLiked()
            }),
            interactor.observeImages().subscribe(onNext: { [weak self] images in
                self?.pageControl.numberOfPages = images.count
                self?.matchImages.loading()
            }),
            interactor.observeName()
                .asDriver(onErrorJustReturn: "")
                .drive(nameLabel.rx.text),
            interactor.observePersonalityType()
                .asDriver(onErrorJustReturn: "")
                .drive(personalityTypeLabel.rx.text),
            interactor.observePersonalityType()
                .map {PersonalityType.typeForTitle($0) }
                .compactMap { $0 }
                .map { $0.personalityGroup.color }
                .asDriver(onErrorJustReturn: .white)
                .drive(onNext: { [weak self] color in
                    onMainThread {
                        self?.personalityTypeLabel.backgroundColor = color
                    }
                }),
            interactor.observeIsLiked().asDriver(onErrorJustReturn: false).drive(onNext: { [weak self] liked in
                onMainThread {
                    UIViewPropertyAnimator(duration: 0.25, curve: .easeIn) { [weak self] in
                        self?.likeButton.setAttributedTitle(heartFontIcon(size: 24,  color: liked ? .white : appBlack), for: .normal)
                        self?.likeButton.backgroundColor = liked ? appGold : .white
                    }.startAnimation()
                }
            }),
            interactor.observeAttributes().subscribe(onNext: { [weak self] attributes in
                self?.attributesCollection.setItems(attributes)
            }),
            attributesCollection.didSelectAttribute.asObservable().subscribe(onNext: { attributeOption in
                //TODO: present modal via router
//                let modal = attributeOptionModal(bag: match_user_info_bag, attributes: attributeOption)
//                modal.alpha = 0.0
//                modal.frame = CGRect(x: 0, y: 50, width: appFrame.width, height: appFrame.height - 50)
//                currentWindow().add(views: modal)
//                UIView.animate(withDuration: 0.25) {
//                    modal.alpha = 1.0
//                }
            })
        )
    }
}
