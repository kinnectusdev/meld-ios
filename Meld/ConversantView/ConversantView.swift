//
//  ConversantView.swift
//  Meld
//
//  Created by Blake Rogers on 6/19/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UIKit
import UtilitiesPackage

final class ConversantView: UIViewController {
    private var presenter: ConversantPresenter!
    private func setViews() {
        let a = appendSubView(presenter.nameLabel
                |> named("name")
                >>> textColored(appBlack)
                >>> textFont(appFontFormal(30))
                >>> alignToLeft(20) >>> alignToTop(30))
            >>> appendSubView(
                presenter.backButton
                |> named("back_button")
                >>> setImage(UIImage(named: "BackButton"))
                >>> alignToLeft(10)
                >>> alignToSibling("name", constraint: alignToSiblingCenterY())
            )
            >>> appendSubView(presenter.likeButton
                |> named("likeButton")
                >>> alignToRight(-20)
                >>> styledTitle(heartFontIcon(size: 24, color: appBlack))
                >>> alignToSibling("name", constraint: alignedToSiblingTop)
                >>> rounded(18)
                >>> width_Height(36, 36))
            >>> appendSubView(UILabel()
                |> named("attributesHeading")
                >>> alignToSibling("name", constraint: alignedToSiblingLeft)
                >>> alignToSibling("name", constraint: alignTopToSiblingBottom(8))
                >>> textFont(appFontFormal(20))
                >>> titled("Attributes")
                >>> textColored(appBlack))
            >>> appendSubView(presenter.attributesCollection
                |> named("attributes")
                >>> alignToSibling("name", constraint: alignedToSiblingLeft)
                >>> alignToSibling("likeButton", constraint: alignedToSiblingRight)
                >>> alignToSibling("attributesHeading", constraint: alignTopToSiblingBottom(8))
                >>> viewHeight(50))
        
        let b = appendSubView(UILabel()
                |> named("personalityHeading")
                >>> textFont(appFontFormal(20))
                >>> textColored(appBlack)
                >>> titled("Personality")
                >>> alignToSibling("attributes", constraint: alignedToSiblingLeft)
                >>> alignToSibling("attributes", constraint: alignTopToSiblingBottom(8)))
            >>> appendSubView(presenter.personalityTypeLabel
                |> named("personalityType")
                >>> rounded(15)
                >>> backgroundColored(appRoyalPurple)
                >>> width_Height(100, 30)
                >>> textColored(.white)
                >>> textCenterAligned
                >>> textFont(appFont(18))
                >>> alignToSibling("personalityHeading", constraint: alignTopToSiblingBottom(8))
                >>> alignToSibling("personalityHeading", constraint: alignedToSiblingLeft))
        
        let view = UIView()
        |>  diamondBackground
            >>> appendSubView(presenter.matchImages
            |> named("user_image")
            >>> alignedToLeftTopRight
            >>> alignToBottom(-250))
            >>> appendSubView(presenter.pageControl
            |> utilizeConstraints
            |> isInvisible
            >>> alignedToCenterX
            >>> alignToSibling("user_image", constraint: alignToSiblingBottom(-60)))
        >>> appendSubView( UIView()
                            |> whiteBackground
                            |> framed(CGRect(x: 0, y: appFrame.height - 350, width: appFrame.width, height: 350))
                            >>> roundedGreyShadowed(10)
                            >>> a
                            >>> b)
        
        self.view = view
    }
}
extension ConversantView {
    convenience init(presenter: ConversantPresenter) {
        self.init()
        self.presenter = presenter
        self.setViews()
        self.modalPresentationStyle = ( DeviceInfo.deviceType == .iPad ? .overCurrentContext : .automatic)
    }
}


