//
//  ConversantViewInteractor.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol ConversantManagerIdentity {
    func didSelectLikeUser(conversant: MeldUser)
    func didSelectUnlikeUser(conversant: MeldUser)
    func isInConversation(conversant: MeldUser) -> Observable<Bool>
}
protocol ConversantInteractorIdentity {
    func didToggleIsLiked()
    
    func observeName() -> Observable<String>
    func observeAge() ->Observable<String>
    func observeIsLiked() -> Observable<Bool>
    func observeAttributes() -> Observable<[AttributeOption]>
    func observePersonalityType() -> Observable<String>
    func observeImages() -> Observable<[String]>
    func observeIsInConversation() -> Observable<Bool>
}
final class ConversantInteractor {
    
    private let bag = DisposeBag()
    private let conversantManager: ConversantManagerIdentity
    private let conversant: MeldUser
    private let name = BehaviorRelay<String>(value: .empty)
    private let age = BehaviorRelay<String>(value: .empty)
    private let isLiked = BehaviorRelay<Bool>(value: false)
    private let attributes = BehaviorRelay<[AttributeOption]>(value: [])
    private let personalityType = BehaviorRelay<String>(value: .empty)
    private let images = BehaviorRelay<[String]>(value: [])
    
        init(user: MeldUser, manager: ConversantManagerIdentity) {
        self.conversant = user
        self.conversantManager = manager
        let user = Observable.just(user)
        bag.insert(
            user.map { $0.name }.bind(to: name),
            user.map { $0.age?.presentation ?? .empty }.bind(to: age),
            user.map { $0.id }.flatMap { userId -> Observable<Bool> in
                return UserService.observeUserLikedStatus(id: userId)
            }.bind(to: isLiked),
            user.map { $0.attributes }.compactMap { $0.compactMap { $0 }.filter { $0.isListable } }.bind(to: attributes),
            user.map { $0.personalityType?.title ?? .empty }.bind(to: personalityType),
            user.map { $0.imageURLs }.bind(to: images)
        )
    }
}
extension ConversantInteractor: ConversantInteractorIdentity {
    func didToggleIsLiked() {
        isLiked.take(1).do(onNext: { [weak self] isLiked in
            guard let this = self else { return }
            self?.isLiked.accept(!isLiked)
            if isLiked {
                self?.conversantManager.didSelectUnlikeUser(conversant: this.conversant)
            } else {
                self?.conversantManager.didSelectUnlikeUser(conversant: this.conversant)
            }
        }).flatMap { [weak self] isLiked -> Observable<Bool> in
            guard let id = self?.conversant.id else { return .never() }
            return UserService.updateUserLikedStatus(id: id, isLiked: !isLiked)
        }.subscribe().disposed(by: bag)
    }
    
    func observeName() -> Observable<String> {
        name.asObservable()
    }
    
    func observeAge() -> Observable<String> {
        age.asObservable()
    }
    
    func observeIsLiked() -> Observable<Bool> {
        isLiked.asObservable()
    }
    
    func observeAttributes() -> Observable<[AttributeOption]> {
        attributes.asObservable()
    }
    
    func observePersonalityType() -> Observable<String> {
        personalityType.asObservable()
    }
    
    func observeImages() -> Observable<[String]> {
        images.asObservable()
    }
    func observeIsInConversation() -> Observable<Bool> {
        conversantManager.isInConversation(conversant: conversant)
    }
    
}
