//
//  Onboarding2.swift
//  UIFunctions
//
//  Created by blakerogers on 9/22/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//
import RxSwift
import RxCocoa
import UtilitiesPackage
import UIKit

public let testingText = "Answer Honestly as Possible \n\n\n Meld Asseses your personality and your Interests to map your Associative Style. Your unique profile is then utilized to help pair you with like minded individulas likley to become friends and complementatry people likely to become romantic interests"

public func personality_test_intro_cell() -> (UIView, ControlEvent<Void>) {
    let beginButton: UIButton = UIButton()
    let pageSelection = beginButton.rx.tap
    let view = UIView()
        |> whiteBackground
        |> appendSubView(
            UILabel()
            |> alignToTop(40)
            |> alignedToCenterX
            |> titled("Personality Test")
            |> textFont(appFontFormal(20))
        )
        >>> appendSubView(
            UILabel()
            |> textAligned(.center)
            >>> unlimitedLines
            >>> textForLabel(testingText)
            >>> textFont(appFont(18))
            >>> textColored(appDarkGrey)
            >>> alignToCenterY(-50)
            >>> alignToLeft(20)
            >>> alignToRight(-20)
        )
        >>> appendSubView(
            beginButton
            |> styledTitle(boldBlackAttributedString("Begin", 18))
            >>> alignToBottom(-50)
            >>> alignToCenterX()
            >>> width_Height(250, 60)
        )
    return (view, pageSelection)
}
