//
//  PersonalityTestRouter.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UIKit
import UtilitiesPackage

final class PersonalityTestRouter {
    func displayResults(results: PersonalityTestResult) {
        let view = PersonalityTestResultRouter.presentation(testResult: results)
        currentController().present(view, animated: true, completion: nil)
    }
    func dismiss() {
        currentController().dismiss(animated: true, completion: nil)
    }
}

extension PersonalityTestRouter {
    static func presentation() -> UIViewController {
        PersonalityTestView(presenter: PersonalityTestPresenter(interactor: PersonalityTestInteractor()))
    }
}
