//
//  PersonalityTestInteractor.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import UtilitiesPackage

protocol PersonalityTestInteractorIdentity: PersonalityTestItemManager {
    func observeCompletionRate() -> Observable<Int>
    func observeCurrentAnswer(question: PersonalityTestQuestion) -> PersonalityTestAnswer?
    func observeQuestions() -> [PersonalityTestQuestion]
    func observeTestIsComplete() -> Bool
    func observeTestResults() -> PersonalityTestResult
}

final class PersonalityTestInteractor {
    private let bag = DisposeBag()
    private var testResults: [PersonalityTestQuestion: PersonalityTestAnswer] = [:]
    private let testCompletion = PublishSubject<Int>()
}

extension PersonalityTestInteractor: PersonalityTestInteractorIdentity {
    func observeTestResults() -> PersonalityTestResult {
        if observeTestIsComplete() {
            let testResult = testResults.reduce(PersonalityTestResult.empty, { result, args -> PersonalityTestResult in
                let (question, answer) = args
                let (factor, score) = question.score(answer: answer)
                switch factor {
                case .extraversion:
                    return result |> prop(\.extraversion)({ $0 + score })
                case .agreeableness:
                    return result |> prop(\.agreeableness)({ $0 + score })
                case .conscientious:
                    return result |> prop(\.conscientious)({ $0 + score })
                case .emotionalStability:
                    return result |> prop(\.emotionalStability)({ $0 + score })
                case .intellect:
                    return result |> prop(\.intellect)({ $0 + score })
                }
            })
            return testResult
        }
        
        return PersonalityTestResult.empty
    }
    
    func didSelectAnswer(answer: PersonalityTestAnswer, question: PersonalityTestQuestion) {
        testResults.updateValue(answer, forKey: question)
        testCompletion.onNext(testResults.count)
        if observeTestIsComplete() {
            UserService.updatePersonalityTestResult(result: observeTestResults()).subscribe(onNext: { [weak self] result in
                
                if !result {
                    
                }
                
            }).disposed(by: bag)
        }
    }
    
    func observeCurrentAnswer(question: PersonalityTestQuestion) -> PersonalityTestAnswer? {
        testResults[question]
    }
    
    func observeQuestions() -> [PersonalityTestQuestion] {
        PersonalityTestQuestion.allCases
    }
    
    func observeTestIsComplete() -> Bool {
        testResults.count == 50
    }
    
    func observeCompletionRate() -> Observable<Int> {
        testCompletion.asObservable()
    }
}
