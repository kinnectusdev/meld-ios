

//
//  PersonalityTestController.swift
//  Onboarding
//
//  Created by blakerogers on 9/23/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import UIKit
import UtilitiesPackage

final class PersonalityTestView: UIViewController {
    private var presenter: PersonalityTestPresenter!
    private func setViews() {
        let view = UIView()
            |> whiteBackground
            |> framed(UIScreen.main.bounds)
            |> appendSubView(UILabel()
                            |> named("title")
                            |> alignToTop(50)
                            |> alignedToCenterX
                            |> titled("Personality Test")
                            |> textColored(appDarkGrey)
                            |> textFont(appFontFormal(20)))
            |> appendSubView(UILabel()
                            |> named("instructions")
                            |> styledTitle(blackAttributedString(PersonalityTestQuestion.instructions, 12))
                            |> textCenterAligned
                            |> alignToSibling("title", constraint: alignTopToSiblingBottom(20))
                            |> alignToLeft(20)
                            |> alignToRight(-20)
                            |> unlimitedLines)
            |> appendSubView(presenter.list
                            |> isInvisible
                            |> alignedToCenterX
                            |> alignToSibling("instructions", constraint: alignTopToSiblingBottom(20))
                            |> width_Height(appFrame.width, 400)
                            |> appendSubviews(presenter.questions))
            |> appendSubView(presenter.countLabel
                            |> textFont(appFont(12))
                            |> textCenterAligned
                            |> textColored(appBlack)
                            |> alignedToCenterX
                            |> alignToBottom(-20))
            |> appendSubView(presenter.beginTestView |> fill)
            >>> appendSubView(
                presenter.backButton
                |> named("back_button")
                >>> setImage(UIImage(named: "BackButton"))
                >>> alignToLeft(10)
                >>> alignToSibling("title", constraint: alignToSiblingCenterY())
            )
        self.view = view
    }
}
extension PersonalityTestView {
    convenience init(presenter: PersonalityTestPresenter) {
        self.init()
        self.presenter = presenter
        self.setViews()
        self.modalPresentationStyle = ( DeviceInfo.deviceType == .iPad ? .overCurrentContext : .automatic)
    }
}
