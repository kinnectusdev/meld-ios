//
//  PersonalityTestPresenter.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import UtilitiesPackage
import UIKit

final class PersonalityTestPresenter {
    private let interactor: PersonalityTestInteractorIdentity
    private let router: PersonalityTestRouter
    private let bag = DisposeBag()
    
    let (beginTestView, didSelectBeginTest) = personality_test_intro_cell()
    let list = UIScrollView()
    |> isPaging
    |> scrollSize(CGSize(width: appFrame.width * PersonalityTestQuestion.allCases.count.cgFloat(), height: 400))
    |> asScrollView
    var questions: [UIView]!
    let countLabel = UILabel()
    let backButton = UIButton()
    |> visibility( DeviceInfo.deviceType == .iPad ? 1.0 : 0.0 )
    |> asButton
    init(interactor: PersonalityTestInteractorIdentity) {
        self.interactor = interactor
        self.router = PersonalityTestRouter()
        self.questions = PersonalityTestQuestion.allCases.map { [weak self] question -> UIView in
            guard let this = self else { return UIView() }
            let index = PersonalityTestQuestion.allCases |> index(of: question)
            let offset = appFrame.width * index.cgFloat()
            return PersonalityTestItemRouter.presentation(question: question, answer: interactor.observeCurrentAnswer(question: question), manager: interactor, onDidSelectAnswer: this.onDidSelectAnswer)
            |> framed(CGRect(x: offset, y: 0, width: appFrame.width, height: 400))
        }
        bag.insert(
            didSelectBeginTest.subscribe(onNext: { [weak self] _ in
                UIView.animate(withDuration: 0.25) {
                    self?.beginTestView.alpha = 0.0
                    self?.list.alpha = 1.0
                }
            }),
            backButton.rx.tap.subscribe(onNext: { [weak self] _ in
                self?.router.dismiss()
            }),
            interactor.observeCompletionRate()
                .map { "\($0) ・ \(PersonalityTestQuestion.allCases.count) Answered"}
                .asDriver(onErrorJustReturn: .empty)
                .drive(countLabel.rx.text)
        )
    }
    
    private func onDidSelectAnswer(question: PersonalityTestQuestion) {
        guard interactor.observeTestIsComplete() else {
            switch question {
            case .question50:
                break
            default:
                let contentOffset = list.contentOffset.x + appFrame.width
                list.setContentOffset(CGPoint(x: contentOffset, y: 0), animated: true)
            }
            return
        }
        self.router.displayResults(results: interactor.observeTestResults())
    }
}
