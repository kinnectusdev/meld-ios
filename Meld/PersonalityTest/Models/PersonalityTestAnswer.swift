//
//  PersonalityTestAnswer.swift
//  Meld
//
//  Created by blakerogers on 12/5/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import Foundation

public enum PersonalityTestAnswer: Int  {
    case stronglyAgree = 1
    case agree
    case neutral
    case disagree
    case stronglyDisagree
    
    func value(scoringValue: ScoringValue) -> Int {
        switch self {
        case .stronglyAgree:
            return scoringValue == .positive ? 5 : 1
        case .agree:
            return scoringValue == .positive ? 4 : 2
        case .neutral:
            return scoringValue == .positive ? 3 : 3
        case .disagree:
            return scoringValue == .positive ? 2 : 4
        case .stronglyDisagree:
            return scoringValue == .positive ? 1 : 5
        }
    }
    
    public var text: String {
        switch self {
        case .stronglyAgree:
            return "Strongly Agree"
        case .agree:
            return "Agree"
        case .neutral:
            return "Neutral"
        case .disagree:
            return "Disagree"
        case .stronglyDisagree:
            return "Strongly Disagree"
        }
    }
}
