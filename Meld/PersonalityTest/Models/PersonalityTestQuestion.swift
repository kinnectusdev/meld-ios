//
//  PersonalityTestData.swift
//  Onboarding
//
//  Created by blakerogers on 9/23/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.

// Questionaire link
//https://ipip.ori.org/new_ipip-50-item-scale.htm
// Scoring Procedure link
//https://ipip.ori.org/newScoringInstructions.htm
import Foundation
import UtilitiesPackage

public enum ScoringValue {
    case negative
    case positive
}
public enum PersonalityTestQuestion: Int, CaseIterable {
    case question1 = 1
    case question2
    case question3
    case question4
    case question5
    case question6
    case question7
    case question8
    case question9
    case question10
    case question11
    case question12
    case question13
    case question14
    case question15
    case question16
    case question17
    case question18
    case question19
    case question20
    case question21
    case question22
    case question23
    case question24
    case question25
    case question26
    case question27
    case question28
    case question29
    case question30
    case question31
    case question32
    case question33
    case question34
    case question35
    case question36
    case question37
    case question38
    case question39
    case question40
    case question41
    case question42
    case question43
    case question44
    case question45
    case question46
    case question47
    case question48
    case question49
    case question50
        
    public var next: PersonalityTestQuestion {
        guard let question = PersonalityTestQuestion.allCases.itemAt(self.rawValue + 1) else { return self }
        return question
    }
    
    public var question: String {
        switch self {
        case .question1:
        return "I am the life of the party."
        case .question2:
            return "I feel little concern for others."
        case .question3:
            return "I am always prepared."
        case .question4:
            return "I get stressed out easily."
        case .question5:
            return "I have a rich vocabulary."
        case .question6:
            return "I don't talk a lot."
        case .question7:
            return " I am interested in people."
        case .question8:
            return " I leave my belongings around."
        case .question9:
            return " I am relaxed most of the time."
        case .question10:
            return "I have difficulty understanding abstract ideas."
        case .question11:
            return "I feel comfortable around people."
        case .question12:
            return "I insult people."
        case .question13:
            return "I pay attention to details."
        case .question14:
            return "I worry about things."
        case .question15:
            return "I have a vivid imagination."
        case .question16:
            return "I keep in the background."
        case .question17:
            return "I sympathize with others' feelings."
        case .question18:
            return "I make a mess of things."
        case .question19:
            return "I seldom feel blue."
        case .question20:
            return "I am not interested in abstract ideas."
        case .question21:
            return "I start conversations."
        case .question22:
            return "I am not interested in other people's problems."
        case .question23:
            return "I get chores done right away."
        case .question24:
            return "I am easily disturbed."
        case .question25:
            return "I have excellent ideas."
        case .question26:
            return "I have little to say."
        case .question27:
            return "I have a soft heart."
        case .question28:
            return "I often forget to put things back in their proper place."
        case .question29:
            return "I get upset easily."
        case .question30:
            return "I do not have a good imagination."
        case .question31:
            return "I talk to a lot of different people at parties."
        case .question32:
            return "I am not really interested in others."
        case .question33:
            return "I like order."
        case .question34:
            return "I change my mood a lot."
        case .question35:
            return "I am quick to understand things."
        case .question36:
            return "I don't like to draw attention to myself."
        case .question37:
            return "I take time out for others."
        case .question38:
            return "I shirk my duties."
        case .question39:
            return "I have frequent mood swings."
        case .question40:
            return "I use difficult words."
        case .question41:
            return "I don't mind being the center of attention."
        case .question42:
            return "I feel others' emotions."
        case .question43:
            return "I follow a schedule."
        case .question44:
            return "I get irritated easily."
        case .question45:
            return "I spend time reflecting on things."
        case .question46:
            return "I am quiet around strangers."
        case .question47:
            return  "I make people feel at ease."
        case .question48:
            return "I am exacting in my work."
        case .question49:
            return "I often feel blue."
        case .question50:
            return  "I am full of ideas."
        }
    }
    var scoringValue: ScoringValue {
        switch self {
        case .question1, .question3, .question5, .question7, .question9, .question11, .question13, .question15, .question17, .question19, .question21
             ,.question23, .question25, .question27, .question31, .question33, .question35, .question37, .question40, .question41, .question42,
             .question43, .question45, .question47, .question48, .question50:
            return .positive
        default:
            return .negative
        }
    }
    var relevantFactor: PersonalityFiveFactors {
        switch self {
        case .question1, .question6,
                .question11,
                .question16,
                .question21,
                .question26,
                .question31,
                .question36,
                .question41,
                .question46: return .extraversion
        case .question2,
                .question7,
                .question12,
                .question17,
                .question22,
                .question27,
                .question32,
                .question37,
                .question42,
                .question47: return .agreeableness
        case .question3,
                .question8,
                .question13,
                .question18,
                .question23,
                .question28,
                .question33,
                .question38,
                .question43,
                .question48: return .conscientious
        case .question4,
                .question9,
                .question14,
                .question19,
                .question24,
                .question29,
                .question34,
                .question39,
                .question44,
                .question49: return .emotionalStability
        case .question5,
                .question10,
                .question15,
                .question20,
                .question25,
                .question30,
                .question35,
                .question40,
                .question45,
                .question50: return .intellect
        }
    }
    static let instructions: String = "Describe yourself as you generally are now, not as you wish to be in the future. Describe yourself as you honestly see yourself, in relation to other people you know of the same sex as you are, and roughly your same age. So that you can describe yourself in an honest manner, your responses will be kept in absolute confidence."
}


extension PersonalityTestQuestion {
    static var questions: [String] = PersonalityTestQuestion.allCases.map { $0.question }

    public func score(answer: PersonalityTestAnswer) -> (PersonalityFiveFactors, Int) {
        (self.relevantFactor, answer.value(scoringValue: self.scoringValue))
    }
}
