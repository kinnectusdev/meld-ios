//
//  PersonalityTestResult.swift
//  Meld
//
//  Created by Blake Rogers on 8/18/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation


struct PersonalityTestResult: Codable {
    var extraversion: Int
    var agreeableness: Int
    var conscientious: Int
    var emotionalStability: Int
    var intellect: Int
    
    func personalityType() -> PersonalityType {
        let trait1: PersonalityTrait = extraversion > 25 ? .extravert : .introvert
        let trait2: PersonalityTrait = agreeableness > 25 ? .feeling : .thinking
        let trait3: PersonalityTrait = conscientious > 25 ? .judging : .perceiving
        let trait4: PersonalityTrait = intellect > 25 ? .intuitive : .sensing
        
        return PersonalityType.typeForTraits(trait1: trait1, trait2: trait2, trait3: trait3, trait4: trait4)
        
    }
    
    func isNeurotic() -> Bool {
        emotionalStability < 5
    }
    static let empty: PersonalityTestResult = PersonalityTestResult(extraversion: 0, agreeableness: 0, conscientious: 0, emotionalStability: 0, intellect: 0)
}
