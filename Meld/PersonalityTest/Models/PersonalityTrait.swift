//
//  PersonalityTrait.swift
//  Meld
//
//  Created by blakerogers on 12/5/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UIKit
import UtilitiesPackage

public enum PersonalityTrait: Int {
    case introvert = 0
    case extravert
    case intuitive
    case sensing
    case thinking
    case feeling
    case judging
    case perceiving
    
    var title: String {
        switch self {
        case .introvert: return "Introvert"
        case .extravert: return "Extravert"
        case .intuitive: return "Intuitive"
        case .sensing: return "Sensing"
        case .thinking: return "Thinking"
        case .feeling: return "Feeling"
        case .judging: return "Judging"
        case .perceiving: return "Perceiving"
        }
    }
    var color: UIColor {
        switch self {
        case .introvert: return appSeaBlue
        case .extravert: return appSeaBlue.withAlphaComponent(0.9)
            
        case .intuitive: return appGold
        case .sensing: return appGold.withAlphaComponent(0.9)
            
        case .thinking: return appRoyalPurple
        case .feeling: return appRoyalPurple.withAlphaComponent(0.9)
            
        case .judging: return appForestGreen
        case .perceiving: return appForestGreen.withAlphaComponent(0.9)
        }
    }
}

public enum PersonalityFiveFactors {
    case extraversion
    case agreeableness
    case conscientious
    case emotionalStability
    case intellect
}
