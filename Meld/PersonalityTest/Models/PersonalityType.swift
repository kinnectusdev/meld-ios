//
//  PersonalityType.swift
//  Meld
//
//  Created by blakerogers on 12/5/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UtilitiesPackage

protocol PersonalityProCon {
    var title: String { get }
    var description: String { get }
}
public struct PersonalityStrength: PersonalityProCon {
    var title: String
    var description: String
}
public struct PersonalityWeakness: PersonalityProCon {
    var title: String
    var description: String
}

public enum PersonalityType: String, CaseIterable, Codable, ExpressibleByStringLiteral, CodingKey {
    public typealias StringLiteralType = String
    
    
    case intj
    case intp
    case infp
    case infj
    
    case isfj
    case isfp
    case istj
    case istp
    
    case entj
    case entp
    case enfj
    case enfp
    
    case estj
    case estp
    case esfj
    case esfp
    
    static func typeForTraits(trait1: PersonalityTrait, trait2: PersonalityTrait, trait3: PersonalityTrait, trait4: PersonalityTrait) -> PersonalityType {
        let types = PersonalityType.allCases
            .filter { $0.traits.contains(trait1) }
            .filter { $0.traits.contains(trait2) }
            .filter { $0.traits.contains(trait3) }
            .filter { $0.traits.contains(trait4) }
        guard let type = types.first else { assertionFailure("type not found"); return .intj }
        return type
            
    }
    public init(stringLiteral value: String) {
        self = PersonalityType.typeForTitle(value) ?? .intj
    }
    public var title: String {
        return "\(rawValue)".map { $0.uppercased()}.reduce ( "", { $0 + $1 })
    }
    public static func typeForTitle(_ title: String) -> PersonalityType? {
        return PersonalityType.allCases.filter { $0.title == title}.first
    }
    public var description: String {
        switch self {
        case .intj: return "Imaginative and strategic thinkers, with a plan for everything"
        case .intp: return "Innovative inventors with an unquenchable thirst for knowledge."
        case .infp: return "Poetic, kind and altruistic people, always eager to help a good cause."
        case .infj: return "Quiet and mystical, yet very inspiring and tireless idealists."
        case .isfj: return "Very dedicated and warm protectors, always ready to defend their loved ones."
        case .isfp: return "Flexible and charming artists, always ready to explore and experience something new."
        case .istj: return "Practical and fact-minded individuals, whose reliability cannot be doubted."
        case .istp: return "Bold and practical experimenters, masters of all kinds of tools."
        case .entj: return "Bold, imaginative and strong-willed leaders, always finding a way – or making one."
        case .entp: return "Smart and curious thinkers who cannot resist an intellectual challenge."
        case .enfj: return "Charismatic and inspiring leaders, able to mesmerize their listeners."
        case .enfp: return "Enthusiastic, creative and sociable free spirits, who can always find a reason to smile."
        case .estj: return "Excellent administrators, unsurpassed at managing things – or people."
        case .estp: return "Smart, energetic and very perceptive people, who truly enjoy living on the edge."
        case .esfj: return "Extraordinarily caring, social and popular people, always eager to help."
        case .esfp: return "Spontaneous, energetic and enthusiastic people – life is never boring around them."
        }
    }
    public static var sortedList: [PersonalityType] {
       return [intj, esfp,
        infp,entj,
        entp,istp,
        enfj, istj,
        enfp, isfp,
        estj, isfj,
        estp, infj,
        esfj, intp]
    }
    public var background: String {
        return self.title
    }
    
    public var strengths: [PersonalityStrength] {
        switch self {
        case .intj:
            return [
            PersonalityStrength(title: "Quick, Imaginative, and Strategic Mind", description: "Architects pride themselves on their minds, and they take every chance they can to improve their knowledge. This shows in the strength and flexibility of their strategic thinking. Highly curious and always up for an intellectual challenge, Architects see things from many different angles. Architect personalities use their creativity to plan for unforeseen possibilities."),
            PersonalityStrength(title: "High Self-Confidence", description: "Architects trust their rationalism above all else, so when they decide something, they have no reason to doubt their decisions or insights. This creates an honest, direct style of communication that isn’t held back by what others expect of them. When Architects are right, they’re right, and nothing will change that fact. Whether it’s a person, a process, or themselves whose course needs to be corrected, they’ll hold their ground and have it no other way."),
            PersonalityStrength(title: "Independent and Decisive", description: "This creativity, logic, and confidence form individuals who stand on their own and take responsibility for their actions. Authority figures don’t impress Architects, and neither do social conventions. And no matter how popular something is, if they have a better idea, Architects stand against anyone they must to promote their plan. Either an idea is rational, or it’s wrong – and they’re happy to apply that standard to themselves."),
            PersonalityStrength(title: "Hard-Working and Determined", description: "If something grabs their interest, Architect personalities can be very dedicated to their work. They often put in long hours and intense effort. Architects are goal-oriented, and if tasks lead to something clear and relevant, they strive to accomplish those tasks."),
            PersonalityStrength(title: "Open-Minded", description: "Architects are open to new ideas as long as they’re argued well. People with this personality type are even (maybe especially) open to ideas that go against their own if the thinking is sound. They often apply their openness and independence when thinking about matters like alternative lifestyles. Mix this with their dislike for rules and traditions, and it’s easy to see how Architects might lean toward more tolerant social views."),
            PersonalityStrength(title: "Jacks-of-all-Trades", description: "Because of Architects’ open-mindedness, willpower, independence, confidence, and planning abilities, they are capable of doing anything they set their minds to. Skilled at hacking anything life throws their way, Architects are able to break down and learn from almost any system. They then use the ideas found there wherever they’re needed. Architects tend to have their pick of jobs, from IT system designers to political organizers.")
           ]
        case .intp:
            return [
            PersonalityStrength(title: "Great Analysts and Abstract Thinkers", description: "People with the Logician personality type view the world as a big, complex machine, and recognize that as with any machine, all parts are interrelated. Logicians excel in analyzing these connections, seeing how seemingly unrelated factors tie in with each other in ways that bewilder most other personality types."),
            PersonalityStrength(title: "Imaginative and Original", description: "These connections are the product of an unrelenting imagination – Logicians’ ideas may seem counter-intuitive at a glance, and may never even see the light of day, but they will always prove remarkable innovations."),
            PersonalityStrength(title: "Open-Minded", description: "Logicians couldn’t make these connections if they thought they knew it all – they are highly receptive to alternate theories, so long as they’re supported by logic and facts. In more subjective matters like social norms and traditions, Logicians are usually fairly liberal, with a “none of my business” sort of attitude – peoples’ ideas are what matter."),
            PersonalityStrength(title: "Enthusiastic", description: "When a new idea piques their interest, Logicians can be very enthusiastic – they are a reserved personality type, but if another person shares an interest, they can be downright excited about discussing it. More likely though, the only outward evidence of this enthusiasm will be Logicians’ silent pacing or their staring into the distance."),
            PersonalityStrength(title: "Objective", description: "Logicians’ analysis, creativity and open-mindedness aren’t the tools of some quest for ideology or emotional validation. Rather, it’s as though people with the Logician personality type are a conduit for the truths around them, so far as they can be expressed, and they are proud of this role as theoretical mediator."),
            PersonalityStrength(title: "Honest and Straightforward", description: "To know one thing and say another would be terribly disingenuous – Logicians don’t often go around intentionally hurting feelings, but they believe that the truth is the most important factor, and they expect that to be appreciated and reciprocated.")
            ]
        case .infp:
            
            return [
            PersonalityStrength(title: "Idealistic", description: "Mediators’ friends and loved ones will come to admire and depend on them for their optimism. Their unshaken belief that all people are inherently good, perhaps simply misunderstood, lends itself to an incredibly resilient attitude in the face of hardship."),
                PersonalityStrength(title: "Seek and Value Harmony", description: "People with the Mediator personality type have no interest in having power over others, and don’t much care for domineering attitudes at all. They prefer a more democratic approach, and work hard to ensure that every voice and perspective is heard."),
                PersonalityStrength(title: "Open-Minded and Flexible", description: "A live-and-let-live attitude comes naturally to Mediators, and they dislike being constrained by rules. Mediators give the benefit of the doubt too, and so long as their principles and ideas are not being challenged, they’ll support others’ right to do what they think is right."),
                PersonalityStrength(title: "Very Creative", description: "Mediators combine their visionary nature with their open-mindedness to allow them to see things from unconventional perspectives. Being able to connect many far-flung dots into a single theme, it’s no wonder that many Mediators are celebrated poets and authors."),
                PersonalityStrength(title: "Passionate and Energetic", description: "When something captures Mediators’ imagination and speaks to their beliefs, they go all in, dedicating their time, energy, thoughts and emotions to the project. Their shyness keeps them from the podium, but they are the first to lend a helping hand where it’s needed."),
                PersonalityStrength(title: "Dedicated and Hard-Working", description: "While others focusing on the challenges of the moment may give up when the going gets tough, Mediators (especially Assertive ones) have the benefit of their far-reaching vision to help them through. Knowing that what they are doing is meaningful gives people with this personality type a sense of purpose and even courage when it comes to accomplishing something they believe in.")
            ]
        case .infj:
            return [
            PersonalityStrength(title: "Creative", description: "Combining a vivid imagination with a strong sense of compassion, Advocates use their creativity to resolve not technical challenges, but human ones. People with the Advocate personality type enjoy finding the perfect solution for someone they care about. This strength makes them excellent counselors and advisors."),
                PersonalityStrength(title: "Insightful", description: "Seeing through dishonesty and disingenuous motives, Advocates step past manipulation and sales tactics and into a more honest discussion. Advocate personalities see how people and events are connected. They are then able to use that insight to get to the heart of the matter."),
                PersonalityStrength(title: "Inspiring and Convincing", description: "Speaking in human terms, not technical, Advocates have a fluid, inspirational writing style that appeals to the inner idealist in their audience. Advocates can even be astonishingly good orators, speaking with warmth and passion. This is especially true if they are proud of what they are speaking for."),
                PersonalityStrength(title: "Decisive", description: "Advocates’ creativity, insight, and inspiration are able to have a real impact on the world. This is because they are able to follow through on their ideas with conviction, willpower, and the planning necessary to see complex projects through to the end. People with the Advocate personality type don’t just see the way things ought to be; they act on those insights."),
                PersonalityStrength(title: "Determined and Passionate", description: "When Advocates come to believe that something is important, they pursue that goal with a conviction and energy that can catch others off-guard. Advocates will rock the boat if they must. Not everyone likes to see this, but their passion for their chosen cause is an inseparable part of the Advocate personality."),
                PersonalityStrength(title: "Altruistic", description: "These strengths are used for good. Advocates will not engage in any actions or promote beliefs just to benefit themselves. They have strong beliefs and take the actions that they do because they are trying to advance an idea that they truly believe will make the world a better place.")]
        case .isfj:
            return [
                PersonalityStrength(title: "Supportive", description: "Defenders are the universal helpers, sharing their knowledge, experience, time and energy with anyone who needs it, and all the more so with friends and family. People with this personality type strive for win-win situations, choosing empathy over judgment whenever possible."),
                PersonalityStrength(title: "Reliable and Patient", description: "Rather than offering sporadic, excited rushes that leave things half finished, Defenders are meticulous and careful, taking a steady approach and bending with the needs of the situation just enough to accomplish their end goals. Defenders not only ensure that things are done to the highest standard, but often go well beyond what is required."),
                PersonalityStrength(title: "Imaginative and Observant", description: "Defenders are very imaginative, and use this quality as an accessory to empathy, observing others’ emotional states and seeing things from their perspective. With their feet firmly planted on the ground, it is a very practical imagination, though they do find things quite fascinating and inspiring."),
                PersonalityStrength(title: "Enthusiastic", description: "When the goal is right, Defenders take all this support, reliability and imagination and apply it to something they believe will make a difference in people’s lives – whether fighting poverty with a global initiative or simply making a customer’s day."),
                PersonalityStrength(title: "Loyal and Hard-Working", description: "Given a little time, this enthusiasm grows into loyalty – Defender personalities often form an emotional attachment to the ideas and organizations they’ve dedicated themselves to. Anything short of meeting their obligations with good, hard work fails their own expectations."),
                PersonalityStrength(title: "Good Practical Skills", description: "The best part is, Defenders have the practical sense to actually do something with all this altruism. If mundane, routine tasks are what need to be done, Defenders can see the beauty and harmony that they create, because they know that it helps them to care for their friends, family, and anyone else who needs it.")]
        case .isfp:
            
            return [
                PersonalityStrength(title: "Charming", description: "People with the Adventurer personality type are relaxed and warm, and their “live and let live” attitude naturally makes them likable and popular."),
                PersonalityStrength(title: "Sensitive to Others", description: "Adventurers easily relate to others’ emotions, helping them to establish harmony and good will, and minimize conflict."),
                PersonalityStrength(title: "Imaginative", description: "Being so aware of others’ emotions, Adventurer personalities use creativity and insight to craft bold ideas that speak to people’s hearts. While it’s hard to explain this quality on a resume, this vivid imagination and exploratory spirit help Adventurers in unexpected ways."),
                PersonalityStrength(title: "Passionate", description: "Beneath Adventurers’ quiet shyness beats an intensely feeling heart. When people with this personality type are caught up in something exciting and interesting, they can leave everything else in the dust."),
                PersonalityStrength(title: "Curious", description: "Ideas are well and good, but Adventurers need to see and explore for themselves whether their ideas ring true. Work revolving around the sciences may seem a poor match for their traits, but a boldly artistic and humanistic vision is often exactly what research needs to move forward – if Adventurers are given the freedom they need to do so."),
                PersonalityStrength(title: "Artistic", description: "Adventurers are able to show their creativity in tangible ways and with stunning beauty. Whether writing a song, painting an emotion, or presenting a statistic in a graph, Adventurers have a way of visualizing things that resonates with their audience.")]
        case .istj:
            return [
                PersonalityStrength(title: "Honest and Direct", description: "Integrity is the heart of the Logistician personality type. Emotional manipulation, mind games and reassuring lies all run counter to Logisticians’ preference for managing the reality of the situations they encounter with plain and simple honesty."),
                PersonalityStrength(title: "Strong-willed and Dutiful", description: "Logisticians embody that integrity in their actions too, working hard and staying focused on their goals. Patient and determined, people with the Logistician personality type meet their obligations, period."),
                PersonalityStrength(title: "Very Responsible", description: "Logisticians’ word is a promise, and a promise means everything. Logisticians would rather run themselves into the ground with extra days and lost sleep than fail to deliver the results they said they would. Loyalty is a strong sentiment for Logistician personalities, and they fulfill their duties to the people and organizations they’ve committed themselves to."),
                PersonalityStrength(title: "Calm and Practical", description: "None of their promises would mean much if Logisticians lost their tempers and broke down at every sign of hardship – they keep their feet on the ground and make clear, rational decisions. Peoples’ preferences are a factor to consider in this process, and Logisticians work to make the best use of individual qualities, but these decisions are made with effectiveness in mind more so than empathy. The same applies to criticisms, for others and themselves."),
                PersonalityStrength(title: "Create and Enforce Order", description: "The primary goal of any Logistician is to be effective in what they’ve chosen to do, and they believe that this is accomplished best when everyone involved knows exactly what is going on and why. Unclear guidelines and people who break established rules undermine this effort, and are rarely tolerated by Logisticians. Structure and rules foster dependability; chaos creates unforeseen setbacks and missed deadlines."),
                PersonalityStrength(title: "Jacks-of-all-trades", description: "Much like Analyst personality types, Logisticians are proud repositories of knowledge, though the emphasis is more on facts and statistics than concepts and underlying principles. This allows Logisticians to apply themselves to a variety of situations, picking up and applying new data and grasping the details of challenging situations as a matter of course.")]
        case .istp:
            return [
                PersonalityStrength(title: "Optimistic and Energetic", description: "Virtuosos are usually up to their elbows in some project or other. Cheerful and good-natured, people with the Virtuoso personality type (especially Assertive ones) rarely get stressed out, preferring to go with the flow."),
                PersonalityStrength(title: "Creative and Practical", description: "Virtuosos are very imaginative when it comes to practical things, mechanics, and crafts. Novel ideas come easily, and they love using their hands to put them into action."),
                PersonalityStrength(title: "Spontaneous and Rational", description: "Combining spontaneity with logic, Virtuosos can switch mindsets to fit new situations with little effort, making them flexible and versatile individuals."),
                PersonalityStrength(title: "Know How to Prioritize", description: "This flexibility comes with some unpredictability, but Virtuoso personalities are able to store their spontaneity for a rainy day, releasing their energy just when it’s needed most."),
                PersonalityStrength(title: "Great in a Crisis", description: "With all this hands-on creativity and spontaneity, it’s no wonder that Virtuosos are naturals in crisis situations. People with this personality type usually enjoy a little physical risk, and they aren’t afraid to get their hands dirty when the situation calls for it."),
                PersonalityStrength(title: "Relaxed", description: "Through all this, Virtuosos are able to stay quite relaxed. They live in the moment and go with the flow, refusing to worry too much about the future.")]
        case .entj:
           
            return [
                PersonalityStrength(title: "Efficient", description: "Commanders see inefficiency not just as a problem in its own right, but as something that pulls time and energy away from all their future goals, an elaborate sabotage consisting of irrationality and laziness. People with the Commander personality type will root out such behavior wherever they go."),
                PersonalityStrength(title: "Energetic", description: "Rather than finding this process taxing Commanders are energized by it, genuinely enjoying leading their teams forward as they implement their plans and goals."),
                PersonalityStrength(title: "Self-Confident", description: "Commanders couldn’t do this if they were plagued by self-doubt – they trust their abilities, make known their opinions, and believe in their capacities as leaders."),
                PersonalityStrength(title: "Strong-Willed", description: "Nor do they give up when the going gets tough – Commander personalities strive to achieve their goals, but really nothing is quite as satisfying to them as rising to the challenge of each obstacle in their run to the finish line."),
                PersonalityStrength(title: "Strategic Thinkers", description: "Commanders exemplify the difference between moment-to-moment crisis management and navigating the challenges and steps of a bigger plan, and are known for examining every angle of a problem and not just resolving momentary issues, but moving the whole project forward with their solutions."),
                PersonalityStrength(title: "Charismatic and Inspiring", description: "These qualities combine to create individuals who are able to inspire and invigorate others, who people actually want to be their leaders, and this in turn helps Commanders to accomplish their often ambitious goals that could never be finished alone.")]
        case .entp:
          
            return [
                PersonalityStrength(title: "Knowledgeable", description: "Debaters rarely pass up a good opportunity to learn something new, especially abstract concepts. This information isn’t usually absorbed for any planned purpose as with dedicated studying, people with the Debater personality type just find it fascinating."),
                PersonalityStrength(title: "Quick Thinkers", description: "Debaters have tremendously flexible minds, and are able to shift from idea to idea without effort, drawing on their accumulated knowledge to prove their points, or their opponents’, as they see fit."),
                PersonalityStrength(title: "Original", description: "Having little attachment to tradition, Debater personalities are able to discard existing systems and methods and pull together disparate ideas from their extensive knowledge base, with a little raw creativity to hold them together, to formulate bold new ideas. If presented with chronic, systemic problems and given rein to solve them, Debaters respond with unabashed glee."),
                PersonalityStrength(title: "Excellent Brainstormers", description: "Nothing is quite as enjoyable to Debaters as analyzing problems from every angle to find the best solutions. Combining their knowledge and originality to splay out every aspect of the subject at hand, rejecting without remorse options that don’t work and presenting ever more possibilities, Debaters are irreplaceable in brainstorming sessions."),
                PersonalityStrength(title: "Charismatic", description: "People with the Debater personality type have a way with words and wit that others find intriguing. Their confidence, quick thought and ability to connect disparate ideas in novel ways create a style of communication that is charming, even entertaining, and informative at the same time."),
                PersonalityStrength(title: "Energetic", description: "When given a chance to combine these traits to examine an interesting problem, Debaters can be truly impressive in their enthusiasm and energy, having no qualms with putting in long days and nights to find a solution.")]
        case .enfj:
          
            return [
                PersonalityStrength(title: "Tolerant", description: "Protagonists are true team players, and they recognize that that means listening to other peoples’ opinions, even when they contradict their own. They admit they don’t have all the answers, and are often receptive to dissent, so long as it remains constructive."),
                PersonalityStrength(title: "Reliable", description: "The one thing that galls Protagonists the most is the idea of letting down a person or cause they believe in. If it’s possible, Protagonists can always be counted on to see it through."),
                PersonalityStrength(title: "Charismatic", description: "Charm and popularity are qualities Protagonists have in spades. They instinctively know how to capture an audience, and pick up on mood and motivation in ways that allow them to communicate with reason, emotion, passion, restraint – whatever the situation calls for. Talented imitators, Protagonists are able to shift their tone and manner to reflect the needs of the audience, while still maintaining their own voice."),
                PersonalityStrength(title: "Altruistic", description: "Uniting these qualities is Protagonists’ unyielding desire to do good in and for their communities, be it in their own home or the global stage. Warm and selfless, Protagonists genuinely believe that if they can just bring people together, they can do a world of good."),
                PersonalityStrength(title: "Natural Leaders", description: "More than seeking authority themselves, Protagonists often end up in leadership roles at the request of others, cheered on by the many admirers of their strong personality and positive vision.")]
        case .enfp:
            return [
                PersonalityStrength(title: "Curious", description: "When it comes to new ideas, Campaigners aren’t interested in brooding – they want to go out and experience things, and don’t hesitate to step out of their comfort zones to do so. Campaigners are imaginative and open-minded, seeing all things as part of a big, mysterious puzzle called life."),
                PersonalityStrength(title: "Observant", description: "Campaigners believe that there are no irrelevant actions, that every shift in sentiment, every move and every idea is part of something bigger. To satisfy their curiosity, Campaigners try to notice all of these things, and to never miss a moment."),
                PersonalityStrength(title: "Energetic and Enthusiastic", description: "As they observe, forming new connections and ideas, Campaigners won’t hold their tongues – they’re excited about their findings, and share them with anyone who’ll listen. This infectious enthusiasm has the dual benefit of giving Campaigners a chance to make more social connections, and of giving them a new source of information and experience, as they fit their new friends’ opinions into their existing ideas."),
                PersonalityStrength(title: "Excellent Communicators", description: "It’s a good thing that Campaigners have such strong people skills, or they’d never express these ideas. Campaigners enjoy both small talk and deep, meaningful conversations, which are just two sides of the same coin for them, and are adept at steering conversations towards their desired subjects in ways that feel completely natural and unforced."),
                PersonalityStrength(title: "Know How to Relax", description: "It’s not all “nature of the cosmos” discussions with Campaigners – people with this personality type know that sometimes, nothing is as important as simply having fun and experiencing life’s joys. That Intuitive trait lets Campaigners know that it’s time to shake things up, and these wild bursts of enthusiastic energy can surprise even their closest friends."),
                PersonalityStrength(title: "Very Popular and Friendly", description: "All this adaptability and spontaneity comes together to form a person who is approachable, interesting and exciting, with a cooperative and altruistic spirit and friendly, empathetic disposition. Campaigners get along with pretty much everyone, and their circles of friends stretch far and wide.")]
        case .estj:
           return [
                PersonalityStrength(title: "Dedicated", description: "Seeing things to completion borders on an ethical obligation for Executives. Tasks aren’t simply abandoned because they’ve become difficult or boring – people with the Executive personality type take them up when they are the right thing to do, and they will be finished so long as they remain the right thing to do."),
                PersonalityStrength(title: "Strong-willed", description: "A strong will makes this dedication possible, and Executives don’t give up their beliefs because of simple opposition. Executives defend their ideas and principles relentlessly, and must be proven clearly and conclusively wrong for their stance to budge."),
                PersonalityStrength(title: "Direct and Honest", description: "Executives trust facts far more than abstract ideas or opinions. Straightforward statements and information are king, and Executive personalities return the honesty (whether it’s wanted or not)."),
                PersonalityStrength(title: "Loyal, Patient and Reliable", description: "Executives work to exemplify truthfulness and reliability, considering stability and security very important. When Executives say they’ll do something, they keep their word, making them very responsible members of their families, companies and communities."),
                PersonalityStrength(title: "Enjoy Creating Order", description: "Chaos makes things unpredictable, and unpredictable things can’t be trusted when they are needed most – with this in mind, Executives strive to create order and security in their environments by establishing rules, structures and clear roles."),
                PersonalityStrength(title: "Excellent Organizers", description: "This commitment to truth and clear standards makes Executives capable and confident leaders. People with this personality type have no problem distributing tasks and responsibilities to others fairly and objectively, making them excellent administrators.")]
        case .estp:
            
            return [
                PersonalityStrength(title: "Bold", description: "People with the Entrepreneur personality type are full of life and energy. There is no greater joy for Entrepreneurs than pushing boundaries and discovering and using new things and ideas."),
                PersonalityStrength(title: "Rational and Practical", description: "Entrepreneurs love knowledge and philosophy, but not for their own sake. What’s fun for Entrepreneur personalities is finding ideas that are actionable and drilling into the details so they can put them to use. If a discussion is completely arbitrary, there are better uses for Entrepreneurs’ time."),
                PersonalityStrength(title: "Original", description: "Combining their boldness and practicality, Entrepreneurs love to experiment with new ideas and solutions. They put things together in ways no one else would think to."),
                PersonalityStrength(title: "Perceptive", description: "This originality is helped by Entrepreneurs’ ability to notice when things change – and when they need to change! Small shifts in habits and appearances stick out to Entrepreneurs, and they use these observations to help create connections with others."),
                PersonalityStrength(title: "Direct", description: "This perceptive skill isn’t used for mind games – Entrepreneurs prefer to communicate clearly, with direct and factual questions and answers. Things are what they are."),
                PersonalityStrength(title: "Sociable", description: "All these qualities pull together to make a natural group leader in Entrepreneurs. This isn’t something that they actively seek – people with this personality type just have a knack for making excellent use of social interactions and networking opportunities.")]
        case .esfj:
          
            return [
                PersonalityStrength(title: "Strong Practical Skills", description: "Consuls are excellent managers of day-to-day tasks and routine maintenance, enjoying making sure that those who are close to them are well cared for."),
                PersonalityStrength(title: "Strong Sense of Duty", description: "People with the Consul personality type have a strong sense of responsibility and strive to meet their obligations, though this may sometimes be more from a sense of social expectations than intrinsic drive."),
                PersonalityStrength(title: "Very Loyal", description: "Valuing stability and security very highly, Consuls are eager to preserve the status quo, which makes them extremely loyal and trustworthy partners and employees. Consuls are true pillars of any groups they belong to – whether it is their family or a community club, people with this personality type can always be relied upon."),
                PersonalityStrength(title: "Sensitive and Warm", description: "Helping to ensure that stability, Consul personalities seek harmony and care deeply about other people’s feelings, being careful not to offend or hurt anybody. Consuls are strong team players, and win-win situations are the stuff smiles are made of."),
                PersonalityStrength(title: "Good at Connecting with Others", description: "These qualities come together to make Consuls social, comfortable and well-liked. Consul personalities have a strong need to “belong”, and have no problem with small talk or following social cues in order to help them take an active role in their communities.")]
        case .esfp:
            return [
                PersonalityStrength(title: "Bold", description: "Entertainers aren’t known for holding back. Wanting to experience everything there is to experience, people with the Entertainer personality type don’t mind stepping out of their comfort zones when no one else is willing."),
                PersonalityStrength(title: "Original", description: "Traditions and expectations are secondary to Entertainers, if a consideration at all. Entertainer personalities love to experiment with new styles, and constantly find new ways to stick out in the crowd."),
                PersonalityStrength(title: "Aesthetics and Showmanship", description: "Not stopping at mere outfits, Entertainers inject artistic creativity into their words and actions, too. Every day is a performance, and Entertainers love to put on a show."),
                PersonalityStrength(title: "Practical", description: "To Entertainers, the world is meant to be felt and experienced. Truth is stranger than fiction, and Entertainers prefer to see and do than to wax philosophical about “what-ifs”."),
                PersonalityStrength(title: "Observant", description: "With all this focus on the here and now, on doing and acting, it makes sense that Entertainers are naturals when it comes to noticing real, tangible things and changes."),
                PersonalityStrength(title: "Excellent People Skills", description: "More so than things though, Entertainers love to pay attention to people. They are talkative, witty, and almost never run out of things to discuss. For people with this personality type, happiness and satisfaction stem from the time they spend with the people they enjoy being with.")]
        }
    }
    public var weakness: [PersonalityWeakness] {
        switch self {
        case .intj:
            return [
            PersonalityWeakness(title: "Arrogant", description: "Architect personalities can carry their confidence too far. They may falsely believe that they’ve fixed all the issues of a matter, then call it a day, rejecting the opposing opinions of those they believe to be intellectually inferior. With their disrespect for social standards, Architects can be insensitive when offering their opinions if they aren’t mindful of their attitude and less-developed social skills."),
            PersonalityWeakness(title: "Judgmental", description: "Architects have complete confidence in their thought processes because rational arguments are almost by definition correct – at least in theory. In practice, emotional considerations and history are hugely influential. A weak point for people with the Architect personality type is that they brand these factors and those who embrace them as illogical. They often dismiss them as people who think in an inferior way. This can make it all but impossible for others to be heard."),
            
            PersonalityWeakness(title: "Overly Analytical", description: "A repeated theme when discussing the strengths of Architects is their analytical skills. But this strength can fail them when logic isn’t the most important factor. Relationships, in all their complexities, often resist neat explanations. Architects may be highly critical and sometimes reach a high level of picky thoroughness when dealing with others. At that point, many people – with the exception of extremely loyal and understanding friends – are likely to flee, sometimes not to be seen again."),
            PersonalityWeakness(title: "Loathe Highly Structured Environments", description: "Architects hate blindly following...anything...without understanding why. They have a greater dislike for authority figures who go around forcing others to obey laws and rules without understanding the purpose of the standards themselves. Architects are likely not to get along with anyone who prefers how things “have always been.” However, by not going along with the standards – even when doing so might not be a big deal – Architects can make their lives harder than they need to be."),
            PersonalityWeakness(title: "Clueless in Romance", description: "This resentment toward rules and their tendency to overanalyze and judge, even to the point of arrogance, adds up to a personality type often clueless in dating. Having a new relationship last long enough for Architects to understand what is going on and how to behave is difficult. Trying harder in the ways that Architects are used to can only make things worse, and, unfortunately, it’s common for them to simply give up the search for love. Ironically, this more-relaxed state is when they’re at their best, and only then are they most likely to attract that special someone without effort.")
        ]
        case .intp:
            return [
            PersonalityWeakness(title: "Very Private and Withdrawn", description: "While Logicians’ intellectualism yields many insights into their surroundings, their surroundings are ironically considered an intrusion on their thoughts. This is especially true with people – Logicians are quite shy in social settings. More complicated situations such as parties exacerbate this, but even close friends struggle to get into Logicians’ hearts and minds."),
                PersonalityWeakness(title: "Insensitive", description: "Oftentimes Logician personalities get so caught up in their logic that they forget any kind of emotional consideration – they dismiss subjectivity as irrational and tradition as an attempt to bar much-needed progress. Purely emotional situations are often utterly puzzling to Logicians, and their lack of timely sympathy can easily offend."),
                PersonalityWeakness(title: "Absent-minded", description: "When Logicians’ interest is captured, their absence goes beyond social matters to include the rest of the physical world. Logicians become forgetful, missing even the obvious if it’s unrelated to their current infatuation, and they can even forget their own health, skipping meals and sleep as they muse."),
                PersonalityWeakness(title: "Condescending", description: "Attempts at connecting with others are often worse than Logicians’ withdrawal. People with the Logician personality type take pride in their knowledge and rationale, and enjoy sharing their ideas, but in trying to explain how they got from A to B to Z, they can get frustrated, sometimes simplifying things to the point of insult as they struggle to gauge their conversation partners’ perspective. The ultimate insult comes as Logicians give up with a dismissive `never mind`."),
                PersonalityWeakness(title: "Loathe Rules and Guidelines", description: "These social struggles are partly a product of Logicians’ desire to bypass the rules, of social conduct and otherwise. While this attitude helps Logicians’ strength of unconventional creativity, it also causes them to reinvent the wheel constantly and to shun security in favor of autonomy in ways that can compromise both."),
                PersonalityWeakness(title: "Second-Guess Themselves", description: "Logicians remain so open to new information that they often never commit to a decision at all. This applies to their own skills as well – Logician personalities know that as they practice, they improve, and any work they do is second-best to what they could do. Unable to settle for this, Logicians sometimes delay their output indefinitely with constant revisions, sometimes even quitting before they ever begin.")
                ]
        case .infp:
            return [
                PersonalityWeakness(title: "Too Idealistic", description: "Mediators often take their idealism too far, setting themselves up for disappointment as, again and again, evil things happen in the world. This is true on a personal level too, as Mediators may not just idealize their partners, but idolize them, forgetting that no one is perfect."),
                PersonalityWeakness(title: "Too Altruistic", description: "Mediators sometimes see themselves as selfish, but only because they want to give so much more than they are able to. This becomes a self-fulfilling prophecy, as they try to push themselves to commit to a chosen cause or person, forgetting to take care of the needs of others in their lives, and especially themselves."),
                PersonalityWeakness(title: "Impractical", description: "When something captures Mediators’ imagination, they can neglect practical matters like day-to-day maintenance and simple pleasures. Sometimes people with the Mediator personality type will take this asceticism so far as to neglect eating and drinking as they pursue their passion or cause."),
                PersonalityWeakness(title: "Dislike Dealing With Data", description: "Mediators are often so focused on the big picture that they forget the forest is made of individual trees. Mediators are in tune with emotions and morality, and when the facts and data contradict their ideals, it can be a real challenge for them."),
                PersonalityWeakness(title: "Take Things Personally", description: "Mediators often take challenges and criticisms personally, rather than as inspiration to reassess their positions. Avoiding conflict as much as possible, Mediators will put a great deal of time and energy into trying to align their principles and the criticisms into a middle ground that satisfies everybody."),
                PersonalityWeakness(title: "Difficult to Get to Know", description: "Mediators are private, reserved and self-conscious. This makes them notoriously difficult to really get to know, and their need for these qualities contributes to the guilt they often feel for not giving more of themselves to those they care about.")
            ]
        case .infj:
            return [
                PersonalityWeakness(title: "Sensitive", description: "When someone challenges or criticizes Advocates’ principles or values, they are likely to receive an alarmingly strong response. People with the Advocate personality type are highly vulnerable to criticism and conflict. Questioning their motives is the quickest way to their bad side."),
                PersonalityWeakness(title: "Extremely Private", description: "Advocates tend to present themselves as the culmination of an idea. This is partly because they believe in this idea, but also because Advocates are extremely private when it comes to their personal lives. They use this image to keep themselves from having to truly open up, even to close friends. Trusting a new friend can be even more challenging for Advocates."),
                PersonalityWeakness(title: "Perfectionistic", description: "Advocate personalities are all but defined by their pursuit of ideals. While this is a wonderful quality in many ways, an ideal situation is not always possible – in politics, in business, in romance. Advocates, especially Turbulent ones, too often drop or ignore healthy and productive situations and relationships, always believing there might be a better option down the road."),
                PersonalityWeakness(title: "Always Need to Have a Cause", description: "Advocate personalities get so caught up in their pursuits that any of the cumbersome tasks that come between them and their ideal vision is deeply unwelcome. Advocates like to know that they are taking concrete steps toward their goals. If routine tasks feel like they are getting in the way – or worse yet, there is no goal at all – they will feel restless and disappointed."),
                PersonalityWeakness(title: "Can Burn Out Easily", description: "Their passion, impatience for routine maintenance, idealism, and extreme privacy tend to leave Advocates with few options for letting off steam. People with this personality type are likely to exhaust themselves in short order if they don’t find a way to balance their ideals with the realities of day-to-day living.")
        ]
        case .isfj:
            return [
                PersonalityWeakness(title: "Humble and Shy", description: "The meek shall inherit the earth, but it’s a long road if they receive no recognition at all. This is possibly Defenders’ biggest challenge, as they are so concerned with others’ feelings that they refuse to make their thoughts known, or to take any duly earned credit for their contributions. Defenders’ standards for themselves are also so high that, knowing they could have done some minor aspect of a task better, they often downplay their successes entirely."),
                PersonalityWeakness(title: "Take Things Too Personally", description: "Defenders have trouble separating personal and impersonal situations – any situation is still an interaction between two people, after all – and any negativity from conflict or criticism can carry over from their professional to their personal lives, and back again."),
                PersonalityWeakness(title: "Repress Their Feelings", description: "People with the Defender personality type are private and very sensitive, internalizing their feelings a great deal. Much in the way that Defenders protect others’ feelings, they must protect their own, and this lack of healthy emotional expression can lead to a lot of stress and frustration."),
                PersonalityWeakness(title: "Overload Themselves", description: "Their strong senses of duty and perfectionism combine with this aversion to emotional conflict to create a situation where it is far too easy for Defenders to overload themselves – or to be overloaded by others – as they struggle silently to meet everyone’s expectations, especially their own."),
                PersonalityWeakness(title: "Reluctant to Change", description: "These challenges can be particularly hard to address since Defender personalities value traditions and history highly in their decisions. A situation sometimes needs to reach a breaking point before Defenders are persuaded by circumstance, or the strong personality of a loved one, to alter course."),
                PersonalityWeakness(title: "Too Altruistic", description: "This is all compounded and reinforced by Defenders’ otherwise wonderful quality of altruism. Being such warm, good-natured people, Defenders are willing to let things slide, to believe that things will get better soon, to not burden others by accepting their offers of help, while their troubles mount unassisted.")]
        case .isfp:
            return [
                PersonalityWeakness(title: "Fiercely Independent", description: "Freedom of expression is often Adventurers’ top priority. Anything that interferes with that, like traditions and hard rules, creates a sense of oppression for Adventurer personalities. This can make more rigidly structured academics and work a challenge."),
                PersonalityWeakness(title: "Unpredictable", description: "Adventurers’ dislike long-term commitments and plans. The tendency to actively avoid planning for the future can cause strain in Adventurers’ romantic relationships and financial hardship later in life."),
                PersonalityWeakness(title: "Easily Stressed", description: "Adventurers live in the present, full of emotion. When situations get out of control, people with this personality type (especially Turbulent ones) can shut down, losing their characteristic charm and creativity in favor of gnashing teeth."),
                PersonalityWeakness(title: "Overly Competitive", description: "Adventurers can escalate small things into intense competitions, turning down long-term success in their search for glory in the moment, and are unhappy when they lose."),
                PersonalityWeakness(title: "Fluctuating Self-Esteem", description: "It’s demanded that skills be quantified, but that’s hard to do with Adventurers’ strengths of sensitivity and artistry. Adventurers’ efforts are often dismissed, a hurtful and damaging blow, especially early in life. Adventurers can start to believe the naysayers without strong support.")
         ]
        case .istj:
            return [
                PersonalityWeakness(title: "Stubborn", description: "The facts are the facts, and Logisticians tend to resist any new idea that isn’t supported by them. This factual decision-making process also makes it difficult for people with the Logistician personality type to accept that they were wrong about something – but anyone can miss a detail, even them."),
                PersonalityWeakness(title: "Insensitive", description: "While not intentionally harsh, Logisticians often hurt more sensitive types’ feelings by the simple mantra that honesty is the best policy. Logistician personalities may take emotions into consideration, but really only so far as to determine the most effective way to say what needs to be said."),
                PersonalityWeakness(title: "Always by the Book", description: "Logisticians believe that things work best with clearly defined rules, but this makes them reluctant to bend those rules or try new things, even when the downside is minimal. Truly unstructured environments leave Logisticians all but paralyzed."),
                PersonalityWeakness(title: "Judgmental", description: "Opinions are opinions and facts are facts, and Logisticians are unlikely to respect people who disagree with those facts, or especially those who remain willfully ignorant of them."),
                PersonalityWeakness(title: "Often Unreasonably Blame Themselves", description: "All this can combine to make Logisticians believe they are the only ones who can see projects through reliably. As they load themselves with extra work and responsibilities, turning away good intentions and helpful ideas, Logisticians sooner or later hit a tipping point where they simply can’t deliver. Since they’ve heaped the responsibility on themselves, Logisticians then believe the responsibility for failure is theirs alone to bear.")
          ]
        case .istp:
            return [
                PersonalityWeakness(title: "Stubborn", description: "As easily as Virtuosos go with the flow, they can also ignore it entirely, and usually move in another direction with little apology or sensitivity. If someone tries to change Virtuosos’ habits, lifestyle or ideas through criticism, they can become quite blunt in their irritation."),
                PersonalityWeakness(title: "Insensitive", description: "Virtuosos use logic, and even when they try to meet others halfway with empathy and emotional sensitivity, it rarely seems to quite come out right, if anything is even said at all."),
                PersonalityWeakness(title: "Private and Reserved", description: "Virtuoso personalities are notoriously difficult to get to know. They are true introverts, keeping their personal matters to themselves, and often just prefer silence to small talk."),
                PersonalityWeakness(title: "Easily Bored", description: "Virtuosos enjoy novelty, which makes them excellent tinkerers, but much less reliable when it comes to focusing on things long-term. Once something is understood, Virtuosos tend to simply move on to something new and more interesting."),
                PersonalityWeakness(title: "Dislike Commitment", description: "Long-term commitments are particularly onerous for Virtuosos. They prefer to take things day-by-day, and the feeling of being locked into something for a long time is downright oppressive. This can be a particular challenge in Virtuosos’ romantic relationships."),
                PersonalityWeakness(title: "Risky Behavior", description: "This stubbornness, difficulty with others’ emotions, focus on the moment, and easy boredom can lead to unnecessary and unhelpful boundary-pushing, just for fun. Virtuosos have been known to escalate conflict and danger just to see where it goes, something that can have disastrous consequences for everyone around if they lose control of the situation.")
            ]
        
        case .entj:
            return [
                PersonalityWeakness(title: "Stubborn and Dominant", description: "Sometimes all this confidence and willpower can go too far, and Commanders are all too capable of digging in their heels, trying to win every single debate and pushing their vision, and theirs alone."),
                PersonalityWeakness(title: "Intolerant – “It’s my way or the highway”", description: "People with the Commander personality type are notoriously unsupportive of any idea that distracts from their primary goals, and even more so of ideas based on emotional considerations. Commanders won’t hesitate a second to make that fact clear to those around them."),
                PersonalityWeakness(title: "Impatient", description: "Some people need more time to think than others, an intolerable delay to quick-thinking Commanders. They may misinterpret contemplation as stupidity or disinterest in their haste, a terrible mistake for a leader to make."),
                PersonalityWeakness(title: "Arrogant", description: "Commander personalities respect quick thoughts and firm convictions, their own qualities, and look down on those who don’t match up. This relationship is a challenge for most other personality types who are perhaps not timid in their own right, but will seem so beside overbearing Commanders."),
                PersonalityWeakness(title: "Poor Handling of Emotions", description: "All this bluster, alongside the assumed supremacy of rationalism, makes Commanders distant from their own emotional expression and sometimes downright scornful of others’. People with this personality type often trample others’ feelings, inadvertently hurting their partners and friends, especially in emotionally charged situations."),
                PersonalityWeakness(title: "Cold and Ruthless", description: "Their obsession with efficiency and unwavering belief in the merits of rationalism, especially professionally, makes Commanders incredibly insensitive in pursuing their goals, dismissing personal circumstances, sensitivities, and preferences as irrational and irrelevant.")
        ]
        case .entp:
            return [
                PersonalityWeakness(title: "Very Argumentative", description: "If there’s anything Debaters enjoy, it’s the mental exercise of debating an idea, and nothing is sacred. More consensus-oriented personality types rarely appreciate the vigor with which Debater personalities tear down their beliefs and methods, leading to a great deal of tension."),
                PersonalityWeakness(title: "Insensitive", description: "Being so rational, Debaters often misjudge others feelings and push their debates well past others’ tolerance levels. People with this personality type don’t really consider emotional points to be valid in such debates either, which magnifies the issue tremendously."),
                PersonalityWeakness(title: "Intolerant", description: "Unless people are able to back up their ideas in a round of mental sparring, Debaters are likely to dismiss not just the ideas but the people themselves. Either a suggestion can stand up to rational scrutiny or it’s not worth bothering with."),
                PersonalityWeakness(title: "Can Find It Difficult to Focus", description: "The same flexibility that allows Debaters to come up with such original plans and ideas makes them readapt perfectly good ones far too often, or to even drop them entirely as the initial excitement wanes and newer thoughts come along. Boredom comes too easily for Debaters, and fresh thoughts are the solution, though not always a helpful one."),
                PersonalityWeakness(title: "Dislike Practical Matters", description: "Debaters are interested in what could be – malleable concepts like ideas and plans that can be adapted and debated. When it comes to hard details and day-to-day execution where creative flair isn’t just unnecessary but actually counter-productive, Debater personalities lose interest, often with the consequence of their plans never seeing the light of day.")
          ]
        case .enfj:
            return [
                PersonalityWeakness(title: "Overly Idealistic", description: "People with the Protagonist personality type can be caught off guard as they find that, through circumstance or nature, or simple misunderstanding, people fight against them and defy the principles they’ve adopted, however well-intentioned they may be. They are more likely to feel pity for this opposition than anger, and can earn a reputation of naïveté."),
                PersonalityWeakness(title: "Too Selfless", description: "Protagonists can bury themselves in their hopeful promises, feeling others’ problems as their own and striving hard to meet their word. If they aren’t careful, they can spread themselves too thin, and be left unable to help anyone."),
                PersonalityWeakness(title: "Too Sensitive", description: "While receptive to criticism, seeing it as a tool for leading a better team, it’s easy for Protagonists to take it a little too much to heart. Their sensitivity to others means that Protagonists sometimes feel problems that aren’t their own and try to fix things they can’t fix, worrying if they are doing enough."),
                PersonalityWeakness(title: "Fluctuating Self-Esteem", description: "Protagonists define their self-esteem by whether they are able to live up to their ideals, and sometimes ask for criticism more out of insecurity than out of confidence, always wondering what they could do better. If they fail to meet a goal or to help someone they said they’d help, their self-confidence will undoubtedly plummet."),
                PersonalityWeakness(title: "Struggle to Make Tough Decisions", description: "If caught between a rock and a hard place, Protagonists can be stricken with paralysis, imagining all the consequences of their actions, especially if those consequences are humanitarian.")
    ]
        case .enfp:
            return [
                PersonalityWeakness(title: "Poor Practical Skills", description: "When it comes to conceiving ideas and starting projects, especially involving other people, Campaigners have exceptional talent. Unfortunately their skill with upkeep, administration, and follow-through on those projects struggles. Without more hands-on people to help push day-to-day things along, Campaigners’ ideas are likely to remain just that – ideas."),
                PersonalityWeakness(title: "Find it Difficult to Focus", description: "Campaigners are natural explorers of interpersonal connections and philosophy, but this backfires when what needs to be done is that TPS report sitting right in front of them. It’s hard for Campaigners to maintain interest as tasks drift towards routine, administrative matters, and away from broader concepts."),
                PersonalityWeakness(title: "Overthink Things", description: "Campaigners don’t take things at face value – they look for underlying motives in even the simplest things. It’s not uncommon for Campaigners to lose a bit of sleep asking themselves why someone did what they did, what it might mean, and what to do about it."),
                PersonalityWeakness(title: "Get Stressed Easily", description: "All this overthinking isn’t just for their own benefit – Campaigners, especially Turbulent ones, are very sensitive, and care deeply about others’ feelings. A consequence of their popularity is that others often look to them for guidance and help, which takes time, and it’s easy to see why Campaigners sometimes get overwhelmed, especially when they can’t say yes to every request."),
                PersonalityWeakness(title: "Highly Emotional", description: "While emotional expression is healthy and natural, with Campaigners even viewing it as a core part of their identity, it can come out strongly enough to cause problems for this personality type. Particularly when under stress, criticism or conflict, Campaigners can experience emotional bursts that are counter-productive at best."),
                PersonalityWeakness(title: "Independent to a Fault", description: "Campaigners loathe being micromanaged and restrained by heavy-handed rules – they want to be seen as highly independent masters of their own fates, even possessors of an altruistic wisdom that goes beyond draconian law. The challenge for Campaigners is that they live in a world of checks and balances, a pill they are not happy to swallow.")
       ]
        
        case .estj:
            return [
                PersonalityWeakness(title: "Inflexible and Stubborn", description: "The problem with being so fixated on what works is that Executives too often dismiss what might work better. Everything is opinion until proven, and Executive personalities are reluctant to trust an opinion long enough for it to have that chance."),
                PersonalityWeakness(title: "Uncomfortable with Unconventional Situations", description: "Executives are strong adherents to tradition and when suddenly forced to try unvetted solutions, they become uncomfortable and stressed. New ideas suggest that their methods weren’t good enough, and abandoning what has always worked before in favor of something that may yet fail risks their image of reliability."),
                PersonalityWeakness(title: "Judgmental", description: "Executives have strong convictions about what is right, wrong, and socially acceptable. Executives’ compulsion to create order often extends to all things and everyone, ignoring the possibility that there are two right ways to get things done. Executives do not hesitate to let these “deviants” know what they think, considering it their duty to set things right."),
                PersonalityWeakness(title: "Too Focused on Social Status", description: "Executives take pride in the respect of their friends, colleagues and community and while difficult to admit, are very concerned with public opinion. Executives (especially Turbulent ones), can get so caught up in meeting others’ expectations that they fail to address their own needs."),
                PersonalityWeakness(title: "Difficult to Relax", description: "This need for respect fosters a need to maintain their dignity, which can make it difficult to cut loose and relax for risk of looking the fool, even in good fun."),
                PersonalityWeakness(title: "Difficulty Expressing Emotion", description: "This is all evidence of Executives’ greatest weakness: expressing emotions and feeling empathy. People with the Executive personality type get so caught up in the facts and most effective methods that they forget to think of what makes others happy, or of their sensitivity. A detour can be breathtakingly beautiful, a joy for the family, but Executives may only see the consequence of arriving at their destination an hour late, hurting their loved ones by rejecting the notion too harshly.")
           ]
        case .estp:
            return [
                PersonalityWeakness(title: "Insensitive", description: "Feelings and emotions come second to facts and “reality” for Entrepreneurs. Emotionally charged situations are awkward, uncomfortable affairs, and Entrepreneurs’ blunt honesty doesn’t help here. These personalities often have a lot of trouble acknowledging and expressing their own feelings as well."),
                PersonalityWeakness(title: "Impatient", description: "Entrepreneurs move at their own pace to keep themselves excited. Slowing down because someone else “doesn’t get it” or having to stay focused on a single detail for too long is extremely challenging for Entrepreneurs."),
                PersonalityWeakness(title: "Risk-prone", description: "This impatience can lead Entrepreneurs to push into uncharted territory without thinking of the long-term consequences. Entrepreneur personalities sometimes intentionally combat boredom with extra risk."),
                PersonalityWeakness(title: "Unstructured", description: "Entrepreneurs see an opportunity – to fix a problem, to advance, to have fun – and seize the moment, often ignoring rules and social expectations in the process. This may get things done, but it can create unexpected social fallout."),
                PersonalityWeakness(title: "May Miss the Bigger Picture", description: "Living in the moment can cause Entrepreneurs to miss the forest for the trees. People with this personality type love to solve problems here and now, perhaps too much. All parts of a project can be perfect, but the project will still fail if those parts do not fit together."),
                PersonalityWeakness(title: "Defiant", description: "Entrepreneurs won’t be boxed in. Repetition, hardline rules, sitting quietly while they are lectured at – this isn’t how Entrepreneurs live their lives. They are action-oriented and hands-on. Environments like school and much entry-level work can be so tedious that they’re intolerable, requiring extraordinary effort from Entrepreneurs to stay focused long enough to get to freer positions.")
       ]
        case .esfj:
            return [
                PersonalityWeakness(title: "Worried about Their Social Status", description: "These Strengths are related to a chief Weakness: Consuls’ preoccupation with social status and influence, which affects many decisions they make, potentially limiting their creativity and open-mindedness."),
                PersonalityWeakness(title: "Inflexible", description: "Consuls place a lot of importance on what is socially acceptable, and can be very cautious, even critical of anything unconventional or outside the mainstream. People with this personality type may also sometimes push their own beliefs too hard in an effort to establish them as mainstream."),
                PersonalityWeakness(title: "Reluctant to Innovate or Improvise", description: "Just as they can be critical of others’ “unusual” behavior, Consuls may also be unwilling to step out of their own comfort zones, usually for fear of being (or just appearing) different."),
                PersonalityWeakness(title: "Vulnerable to Criticism", description: "It can be especially challenging to change these tendencies because Consuls are so conflict-averse. Consul personalities can become very defensive and hurt if someone, especially a person close to them, criticizes their habits, beliefs or traditions."),
                PersonalityWeakness(title: "Often Too Needy", description: "Consuls need to hear and see a great deal of appreciation. If their efforts go unnoticed, people with the Consul personality type may start fishing for compliments, in an attempt to get reassurance of how much they are valued."),
                PersonalityWeakness(title: "Too Selfless", description: "The other side of this is that Consuls sometimes try to establish their value with doting attention, something that can quickly overwhelm those who don’t need it, making it ultimately unwelcome. Furthermore, Consuls often neglect their own needs in the process.")
        ]
        case .esfp:
            return [
                PersonalityWeakness(title: "Sensitive", description: "Entertainers (especially Turbulent ones) are strongly emotional, and very vulnerable to criticism – they can feel like they’ve been backed into a corner, sometimes reacting badly. This is probably Entertainers’ greatest weakness, because it makes it so hard to address any other weaknesses brought to light."),
                PersonalityWeakness(title: "Conflict-Averse", description: "Entertainers sometimes ignore and avoid conflict entirely. They tend to say and do what’s needed to get out of such situations, then move on to something more fun."),
                PersonalityWeakness(title: "Easily Bored", description: "Without constant excitement, Entertainers find ways to create it themselves. Risky behavior, self-indulgence, and the pleasures of the moment over long-term plans are all things Entertainers get into a little too often."),
                PersonalityWeakness(title: "Poor Long-Term Planners", description: "In fact, Entertainer personalities rarely make detailed plans for the future. To them, things come as they come, and they rarely bother with taking the time to lay out steps and consequences, with the belief that they could change at any moment – even with things that can be planned."),
                PersonalityWeakness(title: "Unfocused", description: "Anything that requires long-term dedication and focus is a particular challenge for Entertainers. In academics, dense, unchanging subjects like Classic Literature are much more difficult than more dynamic, relatable subjects like psychology. The trick for Entertainers is to find day-to-day joy in broader goals, and to tough it out with those tedious things that must be done.")
           ]
        }
    }
    public var traits: [PersonalityTrait]  {
        switch self {
        case .intj: return [.introvert, .intuitive, .thinking, .judging]
        case .intp: return [.introvert, .intuitive, .thinking, .perceiving]
        case .infp: return [.introvert, .intuitive, .feeling, .perceiving]
        case .infj: return [.introvert, .intuitive, .feeling, .judging]
            
        case .isfj: return [.introvert, .sensing, .feeling, .judging]
        case .isfp: return [.introvert, .sensing, .feeling, .perceiving]
        case .istj: return [.introvert, .sensing, .thinking, .judging]
        case .istp: return [.introvert, .sensing, .thinking, .perceiving]
            
        case .entj: return [.extravert, .intuitive, .thinking, .judging]
        case .entp: return [.extravert, .intuitive, .thinking, .perceiving]
        case .enfj: return [.extravert, .intuitive, .feeling, .judging]
        case .enfp: return [.extravert, .intuitive, .feeling, .perceiving]
            
        case .estj: return [.extravert, .sensing, .thinking, .judging]
        case .estp: return [.extravert, .sensing, .thinking, .perceiving]
        case .esfj: return [.extravert, .sensing, .feeling, .judging]
        case .esfp: return [.extravert, .sensing, .feeling, .perceiving]
        }
    }
    public var complimentaryTypes: [PersonalityType] {
        switch self {
        case .intj: return [.entp, .enfp]
        case .intp: return [.entj, .enfj]
        case .infp: return [.enfj, .entj]
        case .infj: return [.entp, .enfp, .intj, .infj]
        
        case .isfj: return [.esfp, .estp]
        case .isfp: return [.esfj, .estj]
        case .istj: return [.estp, .esfp]
        case .istp: return [.esfj, .estj]
        
        case .entj: return [.intp, .infp]
        case .entp: return [.esfj, .enfj, .isfj, .intj, .infj]
        case .enfj: return [.infp, .intp]
        case .enfp: return [.infj, .intj]
        
        case .estj: return [.istp, .isfp]
        case .estp: return [.istj, .isfj]
        case .esfj: return [.isfp, .istp]
        case .esfp: return [.istj, .isfj]
        }
    }
    public var personalityGroup: PersonalityGroup {
        switch self {
        case .enfj, .enfp, .infj, .infp: return .diplomat
        case .esfp, .estp, .isfp, .istp: return .explorer
        case .entj, .entp, .intj, .intp: return .logician
        case .esfj, .estj, .isfj, .istj: return .sentinel
        }

    }
}
import UIKit
public enum PersonalityGroup {
    case diplomat
    case explorer
    case logician
    case sentinel
    public var title: String {
        switch self {
        case .diplomat: return "Diplomat"
        case .explorer: return "Explorer"
        case .logician: return "Logician"
        case .sentinel: return "Sentinel"
        }
    }
    public var color: UIColor {
        switch self {
        case .diplomat: return appSeaBlue
        case .explorer: return appRoyalPurple
        case .logician: return appGold
        case .sentinel: return appForestGreen
        }
    }
}

