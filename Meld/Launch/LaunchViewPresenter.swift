//
//  LaunchViewPresenter.swift
//  Meld
//
//  Created by Blake Rogers on 7/4/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift

final class LaunchViewPresenter {
    private let bag = DisposeBag()
    private let router: LaunchViewRouter
    private let interactor: LaunchViewInteractor
    init(interactor: LaunchViewInteractor) {
        self.router = LaunchViewRouter()
        self.interactor = interactor
        interactor.determineRouteForInitialLaunch().subscribe(onNext: { [weak self] result in
            switch result {
            case .shouldSignIn:
                self?.router.displaySplash()
            case .signedIn:
                self?.router.displayHome()
            }
        }).disposed(by: bag)
    }
}
