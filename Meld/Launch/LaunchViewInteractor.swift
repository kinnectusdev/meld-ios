//
//  LaunchViewModel.swift
//  Meld
//
//  Created by Blake Osonduagwueki on 11/15/20.
//  Copyright © 2020 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
enum LaunchCoordination {
    case signedIn
    case shouldSignIn
}
protocol LaunchViewInteractorIdentity {
    func determineRouteForInitialLaunch() -> Observable<LaunchCoordination>
}

final class LaunchViewInteractor: LaunchViewInteractorIdentity {
    
    func  determineRouteForInitialLaunch() -> Observable<LaunchCoordination> {
        UserService.currentUser().take(1).delay(.seconds(3), scheduler: ConcurrentMainScheduler.instance).flatMap { userResult  -> Observable<LaunchCoordination> in
            switch userResult {
            case .success(let user):
                guard user.id.notEmpty() else {
                    return .just(.shouldSignIn)
                }
                return UserService.fetchUser(user.id)
                    .compactMap { $0.user }
                    .flatMap { UserService.setCurrentUser(user: $0)}
                    .map { $0 ? LaunchCoordination.signedIn : LaunchCoordination.shouldSignIn }
            case .noCurrentUser:
                return .just(.shouldSignIn)
            }
        }
    }
}
