//
//  LaunchViewController.swift
//  Meld
//
//  Created by Blake Osonduagwueki on 11/15/20.
//  Copyright © 2020 com.meld.kinnectus. All rights reserved.
//

import UIKit
import RxSwift
import FirebaseFirestore
import FirebaseFirestoreSwift
import UtilitiesPackage

final class LaunchView: UIViewController {
    private var presenter: LaunchViewPresenter?
    
    convenience init(presenter: LaunchViewPresenter) {
        self.init()
        self.presenter = presenter
        self.setView()
        
    }
    private func setView() {
        let view = UIView()
        |> whiteBackground
        |> appendSubView(
            UIImageView(image: UIImage(named: "launchBackground"))
            |> scaledToFill
            |> fill
        )
        |> appendSubView(UIView() |>  purpleRoseBackground |> fill)
        |> appendSubView(
            UIImageView(image: UIImage(named: "logoDark"))
            |> named("logo")
            |> scaledToFit
            |> alignToTop(69)
            |> width_Height(240, 128)
            |> alignedToCenterX
        )
        |> appendSubView( UILabel()
            |> alignToSibling("logo", constraint: alignToSiblingBottom(10))
            |> alignedToCenterX
            |> styledTitle(silverAttributedString("", 18))
        )
        self.view = view
    }
}

