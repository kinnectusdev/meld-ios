//
//  LaunchViewRouter.swift
//  Meld
//
//  Created by Blake Rogers on 7/4/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import UIKit
import UtilitiesPackage

final class LaunchViewRouter {
    /// Home router presentation
    func displayHome() {
        currentWindow().rootViewController = HomeRouter.presentation()
        currentWindow().makeKeyAndVisible()
    }
    /// Sign up router presentation
    func displaySplash() {
        currentWindow().rootViewController = OnboardingRouter.presentation()
        currentWindow().makeKeyAndVisible()
    }
}
extension LaunchViewRouter {
    static func presentation() -> UIViewController {
        LaunchView(presenter: LaunchViewPresenter(interactor: LaunchViewInteractor()))
    }
}
