//
//  GlobalEventNotifier.swift
//
//
//  Created by Blake Rogers on 8/27/21.
//

import Foundation
import RxSwift

final class GlobalEventNotifier {
    enum Event {
        case didUpdateFilter
        case didRotateScreen
    }
    private let event = PublishSubject<Event>()
    
    func observeEvent() -> Observable<Event> {
        event.asObservable()
    }
    func emit(event: Event) {
        self.event.onNext(event)
    }
}
extension GlobalEventNotifier {
    static let shared: GlobalEventNotifier = AppCoordinator.shared.provideGlobalEventNotifier()

}
