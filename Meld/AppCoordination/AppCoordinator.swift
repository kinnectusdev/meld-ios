//
//  OnboardingCoordinator.swift
//  Meld
//
//  Created by blakerogers on 11/10/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import RxSwift
import Firebase
import CoreLocation
import UtilitiesPackage

public enum AppCoordination {
    case dormant
    case displayOnboarding
    case displayHome
    case logout
}
public let app_coordinator_bag = DisposeBag()
/// Read Current User Services
final class AppCoordinator {
    private var window: UIWindow
    private let pricePlanService = PricePlanService()
    private let pushNotificationService = PushNotificationService()
    private let pushKitService = PushKitService()
    private let callManager = CallManager()
    private var providerDelegate: ProviderDelegate!
    private let globalEventNotifier = GlobalEventNotifier()
    init(window: UIWindow) {
        self.window = window
        self.configure()
        self.initiateApp()
    }
    private func configure() {
        FirebaseApp.configure()
    }
    private func initiateApp() {
        providerDelegate = ProviderDelegate(callManager: callManager)
        window.rootViewController = LaunchViewRouter.presentation()
        window.makeKeyAndVisible()
    }
    func provideGlobalEventNotifier() -> GlobalEventNotifier {
        globalEventNotifier
    }
    func providePricePlanService() -> PricePlanService {
        pricePlanService
    }
}
extension AppCoordinator {
    static let shared: AppCoordinator = AppDelegate.shared.provideCoordinator()
    
}
extension AppCoordinator {
    func displayIncomingCall(
    uuid: UUID,
    payload: [AnyHashable: Any],
    hasVideo: Bool = false,
    completion: ((Error?) -> Void)?
    ) {
        providerDelegate.reportIncomingCall(uuid: uuid, payload: payload, hasVideo: hasVideo, completion: completion)
    }
    func startVideoCall(_ call: Call) {
        let controller = videoChatController(conversation: Conversation.new |> prop(\.id)({_ in call.convoID}))
        currentWindow().rootViewController?.present(controller, animated: true, completion: nil)
    }
}
