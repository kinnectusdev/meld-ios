//
//  PersonalityTestItemRouter.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UIKit

final class PersonalityTestItemRouter {
    
}
extension PersonalityTestItemRouter {
    static func presentation(question: PersonalityTestQuestion, answer: PersonalityTestAnswer?, manager: PersonalityTestItemManager, onDidSelectAnswer: @escaping (PersonalityTestQuestion) -> Void) -> UIView {
        PersonalityTestItem(presenter: PersonalityTestItemPresenter(interactor: PersonalityTestItemInteractor(testItemManager: manager, question: question, answer: answer), onDidSelectAnswer: onDidSelectAnswer))
    }
}
