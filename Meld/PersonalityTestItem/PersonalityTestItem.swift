//
//  PersonalityTestItem.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UIKit
import UtilitiesPackage

class PersonalityTestItem: UIView {
    private var presenter: PersonalityTestItemPresenter!
}

extension PersonalityTestItem {
        
    private func setViews() {
        backgroundColor = .clear
        let background = UIView()
                        |> whiteBackground
                        >>> utilizeConstraints
                        >>> roundedGreyShadowed(20)
        let underline = UIView() |> backgroundColored(appSilver) |> utilizeConstraints
        
        add(views: background)
        
        background.constrainInView(view: self, top: 20, left: 20, right: -20, bottom: -30)
        
        background.add(views:   presenter.abstractImage,
                       presenter.questionLabel,
                       underline,
                       presenter.stronglyAgreeButton,
                       presenter.agreeButton,
                       presenter.neutralButton,
                       presenter.disagreeButton,
                       presenter.stronglyDisagreeButton)
        presenter.abstractImage.constrainCenterToCenter(of: background)
        presenter.questionLabel.constrainCenterXTo(view: background, constant: 0)
        presenter.questionLabel.constrainTopToTop(of: background, constant: 50)
        
        background.alignAllViews(position: .centerX, offset: 0, views:  presenter.stronglyAgreeButton,
                                 presenter.agreeButton,
                                 presenter.neutralButton,
                                 presenter.disagreeButton,
                                 presenter.stronglyDisagreeButton)
        underline.constrainWidth_Height(width: 250, height: 1)
        underline.constrainCenterXTo(view: background, constant: 0)
        underline.constrainTopToBottom(of: presenter.questionLabel, constant: 4)
        presenter.stronglyAgreeButton.constrainTopToBottom(of: presenter.questionLabel, constant: 24)
        presenter.agreeButton.constrainTopToBottom(of: presenter.stronglyAgreeButton, constant: 8)
        presenter.neutralButton.constrainTopToBottom(of: presenter.agreeButton, constant: 8)
        presenter.disagreeButton.constrainTopToBottom(of: presenter.neutralButton, constant: 8)
        presenter.stronglyDisagreeButton.constrainTopToBottom(of: presenter.disagreeButton, constant: 8)
    }
    
    convenience init(presenter: PersonalityTestItemPresenter) {
        self.init()
        self.presenter = presenter
        self.setViews()
    }
}



