//
//  PersonalityTestItemInteractor.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol PersonalityTestItemInteractorIdentity {
    func observeQuestionText() -> Observable<String>
    func observeQuestion() -> PersonalityTestQuestion
    func observeCurrentAnswer() -> Observable<PersonalityTestAnswer>
    func didSelectAnswer(answer: PersonalityTestAnswer)
}

protocol PersonalityTestItemManager {
    func didSelectAnswer(answer: PersonalityTestAnswer, question: PersonalityTestQuestion)
}

final class PersonalityTestItemInteractor {
    
    private let testItemManager: PersonalityTestItemManager
    private let question: PersonalityTestQuestion
    private let currentAnswer: BehaviorRelay<PersonalityTestAnswer?> = .init(value: nil)
    
    init(testItemManager: PersonalityTestItemManager, question: PersonalityTestQuestion, answer: PersonalityTestAnswer?) {
        self.testItemManager = testItemManager
        self.question = question
        self.currentAnswer.accept(answer)
    }
}

extension PersonalityTestItemInteractor: PersonalityTestItemInteractorIdentity {
    func observeCurrentAnswer() -> Observable<PersonalityTestAnswer> {
        currentAnswer.compactMap { $0 }
    }
    func observeQuestion() -> PersonalityTestQuestion {
        question
    }
    func observeQuestionText() -> Observable<String> {
        Observable.just(question.question)
    }
    func didSelectAnswer(answer: PersonalityTestAnswer) {
        currentAnswer.accept(answer)
        testItemManager.didSelectAnswer(answer: answer, question: question)
    }
}
