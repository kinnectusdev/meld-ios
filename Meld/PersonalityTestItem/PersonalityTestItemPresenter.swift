//
//  PersonalityTestItemPresenter.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import UtilitiesPackage

final class PersonalityTestItemPresenter {
    
    private let interactor: PersonalityTestItemInteractorIdentity
    private let bag = DisposeBag()
    
    let questionLabel = UILabel()
            |> textFont(appFontFormal(18))
            >>> textColored(appBlack)
            >>> textCenterAligned
            >>> unlimitedLines
            >>> utilizeConstraints
            >>> alignToLeft(10)
            >>> alignToRight(-10)
            >>> asLabel
    let abstractImage = UIImageView()
                    |> scaledToFill
                    >>> visibility(0.1)
                    >>> rounded(20)
                    >>> utilizeConstraints
                    >>> fill
                    >>> maskedToBounds
                    >>> asImage
    let stronglyAgreeButton = UIButton()
            |> styledTitle(blackAttributedString("I Strongly Agree", 16))
            >>> utilizeConstraints
            >>> asButton
    let agreeButton = UIButton()
            |> styledTitle(blackAttributedString("I Agree", 16))
            >>> utilizeConstraints
            >>> asButton
    let neutralButton = UIButton()
            |> styledTitle(blackAttributedString("Neither Agree or Disagree", 16))
            >>> utilizeConstraints
            >>> asButton
    let disagreeButton = UIButton()
            |> styledTitle(blackAttributedString("I Disagree", 16))
            >>> utilizeConstraints
            >>> asButton
    let stronglyDisagreeButton = UIButton()
            |> styledTitle(blackAttributedString("I Strongly Disagree", 16))
            >>> utilizeConstraints
            >>> asButton
    
    init(interactor: PersonalityTestItemInteractorIdentity, onDidSelectAnswer: @escaping (PersonalityTestQuestion) -> Void) {
        self.interactor = interactor
        
        bag.insert(
            interactor.observeQuestionText().asDriver(onErrorJustReturn: .empty).drive(questionLabel.rx.text),
            stronglyAgreeButton.rx.tap.map { _ in PersonalityTestAnswer.stronglyAgree }.subscribe(onNext: { [weak self ] answer in
                self?.interactor.didSelectAnswer(answer: answer)
                onDidSelectAnswer(interactor.observeQuestion())
            }),
            agreeButton.rx.tap.map { _ in PersonalityTestAnswer.agree }.subscribe(onNext: { [weak self ] answer in
                self?.interactor.didSelectAnswer(answer: answer)
                onDidSelectAnswer(interactor.observeQuestion())
            }),
            neutralButton.rx.tap.map { _ in PersonalityTestAnswer.neutral }.subscribe(onNext: { [weak self ] answer in
                self?.interactor.didSelectAnswer(answer: answer)
                onDidSelectAnswer(interactor.observeQuestion())
            }),
            disagreeButton.rx.tap.map { _ in PersonalityTestAnswer.disagree }.subscribe(onNext: { [weak self ] answer in
                self?.interactor.didSelectAnswer(answer: answer)
                onDidSelectAnswer(interactor.observeQuestion())
            }),
            stronglyDisagreeButton.rx.tap.map { _ in PersonalityTestAnswer.stronglyDisagree }.subscribe(onNext: { [weak self ] answer in
                self?.interactor.didSelectAnswer(answer: answer)
                onDidSelectAnswer(interactor.observeQuestion())
            }),
            interactor.observeCurrentAnswer().map { $0 == .stronglyAgree }.subscribe(onNext: { [weak self] isSelected in
                guard let this = self else { return }
                if isSelected {
                    this.stronglyAgreeButton.setAttributedTitle(boldBlackAttributedString("I Strongly Agree", 16), for: .normal)
                } else {
                    this.stronglyAgreeButton.setAttributedTitle(blackAttributedString("I Strongly Agree", 16), for: .normal)
                }
            }),
            interactor.observeCurrentAnswer().map { $0 == .agree }.subscribe(onNext: { [weak self] isSelected in
                guard let this = self else { return }
                if isSelected {
                    this.agreeButton.setAttributedTitle(boldBlackAttributedString("I Agree", 16), for: .normal)
                } else {
                    this.agreeButton.setAttributedTitle(blackAttributedString("I Agree", 16), for: .normal)
                }
            }),
            interactor.observeCurrentAnswer().map { $0 == .neutral }.subscribe(onNext: { [weak self] isSelected in
                guard let this = self else { return }
                if isSelected {
                    this.neutralButton.setAttributedTitle(boldBlackAttributedString("Neither Agree or Disagree", 16), for: .normal)
                } else {
                    this.neutralButton.setAttributedTitle(blackAttributedString("Neither Agree or Disagree", 16), for: .normal)
                }
            }),
            interactor.observeCurrentAnswer().map { $0 == .disagree }.subscribe(onNext: { [weak self] isSelected in
                guard let this = self else { return }
                if isSelected {
                    this.disagreeButton.setAttributedTitle(boldBlackAttributedString("I Disagree", 16), for: .normal)
                } else {
                    this.disagreeButton.setAttributedTitle(blackAttributedString("I Disagree", 16), for: .normal)
                }
            }),
            interactor.observeCurrentAnswer().map { $0 == .stronglyDisagree }.subscribe(onNext: { [weak self] isSelected in
                guard let this = self else { return }
                if isSelected {
                    this.stronglyDisagreeButton.setAttributedTitle(boldBlackAttributedString("I Strongly Disagree", 16), for: .normal)
                } else {
                    this.stronglyDisagreeButton.setAttributedTitle(blackAttributedString("I Strongly Disagree", 16), for: .normal)
                }
            })
        )
    }
}
