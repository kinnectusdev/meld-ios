//
//  SignInController.swift
//  Meld
//
//  Created by blakerogers on 11/11/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import UtilitiesPackage

final class SignInView: UIViewController {
    private var presenter: SignInPresenter!
    
    private func setViews() {
        let view = UIView()
        |>  whiteBackground
        |> framed(UIScreen.main.bounds)
        >>> appendSubView(
            UILabel()
            |> named("title")
            |> titled("Sign In")
            |> textFont(appFontFormal(20))
            |> textColored(appDarkGrey)
            >>> alignedToCenterX
            >>> alignToTop(50)
        )
        >>> appendSubView(
            presenter.backButton
            |> named("back_button")
            >>> setImage(UIImage(named: "BackButton"))
            >>> alignToLeft(10)
            >>> alignToSibling("title", constraint: alignToSiblingCenterY())
        )
        >>> appendSubView(
            UIView()
            |> named("email")
            |> bordered(appBlack, 1)
            >>> rounded(30)
            >>> viewHeight(60)
            >>> alignedToCenterX
            >>> alignToLeft(16)
            >>> alignToRight(-16)
            >>> alignToTop(120)
            >>> appendSubView(
                presenter.emailField
                |> placeholder("Email Address")
                >>> width_Height(300, 50)
                >>> alignedToCenter
                >>> tintColor(appDarkGrey)
                >>> textColored(appBlack)
                >>> textFont(appFont(18))
            )
        )
        >>> appendSubView(UILabel()
                        |> styledTitle(blackAttributedString("Email Address", 10))
                        |> alignToSibling("email", constraint: alignBottomToSiblingTop(-4))
                        |> alignToSibling("email", constraint: alignToSiblingLeft(20)))
        >>> appendSubView(
            UIView()
            |> bordered(appBlack, 1)
            |> named("password")
            >>> rounded(30)
            >>> viewHeight(60)
            >>> alignedToCenterX
            >>> alignToLeft(16)
            >>> alignToRight(-16)
            >>> alignToSibling("email", constraint: alignTopToSiblingBottom(30)
        )
        >>> appendSubView(
            presenter.passwordField
            |> width_Height(300, 50)
            >>> placeholder("Password")
            >>> isSecure
            >>> tintColor(appDarkGrey)
            >>> alignedToCenter
            >>> textColored(appBlack)
            >>> textFont(appFont(18))
            )
        )
        >>> appendSubView(UILabel()
                        |> styledTitle(blackAttributedString("Password", 10))
                        |> alignToSibling("password", constraint: alignBottomToSiblingTop(-4))
                        |> alignToSibling("password", constraint: alignToSiblingLeft(20)))
        >>> appendSubView(
            presenter.signInButton
            |> named("sign_in_button")
            >>> width_Height(200, 60)
            >>> backgroundColored(appBlack)
            >>> styledTitle(silverAttributedString("Sign In", 18))
            >>> rounded(30)
            >>> alignedToCenterX
            >>> alignToSibling("password", constraint: alignTopToSiblingBottom(10))
        )
        
        self.view = view
    }
}
extension SignInView {
    convenience init(presenter: SignInPresenter) {
        self.init()
        self.presenter = presenter
        self.setViews()
        self.modalPresentationStyle = ( DeviceInfo.deviceType == .iPad ? .overCurrentContext : .automatic)
    }
}
