//
//  SignInPresenter.swift
//  Meld
//
//  Created by Blake Rogers on 7/15/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import UtilitiesPackage

final class SignInPresenter {
    
    private let interactor: SignInInteractorIdentity
    private let router: SignInRouter
    private let bag = DisposeBag()
    
    let backButton = UIButton()
    |> visibility( DeviceInfo.deviceType == .iPad ? 1.0 : 0.0 )
    |> asButton
    let emailField = UITextField()
    let passwordField = UITextField()
    let signInButton = UIButton()
    
    init(interactor: SignInInteractorIdentity) {
        self.interactor = interactor
        self.router = SignInRouter()
        
        bag.insert(
            backButton.rx.tap.subscribe(onNext: { [weak self] _ in
                self?.router.displaySplash()
            }),
            emailField.rx.value.compactMap { $0 }.subscribe(onNext: { email in
                interactor.didUpdateEmail(email: email)
            }),
            passwordField.rx.value.compactMap { $0 }.subscribe(onNext: { password in
                interactor.didUpdatePassword(password: password)
            }),
            signInButton.rx.controlEvent(.touchUpInside).subscribe(onNext: { _ in
                interactor.didSelectSignIn()
            }),
            interactor.observeSignInAlert().subscribe(onNext: { alert in
                currentWindow().addSubview(full_screen_modal(alert))
            }),
            interactor.didCompleteSignIn().subscribe(onNext: { [weak self] _ in
                self?.router.displayHome()
            })
        )
    }
}
