//
//  SignInRouter.swift
//  Meld
//
//  Created by Blake Rogers on 7/15/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import UIKit
import UtilitiesPackage

final class SignInRouter {
    func displaySplash() {
        currentController().dismiss(animated: true, completion: nil)
    }
    func displayHome() {
        let view = HomeRouter.presentation()
        currentWindow().rootViewController = view
        currentWindow().makeKeyAndVisible()
    }
}

extension SignInRouter {
    static func presentation() -> UIViewController {
        SignInView(presenter: SignInPresenter(interactor: SignInInteractor()))
    }
}
