//
//  SignInService.swift
//  Meld
//
//  Created by blakerogers on 11/15/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import AuthenticationServices
import FirebaseAuth
import CryptoKit
import CoreLocation

public enum SignInServiceResult {
    case success(id: String, name: String, email: String, idToken: String)
    case failure(String)
}
public enum SignInResult: Error {
    case success
    case failure(String)
}
class SignInService: NSObject {
    fileprivate let signInResult = PublishSubject<SignInServiceResult>()
    fileprivate var nonce: String?
    static func signIn(email: String, password: String) -> Observable<SignInResult> {
        return Observable<SignInResult>.create { observer in
            Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
                if let error = error {
                    observer.onNext(.failure(error.localizedDescription))
                } else if let _ = result {
                    observer.onNext(.success)
                }
            }
            return Disposables.create()
        }
    }
    
    static var didLinkFromEmail: Observable<Bool> {
        UserDefaults.standard.rx.observe(Bool.self, "didLinkFromEmail").compactMap { $0 }
    }
}
