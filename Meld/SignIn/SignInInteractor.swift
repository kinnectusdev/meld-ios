//
//  SignI ViewModel.swift
//  Meld
//
//  Created by blakerogers on 11/11/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import RxSwift
import RxCocoa
import UtilitiesPackage

struct SignInInfo {
    var email: String
    var password: String
    var isValid: Bool {
        return email.notEmpty() && password.notEmpty()
    }
    static let empty = SignInInfo(email: .empty, password: .empty)
}
protocol SignInInteractorIdentity {
    func didUpdateEmail(email: String)
    func didUpdatePassword(password: String)
    func didSelectSignIn()
    func didCompleteSignIn() -> Observable<Void>
    func observeSignInAlert() -> Observable<String>
    
}
final class SignInInteractor {
    
    //MARK: Stored Properties
    private let bag = DisposeBag()
    
    //MARK: Stored Values
    private var signInInfo: SignInInfo = SignInInfo(email: .empty, password: .empty)
    
    //MARK: Observed Properties
    private let signInAlertListener = PublishSubject<String>()
    private let signInCompleteListener = PublishSubject<Void>()
    
}

extension SignInInteractor: SignInInteractorIdentity {
    func didUpdateEmail(email: String) {
        self.signInInfo = (signInInfo |> prop(\.email)({_ in email}))
    }
    func didUpdatePassword(password: String) {
        self.signInInfo = (signInInfo |> prop(\.password)({_ in password }))
    }
    func didSelectSignIn() {
        Observable.just(signInInfo).flatMap { info -> Observable<SignInResult> in
            if info.isValid {
                return SignInService.signIn(email: info.email, password: info.password).flatMap { result -> Observable<SignInResult> in
                    switch result {
                    case .success:
                        return UserService.fetchUserByEmail(info.email).flatMap { result -> Observable<SignInResult> in
                            switch result {
                            case .success(let user):
                                return UserService.setCurrentUser(user: user)
                                    .map { _ in
                                        return SignInResult.success
                                    }
                            case .failure(let error):
                                return .just(.failure(error))
                            }
                        }
                    case .failure(let error):
                        return .just(.failure(error))
                    }
                }
            } else {
                return Observable.just(SignInResult.failure("Invalid Credentials"))
            }
        }.subscribe(onNext: { [weak self ] result in
            switch result {
            case .failure(let error):
                self?.signInAlertListener.onNext(error)
            case .success:
                self?.signInCompleteListener.onNext(())
            }
        }).disposed(by: bag)
    }
    func observeSignInAlert() -> Observable<String> {
        signInAlertListener.asObservable()
    }
    func didCompleteSignIn() -> Observable<Void> {
        signInCompleteListener.asObservable()
    }
}
