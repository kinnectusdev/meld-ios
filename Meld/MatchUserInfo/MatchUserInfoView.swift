//
//  MatchUserInfoView.swift
//  Meld
//
//  Created by blakerogers on 3/25/20.
//  Copyright © 2020 com.meld.kinnectus. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import UtilitiesPackage

final class MatchUserInfoView: UIView {
    private var presenter: MatchUserInfoPresenter!
    private func setViews() {
        let a = appendSubView(presenter.nameLabel
                |> named("name")
                >>> textColored(appBlack)
                >>> textFont(appFontFormal(30))
                >>> alignToLeft(20) >>> alignToTop(30))
            >>> appendSubView(presenter.nameShadow
                |> backgroundColored(appLightShadow)
                |> alignToLeft(17) >>> alignToTop(29)
                |> width_Height(100, 30 * 1)
                |> rounded(10))
            >>> appendSubView(presenter.likeButton
                |> named("likeButton")
                >>> alignToRight(-20)
                >>> styledTitle(heartFontIcon(size: 24, color: appBlack))
                >>> alignToSibling("name", constraint: alignedToSiblingTop)
                >>> rounded(18)
                >>> width_Height(36, 36))
            >>> appendSubView(presenter.isAMatchButton
                |> named("isAMatchButton")
                >>> styledTitle(heartFontIcon(size: 24, color: appBlack))
                >>> alignToSibling("likeButton", constraint: alignedToSiblingCenter)
                >>> rounded(18)
                >>> width_Height(50, 50))
            >>> appendSubView(presenter.likeButtonShadow
                |> backgroundColored(appLightShadow)
                |> alignToSibling("likeButton", constraint: alignedToSiblingCenterX)
                |> alignToSibling("likeButton", constraint: alignToSiblingCenterY(-8))
                |> width_Height(36, 36)
                |> rounded(18))
            >>> appendSubView(UILabel()
                |> named("attributesHeading")
                >>> alignToSibling("name", constraint: alignedToSiblingLeft)
                >>> alignToSibling("name", constraint: alignTopToSiblingBottom(8))
                >>> textFont(appFontFormal(20))
                >>> textColored(appBlack)
                >>> titled("Attributes"))
            >>> appendSubView(presenter.attributesTitleShadow
                  |> backgroundColored(appLightShadow)
                  |> alignToSibling("attributesHeading", constraint: alignedToSiblingCenter)
                  |> width_Height(100, 20 * 1.1)
                  |> rounded(8))
            >>> appendSubView(presenter.attributesCollection
                |> named("attributes")
                >>> alignToSibling("name", constraint: alignedToSiblingLeft)
                >>> alignToSibling("likeButton", constraint: alignedToSiblingRight)
                >>> alignToSibling("attributesHeading", constraint: alignTopToSiblingBottom(8))
                >>> viewHeight(50))
            >>> appendSubView(presenter.attributesShadow
                  |> backgroundColored(appLightShadow)
                  |> alignToSibling("attributes", constraint: alignToSiblingLeft(-1))
                  |> alignToSibling("attributes", constraint: alignToSiblingTop(-1))
                  |> alignToSibling("attributes", constraint: alignToSiblingRight(1))
                  |> alignToSibling("attributes", constraint: alignToSiblingBottom(1))
                  |> rounded(10))
        
        let b = appendSubView(UILabel()
                |> named("personalityHeading")
                >>> textFont(appFontFormal(20))
                >>> textColored(appBlack)
                >>> titled("Personality")
                >>> alignToSibling("attributes", constraint: alignedToSiblingLeft)
                >>> alignToSibling("attributes", constraint: alignTopToSiblingBottom(8)))
            >>> appendSubView(presenter.personalityTitleShadow
                  |> backgroundColored(appLightShadow)
                  |> alignToSibling("personalityHeading", constraint: alignedToSiblingCenter)
                  |> width_Height(100, 20 * 1.1)
                  |> rounded(10))
            >>> appendSubView(presenter.personalityTypeLabel
                |> named("personalityType")
                >>> rounded(15)
                >>> backgroundColored(appRoyalPurple)
                >>> width_Height(100, 30)
                >>> textColored(.white)
                >>> textCenterAligned
                >>> textFont(appFont(18))
                >>> alignToSibling("personalityHeading", constraint: alignTopToSiblingBottom(8))
                >>> alignToSibling("personalityHeading", constraint: alignedToSiblingLeft))
                >>> appendSubView(presenter.personalityShadow
                  |> backgroundColored(appLightShadow)
                  |> alignToSibling("personalityType", constraint: alignedToSiblingCenter)
                  |> width_Height(100, 30 * 1.1)
                  |> rounded(10))
            >>> appendSubView(presenter.learnMoreButton
                |> named("learn_more")
                |> textFont(appFont(12))
                >>> textColored(appBlack)
                >>> titled("Learn More...")
                >>> alignToSibling("personalityType", constraint: alignLeftToSiblingRight(8))
                >>> alignToSibling("personalityType", constraint: alignedToSiblingBottom))
            >>> appendSubView(presenter.learnMoreShadow
                  |> backgroundColored(appLightShadow)
                  |> alignToSibling("learn_more", constraint: alignedToSiblingCenter)
                  |> width_Height(50, 20)
                  |> rounded(10))
        
        let view = UIView()
                    |> whiteBackground
                    >>> roundedGreyShadowed(10)
                    >>> fill
                    >>> a
                    >>> b
        
        add(views: view)
    }
}
extension MatchUserInfoView {
    convenience init(presenter: MatchUserInfoPresenter) {
        self.init()
        self.presenter = presenter
        self.setViews()
    }
}
