//
//  MatchUserInfoRouter.swift
//  Meld
//
//  Created by Blake Rogers on 3/22/22.
//  Copyright © 2022 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UtilitiesPackage

final class MatchUserInfoRouter {
    func displayAttributeModal(attribute: AttributeOption) {
        let view = AttributeModalRouter.presentation(attribute: attribute)
        currentController().present(view, animated: true , completion: nil)
    }
    func displayPersonality(personality: PersonalityType) {
        let view = PersonalityTypeView(presenter: PersonalityTypePresenter(interactor: PersonalityTypeInteractor(personalityType: personality)))
        currentController().present(view, animated: true, completion:  nil)
    }
}
