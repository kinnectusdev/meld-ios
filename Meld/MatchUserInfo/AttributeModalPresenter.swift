//
//  AttributeModalPresenter.swift
//  Meld
//
//  Created by Blake Rogers on 3/22/22.
//  Copyright © 2022 com.meld.kinnectus. All rights reserved.
//

import UIKit
import RxSwift
import UtilitiesPackage

final class AttributeModalPresenter {
    private let bag = DisposeBag()
    private let interactor: AttributeModalInteractorIdentity
    private let router: AttributeModalRouter
    
    let titleLabel = UILabel()
    let table = UITableView()
        |> registerTableCell(OptionCell.self, OptionCell.identifier)
        |> tableWithoutSeparators
        |> asTable
    
    init(interactor: AttributeModalInteractorIdentity) {
        self.interactor = interactor
        self.router = AttributeModalRouter()
        
        bag.insert(
            interactor.observeTitle().asDriver(onErrorJustReturn: .empty).drive(titleLabel.rx.text),
            interactor.observeAttributeList()
                .bind(to: table.rx.items(cellIdentifier: OptionCell.identifier, cellType: OptionCell.self))
                { index, description, cell in
                    cell.configure(text: description, isSelected: false)
                }
        )
        
    }
}
