//
//  MatchUserInfoViewModel.swift
//  Meld
//
//  Created by blakerogers on 3/26/20.
//  Copyright © 2020 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol MatchUserInfoConversationDelegate: AnyObject
{
    func displayedUserInfo() -> Observable<MeldUser>
}
protocol MatchProfileDelegate: AnyObject {
    func isLoadingUserInfo() -> Observable<()>
}
protocol MatchUserInfoInteractorIdentity {
    // Input Action
    func didToggleLikeUser()
    func didSelectUnmatchUser()
    //Observed
    func observeMatchName() -> Observable<String>
    func observeMatchAge() -> Observable<String>
    func observeMatchIsLiked() -> Observable<Bool>
    func observeMatchAttribues() -> Observable<[AttributeOption]>
    func observeMatchPersonality() -> Observable<PersonalityType>
    func observeDidMatchUser() -> Observable<MeldUser>
    func observeIsAMatch() -> Observable<Bool>
    func isLoadingUserInfo() -> Observable<()>
}

final class MatchUserInfoInteractor {

    //MARK: Stored Properties
    private weak var conversationDelegate: MatchUserInfoConversationDelegate?
    private weak var profileManager: MatchProfileDelegate!
    let bag = DisposeBag()
    private var user = BehaviorRelay<MeldUser>(value: .empty)
    
    //MARK: Observed Properties
    private let name = PublishSubject<String>()
    private let age = PublishSubject<String>()
    private let isLiked = BehaviorRelay<Bool>(value: false)
    private let attributes = PublishSubject<[AttributeOption]>()
    private let personality = PublishSubject<PersonalityType>()
    private let didMatchUserEvent = PublishSubject<MeldUser>()
    
    //MARK: Initializer
    init(conversationDelegate: MatchUserInfoConversationDelegate, profileManager: MatchProfileDelegate) {
        self.conversationDelegate = conversationDelegate
        self.profileManager = profileManager
        
        let filterInvalidAttributes: (AttributeOption?) -> AttributeOption? = { atts -> AttributeOption? in
            if let vices = atts as? Vices {
                if vices.vices.removing(element: .prefer_Not_To_Say).isEmpty {
                    return nil
                } else {
                    return vices
                }
            } else if let values = atts as? CoreValues {
                if values.values.removing(element: .prefer_Not_To_Say).isEmpty {
                    return nil
                } else {
                    return values
                }
            } else {
                return atts
            }
        }
        
        bag.insert(
            conversationDelegate.displayedUserInfo().subscribe(onNext: { [weak self] userInfo in
                // set candidate info
                self?.age.onNext(userInfo.age?.presentation ?? .empty)
                self?.name.onNext(userInfo.name)
                self?.attributes.onNext(userInfo.publicAttributes.map(filterInvalidAttributes).compactMap { $0 })
                self?.personality.onNext(userInfo.personalityType ?? .intj)
                self?.user.accept(userInfo)
                let isMutuallyLiked = userInfo.likedUsers.contains(UserService.immediateCurrentUser()?.id ?? "")
                // set staus of liked user for canidate
                if isMutuallyLiked {
                    self?.didMatchUserEvent.onNext(userInfo)
                }
            }),
            Observable.combineLatest(UserService.observeLikedUsers(), user.asObservable()).map { userIDs, user -> Bool in
                userIDs.contains(user.id)
            }.bind(to: isLiked)
        )
    }
}

extension MatchUserInfoInteractor: MatchUserInfoInteractorIdentity {
    func didSelectUnmatchUser() {
        UserService.updateUserLikedStatus(id: user.value.id, isLiked: false)
            .subscribe(onNext: { [weak self] _ in
                self?.isLiked.accept(false)
        }).dispose()
    }
    func didToggleLikeUser() {
        guard let currentUser = UserService.immediateCurrentUser() else { return }
        let likedUsers = currentUser.likedUsers
        
        Observable.zip(Observable.just(likedUsers), user).flatMap { likedUserIds, user -> Observable<(Bool, MeldUser)> in
            let currentUserId = currentUser.id
            let userIsLiked = likedUserIds.contains(user.id)
            let isAMatch = user.likedUsers.contains(currentUserId)

            if userIsLiked {
                return UserService.updateUserLikedStatus(id: user.id, isLiked: false).map { _ in (false, user) }
            } else {
                return UserService.updateUserLikedStatus(id: user.id, isLiked: true).map { _ in (isAMatch, user) }
            }
        }.flatMap { (isAMatch, conversant) -> Observable<(ConversationServiceResult, MeldUser)> in
            if isAMatch {
               return ConversationService.startConversation(conversant: conversant).map { ($0, conversant)}
            } else {
                return .never()
            }
        }.subscribe(onNext: { [weak self] (result, conversant) in
            switch result {
            case .success:
                self?.didMatchUserEvent.onNext(conversant)
            case .failure:
                break
            }
        }).disposed(by: bag)
    }

    func observeMatchName() -> Observable<String> {
        name.asObservable().startWith(" ")
    }
    
    func observeMatchAge() -> Observable<String> {
        age.asObservable().startWith(" ")
    }
    
    func observeMatchIsLiked() -> Observable<Bool> {
        isLiked.asObservable()
    }
    
    func observeMatchAttribues() -> Observable<[AttributeOption]> {
        attributes.asObservable()
    }
    func observeMatchPersonality() -> Observable<PersonalityType> {
        personality.asObservable()
    }
    func observeDidMatchUser() -> Observable<MeldUser> {
        didMatchUserEvent.asObservable()
    }
    func isLoadingUserInfo() -> Observable<()> {
        profileManager.isLoadingUserInfo()
    }
    func observeIsAMatch() -> Observable<Bool> {
        Observable.combineLatest(
            UserService.currentUser().compactMap { $0.user },
            user.asObservable())
        .map { currentUser, user -> Bool in
            currentUser.likedUsers.contains(user.id) && user.likedUsers.contains(currentUser.id)
        }
    }
}
