//
//  AttributeModalView.swift
//  Meld
//
//  Created by Blake Rogers on 3/22/22.
//  Copyright © 2022 com.meld.kinnectus. All rights reserved.
//

import UIKit
import UtilitiesPackage

final class AttributeModalView: UIViewController {
    private var presenter: AttributeModalPresenter!
    
    private func setViews() {
        let view = UIView()
        |> rounded(10)
        |> whiteBackground
        |> appendSubView(presenter.titleLabel
            |> textFont(appFontFormal(18))
            >>> alignToTop(20)
            >>> alignedToCenterX
            >>> textColored(appBlack))
        >>> appendSubView(presenter.table |> alignToTop(50) >>> alignedToLeftBottomRight)
        
        self.view = view
    }
    
}
extension AttributeModalView {
    convenience init(presenter: AttributeModalPresenter) {
        self.init()
        self.presenter = presenter
        self.setViews()
    }
}
