//
//  AttributeModalInteractor.swift
//  Meld
//
//  Created by Blake Rogers on 3/22/22.
//  Copyright © 2022 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift

protocol AttributeModalInteractorIdentity {
    func observeTitle() -> Observable<String>
    func observeAttributeList() -> Observable<[String]>
}
final class AttributeModalInteractor {
    
    private let bag = DisposeBag()
    
    private let attributeList: [String]
    private let title: String
    
    init(attribute: AttributeOption) {
        if let vices = attribute as? Vices {
            attributeList = vices.vices.map { $0.description }
        } else if let values = attribute as? CoreValues {
            attributeList = values.values.map { $0.description }
        } else {
            attributeList = []
        }
        
        title = attribute.typeAndValue.0
    }
}

extension AttributeModalInteractor: AttributeModalInteractorIdentity {
    func observeTitle() -> Observable<String> {
        Observable.just(title)
    }
    
    func observeAttributeList() -> Observable<[String]> {
        Observable.just(attributeList)
    }
    
    
}
