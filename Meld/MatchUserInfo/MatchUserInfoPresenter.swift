//
//  MatchUserInfoPresenter.swift
//  Meld
//
//  Created by Blake Rogers on 7/18/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import SwiftyGif
import UtilitiesPackage
import UIKit

final class MatchUserInfoPresenter {
    
    //MARK: Stored Properties
    private let interactor: MatchUserInfoInteractorIdentity
    private let router: MatchUserInfoRouter
    private let bag = DisposeBag()
    
    //MARK: UIElements
    let nameLabel = UILabel()
    let nameShadow = UIView()
    let likeButton = UIButton()
    let likeButtonShadow = UIView()
    let personalityTypeLabel = UILabel()
    let personalityTitleShadow = UIView()
    let personalityShadow = UIView()
    let attributesTitleShadow = UIView()
    let attributesCollection = AttributesSectionView.view()
    let attributesShadow = UIView()
    let learnMoreButton = UIButton()
    let learnMoreShadow = UIView()
    let isAMatchButton: UIView = {
        let image: UIView = {
            do {
                let gif = try UIImage(gifName: "heart-animation.gif")
                return UIImageView(gifImage: gif, loopCount: .max)
                |> named("gif")
                |> clearBackground
                |> alignedToCenterX
                |> alignedToBottom
                |> notClipping
                |> width_Height(40, 40)
            } catch {
                print(error)
                return UIView()
                    |> named("background")
                    |> whiteBackground
                    |> fill
            }
        }()
        return UIView()
        |> framed(appFrame)
        |> whiteBackground
        |> isInvisible
        |> appendSubView(
            image
        )
    }()
    //MARK: Initializer
    init(interactor: MatchUserInfoInteractorIdentity) {
        
        self.interactor = interactor
        self.router = MatchUserInfoRouter()
        
        let matchName = interactor.observeMatchName()
        let personalityType = interactor.observeMatchPersonality()
        let isLiked = interactor.observeMatchIsLiked()
        let attributes = interactor.observeMatchAttribues()
        let isLoading = interactor.isLoadingUserInfo()
        let isAMatch = interactor.observeIsAMatch()
        
        let personalityTypeTapGesture = UITapGestureRecognizer()
        personalityTypeLabel.isUserInteractionEnabled = true
        personalityTypeLabel.addGestureRecognizer(personalityTypeTapGesture)
        let unmatchTapGesture = UITapGestureRecognizer()
        isAMatchButton.isUserInteractionEnabled = true
        isAMatchButton.addGestureRecognizer(unmatchTapGesture)
        
        bag.insert(
            isAMatch.subscribe(onNext: { [weak self] isAMatch in
                self?.isAMatchButton.alpha = isAMatch ? 1.0 : 0.0
                self?.isAMatchButton.isHidden = !isAMatch
                self?.likeButton.alpha = isAMatch ? 0.0 : 1.0
                self?.likeButton.isHidden = isAMatch
            }),
            personalityTypeTapGesture.rx.event.withLatestFrom(interactor.observeMatchPersonality()).subscribe(onNext: { [weak self] type in
                self?.router.displayPersonality(personality: type)
            }),
            likeButton.rx.tap.subscribe(onNext: { [weak self] _ in
                self?.interactor.didToggleLikeUser()
            }),
            isLoading.subscribe(onNext: { [weak self] in
                self?.loadElements()
            }),
            matchName
                .take(1)
                .map { _ in () }
                .subscribe(onNext: { [weak self] in
                self?.hideShadows()
            }),
            matchName
                .asDriver(onErrorJustReturn: "")
                .drive(nameLabel.rx.text),
            personalityType
                .map { $0.title }
                .startWith(" ")
                .asDriver(onErrorJustReturn: "")
                .drive(personalityTypeLabel.rx.text),
            personalityType
                .map { $0.personalityGroup.color }
                .asDriver(onErrorJustReturn: .white)
                .drive(personalityTypeLabel.rx.backgroundColor),
            isLiked.subscribe(onNext: { [weak self] liked in
                UIViewPropertyAnimator(duration: 0.25, curve: .easeIn) { [weak self] in
                    self?.likeButton.setAttributedTitle(heartFontIcon(size: 24,  color: liked ? .white : appBlack), for: .normal)
                    self?.likeButton.backgroundColor = liked ? appGold : .white
                    self?.isAMatchButton.alpha = 0.0
                }.startAnimation()
            }),
            attributes.subscribe(onNext: { [weak self] attributes in
                self?.attributesCollection.setItems(attributes)
            }),
            attributesCollection.didSelectAttribute.asObservable().subscribe(onNext: { [weak self] attributeOption in
                guard attributeOption is Vices || attributeOption is CoreValues else { return }
                self?.router.displayAttributeModal(attribute: attributeOption)
            }),
            interactor.observeDidMatchUser().subscribe(onNext: { [weak self] matchedUser in
                self?.animateIsAMatch()
            }),
            unmatchTapGesture.rx.event.subscribe(onNext: { [weak self] type in
                self?.interactor.didSelectUnmatchUser()
            })
        )
    }
    
    private func loadElements() {
        nameLabel.alpha = 0
        personalityTypeLabel.alpha = 0
        attributesCollection.alpha = 0
        learnMoreButton.alpha = 0
        UIView.animate(withDuration: 0.24, delay: 1.0, options: [], animations: { [weak self] in
            self?.nameLabel.alpha = 1
            self?.personalityTypeLabel.alpha = 1
            self?.attributesCollection.alpha = 1
            self?.learnMoreButton.alpha = 1
        }, completion: nil)
    }
    
    private func hideShadows() {
        UIView.animate(withDuration: 0.24, delay: 3.0, options: [], animations: { [weak self] in
            self?.nameShadow.alpha = 0
            self?.likeButtonShadow.alpha = 0
            self?.attributesTitleShadow.alpha = 0
            self?.attributesShadow.alpha = 0
            self?.personalityTitleShadow.alpha = 0
            self?.personalityShadow.alpha = 0
            self?.learnMoreShadow.alpha = 0
        }, completion: nil)
    }
    private func animateIsAMatch() {
//        UIViewPropertyAnimator(duration: 0.25, curve: .easeIn) { [weak self] in
//            self?.likeButton.alpha = 0
//            self?.isAMatchButton.alpha = 1.0
//        }.startAnimation()
    }
}
