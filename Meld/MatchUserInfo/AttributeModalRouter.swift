//
//  AttributeModalRouter.swift
//  Meld
//
//  Created by Blake Rogers on 3/22/22.
//  Copyright © 2022 com.meld.kinnectus. All rights reserved.
//

import UIKit
final class AttributeModalRouter {
    
}
extension AttributeModalRouter {
    static func presentation(attribute: AttributeOption) -> UIViewController {
        AttributeModalView(presenter: AttributeModalPresenter(interactor: AttributeModalInteractor(attribute: attribute)))
    }
}
