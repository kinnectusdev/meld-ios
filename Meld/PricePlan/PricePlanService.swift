//
//  PricePlanService.swift
//  Meld
//
//  Created by blakerogers on 11/12/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import RxSwift
import RxCocoa
import StoreKit

public enum PricePlanServiceResult {
    case success
    case failure(String)
    case processing
}
public class PricePlanService: NSObject {
    static let isMockService: Bool = true
    static let shouldServicePass: Bool = true
    let serviceResult = PublishSubject<PricePlanServiceResult>()
    
    static func makePurchase(plan: PricePlan) -> Observable<PricePlanServiceResult> {
        guard !isMockService else {
            return shouldServicePass ? Observable.just(.success) : .just(.failure("None"))
        }
        return Observable.create { observer in
            let delegate = PricePlanService.shared
            let request = SKProductsRequest(productIdentifiers: [plan.productIdentifier])
            request.delegate = delegate
            request.start()
            _ = delegate.serviceResult.asObservable().bind(to: observer)
            return Disposables.create()
        }
    }
    
    static var shared: PricePlanService = AppCoordinator.shared.providePricePlanService()
}
extension PricePlanService: SKProductsRequestDelegate {
    public func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        guard let product = response.products.first else { return }
        let payment = SKPayment(product: product)
        SKPaymentQueue.default().add(payment)
    }
}
extension PricePlanService: SKPaymentTransactionObserver {
    public func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        guard let transaction = transactions.first else { return }
        switch transaction.transactionState {
        case .purchasing:
            serviceResult.onNext(.processing)
        case .purchased:
            ///TODO: Update User Defaults or stored user object as having made a purchase
            serviceResult.onNext(.success)
        case .failed:
            serviceResult.onNext(.failure( transaction.error?.localizedDescription ?? "Payment Failed"))
        case .restored:
            serviceResult.onNext(.success)
        case .deferred:
            serviceResult.onNext(.failure("Deferred Payment"))
        @unknown default:
            break
        }
    }
}
