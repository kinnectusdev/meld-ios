//
//  PricePlanPresenter.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import RxSwift
import RxCocoa
import UtilitiesPackage
import UIKit

final class PricePlanPresenter {
    private let bag = DisposeBag()
    private let interactor: PricePlanInteractorIdentity
    private let router: PricePlanRouter
    
    lazy var pricePlanButton: UIView = { [weak self] in
        guard let this = self else { return UIView() }
        return UIView()
          |> roundedGreyShadowed(40)
          >>> appendSubView(UILabel()
              |> alignedToCenterY
              >>> styledTitle(darkGreyAttributedString("$9.99", 30))
              >>> alignToLeft(20)
              >>> alignToTop(20))
          >>> appendSubView(UILabel()
              |> alignToLeft(100)
            >>> styledTitle(blackAttributedString("Video Chat within your conversations", 20))
              >>> alignedToRight
              >>> alignedToCenterY
              >>> unlimitedLines)
    }()
    
    let backButton = UIButton()
    |> visibility( DeviceInfo.deviceType == .iPad ? 1.0 : 0.0 )
    |> asButton
    init(interactor: PricePlanInteractorIdentity) {
        self.interactor = interactor
        self.router = PricePlanRouter()
        
    }
    
}
