//
//  PricePlanController.swift
//  Meld
//
//  Created by blakerogers on 11/11/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import UIKit
import UtilitiesPackage

final class PricePlanView: UIViewController {
    private var presenter: PricePlanPresenter!
    private func setViews() {
        let view = UIView()
            |> framed(appFrame)
            >>> whiteBackground
            >>> appendSubView(UIImageView(image: UIImage(named: "logo"))
                |> width_Height(50, 50)
                >>> alignedToCenterX
                >>> scaledToFit
                >>> alignToTop(50)
                >>> named("logo"))
            >>> appendSubView(presenter.backButton
                |> named("back_button")
                >>> setImage(UIImage(named: "BackButton"))
                >>> alignToLeft(10)
                >>> alignToTop(70))
            >>> appendSubView(presenter.pricePlanButton
                                |> viewHeight(150)
                                >>> alignToLeft(20)
                                >>> alignToRight(-20)
                                |> alignedToCenter)
        
        self.view = view
    }
}
extension PricePlanView {
    convenience init(presenter: PricePlanPresenter) {
        self.init()
        self.presenter = presenter
        self.setViews()
        self.modalPresentationStyle = ( DeviceInfo.deviceType == .iPad ? .overCurrentContext : .automatic)
    }
}

