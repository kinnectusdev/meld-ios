//
//  PricePlanData.swift
//  Meld
//
//  Created by blakerogers on 11/13/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import Foundation

public enum PricePlan {
    case low
    case moderate
    case high
    var productIdentifier: String {
        switch self {
        case .low: return "com.meld.kinnectus.Meld.Level1Membership"
        default: return ""
        }
    }
    static var all: [PricePlan] {
        return [low, moderate, high]
    }
    static func matchPlan(_ description: String) -> PricePlan? {
        return all.first { (plan) -> Bool in
            return plan.productIdentifier == description
        }
    }
}
