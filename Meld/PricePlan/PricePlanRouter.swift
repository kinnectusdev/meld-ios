//
//  PricePlanRouter.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UIKit

final class PricePlanRouter {
    
}

extension PricePlanRouter {
    static func presentation() -> UIViewController {
        PricePlanView(presenter: PricePlanPresenter(interactor: PricePlanInteractor()))
    }
}
