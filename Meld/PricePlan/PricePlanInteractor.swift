//
//  PricePlanInteractor.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation

protocol PricePlanInteractorIdentity {
    
}

final class PricePlanInteractor {
    
}

extension PricePlanInteractor: PricePlanInteractorIdentity {
    
}
