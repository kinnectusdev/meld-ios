//
//  ConversantsViewItem.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import AVKit
import SwiftyGif
import NVActivityIndicatorView
import UtilitiesPackage

final class ConversantsViewItem: UICollectionViewCell {
    
    static let identifier: String = "ConversantsViewItem"
    
    private var bag = DisposeBag()
    
    private let image = UIImageView(image: UIImage(named: "profileImage"))
        |> rounded(15)
        |> named("image")
        |> width_Height(30, 30)
        |> alignToSibling("spotlight", constraint: alignedToSiblingCenter)
        |> maskedToBounds
        |> alignedToCenter
        |> scaledToFill
        |> isInterActive
        |> asImage
    
    private let nameLabel = UILabel()
    |> textColored(appBlack)
    |> textFont(appFontFormal(10))
    |> alignedToBottom
    |> alignedToCenterX
    |> textCenterAligned
    |> asLabel
    
    private let spotlightImage: UIView = {
        do {
            let gif = try UIImage(gifName: "spotlight.gif")
            return UIImageView(gifImage: gif, loopCount: .max) // Will loop 3 times
            |> named("spotlight")
            |> alignedToCenter
            |> width_Height(50, 50)
        } catch {
            print(error)
            return UIView()
                |> named("spotlight")
                |> backgroundColored(appPurple)
                |> fill
                |> rounded(25)
        }
    }()

    let loadingIndicator = NVActivityIndicatorView(frame: CGRect(origin: .zero, size: CGSize(width: 20, height: 20)),
                                                   type: .ballClipRotate,
                                                   color: appBlack,
                                                   padding: nil)
        |> alignedToCenter
        |> { $0 as! NVActivityIndicatorView }
    
    private func setViews() {

        contentView.add(views: spotlightImage,
                        image,
                        nameLabel,
                        loadingIndicator)
    }
    
    func configure(matchedUser: MeldUser,
                   currentConversantId: Observable<String>,
                   didSelectConversant: @escaping (MeldUser) -> Void) {
        
        nameLabel.text = matchedUser.name
        
        contentView.addTapGestureRecognizer(action: { [weak self] in
            didSelectConversant(matchedUser)
        })
        
        image.addTapGestureRecognizer(action: { [weak self] in
            didSelectConversant(matchedUser)
        })
                
        bag.insert(
            currentConversantId.map { id -> Bool in
                let matchedUserId = matchedUser.id
                return matchedUserId == id
            }.asDriver(onErrorJustReturn: false).drive(onNext: { [weak self] isCurrentUser in
                self?.spotlightImage.isHidden = !isCurrentUser
            }),
            Observable.just(matchedUser)
                .map { $0.imageURLs.first }
                .do(onNext: handleDoOnNextURL)
                .flatMap(handleURLToData)
                .asDriver(onErrorJustReturn: nil)
                .drive(onNext: handleImageLoad)
        )
    }
    private func handleDoOnNextURL(url: String?) -> Void {
        image.alpha = 0.0
        image.image = UIImage(named: "profileImage")
        loadingIndicator.isHidden = false
        loadingIndicator.startAnimating()
    }
    
    private func handleURLToData(url: String?) -> Observable<Data?> {
        if let url = url {
            return MediaImageService.fetchImageData(from: url)
        } else {
            return Observable.just(nil)
        }
    }
    private func handleImageLoad(data: Data?) {
        if let data = data {
            image.image = UIImage(data: data)
        } else {
            image.image = UIImage(named: "profileImage")
        }
        UIView.animate(withDuration: 0.24) { [weak self] in
            self?.image.alpha = 1.0
        }
        loadingIndicator.stopAnimating()
        loadingIndicator.isHidden = true
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        setViews()
    }
    override func prepareForReuse() {
        self.image.image = nil
        self.bag = DisposeBag()
    }
}

