//
//  ConversantsView.swift
//  Meld
//
//  Created by Blake Rogers on 6/1/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import UIKit

final class ConversantsView: UIView {
    private var presenter: ConversantsPresenter!
    private func setViews() {
        addSubview(presenter.collection)
        presenter.collection.constrainInView(view: self, top: 0, left: 0, right: 0, bottom: 0)
    }
}

extension ConversantsView {
    convenience init(presenter: ConversantsPresenter) {
        self.init()
        self.presenter = presenter
        self.setViews()
    }
}
