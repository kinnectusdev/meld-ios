//
//  ConversantsPresenter.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import UtilitiesPackage
import UIKit

final class ConversantsPresenter: NSObject {
    private let bag = DisposeBag()
    private var interactor: ConversantsInteractorIdentity!
    private var router: ConversantsViewRouter!
    private var conversants: [MeldUser] = []
    
    lazy var collection = UICollectionView.collectionView(width: 60, height: 60, direction: .horizontal)
        |> registerCell(ConversantsViewItem.self, ConversantsViewItem.identifier)
        >>> collectionDatasource(self)
        >>> asCollection

   init(interactor: ConversantsInteractorIdentity) {
        super.init()
        self.interactor = interactor
        self.router = ConversantsViewRouter()
        bag.insert(
            interactor.observeConversants().subscribe(onNext: { [weak self] conversants in
                self?.conversants = conversants
                self?.collection.reloadData()
            }),
            interactor.observeDisplayConversant().subscribe(onNext: { [weak self] conversant in
                self?.router.displayConversant(conversant: conversant, manager: interactor)
            })
        )
    }
}
extension ConversantsPresenter: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        conversants.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ConversantsViewItem.identifier, for: indexPath) as? ConversantsViewItem {
            guard let conversant = conversants.itemAt(indexPath.item) else { return UICollectionViewCell() }
            cell.configure(matchedUser: conversant,
                           currentConversantId: interactor.observeCurrentConversantID(),
                           didSelectConversant: interactor.didSelectConversant)
            return cell
        }
        return UICollectionViewCell()
    }
}
