//
//  ConversantsRouter.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UIKit
import UtilitiesPackage

final class ConversantsViewRouter {
    func displayConversant(conversant: MeldUser, manager: ConversantManagerIdentity) {
        let view = ConversantViewRouter.presentation(conversant: conversant, conversantManager: manager)
        currentController().present(view, animated: true, completion: nil)
    }
}
extension ConversantsViewRouter {

    static func presentation(conversantsManager: ConversantsManagerIdentity) -> UIView {
        ConversantsView(presenter: ConversantsPresenter(interactor: ConversantsInteractor(conversantsManager: conversantsManager)))
    }
}

