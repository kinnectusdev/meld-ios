//
//  ConversantsInteractor.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import UtilitiesPackage

protocol ConversantsManagerIdentity {
    func observeConversants() -> Observable<[MeldUser]>
    func observeCurrentConversantID() -> Observable<String>
    
    func didSelectConversant(conversant: MeldUser)
    func didSelectStartConversation(user: MeldUser)
}

protocol ConversantsInteractorIdentity: ConversantManagerIdentity {
    func observeConversants() -> Observable<[MeldUser]>
    func observeCurrentConversantID() -> Observable<String>
    func observeDisplayConversant() -> Observable<MeldUser>

    func didSelectConversant(conversant: MeldUser)
}
final class ConversantsInteractor {
    private let bag = DisposeBag()
    private let manager: ConversantsManagerIdentity
    
    private let likedUsers = BehaviorRelay<[MeldUser]>(value: [])
    private let onDisplayConversant = PublishSubject<MeldUser>()
    
    init(conversantsManager: ConversantsManagerIdentity) {
        
        self.manager = conversantsManager
        
        bag.insert(
            UserService.observeLikedUsers().flatMap { users -> Observable<[UserFetchServiceResult]> in
                Observable.zip(users.map { UserService.fetchUser($0)})
            }.map { results -> [MeldUser] in
                results.map { result -> MeldUser? in
                    switch result {
                    case .failure:
                        return nil
                    case .success(let user):
                        return user
                    }
                }.compactMap { $0 }
            }.bind(to: likedUsers)
        )
    }
}
extension ConversantsInteractor: ConversantsInteractorIdentity {
    func didSelectLikeUser(conversant: MeldUser) {
        likedUsers.accept(likedUsers.value |> appending(conversant))
    }
    
    func didSelectUnlikeUser(conversant: MeldUser) {
        likedUsers.accept(likedUsers.value |> removing(conversant))
    }
   
    func observeConversants() -> Observable<[MeldUser]> {
        Observable.combineLatest(manager.observeConversants(), likedUsers.asObservable()) { conversants, likedUsers -> [MeldUser] in
            conversants.appendingAll(likedUsers) |> uniqueElements
        }
    }
    
    func observeCurrentConversantID() -> Observable<String> {
        manager.observeCurrentConversantID()
    }
    
    func observeDisplayConversant() -> Observable<MeldUser> {
        onDisplayConversant.asObservable()
    }
    
    func didSelectConversant(conversant: MeldUser) {
        manager.observeConversants().take(1).map { conversants -> Bool in
            conversants.map { $0.id }.contains(conversant.id)
        }.subscribe(onNext: { [weak self] isConversant in
            if isConversant {
                self?.manager.didSelectConversant(conversant: conversant)
            } else {
                self?.onDisplayConversant.onNext(conversant)
            }
        }).disposed(by: bag)
    }
    
    func didSelectStartConversation(conversant: MeldUser) {
        manager.didSelectStartConversation(user: conversant)
    }
    
    func isInConversation(conversant: MeldUser) -> Observable<Bool> {
        manager.observeConversants().map { $0.map { $0.id }.contains(conversant.id) }
    }
}
