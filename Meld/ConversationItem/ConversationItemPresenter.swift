//
//  ConversationItemPresenter.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import UtilitiesPackage
import UIKit

final class ConversationItemPresenter: NSObject {
    
    private let bag = DisposeBag()
    private var interactor: ConversationItemInteractorIdentity!
    private var messages: [MeldMessage] = []
    
    lazy var collection = UICollectionView.collectionView(width: 300, height: 300, backgroundColor: .clear, selectionAllowed: false, direction: .vertical, paging: false)
        |> fill
        |> collectionDelegate(self)
        |> collectionDatasource(self)
        |> registerCell(MessageViewCell.self, MessageViewCell.identifier)
        |> registerCell(NoMessagesItem.self, NoMessagesItem.identifier)
        |> asCollection
    
    init(interactor: ConversationItemInteractorIdentity){
        super.init()
        self.interactor = interactor
        
        bag.insert(
            interactor.observeMessages().asDriver(onErrorJustReturn: []).drive(onNext: { [weak self] messages in
                self?.messages = messages
                self?.collection.reloadData()
                guard let this = self else { return }
                let numberOfMessages = this.collection.numberOfItems(inSection: 0)
                guard numberOfMessages > 0 else { return }
                let lastPath = IndexPath(item: max(numberOfMessages-1,0), section: 0)
                this.collection.scrollToItem(at: lastPath, at: .bottom, animated: true)
            })
        )
    }
}

extension ConversationItemPresenter: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        messages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let message = messages.itemAt(indexPath.item) else {
          return UICollectionViewCell()
        }
        guard message.message.notEmpty() || message.messageImageURL.notEmpty() else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NoMessagesItem.identifier, for: indexPath) as? NoMessagesItem
            return cell!
        }
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MessageViewCell.identifier, for: indexPath) as? MessageViewCell else  { return UICollectionViewCell() }
        let currentUserID = UserService.immediateCurrentUser()?.id ?? ""
        let isSentMessage = message.senderID == currentUserID
           cell.setMessage(message: message.message, timestamp: message.timeStamp, image: "", isSentMessage: isSentMessage)
        return cell
    }
    
    private func sizeForMessage(message: MeldMessage) -> CGFloat {
        if message.message.notEmpty()  {
            let messageRect = message.message.rectForText(width: appFrame.width, textSize: 15)
            return messageRect.height + 50
        } else if message.messageImageURL.notEmpty() {
            return 300
        } else {
            return 200
        }
    }
}
extension ConversationItemPresenter: UICollectionViewDelegateFlowLayout {
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
        guard let message = messages.itemAt(indexPath.item) else {
            return .zero
        }
        let height = sizeForMessage(message: message)
        return CGSize(width: collectionView.frame.width, height: height)
    }
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
