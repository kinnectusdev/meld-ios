//
//  ConversationItem.swift
//  Meld
//
//  Created by Blake Rogers on 6/19/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UIKit

class ConversationItem: UICollectionViewCell {
    
    static let identifier: String = "ConversationItem"
    private var presenter: ConversationItemPresenter!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.setViews()
    }
    
    func configure(presenter: ConversationItemPresenter) {
        self.presenter = presenter
    }
    
    private func setViews() {
        contentView.add(views: presenter.collection)
        presenter.collection.constrainInView(view: contentView, top: 0, left: 0, right: 0, bottom: 0)
    }
}
