//
//  ConversationItemInteracotr.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol ConversationItemInteractorIdentity {
    func observeMessages() -> Observable<[MeldMessage]>
}

final class ConversationItemInteractor {
    private let bag = DisposeBag()
    private let conversation: Conversation
    private let messages = BehaviorRelay<[MeldMessage]>(value: [])
    
    init(conversation: Conversation) {
        
        self.conversation = conversation
        
        bag.insert(
            conversation_model(conversation: conversation).bind(to: messages)
        )
    }
}

extension ConversationItemInteractor: ConversationItemInteractorIdentity {
    func observeMessages() -> Observable<[MeldMessage]> {
        messages.asObservable()
    }
}
