//
//  NoMessagesIem.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UIKit
import UtilitiesPackage

final class NoMessagesItem: UICollectionViewCell {
    static let identifier: String = "NoMessagesItem"
    
    private let noMessagesLabel = UILabel()
                                |> named("no_messages_label")
                                |> textColored(appBlack)
                                >>>  titled("No Messages. \n Umm...Just say hello")
                                >>> unlimitedLines
                                >>> textFont(appFontFormal(20))
                                >>> textCenterAligned
                                >>> alignedToLeftTopRight
                                >>> viewHeight(100)
    
    private let image = UIImageView(image: UIImage(named: "box"))
    |> width_Height(64, 64)
    |> scaledToFit
    |> alignToSibling("no_messages_label", constraint: alignTopToSiblingBottom(10))
    |> alignedToCenterX
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setViews()
    }
    
    private func setViews() {
        contentView.add(views: noMessagesLabel, image)
    }
}
