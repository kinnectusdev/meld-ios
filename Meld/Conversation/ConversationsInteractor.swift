//
//  ConversationsInteractor.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import UtilitiesPackage

protocol ConversationsInteractorIdentity: AnyObject, MessageManagerIdentity {
    
    func didSelectConversant(conversant: MeldUser)
    func didCompleteMessage(message: String)
    
    func didSelectEndConversation()
    func didSelectBlock()
    func didSelectReportAsAbusive()
    func didSelectVideoChat()
    
    func willDisplayConversation(conversation: Conversation)
    func didSelectStartConversation(conversant: MeldUser)
    
    func observeConversations() -> Observable<[Conversation]>
    func observeIsConversationAvailable() -> Observable<Bool>
    func observeConversants() -> Observable<[MeldUser]>
    func observeCurrentConversantID() -> Observable<String>
    func observeInitiateVideoChat() -> Observable<Conversation>
}

final class ConversationsInteractor {
    
    private let bag = DisposeBag()
    
    private var currentConversation = BehaviorRelay<Conversation?>(value: nil)
    private let conversations = BehaviorRelay<[Conversation]>(value: [])
    private let currentConversant = BehaviorRelay<MeldUser>(value: .empty)
    private let displayedUser = BehaviorRelay<MeldUser>(value: .empty)
    private let initiateVideoChat = BehaviorRelay<Void>(value: ())
    
    init() {
        bag.insert(
            ///Fetch available conversation
            ConversationService.fetchUserConversations().map { conversationResult -> [Conversation] in
                switch conversationResult {
                case let .success(conversations):
                    return conversations.sorted(by: { $0.latestActivity.doubleValue < $1.latestActivity.doubleValue })
                default:
                    return []
                }
            }.bind(to: conversations),
            currentConversation.compactMap { $0 }.map { conversation -> String in
                let isCurrentUser = conversation.conversantA_id == UserService.immediateCurrentUser()?.id
                return isCurrentUser ? conversation.conversantB_id : conversation.conversantA_id
            }.flatMap { id -> Observable<MeldUser> in
                UserService.fetchUser(id).map { result -> MeldUser? in
                    switch result {
                    case .failure:
                        return nil
                    case .success(let user):
                        return user
                    }
                }.compactMap({$0})
            }
            .bind(to: currentConversant)
        )
    }
    
    private func endConversation() {
        Observable.combineLatest(currentConversation.compactMap { $0 }, conversations, currentConversant).flatMap { args -> Observable<(ConversationServiceResult, Conversation, [Conversation])> in
            let (conversation, conversations, conversant) = args
            return ConversationService.endConversation(conversation: conversation,
                                                       conversant: conversant).map { ($0, conversation, conversations) }
        }.subscribe(onNext: { [weak self] (result, conversation, conversations) in
            if case .success = result {
                let filtered = conversations |> removingWhere { $0.id == conversation.id }
                self?.conversations.accept(filtered)
            }
        }).disposed(by: bag)
    }
}

extension ConversationsInteractor: ConversationsInteractorIdentity {
    func didSelectVideoChat() {
        initiateVideoChat.accept(())
    }
    func observeInitiateVideoChat() -> Observable<Conversation> {
        initiateVideoChat
            .withLatestFrom(currentConversation.asObservable())
            .compactMap { $0 }
    }
    func didSelectEndConversation() {
      endConversation()
    }
    
    func didSelectBlock() {
        endConversation()
    }
    
    func didSelectReportAsAbusive() {
        endConversation()
    }
    
    func observeConversations() -> Observable<[Conversation]> {
        conversations.asObservable()
    }
    func observeConversants() -> Observable<[MeldUser]> {
        conversations
           .map { $0.map { [$0.conversantA_id, $0.conversantB_id]} |> flatten }
           .map { $0.filter { $0 != (UserService.immediateCurrentUser()?.id)}}
           .flatMap { userIds -> Observable<[MeldUser]> in
               let users = userIds.compactMap { $0 }.map { UserService.fetchUser($0).compactMap { $0.user }}
               return Observable.zip(users)
           }.startWith([])
    }
    
    func observeCurrentConversantID() -> Observable<String> {
        currentConversation.asObservable()
            .map { ($0?.conversantA_id ?? .empty, $0?.conversantB_id ?? .empty ) }
            .map { conversantA, conversantB -> String in
            let currentUserId = UserService.immediateCurrentUser()?.id ?? .empty
            return conversantA == currentUserId ? conversantB : conversantA
        }
    }
    
    func observeIsConversationAvailable() -> Observable<Bool> {
        conversations.asObservable().map { $0.count > 0 }
    }
    
    func didSelectConversant(conversant: MeldUser) {
        conversations.asObservable().take(1).subscribe(onNext: { [weak self] conversations in
            let conversationContainsConversant: (Conversation) -> Bool = { conversation in
                conversation.conversantA_id == conversant.id || conversation.conversantB_id == conversant.id
            }
            let relevantConversation = conversations.filter(conversationContainsConversant).first
            if let conversation = relevantConversation {
                let popAndPushConversation: (Conversation) -> ([Conversation]) -> [Conversation] = { conversation in
                    return { conversations in
                        return [conversation].appendingAll(conversations.filter { $0.id != conversation.id })
                    }
                }
                let orderedConversations = conversations |> popAndPushConversation(conversation)
                self?.conversations.accept(orderedConversations)
            } else {
                // Display user
                self?.displayedUser.accept(conversant)
            }
           
        }).disposed(by: bag)
    }
    
    func didCompleteMessage(message: String) {
        /// Filter message and screen for validity
        let validMessage = Observable.just(message).filter { $0.notEmpty() }
        Observable.zip(validMessage,
                       currentConversation.compactMap { $0 }.take(1),
                       currentConversant)
        .flatMap { args -> Observable<Bool> in
            let (message, conversation, receiver) = args
                return UserService.currentUser().take(1).map{ $0.user }.compactMap { $0 }.flatMap { sender -> Observable<Bool> in
                    let dateTime = Date().timeIntervalSince1970
                    let newMessage = MeldMessage.new
                        |> prop(\.creationDate)({_ in NSNumber(value: dateTime)})
                        |> prop(\.conversationID)({_ in conversation.id })
                        |> prop(\.senderID)({_ in sender.id})
                        |> prop(\.receiverID)({_ in receiver.id })
                        |> prop(\.senderImageURL)({_ in sender.imageURLs |> first |> otherwise("")})
                        |> prop(\.receiverImageURL)({_ in receiver.imageURLs |> first |> otherwise("") })
                        |> prop(\.senderName)({_ in sender.name })
                        |> prop(\.receiverName)({_ in receiver.name })
                        |> prop(\.message)({_ in message })
                return ConversationService.sendMessage(message: newMessage).map { result -> Bool in
                    switch result {
                    case .failure:
                        return false
                    case .success:
                        return true
                    }
                }
            }
        }.subscribe(onNext: { [weak self] result in
            if !result {
                ///TODO: Provide alert for failure to send message
            }
        }).disposed(by: bag)
    }
    
    func willDisplayConversation(conversation: Conversation) {
        currentConversation.accept(conversation)
    }
    
    func didSelectStartConversation(conversant: MeldUser) {
        ConversationService.startConversation(conversant: conversant).subscribe(onNext: { [weak self] result in
            switch result {
            case .success:
                ///TODO: display alert for starting conversation
                break
            case .failure(let error):
                ///TODO: display alert for error
                break
            }
        }).disposed(by: bag)
    }
}
 
