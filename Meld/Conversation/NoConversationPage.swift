//
//  NoConversationPage.swift
//  Meld
//
//  Created by blakerogers on 11/30/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import UtilitiesPackage

public let no_conversations_bag = DisposeBag()
func noConversationPage() -> UIView {
    let noConversationLabel = UILabel()
    let detailTextLabel = UILabel()
  
    let view = UIView()
            |> whiteBackground
            >>> appendSubView(noConversationLabel
                |> named("noConversation_label")
                >>> alignToTop(100)
                >>> alignedToCenterX
                >>> styledTitle(formalBlackAttributedString("No Conversations", 30)))
            >>> appendSubView(detailTextLabel
                |> named("detail_label")
                >>> alignedToCenterX
                >>> unlimitedLines
                >>> titled("Find a Match to Start Chatting.")
                >>> textColored(appDarkGrey)
                >>> textFont(appFont(15))
                >>> textCenterAligned
                >>> alignToSibling("noConversation_label", constraint: alignTopToSiblingBottom(20)))
    return view
}
