//
//  ConversationStarters.swift
//  Meld
//
//  Created by blakerogers on 4/29/20.
//  Copyright © 2020 com.meld.kinnectus. All rights reserved.
//

import Foundation
enum ConversationStarters: Int, CaseIterable {
    case funny = 0
    case teens
    case couples
    case firstDate
    case dinner
    case gathering
    case bar
    var questions: [String] {
        switch self {
        ///Funny
        case .funny:
            return ["If you were in a circus, which character would you be?",
                    "What is the worst advice you have given?",
                    "What is one thing you should never say at a wedding?",
                    "What is the worst pickup line you have ever heard?",
                    "If you could only store one type of food in your pocket, what would you carry?",
                    "What is the worst present you have ever received and why?",
                    "If you were a farm animal, which would you be and why?",
                    "What is the worst first date you have ever been on?",
                    "Have you ever stalked someone on social media?",
                    "What is the best part about taking a selfie?",
                    "What is your favorite celebrity scandal?",
                    "If you could do anything illegal without getting caught, what would you do?",
                    "What is the weirdest food combination you’ve ever tried?",
                    "Did you have an imaginary friend? What was his/her name?",
                    "Have you ever had a dream where everyone was in their underwear?",
                    "Who’s your favorite comedian?",
                    "Have you ever been on a blind date?"]
        ///Teens
        case .teens:
            return ["What do you most like about yourself?",
                    "What hurts your feelings?",
                    "If you could be famous, would you want to? Why?",
                    "Who is a celebrity you admire?",
                    "What made you laugh at school today?",
                    "Did anything make you upset today?",
                    "If you could have more friends, would you?",
                    "What do you like most about your friends?",
                    "Is there anyone at school you’d like to know better?",
                    "Have you ever lost a friend? Why?",
                    "Where do you want to be ten years from now?",
                    "If you had $100, what would you spend it on?",
                    "Is there something you’d want to do as a family?",
                    "If you were to choose one way to be disciplined, what would it be?",
                    "What do you think are the best traits for a person to have?",
                    "Would you ever get a tattoo? What would it be?",
                    "What do you think is a good age to start dating?",
                    "Why do you think popular kids are popular?",
                    "If you could go anywhere in the world, where would you choose and why?",
                    "What is something you wish you could do everyday?",
                    "What are the top three things on your bucket list?",
                    "How do you think you will die?",
                    "What has been the lowest point of your life?",
                    "If you could ask for a miracle, what would it be?",
                    "Where do you see yourself in five years?",
                    "What is the biggest risk you’ve ever taken?",
                    "What would your ideal life look like?",
                    "If someone gave you an envelope with your death date inside of it, would you open it?",
                    "When have you been the most happy?",
                    "Do you know anyone who is living their life to the fullest?",
                    "What is your idea of the perfect day?",
                    "Who has been the most influential person in your life and why?",
                    "What book had a big influence on you?",
                    "Do you think your priorities have changed since you were younger?",
                    "What is the most memorable lesson you learned from your parents?",
                    "What does success mean to you?",
                    "What is the most difficult thing you’ve ever done?",
                    "What scares you most about your future?",
                    "What keeps you up at night?"]
        ///Couples
        case .couples:
            return ["What do you think people need to know about each other before they get married?",
                    "What makes you very sentimental?",
                    "What do I do that makes you smile?",
                    "What is the best or worst trait you inherited from your parents?",
                    "What is your funniest memory of our dates?",
                    "Who was your first crush and why?",
                    "What three things do you want to be remembered for?",
                    "Is there a dream you’ve always had?",
                    "What is something you’ve always wanted to do, but are scared to?",
                    "What gives you butterflies?",
                    "What do you consider most romantic?",
                    "What motivates you most in life?",
                    "What things do you consider to be relationship deal breakers?",
                    "Do you think arguing is part of a relationship?",
                    "If we were to raise children, what are the most important things you would want them to", "learn?",
                    "How would you describe our relationship?",
                    "How would your partner describe you?",
                    "Do you consider yourself a spiritual person? How?",
                    "If you made a bucket list for us, what would you add to it?",
                    "How has our relationship changed since we started dating?",
                    "What do I do that comforts you the most?",
                    "In what ways are you most similar to me?",
                    "What makes you feel discouraged?",
                    "What is a significant event that has changed you?",
                    "If your life was a movie, which celebrity would play you?"]
        ///FirstDate
        case .firstDate:
            return ["What’s something not many people know about you?",
                    "What are you most passionate about?",
                    "What makes you laugh out loud?",
                    "What was your favorite thing to do as a kid?",
                    "Who do you text the most?",
                    "What do you like to cook the most?",
                    "What’s your favorite TV show?",
                    "What is your favorite book?",
                    "Who is most influential in your life?",
                    "What was your best friend’s name growing up?",
                    "How do you spend your mornings?",
                    "What’s your all-time favorite band?",
                    "What’s your dream job?",
                    "What did you study?",
                    "What did you want to be growing up?",
                    "What is the best pickup line you’ve ever used? Heard?",
                    "Do you have any nicknames?",
                    "What talent do you wish you had?",
                    "Where do you see yourself living when you retire?",
                    "What is your favorite weekend activity?",
                    "Do you have any pet peeves?",
                    "Who was your favorite teacher and why?",
                    "What makes you most uncomfortable about dating?",
                    "What is your favorite place in the entire world?"]
        ///Dinner
        case .dinner:
            return ["If you could have dinner with anyone living or not, who would it be?",
                    "Are you a cat person or a dog person?",
                    "What is the silliest thing you’ve posted online?",
                    "What was your worst wardrobe mistake?",
                    "What is the best restaurant you’ve been to?",
                    "What is your favorite kitchen smell?",
                    "When you die, what do you want to be reincarnated as?",
                    "What is your favorite guilty pleasure TV show?",
                    "What was your worst “foot in mouth” moment?",
                    "Who would you swap lives with for a day?",
                    "If you could live anywhere in the world, where would it be?",
                    "What is the strangest gift you have ever received?",
                    "What is the funniest gift you have ever given?",
                    "What are three fun facts about yourself?",
                    "What’s the best compliment you’ve ever received?",
                    "Would you rather be invisible or have X-ray vision?",
                    "If you could only save one item from a house fire, what would it be?",
                    "What is the one food you could eat for the rest of your life?",
                    "How do you know (person hosting dinner)?",
                    "If you won the lottery, what would be your first big splurge?",
                    "What’s one movie you could watch over and over?",
                    "Where’s the most exotic place you’ve ever been?",
                    "If you could have picked your own name, what would it be?"]
        ///Gathering
        case .gathering:
            return ["How long can you go without checking your phone?",
                    "Have you ever really kept a New Year’s resolution?",
                    "What bad habits do you wish you could stop?",
                    "Do you have a morning ritual?",
                    "Have you ever been stalked on social media?",
                    "Can you tell when someone is lying?",
                    "Are you a jealous person?",
                    "Do you prefer polaroid or digital cameras?",
                    "If someone offered to tell you your future, would you accept it?",
                    "Who’s your biggest hero?",
                    "Have you ever stolen anything?",
                    "If you were to remove one social media app from your phone, which would it be and why?",
                    "If you could have tea with a fictional character, who would that be?",
                    "What is your most embarrassing moment?",
                    "If you were on death row, what would your last meal be?",
                    "If you could sit down with your 13-year old self, what would you say?",
                    "If you could only pack one thing for a trip (besides clothing) what would it be?",
                    "What makes you really angry?",
                    "What is your spirit animal?",
                    "What would be the title of your memoire?",
                    "What’s your guilty pleasure?",
                    "What would your theme song be if you had your own show?",
                    "What bores you?",
                    "What would you do if you were home alone and the power went out?",
                    "What’s the weirdest dream you’ve ever had?",
                    "If your plane was going down, who would you would call?",
                    "What would your rock band group be called?"]
        ///Bar
        case .bar:
            return ["I’m NAME, what’s your name?",
                    "Batman or Superman, who would win?",
                    "What’s the worst thing one can say on a first date?",
                    "What made you laugh this week?",
                    "Are you having a good evening?"]
        }
    }
}
