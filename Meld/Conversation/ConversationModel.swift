//
//  ConversationModel.swift
//  Meld
//
//  Created by Blake Rogers on 6/14/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift

func conversation_model(conversation: Conversation) -> Observable<[MeldMessage]> {
    /// Messages of the conversation
    let messages = ConversationService.monitorConversation(conversation: conversation)
    .map { result -> [MeldMessage] in
        switch result {
        case let .messages(messages):
            guard messages.count > 0 else {
                return [MeldMessage.new]
            }
            return messages.sorted(by: { message1, message2 -> Bool in
                let date1 = Date(timeIntervalSince1970: message1.creationDate.doubleValue)
                let date2 = Date(timeIntervalSince1970: message2.creationDate.doubleValue)
                switch Calendar.current.compare(date1, to: date2, toGranularity: .second) {
                case .orderedAscending:
                    return true
                case .orderedDescending:
                    return false
                case .orderedSame:
                    return true
                }
            })
        default:
            return [MeldMessage.new]
        }
    }
    return messages
}
