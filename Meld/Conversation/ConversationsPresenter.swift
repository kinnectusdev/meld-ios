//
//  ConversationsPresenter.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import UtilitiesPackage
import UIKit

final class ConversationsPresenter: NSObject {
    private let bag = DisposeBag()
    private var interactor: ConversationsInteractorIdentity!
    private var router: ConversationsRouter!
    private var conversations: [Conversation] = []
    
    let endConversationButton = UIButton()
    let videoChatButton = UIButton()
    let displayKeyboardButton = UIButton()
    
    lazy var collection = UICollectionView.collectionView(width: 300, height: 300, backgroundColor: .clear, selectionAllowed: false, direction: .vertical, paging: false)
        |> registerCell(ConversationItem.self, ConversationItem.identifier)
        >>> collectionDelegate(self)
        >>> collectionDatasource(self)
        |> asCollection
    
    convenience init(interactor: ConversationsInteractorIdentity) {
        self.init()
        self.interactor = interactor
        self.router = ConversationsRouter()

        bag.insert(
            interactor.observeConversations().subscribe(onNext: { [weak self] conversations in
                self?.conversations = conversations
                self?.collection.reloadData()
            }),
            interactor.observeIsConversationAvailable().asDriver(onErrorJustReturn: false).map { $0 ? 1.0 : 0.0 }.drive(endConversationButton.rx.alpha),
            interactor.observeIsConversationAvailable().asDriver(onErrorJustReturn: false).map { $0 ? 1.0 : 0.0 }.drive(displayKeyboardButton.rx.alpha),
            displayKeyboardButton.rx.tap.subscribe(onNext: { [weak self] _ in
                self?.router.displayKeyboard(messageManager: interactor)
            }),
            endConversationButton.rx.tap.subscribe(onNext: { [weak self] _ in
                self?.router.displayEndConversation(conversationsInteractor: interactor)
            }),
            videoChatButton.rx.tap.subscribe(onNext: { [weak self] _ in
                self?.interactor.didSelectVideoChat()
            }),
            interactor.observeInitiateVideoChat().subscribe(onNext: { [weak self] conversation in
                self?.router.displayVideoChat(conversation: conversation)
            })
        )
    }
}
extension ConversationsPresenter: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return conversations.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ConversationItem.identifier, for: indexPath) as? ConversationItem else { return UICollectionViewCell() }
        guard let conversation = conversations.itemAt(indexPath.item) else { return cell }
        cell.configure(presenter: ConversationItemPresenter(interactor: ConversationItemInteractor(conversation: conversation)))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let conversation = conversations.itemAt(indexPath.item) else {
            return
        }
        interactor.willDisplayConversation(conversation: conversation)
    }
}
extension ConversationsPresenter: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        collectionView.frame.size
    }
}
