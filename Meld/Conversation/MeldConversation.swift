//
//  MeldConversation.swift
//  Meld
//
//  Created by blakerogers on 11/21/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import Foundation

public struct Conversation {
    var creationDate: NSNumber
    var latestActivity: NSNumber
    var id: String
    var conversantA_id: String
    var conversantB_id: String
    var conversantA_name: String
    var conversantB_name: String
    var conversantA_isTyping: Bool
    var conversantB_isTyping: Bool
    var messages: [String]
    var access_tokenA: String
    var access_tokenB: String
    static let new: Conversation = Conversation(creationDate: NSNumber(value:Date().timeIntervalSince1970),
                                                latestActivity: 0,
                                                id: "",
                                                conversantA_id: "",
                                                conversantB_id: "",
                                                conversantA_name: "",
                                                conversantB_name: "",
                                                conversantA_isTyping: false,
                                                conversantB_isTyping: false,
                                                messages: [],
                                                access_tokenA: "",
                                                access_tokenB: "")
}

