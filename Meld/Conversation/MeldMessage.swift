//
//  MeldMessage.swift
//  Meld
//
//  Created by blakerogers on 11/21/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import Foundation


struct MeldMessage {
    var creationDate: NSNumber
    var id: String
    var conversationID: String
    var senderID: String
    var receiverID: String
    var senderImageURL: String
    var receiverImageURL: String
    var senderName: String
    var receiverName: String
    var messageImageURL: String
    var message: String
    static let new: MeldMessage = MeldMessage(creationDate: NSNumber(value: 0), id: "", conversationID: "", senderID: "", receiverID: "", senderImageURL: "", receiverImageURL: "", senderName: "", receiverName: "", messageImageURL: "", message: "")
    var timeStamp: String {
        return creationDate.doubleValue.day_time()
    }
}
