//
//  ConversationService.swift
//  Meld
//
//  Created by blakerogers on 11/21/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import Foundation
import Firebase
import FirebaseFirestore
import RxSwift
import UtilitiesPackage

public typealias Document = [String: Any]

enum ConversationServiceResult {
    
    case success
    case failure(String)
    
    var isSuccess: Bool {
        switch self {
        case .success: return true
        default: return false
        }
    }
}
enum FetchConversationResult {
    case message(MeldMessage)
    case messages([MeldMessage])
    case success([Conversation])
    case noConversations
    case failure(String)
}
class ConversationService {
    static let isMockService: Bool = false
    static let mockShouldSucceed: Bool = true
    static let mockStartConvoShouldSucceed: Bool = true
    static func monitorConversation(conversation: Conversation) -> Observable<FetchConversationResult> {
        guard !isMockService else {
            if mockShouldSucceed {
                return .just(.messages((0...10).map { _ in mockMessage()}))
            } else {
                return .just(.messages([]))
            }
        }
        return Observable.create { observer in
            let conversationDocument = Firestore.firestore().collection("Conversations").document(conversation.id)
            conversationDocument.addSnapshotListener { (snapshot, error) in
                if let error = error {
                    observer.onNext(.failure(error.localizedDescription))
                } else if let document = snapshot?.data() {
                    let conversation = conversationFromDocument(document)
                    let messagesQuery = Firestore.firestore().collection("Messages").whereField("conversationID", isEqualTo: conversation.id)
                    messagesQuery.getDocuments { (querySnapshot, error) in
                        if let error = error {
                            observer.onNext(.failure(error.localizedDescription))
                        } else if let documents = querySnapshot?.documents.map ({ $0.data() }) {
                            let messages = documents |> map { $0 |> messageFromDocument }
                            observer.onNext(.messages(messages))
                        }
                    }
                }
            }
            return Disposables.create()
        }
    }
    /// Send a message
    /// - Parameter message: MeldMessage
    static func sendMessage(message: MeldMessage) -> Observable<ConversationServiceResult> {
        let document = Firestore.firestore().collection("Messages").document()
        return Observable<ConversationServiceResult>.create { observer in
            document.setData( message |> prop(\.id)({_ in document.documentID}) |> documentFromMessage) { (error) in
                if let error = error {
                    observer.onNext(.failure(error.localizedDescription))
                } else {
                    observer.onNext(.success)
                }
            }
            return Disposables.create()
        }.flatMap { result -> Observable<ConversationServiceResult> in
            return fetchConversation(id: message.conversationID).compactMap { $0 }.flatMap { conversation -> Observable<Bool> in
                let updatedConversation = conversation
                    |> prop(\.messages)({ $0.appending(document.documentID)})
                    |> prop(\.latestActivity)({_ in NSNumber(value: Date().timeIntervalSince1970) })
                return updateConversation(updatedConversation).map { convoResult -> Bool in
                    switch convoResult {
                    case .success:
                        return true
                    default: return false
                    }
                }
            }.map { updateResult -> ConversationServiceResult in
                return updateResult ? .success : .failure("Conversation Not Updated")
            }
        }
    }
    /// Delete a message
    /// - Parameter message: MeldMessage
    static func deleteMessage(message: MeldMessage) -> Observable<ConversationServiceResult> {
        return Observable.create { observer in
            let document = Firestore.firestore().collection("Messages").document(message.id)
            document.delete { (error) in
                if let error = error {
                    observer.onNext(.failure(error.localizedDescription))
                } else {
                    observer.onNext(.success)
                }
            }
            return Disposables.create()
        }
    }
    static func updateConversation(_ conversation: Conversation) -> Observable<ConversationServiceResult> {
        return Observable.create { observer in
            Firestore.firestore().collection("Conversations").document(conversation.id).updateData(conversation |> documentFromConversation) { (error) in
                if let error = error {
                    observer.onNext(.failure(error.localizedDescription))
                } else {
                    observer.onNext(.success)
                }
            }
            return Disposables.create()
        }
    }
    /// Start a conversation with user
    /// - Parameter conversant: MeldUser
    static func startConversation(conversant: MeldUser) -> Observable<ConversationServiceResult> {
       
        guard let user = UserService.immediateCurrentUser() else {
            return .empty()
        }
      
        let currentUser = Observable.just(user)
        let conversantUser = Observable.just(conversant)
        
        return Observable.zip(currentUser, conversantUser).flatMap { user, conversant -> Observable<ConversationServiceResult> in
                newConversation(user: user, conversant: conversant)
                .flatMap { result -> Observable<ConversationServiceResult> in
                    switch result {
                    case .success(let conversation):
                        return Observable.zip(updateConversationsList(userId: user.id,
                                                conversationIds: user.currentConversations.appending(conversation.id)),
                        updateConversationsList(userId: conversant.id,
                                                conversationIds: conversant.currentConversations.appending(conversation.id)), Observable.just(conversation))
                        .flatMap { updateUser, updatePerson, conversation -> Observable<Bool> in
                            return Observable.just(updateUser && updatePerson).flatMap { result -> Observable<Bool> in
                                if result {
                                    let update = user
                                        |> prop(\.currentMatch)({ _ in conversant.id})
                                        |> prop(\.currentConversations)({ $0.appending(conversation.id) })
                                    return UserService.setCurrentUser(user: update)
                                } else{
                                    return .just(result)
                                }
                            }
                        }.map { result -> ConversationServiceResult in
                            return result ? .success : .failure("Error creating conversation")
                        }
                    default:
                        switch result {
                        case .success:
                            return Observable.just(.success)
                        case .failure(let error):
                            return Observable.just(.failure(error.localizedDescription))
                        }
                    }
            }
        }
    }
    
    static func newConversation(user: MeldUser, conversant: MeldUser) -> Observable<Result<Conversation, Error>> {
        Observable<Result<Conversation, Error>>.create { observer in
            let document = Firestore.firestore().collection("Conversations").document()
            
            let conversation = Conversation.new
            |> prop(\.id)({_ in document.documentID})
            |> prop(\.conversantA_id)({_ in user.id})
            |> prop(\.conversantB_id)({_ in conversant.id})
            |> prop(\.conversantA_name)({_ in user.name})
            |> prop(\.conversantB_name)({_ in conversant.name})
            document.setData( conversation |> documentFromConversation) { error in
                if let error = error {
                    observer.onNext(.failure(error))
                } else {
                    observer.onNext(.success(conversation))
                }
            }
            return Disposables.create()
        }
    }
    static func updateConversationsList(userId: String, conversationIds: [String]) ->  Observable<Bool> {
        Observable<Bool>.create { observer in
            Firestore.firestore().collection("Users").document(userId).updateData(["currentConversations": conversationIds]) { (error) in
                observer.onNext(error == nil)
            }
            return Disposables.create()
        }
    }
    static func observeConversation(id: String) -> Observable<Conversation?> {
        return Observable.create { observer in
            Firestore.firestore().collection("Conversations").document(id).addSnapshotListener { (snapshot, error) in
                if let _ = error {
                    observer.onNext(nil)
                } else {
                    if let document = snapshot?.data() {
                        let conversation = document |> conversationFromDocument
                        observer.onNext(conversation)
                    } else {
                        observer.onNext(nil)
                    }
                }
            }
            return Disposables.create()
        }
    }
    /// Finds a users conversation
    static func fetchConversation(id: String) -> Observable<Conversation?> {
        guard !isMockService else {
            if mockShouldSucceed {
                return .just(mockConversation())
            } else {
                return .just(nil)
            }
        }
        return Observable.create { observer in
            Firestore.firestore().collection("Conversations").document(id).getDocument { (snapshot, error) in
                if let _ = error {
                    observer.onNext(nil)
                } else {
                    if let document = snapshot?.data() {
                        let conversation = document |> conversationFromDocument
                        observer.onNext(conversation)
                    } else {
                        observer.onNext(nil)
                    }
                }
            }
            return Disposables.create()
        }
    }
    /// Finds a users conversation
    static func fetchUserConversations() -> Observable<FetchConversationResult> {
        guard !isMockService else {
            if mockShouldSucceed {
                return .just(.success([mockConversation()]))
            } else {
                return .just(.failure("No Reason"))
            }
        }
        return UserService.currentUser().map { $0.user }.compactMap { $0 }.flatMap { user -> Observable<FetchConversationResult> in
            guard user.currentConversations.notEmpty() else { return Observable.just(.noConversations)}
            
            return Observable.create { observer in
                Firestore.firestore().collection("Conversations").whereField("id", in: user.currentConversations).getDocuments { (snapshot, error) in
                    if let error = error {
                        observer.onNext(.failure(error.localizedDescription))
                    } else if let docs = snapshot?.documents.map({ $0.data() }) {
                        let conversations = docs.map { $0 |> conversationFromDocument }
                        observer.onNext(.success(conversations))
                    } else {
                        observer.onNext(.noConversations)
                    }
                }
                return Disposables.create()
            }
        }
    }
    /// Ends a conversation with a user
    /// - Parameter id: String
    static func endConversation(conversation: Conversation, conversant: MeldUser) -> Observable<ConversationServiceResult> {
        guard !isMockService else {
            return .just(.success)
        }
        
        let removeConversantConversation = Observable<ConversationServiceResult>.create { observer in
            let filteredConversations = conversant.currentConversations |> removing(conversation.id)
            Firestore.firestore().collection("Users").document(conversant.id).updateData(["currentConversations": filteredConversations], completion: { error in
                if let error = error {
                    observer.onNext(.failure(error.localizedDescription))
                } else {
                    observer.onNext(.success)
                }
            })
            return Disposables.create()
        }
        
        let removeUserConversation = UserService.currentUser().map { $0.user }.compactMap { $0 }.flatMap { user -> Observable<Bool> in
            let updatedUser = user
                |> prop(\.currentMatch)({ _ in ""})
                |> prop(\.currentConversations)({$0.removing(element: conversation.id)})
            return Observable.zip(UserService.setCurrentUser(user: updatedUser),
                                  UserService.updateRemoteCurrentUser(user: updatedUser))
                .map { $0 && $1 }
        }
        let deleteConversation = Observable<ConversationServiceResult>.create { observer in
            Firestore.firestore().collection("Conversations").document(conversation.id).delete { error in
                if let error = error {
                    observer.onNext(.failure(error.localizedDescription))
                } else {
                    observer.onNext(.success)
                }
            }
            return Disposables.create()
        }.do(onNext: { _ in
            let batch = Firestore.firestore().batch()
            conversation.messages.forEach { messageID in
                let document = Firestore.firestore().collection("Messages").document(messageID)
                batch.deleteDocument(document)
            }
            batch.commit()
        })
        
        return Observable.zip(removeConversantConversation, removeUserConversation, deleteConversation).map { args -> ConversationServiceResult in
            let (didRemoveConversantData, didRemoveUserData, didDeleteConversation) = args
            switch (didRemoveConversantData, didDeleteConversation, didRemoveUserData) {
            case (.success, .success, true):
                return .success
            default:
                return .failure("Conversation did not delete")
            }
        }
    }
    
    static func blockUser(id: String) -> Observable<Bool> {
        return UserService.currentUser().take(1).map { $0.user }.compactMap { $0 }.flatMap { user -> Observable<Bool> in
            let updatedUser = user |> prop(\.blockList)({ users in
                let isBlocked = users.contains(id)
                if isBlocked {
                    return users |> removing(id)
                } else {
                    return users |> appending(id)
                }
            })
            return Observable.zip(UserService.setCurrentUser(user: updatedUser), UserService.updateRemoteCurrentUser(user: updatedUser)).map { $0 && $1 }
        }
    }
    static let documentFromConversation: (Conversation) -> [String: Any] = { conversation in
        return [String: Any]()
                    |> prop(\[String: Any].["creationDate"])({ _ in  conversation.creationDate})
                    |> prop(\[String: Any].["id"])({ _ in conversation.id})
                    |> prop(\[String: Any].["conversantA_id"])({ _ in conversation.conversantA_id})
                    |> prop(\[String: Any].["conversantB_id"])({ _ in conversation.conversantB_id})
                    |> prop(\[String: Any].["conversantA_name"])({ _ in conversation.conversantA_name})
                    |> prop(\[String: Any].["conversantB_name"])({ _ in conversation.conversantB_name})
                    |> prop(\[String: Any].["messages"])({ _ in conversation.messages})
                    |> prop(\[String: Any].["access_tokenA"])({ _ in conversation.access_tokenA})
                    |> prop(\[String: Any].["access_tokenB"])({ _ in conversation.access_tokenB})


        

        
    }
    static let conversationFromDocument: (Document) -> Conversation = { document in
        return  Conversation.new
                    |> prop(\.creationDate)({ _ in  document["creationDate"] as Any |> asNumber })
                    |> prop(\.id)({ _ in document["id"] as Any |> asString })
                    |> prop(\.conversantA_id)({ _ in document["conversantA_id"] as Any |> asString})
                    |> prop(\.conversantB_id)({ _ in document["conversantB_id"] as Any |> asString})
                    |> prop(\.conversantA_name)({ _ in document["conversantA_name"] as Any |> asString})
                    |> prop(\.conversantB_name)({ _ in document["conversantB_name"] as Any |> asString})
                    |> prop(\.messages)({ _ in document["messages"] as Any |> asStringSequence })
                    |> prop(\.access_tokenA)({ _ in document["access_tokenA"] as Any |> asString})
                    |> prop(\.access_tokenB)({ _ in document["access_tokenB"] as Any |> asString})
        
    }
    static let documentFromMessage: (MeldMessage) -> [String: Any] = { message in
        return [String: Any]()
                    |> prop(\[String: Any].["creationDate"])({ _ in  message.creationDate})
                    |> prop(\[String: Any].["id"])({ _ in message.id})
                    |> prop(\[String: Any].["conversationID"])({ _ in message.conversationID})
                    |> prop(\[String: Any].["senderID"])({ _ in message.senderID})
                    |> prop(\[String: Any].["receiverID"])({ _ in message.receiverID})
                    |> prop(\[String: Any].["senderImageURL"])({ _ in message.senderImageURL})
                    |> prop(\[String: Any].["receiverImageURL"])({ _ in message.receiverImageURL})
                    |> prop(\[String: Any].["senderName"])({ _ in message.senderName})
                    |> prop(\[String: Any].["receiverName"])({ _ in message.receiverName})
                    |> prop(\[String: Any].["messageImageURL"])({ _ in message.messageImageURL})
                    |> prop(\[String: Any].["message"])({ _ in message.message})
        
    }
    static let messageFromDocument: (Document) -> MeldMessage = { document in
        let setInfo1: (MeldMessage) -> MeldMessage = { message in
        return message
                |> prop(\.creationDate)({ _ in  document["creationDate"] as Any |> asNumber })
                |> prop(\.id)({ _ in document["id"] as Any |> asString })
                |> prop(\.conversationID)({_ in document["conversationID"] as Any |> asString})
                |> prop(\.senderID)({_ in document["senderID"] as Any |> asString})
        }
        let setInfo2: (MeldMessage) -> MeldMessage = { message in
        return message
                |> prop(\.receiverID)({_ in document["receiverID"] as Any |> asString})
                |> prop(\.senderImageURL)({_ in document["senderImageURL"] as Any |> asString})
                |> prop(\.receiverImageURL)({_ in document["receiverImageURL"] as Any |> asString})
                |> prop(\.senderName)({_ in document["senderName"] as Any |> asString})
        }
        let setInfo3: (MeldMessage) -> MeldMessage = { message in
        return message
                |> prop(\.receiverName)({_ in document["receiverName"] as Any |> asString})
                |> prop(\.messageImageURL)({_ in document["messageImageURL"] as Any |> asString})
                |> prop(\.message)({_ in document["message"] as Any |> asString})
        }
        return MeldMessage.new
                |> setInfo1
                <> setInfo2
                <> setInfo3
                
                
    }
    static let mockConversation: () -> Conversation = {
        return Conversation(creationDate: Date().timeIntervalSince1970 |> number,
                            latestActivity: Date().timeIntervalSince1970 |> number,
                            id: "",
                            conversantA_id: "",
                            conversantB_id: "",
                            conversantA_name: "Blake",
                            conversantB_name: "Zahyria",
                            conversantA_isTyping: false,
                            conversantB_isTyping: false,
                            messages: [],
                            access_tokenA: "",
                            access_tokenB: "")
    }
    static let messages: [String] = ["Hi how are you",
                                     "what is your name",
                                     "what would you like to do this evening",
                                     "I'm a carpenter",
                                     "I like Harry Potter Too!",
                                     "lets grab a drink",
                                     "You have a beautiful mustache"]
    static let mockMessage: () -> MeldMessage = {
        let random = Int(arc4random() % 6)
        return MeldMessage(creationDate: NSNumber(value: Date().timeIntervalSince1970), id: "", conversationID: "", senderID: "", receiverID: "", senderImageURL: "", receiverImageURL: "", senderName: "", receiverName: "", messageImageURL: "", message: messages[random])
    }
    ///Returns true if message contains foul language
    static func auditMessage(_ message: String?) -> Observable<Bool> {
        guard let message = message else { return .empty() }
        let forbiddenWords = [" pussy ", " dick ", " ass ",
                              "fuck ", "damn ", " shit ",
                              " whore ", " fag ", " dike ",
                              " cunt ", " bitch ", " ho "]
        let isFoulLanguage = forbiddenWords |> map { message.lowercased().contains($0)} |> reduce(false, { $0 || $01 })
        return .just(isFoulLanguage)
    }
}

