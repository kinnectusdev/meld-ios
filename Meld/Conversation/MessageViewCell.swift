//
//  MessageViewCell.swift
//  Meld
//
//  Created by blakerogers on 1/28/20.
//  Copyright © 2020 com.meld.kinnectus. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import UtilitiesPackage

class MessageViewCell: UICollectionViewCell {
    static let identifier: String = "MessageViewCell"
    private let background = UIView()
                                |> roundedGreyShadowed(20)
    private let messageLabel = UILabel()
                                |> textColored(appDarkGrey)
                                >>> textFont(appFont(12))
                                >>> textCenterAligned
                                >>> asLabel
    
    private let messageImage = UIImageView()
    private let messageImageContainer = UIView()
        |> whiteBackground
        >>> width_Height(appFrame.width*0.9, 400)
    private let timeStamp = UILabel()
                                |> textColored(appDarkGrey)
                                >>> textFont(appFont(12))
                                >>> asLabel
    override func layoutSubviews() {
        super.layoutSubviews()
        setViews()
    }
    func setMessage(message: String?, timestamp: String, image: String?, isSentMessage: Bool) {
        
        
        let messageRect = (message |> otherwise("")).rectForText(width: appFrame.width, textSize: 15)
        
        messageLabel.text = message
        timeStamp.text = timestamp
        
//        if let image = image {
//            messageImage.kf.setImage(with: URL(string: image))
//        }
        
        if isSentMessage {
            let width = max(messageRect.width*1.1, 100)
            background.frame = CGRect(x: contentView.frame.width - width - 10, y: 15, width: width , height: max(40, messageRect.height))
            messageLabel.frame = CGRect(x: contentView.frame.width - messageRect.width - 20 , y: 15, width: messageRect.width , height: max(40, messageRect.height))
            timeStamp.frame = CGRect(x: contentView.frame.width - 120, y: 0, width: 100, height: 12)
        } else {
            let width = max(messageRect.width*1.1, 100) 
            background.frame = CGRect(x: 10, y: 15, width: width, height: max(40, messageRect.height))
            messageLabel.frame = CGRect(x: 20, y: 15, width: messageRect.width , height: max(40, messageRect.height))
            timeStamp.frame = CGRect(x: 20, y: 0, width: 100, height: 12)
        }
    }
    private func setViews() {
        contentView.add(views: background, messageLabel, timeStamp)
    }
}
