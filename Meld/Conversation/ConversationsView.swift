//
//  ConversationsView.swift
//  Meld
//
//  Created by Blake Rogers on 6/14/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UIKit
import UtilitiesPackage

class ConversationsView: UIView {
    private var presenter: ConversationsPresenter!
        
    convenience init(presenter: ConversationsPresenter) {
        self.init()
        self.presenter = presenter
        self.setViews()
    }
    
    private func setViews() {
            let view = UIView()
            |> fill
            |> appendSubView(presenter.collection |> fill)
            >>> appendSubView(presenter.videoChatButton
                |> alignToRight(-20)
                >>> alignToBottom(-100)
                >>> styledTitle(videoFontIcon(size: 18, color: appDarkGrey))
                >>> roundedGreyShadowed(30)
                >>> width_Height(60, 60))
            >>> appendSubView(presenter.displayKeyboardButton
                |> named("keyboard_button")
                >>> alignToRight(-20)
                >>> alignToBottom(-20)
                >>> styledTitle(darkGreyAttributedString("+", 35))
                >>> roundedGreyShadowed(30)
                >>> width_Height(60, 60))
            >>> appendSubView(presenter.endConversationButton
                |> alignToSibling("keyboard_button", constraint: alignRightToSiblingLeft(-10))
                >>> alignToSibling("keyboard_button", constraint: alignedToSiblingBottom)
                >>> styledTitle(dismissFontIcon(size: 18, color: appDarkGrey))
                >>> roundedGreyShadowed(30)
                >>> width_Height(60, 60))
            
            self.add(views: view)
        
    }
}
