//
//  ConversationsRouter.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UIKit
import UtilitiesPackage

final class ConversationsRouter {
    func displayKeyboard(messageManager: MessageManagerIdentity) {
        
        let view = KeyboardRouter.presentation(manager: messageManager)
        currentController().present(view, animated: false, completion: nil)
    }
    
    func displayEndConversation(conversationsInteractor: ConversationsInteractorIdentity) {
        let view = EndConversationRouter.presentation(conversationsInteractor: conversationsInteractor)
        currentController().present(view, animated: true, completion: nil)
    }
    
    func displayConversant(conversant: MeldUser, manager: ConversantManagerIdentity) {
        let view = ConversantViewRouter.presentation(conversant: conversant, conversantManager: manager)
        currentController().present(view, animated: true, completion: nil)
    }
    
    func displayVideoChat(conversation: Conversation) {
        let controller = videoChatController(conversation: conversation)
        currentController().present(controller, animated: true, completion: nil)
    }
    
    static func presentation(interactor: ConversationsInteractorIdentity) -> UIView {
        ConversationsView(presenter: ConversationsPresenter(interactor: interactor))
    }
}
