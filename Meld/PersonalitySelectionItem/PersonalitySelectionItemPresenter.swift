//
//  PersonalitySelectionItemPresenter.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import UtilitiesPackage
import UIKit

final class PersonalityTypeSelectionItemPresenter {
    private let bag = DisposeBag()
    private let interactor: PersonalityTypeSelectionItemInteractorIdentity
    private let router: PersonalityTypeSelectionItemRouter
    
    let titleLabel = UILabel()
    let list = UIScrollView()
    let summary: UIView
    let compatibleTypes: UIView
    let proCons: [UIView]
    let image = UIImageView()
    let imageScreen = UIView() |> whiteBackground

    init(interactor: PersonalityTypeSelectionItemInteractorIdentity) {
        self.interactor = interactor
        
        self.router = PersonalityTypeSelectionItemRouter()
        
        self.summary = PersonalitySummaryItem(presenter: PersonalitySummaryPresenter(interactor: PersonalitySummaryInteractor(personalityType: interactor.observePersonalityType())))
        
        self.compatibleTypes = CompatiblePersonalityTypesRouter.presentation(type: interactor.observePersonalityType())
        
        let contentSize = interactor.observeProCons()
            
            .map { 60 + ($0.description.rectForText(width: appFrame.width, textSize: 14).height) }
            .reduce(500, { $0 + $1 })
        
        self.image.image = UIImage(named: interactor.observePersonalityIcon())
        
        self.list.contentSize = CGSize(width: appFrame.width, height: contentSize)
        
        self.proCons = interactor.observeProCons()
            .map { proCon -> UIView in
                PersonalityProConItem(presenter: PersonalityProConPresenter(interactor: PersonalityProConInteractor(proCon: proCon)))
                    |> width_Height(appFrame.width, proCon.description.rectForText(width: appFrame.width, textSize: 14).height + 50)
            }
        
        bag.insert(
            interactor.observeTitle().asDriver(onErrorJustReturn: .empty).drive(titleLabel.rx.text),
            list.rx.contentOffset.map { $0.y }.map { $0 >= 300 }.subscribe( onNext: { [weak self] isScrolledUp in
                UIView.animate(withDuration: 0.25) {
                    self?.titleLabel.textColor = isScrolledUp ? appBlack : .white
                    self?.imageScreen.alpha = isScrolledUp ? 1.0 : 0.0
                }
            })
        )
    }
}
