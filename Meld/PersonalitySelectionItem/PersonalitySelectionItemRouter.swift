//
//  PersonalitySelectionItemRouter.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import UIKit

final class PersonalityTypeSelectionItemRouter {
    
    func didSelectViewPersonalityType(type: PersonalityType) {
        
    }
}

extension PersonalityTypeSelectionItemRouter {
    
    static func presentation(personalityType: PersonalityType) -> UIView {
        PersonalityTypeSelectionItem(presenter: PersonalityTypeSelectionItemPresenter(interactor: PersonalityTypeSelectionItemInteractor(personalityType: personalityType)))
    }
}
