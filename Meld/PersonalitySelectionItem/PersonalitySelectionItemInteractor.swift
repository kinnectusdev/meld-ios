//
//  PersonalitySelectionItemInteractor.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift

protocol PersonalitySelectionItemManager {
    
    func didSelectPersonalityItem(type: PersonalityType)
    
}
protocol PersonalityTypeSelectionItemInteractorIdentity {
    func observeTitle() -> Observable<String>
    func observePersonalityType() -> PersonalityType
    func observeProCons() -> [PersonalityProCon]
    func observePersonalityIcon() -> String
}

final class PersonalityTypeSelectionItemInteractor {
    
    private let personalityType: PersonalityType
    
    init(personalityType: PersonalityType) {
        self.personalityType = personalityType
    }
}

extension PersonalityTypeSelectionItemInteractor: PersonalityTypeSelectionItemInteractorIdentity {
   
    func observeTitle() -> Observable<String> {
        .just(personalityType.title)
    }
    
    func observePersonalityType() -> PersonalityType {
        personalityType
    }
    
    func observeProCons() -> [PersonalityProCon] {
        let strengths: [PersonalityProCon] = personalityType.strengths
        let weaknesses: [PersonalityProCon] = personalityType.weakness
        return strengths.appendingAll(weaknesses)
    }
    
    func observePersonalityIcon() ->String {
       personalityType.title
    }
}
