//
//  PersonalitySelectionItem.swift
//  Meld
//
//  Created by Blake Rogers on 8/15/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import UIKit
import UtilitiesPackage

final class PersonalityTypeSelectionItem: UIView {
    
    private var presenter: PersonalityTypeSelectionItemPresenter!
   
    private func setViews() {
        let view = UIView()
            |> whiteBackground
            |> fill
            |> appendSubView(UIView()
                |>  alignedToCenterX
                >>> alignToTop(5)
                >>> width_Height(300, 300)
                >>> diamondShadowed
                >>> appendSubView(presenter.image
                                    |> scaledToFill
                                    >>> rounded(10)
                                    >>> isClipping
                                    >>> fill))
            |> appendSubView(presenter.imageScreen |> framed(CGRect(x: 0, y: 0, width: appFrame.width, height: appFrame.width)))
            |> appendSubView(presenter.titleLabel
                                |> named("title")
                                |> textColored(.white)
                                |> backgroundColored(.clear)
                                |> textFont(appFontFormal(20))
                                |> textCenterAligned
                                |> alignToTop(16)
                                |> alignedToCenterX)
            |> appendSubView(presenter.list
                            |> alignedToLeftBottomRight
                            |> alignToSibling("title", constraint: alignTopToSiblingBottom())
                            |> appendSubView(presenter.summary
                            |> framed(CGRect(x: 0, y: 300, width: appFrame.width, height: 130)))
                            |> appendSubView(presenter.compatibleTypes |> framed(CGRect(x: 0, y: 430, width: appFrame.width, height: 120)))
                            |> appendSubView(UIView()
                                    |> framed(CGRect(x: 0, y: 550, width: appFrame.width, height: 30))
                                    |> whiteBackground
                                    |> appendSubView(UILabel()
                                            |> named("title")
                                            |> titled("Common Behavioral Traits")
                                            |> textFont(appFontFormal(20))
                                            |> textColored(appDarkGrey)
                                            |> framed(CGRect(x: 20, y: 0, width: appFrame.width, height: 20)))
                                    |> appendSubView(UIView()
                                                        |> named("underline")
                                                        |> backgroundColored(appSilver)
                                                        |> framed(CGRect(x: 20, y: 28, width: appFrame.width - 40, height: 1))))
                                    |> appendSubviewsVertically(presenter.proCons, 0, 580)
                                    |> alignSubviewsToCenterX(presenter.proCons))
        self.add(views: view)
    }
}
extension PersonalityTypeSelectionItem {
    convenience init(presenter: PersonalityTypeSelectionItemPresenter) {
        self.init()
        self.presenter = presenter
        self.setViews()
    }
}
