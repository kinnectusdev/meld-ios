//
//  PersonalitySummaryPresenter.swift
//  Meld
//
//  Created by Blake Rogers on 8/11/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import RxSwift
import UIKit

final class PersonalitySummaryPresenter {
    private let bag = DisposeBag()
    private let interactor: PersonalitySummaryInteractorIdentity
    
    let titleLabel = UILabel()
    let archetypeLabel = UILabel()
    let introvertExtravertTraitLabel = UILabel()
    let intuitiveSensingTraitLabel = UILabel()
    let thinkingFeelingTraitLabel = UILabel()
    let judgingPerceivingTraitLabel = UILabel()
    let summaryLabel = UILabel()
    
    init(interactor: PersonalitySummaryInteractorIdentity) {
        
        self.interactor = interactor
        
        bag.insert(
            interactor.observeTitle().asDriver(onErrorJustReturn: .empty).drive(titleLabel.rx.text),
            interactor.observeTraits().map { $0.0.title }.asDriver(onErrorJustReturn: .empty).drive(introvertExtravertTraitLabel.rx.text),
//            interactor.observeTraits().map { $0.0.color }.asDriver(onErrorJustReturn: appBlack).drive(introvertExtravertTraitLabel.rx.backgroundColor),

            interactor.observeTraits().map { $0.1.title }.asDriver(onErrorJustReturn: .empty).drive(intuitiveSensingTraitLabel.rx.text),
//            interactor.observeTraits().map { $0.1.color }.asDriver(onErrorJustReturn: appBlack).drive(intuitiveSensingTraitLabel.rx.backgroundColor),
            
            interactor.observeTraits().map { $0.2.title }.asDriver(onErrorJustReturn: .empty).drive(thinkingFeelingTraitLabel.rx.text),
//            interactor.observeTraits().map { $0.2.color }.asDriver(onErrorJustReturn: appBlack).drive(thinkingFeelingTraitLabel.rx.backgroundColor),

            interactor.observeTraits().map { $0.3.title }.asDriver(onErrorJustReturn: .empty).drive(judgingPerceivingTraitLabel.rx.text),
//            interactor.observeTraits().map { $0.3.color }.asDriver(onErrorJustReturn: appBlack).drive(judgingPerceivingTraitLabel.rx.backgroundColor),

            interactor.observeSummary().asDriver(onErrorJustReturn: .empty).drive(summaryLabel.rx.text)
        )
    }
}
