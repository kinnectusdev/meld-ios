//
//  PersonalitySummaryItem.swift
//  Meld
//
//  Created by Blake Rogers on 8/11/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import UIKit
import UtilitiesPackage

final class PersonalitySummaryItemRouter {

}

extension PersonalitySummaryItemRouter {
    static func presentation(personalityType: PersonalityType) -> UIView {
        PersonalitySummaryItem(presenter: PersonalitySummaryPresenter(interactor: PersonalitySummaryInteractor(personalityType: personalityType)))
    }
}

final class PersonalitySummaryItem: UIView {
    
    private var presenter: PersonalitySummaryPresenter!
    
    private func setViews() {
        let view = UIView()
            |> fill
            |> whiteBackground
            >>> appendSubView(presenter.titleLabel
                                |> named("title")
                                >>> textColored(appBlack)
                                >>> textCenterAligned
                                >>> textFont(appFontFormal(18))
                                |> alignToTop(20)
                                |> alignToCenterX())
            >>> appendSubView(presenter.intuitiveSensingTraitLabel
                                |> named("i_sLabel")
                                |> textCenterAligned
                                |> textColored(appBlack)
                                |> textFont(appFontBold(12))
                                |> alignToSibling("title", constraint: alignTopToSiblingBottom(10))
                                |> alignRightToCenter(-4))
            >>> appendSubView(presenter.introvertExtravertTraitLabel
                                |> named("i_eLabel")
                                |> textColored(appBlack)
                                |> textFont(appFontBold(12))
                                |> textCenterAligned
                                |> alignToSibling("i_sLabel", constraint: alignRightToSiblingLeft(-8))
                                |> alignToSibling("title", constraint: alignTopToSiblingBottom(10)))
            >>> appendSubView(presenter.thinkingFeelingTraitLabel
                                |> named("t_fLabel")
                                |> textCenterAligned
                                |> textFont(appFontBold(12))
                                |> textColored(appBlack)
                                |> alignLeftToCenter(4)
                                |> alignToSibling("title", constraint: alignTopToSiblingBottom(10)))
            >>> appendSubView(presenter.judgingPerceivingTraitLabel
                                |> named("j_pLabel")
                                |> textCenterAligned
                                |> textFont(appFontBold(12))
                                |> textColored(appBlack)
                                |> alignToSibling("t_fLabel", constraint: alignLeftToSiblingRight(8))
                                |> alignToSibling("title", constraint: alignTopToSiblingBottom(10)))
            >>> appendSubView(presenter.summaryLabel
                                |> unlimitedLines
                                |> textCenterAligned
                                |> textFont(appFont(12))
                                |> alignToRight(-20)
                                |> alignToLeft(20)
                                |> alignToSibling("i_eLabel", constraint: alignTopToSiblingBottom(10)))
        
        self.add(views: view)
    }
}
extension PersonalitySummaryItem {
    convenience init(presenter: PersonalitySummaryPresenter) {
        self.init()
        self.presenter = presenter
        self.setViews()
    }
    
    func configure(presenter: PersonalitySummaryPresenter) {
        self.presenter = presenter
        self.setViews()
    }
}
