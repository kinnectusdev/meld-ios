//
//  PersonalitySummaryInteractor.swift
//  Meld
//
//  Created by Blake Rogers on 8/11/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import RxSwift
import UtilitiesPackage

protocol PersonalitySummaryInteractorIdentity {
    func observeTitle() -> Observable<String>
    func observeGroup() -> Observable<PersonalityGroup>
    func observeTraits() -> Observable<(PersonalityTrait, PersonalityTrait, PersonalityTrait, PersonalityTrait)>
    func observeSummary() -> Observable<String>
}

final class PersonalitySummaryInteractor {
    private let personalityType: PersonalityType
    init(personalityType: PersonalityType) {
        self.personalityType = personalityType
    }
}
extension PersonalitySummaryInteractor: PersonalitySummaryInteractorIdentity {
    func observeTitle() -> Observable<String> {
        .just(personalityType.personalityGroup.title)
    }
    
    func observeTraits() -> Observable<(PersonalityTrait, PersonalityTrait, PersonalityTrait, PersonalityTrait)> {
        .just(personalityType.traits |> tupleFourFromArray)
    }
    
    func observeSummary() -> Observable<String> {
        .just(personalityType.description)
    }
    func observeGroup() -> Observable<PersonalityGroup> {
        .just(personalityType.personalityGroup)
    }
}

