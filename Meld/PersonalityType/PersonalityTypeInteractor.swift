//
//  PersonalityTypeInteractor.swift
//  Meld
//
//  Created by Blake Rogers on 8/11/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import RxSwift
import RxCocoa

protocol PersonalityTypeInteractorIdentity {
    func observeTitle() -> Observable<String>
    func observePersonalityType() -> PersonalityType
    func observeProCons() -> [PersonalityProCon]
    func observePersonalityIcon() -> String
}
final class PersonalityTypeInteractor {
    private let personalityType: PersonalityType
    init(personalityType: PersonalityType) {
        self.personalityType = personalityType
    }
}
extension PersonalityTypeInteractor: PersonalityTypeInteractorIdentity {
    func observeTitle() -> Observable<String> {
        .just(personalityType.title)
    }
    func observePersonalityType() -> PersonalityType {
        personalityType
    }
    func observeProCons() -> [PersonalityProCon] {
        let strengths: [PersonalityProCon] = personalityType.strengths
        let weaknesses: [PersonalityProCon] = personalityType.weakness
        return strengths.appendingAll(weaknesses)
    }
    func observePersonalityIcon() ->String {
       personalityType.title
    }
}
