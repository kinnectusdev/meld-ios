//
//  CompatiblePersonalityTypesItem.swift
//  Meld
//
//  Created by Blake Rogers on 8/12/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UIKit

final class CompatiblePersonalityTypeItem: UIView {
    private var presenter: CompatiblePersonalityTypesPresenter!
    private func setViews() {
        let view = UIView()
            |> whiteBackground
            |> fill
            |> appendSubView(UILabel()
                                |> named("title")
                                |> titled("Compatible Personalities")
                                |> textFont(appFontFormal(20))
                                |> textColored(appDarkGrey)
                                |> alignToLeft(20)
                                |> alignToTop(20))
            |> appendSubView(UIView()
                                |> named("underline")
                                |> backgroundColored(appSilver)
                                |> alignToLeft(20)
                                |> alignToRight(-20)
                                |> height(1)
                                |> alignToSibling("title", constraint: alignTopToSiblingBottom(4)))
            |> appendSubView(presenter.list
                                |> alignToSibling("underline", constraint: alignTopToSiblingBottom(4))
                                |> alignToLeft(20)
                                |> alignedToRight
                                |> alignedToBottom
                                |> appendSubviewsHorizontally(presenter.types, 10, 10))
        
        self.add(views: view)
    }
}
extension CompatiblePersonalityTypeItem {
    convenience init(presenter: CompatiblePersonalityTypesPresenter) {
        self.init()
        self.presenter = presenter
        self.setViews()
    }
}
