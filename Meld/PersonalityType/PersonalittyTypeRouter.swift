//
//  PersonalittyTypeRouter.swift
//  Meld
//
//  Created by Blake Rogers on 8/11/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import UIKit
import UtilitiesPackage

final class PersonalityTypeRouter {
    func dismiss() {
        currentController().dismiss(animated: true, completion: nil)
    }
}
extension PersonalityTypeRouter {
    static func presentation(personalityType: PersonalityType) -> UIViewController {
        PersonalityTypeView(presenter: PersonalityTypePresenter(interactor: PersonalityTypeInteractor(personalityType: personalityType)))
    }
}
