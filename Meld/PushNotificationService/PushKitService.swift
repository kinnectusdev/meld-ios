//
//  PushKitService.swift
//  Meld
//
//  Created by blakerogers on 5/24/20.
//  Copyright © 2020 com.meld.kinnectus. All rights reserved.
//

import UIKit
import PushKit
import CallKit
class PushKitService: NSObject {
    override init() {
        super.init()
        let mainQueue = DispatchQueue.main
        let registry = PKPushRegistry(queue: mainQueue)
        registry.delegate = self
        /// This value must be updated last or else a registration failure is reported to the delegate
        registry.desiredPushTypes = [PKPushType.voIP]
    }
}
extension PushKitService: PKPushRegistryDelegate {
    //Handle updated push credentials
    public func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for type: PKPushType) {
        //Store updated credentials on device
        UserDefaults.standard.setValue(pushCredentials.token.hexString, forKey: "device_pushkit_token")
    }
    //Handle incoming pushes
    public func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType, completion: @escaping () -> Void) {
        //Process the received push
        let backgroundTaskIdentifier = UIApplication.shared.beginBackgroundTask(expirationHandler: nil)
            AppCoordinator.shared.displayIncomingCall(
            uuid: UUID(),
            payload: payload.dictionaryPayload,
            hasVideo: true
          ) { _ in
            UIApplication.shared.endBackgroundTask(backgroundTaskIdentifier)
          }
    }
}
