//
//  PushNotificationService.swift
//  Meld
//
//  Created by blakerogers on 11/18/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import Foundation
import Firebase
import UserNotifications
import RxSwift
import RxCocoa
import FirebaseMessaging
import UtilitiesPackage

enum PushNotificationError: Error {
    case failedWithMessage(String)
    case getTokens
}
public struct Message {
    let aps: APS
    public let customKey: String
    public let contentId: String
    public init(info: [AnyHashable : Any]) {
        self.aps = APS(info: info["aps"] as? [AnyHashable: Any])
        self.customKey = info["customKey"] as? String ?? ""
        self.contentId = info["contentId"] as? String ?? ""
    }
}
public struct APS {
    let alert: Alert
    let badge: Int
    init(info: [AnyHashable : Any]?) {
        self.alert = Alert(info: info?["alert"] as? [AnyHashable : Any])
        self.badge = info?["badge"] as? Int ?? 0
    }
}
public struct Alert {
    let body: String
    let title: String
    let contentID: String
    init(info: [AnyHashable : Any]?) {
        self.body = info?["body"] as? String ?? ""
        self.title = info?["title"] as? String ?? ""
        self.contentID = info?["contentID"] as? String ?? ""
    }
}
public enum AppNotificationKeys: String {
    case receivedMessage = "received_message"
    case joinInvitation = "join_invitation"
    case newMatchesAvaiable = "new_matches_available"
    case newMatchOpportunity = "new_match_opportunity"
    case dateInvitation = "date_invitation"
    case dateAccepted = "date_accepted"
    case dateCancelled = "date_cancelled"
    case conversationEnded = "conversaton_ended"
    case scheduledDateUpdate = "date_details_changed"
    case scheduledDateLocalReminder = "date_reminder"
    case appAnnouncement = "app_announcement"//Used to track how many users have opened app due to app announcement
}
public class PushNotificationService: NSObject, MessagingDelegate, UNUserNotificationCenterDelegate {
    let serviceStatus = PublishRelay<(String, String)>()
    override init() {
        super.init()
        UNUserNotificationCenter.current().delegate = self
        Messaging.messaging().delegate = self
    }
    public func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        guard let fcmToken = fcmToken else {
            return
        }
        serviceStatus.accept((fcmToken, Messaging.messaging().apnsToken?.hexString ?? ""))
    }
    public func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print(notification)
    }
    public func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        PushNotificationService.didReceiveNotification(notification: response.notification.request.content.userInfo)
        
    }
}

extension PushNotificationService {
    struct PushError: Error {
        let message: String
    }
    static var isRegisteredForNotifications: Bool {
        return UIApplication.shared.isRegisteredForRemoteNotifications
    }
    static func didRegisterForNotifications( deviceToken: Data) {
        Messaging.messaging().setAPNSToken(deviceToken, type: .prod)
        let token = deviceToken.hexString
        let defaults = UserDefaults.standard
        defaults.set(token, forKey: "deviceToken")
        //At development time we use .sandbox
        let fcmToken = Messaging.messaging().fcmToken ?? ""
        defaults.set(fcmToken, forKey: "fcmToken")
        defaults.synchronize()
    }

    public static func didReceiveNotification( notification: [AnyHashable:Any]) {
        if let category = notification["category"] as? String {
            switch AppNotificationKeys.init(rawValue: category) {
            case .receivedMessage:///Open to chate page
                break
            case .joinInvitation:///Open to sign in
                break
            case .newMatchesAvaiable:///Open to home page
                break
            case .newMatchOpportunity:///Open to chat page
                break
            case .dateInvitation:///Open to scheduled date page
                break
            case .dateAccepted:///Open to scheduled date page
                break
            case .dateCancelled:///Open to scheduled date page
                break
            case .conversationEnded:///Open to chat page
                break
            case .scheduledDateUpdate:///Open to scheduled date page
                break
            case .scheduledDateLocalReminder:///Open to scheduled date page
                break
            case .appAnnouncement:///Open to home page
                break
            default: break
            }
        }
    }
}
extension PushNotificationService {
    struct NotificationDevice: Codable {
        let device: String
    }
    struct NotificationResponseResult: Codable {
        let sent: [NotificationDevice]
        let failed: [NotificationDevice]
    }
    struct NotificationResponse: Codable {
        let state: String
        let result: NotificationResponseResult
        let error: String
    }
    enum NotificationResult {
        case success
        case failure(String)
    }
    static func unregisterForNotifications() {
        UIApplication.shared.unregisterForRemoteNotifications()
    }
    static func voipPushServerURL(token: String, caller: String, convoId: String) -> String {
        return "https://meld-push-server.herokuapp.com/push/voip?token=\(token)&caller=\(caller)&convoId=\(convoId)"
    }
    static func notifyVideoCall(user: MeldUser, conversationID: String) -> Observable<NotificationResult> {
        guard user.pushKitToken.notEmpty() else {
            print("user not registered for notifications");
            return  .just(.failure("User Not Registered for Notifications"))
        }
        let session = URLSession.shared
        guard let currentUser = UserService.immediateCurrentUser(), let url = URL(string: "https://us-central1-meld-dd5f8.cloudfunctions.net/notifyVideoCall?userID=\(user.id)&caller=\(currentUser.name)&convoID=\(conversationID)") else {
            return .just(.failure("Invalid URL"))
        }
        return Observable.create { observer in
            let task = session.dataTask(with: url) { (data, response, error) in
                 if let error = error {
                     print(error.localizedDescription)
                    observer.onNext(.failure(error.localizedDescription))
                 } else if let data = data {
                    do {
                     let state = try JSONDecoder().decode(NotificationResponse.self, from: data)
                        print("Did send notification to \(user.pushKitToken)")
                        if state.state != "Error" {
                            observer.onNext(.success)
                        } else {
                            observer.onNext(.failure(state.error))
                        }
                     } catch {
                        print(error.localizedDescription)
                        observer.onNext(.failure(error.localizedDescription))
                     }
                 }
             }
             task.resume()
            return Disposables.create()
        }
       
    }
    static func notify(userToken: String, category: String, title: String, message: String) {
        let session = URLSession.shared
        guard let url = URL(string: "https://us-central1-meld-dd5f8.cloudfunctions.net/notifyUser?token=\(userToken)&category=\(category)&title=\(title)&message=\(message)") else { return }
       let task = session.dataTask(with: url) { (data, response, error) in
            if let error = error {
                print(error.localizedDescription)
            } else {
                print("Did send notification")
            }
        }
        task.resume()
    }
    static func registerForNotifications() -> Observable<Bool> {
        return Observable<Result<Bool, PushNotificationError>>.create { observer in
            //Register for Notification
                let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
                UNUserNotificationCenter.current().requestAuthorization(
                    options: authOptions,
                    completionHandler: { result, error in
                        if let error = error {
                            printLog(error.localizedDescription)
                            observer.onNext(.failure(PushNotificationError.failedWithMessage(error.localizedDescription)))
                        } else {
                            if result {
                                DispatchQueue.main.async {
                                    UIApplication.shared.registerForRemoteNotifications()
                                }
                                observer.onNext(.failure(.getTokens))
                            } else {
                                observer.onNext(.failure(PushNotificationError.failedWithMessage("Failed to Register")))
                            }
                        }
                })
            return Disposables.create()
        }.flatMap { result -> Observable<Bool> in
            switch result {
            case .success(let success):
                return .just(success)
            case .failure(let error):
                switch error {
                case .failedWithMessage: return .just(false)
                case .getTokens:
                    if let fcm = UserDefaults.standard.string(forKey: "fcmToken"), let apns = UserDefaults.standard.string(forKey: "deviceToken"), let pushkit = UserDefaults.standard.string(forKey: "device_pushkit_token")  {
                        return UserService.updateUserNotificationTokens(fcmToken: fcm, apnsToken: apns, pushKitToken: pushkit)
                    } else {
                        return .just(false)
                    }
                }
            }
        }
    }
}
