//
//  NavigationMenuPresenter.swift
//  Meld
//
//  Created by Blake Rogers on 7/13/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//
import UIKit
import RxSwift
import RxCocoa
import UtilitiesPackage

final class NavigationMenuPresenter {
    
    private let interactor: NavigationMenuInteractorIdentity
    private let router: NavigationMenuRouter
    private let bag = DisposeBag()
    
    let socialButton = UIButton() |> styledTitle(conversationFontIcon(size: 30, color: appBlack)) >>> asButton
    let accountButton = UIButton() |> styledTitle(accountFontIcon(size: 30, color: appBlack)) >>> asButton
    let filterButton = UIButton() |> styledTitle(filterFontIcon(size: 30, color: appBlack)) >>> asButton

    
    init(interactor: NavigationMenuInteractorIdentity) {
        self.interactor = interactor
        self.router = NavigationMenuRouter()
        let didSelectViewDating = socialButton.rx.tap
        let didSelectViewAccount = accountButton.rx.tap

       bag.insert(
            didSelectViewDating.subscribe(onNext: { [weak self] _ in
                self?.interactor.didSelectViewDating()
            }),
            didSelectViewAccount.subscribe(onNext: { [weak self] _ in
                self?.interactor.didSelectViewAccount()
            }),
            accountButton.rx.tap.subscribe(onNext: { [weak self] in
                self?.router.displayAccount()
            }),
            socialButton.rx.tap.subscribe(onNext: { [weak self] in
                self?.router.displayConversations()
            }),
            filterButton.rx.tap.subscribe(onNext: { [weak self] _ in
                self?.router.displayFilter()
            })
        )
    }
}
