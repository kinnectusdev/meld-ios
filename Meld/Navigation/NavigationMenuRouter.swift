//
//  NavigationMenuRouter.swift
//  Meld
//
//  Created by Blake Rogers on 7/13/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import UIKit
import UtilitiesPackage

final class NavigationMenuRouter  {
    
    func displayFilter() {
        let view = FilterRouter.presentation()
        currentController().present(view, animated: true, completion: nil)
    }
    func displayAccount() {
        let view = AccountPageRouter.presentation()
        currentController().present(view, animated: true, completion: nil)
    }
    
    func displayConversations() {
        let view = SocialRouter.presentation()
        currentController().present(view, animated: true, completion: nil)
    }
}

extension NavigationMenuRouter {
    static func presentation(homeInteractor: HomeInteractorIdentity) -> UIView {
        let menuInteractor = NavigationMenuInteractor(homeInteractor: homeInteractor)
        let menuView = NavigationMenuView(presenter: NavigationMenuPresenter(interactor: menuInteractor))
        return menuView
    }
}
