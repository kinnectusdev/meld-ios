//
//  HomeViewModel.swift
//  Meld
//
//  Created by blakerogers on 11/26/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

public enum HomeMenuPageCoordination {
    case account
    case conversation
}

protocol NavigationMenuInteractorIdentity {
    func didSelectViewAccount()
    func didSelectViewDating()
    func observeDisplayedPage() -> Observable<HomeMenuPageCoordination>
    func didSelectAccountNavigation(navigation: HomeCoordination)
}

final class NavigationMenuInteractor {
    private let currentDisplayedPage =  PublishSubject<HomeMenuPageCoordination>()
    private let homeInteractor: HomeInteractorIdentity
    init(homeInteractor: HomeInteractorIdentity) {
        self.homeInteractor = homeInteractor
    }
}

extension NavigationMenuInteractor: NavigationMenuInteractorIdentity {
    func didSelectViewAccount() {
        currentDisplayedPage.onNext(.account)
    }
    func didSelectViewDating() {
        currentDisplayedPage.onNext(.conversation)
    }
    func observeDisplayedPage() -> Observable<HomeMenuPageCoordination> {
        currentDisplayedPage.asObservable().startWith(.account)
    }
    func didSelectAccountNavigation(navigation: HomeCoordination) {
        //TODO: Handle route from navigation page
    }
}

public func navigation_menu_viewmodel(
    _ didSelectViewAccount: ControlEvent<Void>,
    _ didSelectViewDating: ControlEvent<Void>,
    _ didSelectViewLikedUsers: ControlEvent<Void>)
    -> Observable<HomeMenuPageCoordination> {
    let viewAccount = didSelectViewAccount.map { _ in HomeMenuPageCoordination.account }
    let viewDating = didSelectViewDating.map { _ in HomeMenuPageCoordination.conversation }
    let coordination = Observable.merge(viewAccount, viewDating)
    
    return coordination
}
