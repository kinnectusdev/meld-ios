//
//  NavigationMenu.swift
//  Meld
//
//  Created by blakerogers on 11/26/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import UtilitiesPackage

enum NavigationPage {
    case account
    case conversation
    case schedule
}

let navigationMenuContainerHeight: CGFloat = appFrame.height - 80
final class NavigationMenuView: UIView {
    private var presenter: NavigationMenuPresenter!
    private func setViews() {
        
       let view = UIView()
        |> whiteBackground
        >>> roundedWithNoMask(20)
        >>> largeGreyShadowed
        >>> utilizeConstraints
        >>> fill
        >>> appendSubView(presenter.accountButton
                                |> named("accountButton")
                                >>> alignToTop(20)
                                >>> alignToLeft(50))
        >>> appendSubView(
            presenter.filterButton
                |> alignedToCenterX
                |> alignToSibling("accountButton", constraint: alignedToSiblingCenterY)
        )
        >>> appendSubView(presenter.socialButton
                                |> alignToTop(20)
                                >>> alignToRight(-50))
                
        add(views: view)
    }
}

extension NavigationMenuView {
    convenience init(presenter: NavigationMenuPresenter) {
        self.init()
        self.presenter = presenter
        self.setViews()
    }
}
