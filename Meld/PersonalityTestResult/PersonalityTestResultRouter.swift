//
//  PersonalityTestResultRouter.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UIKit
import UtilitiesPackage

final class PersonalityTestResultRouter {
    func dismiss() {
        currentController().dismiss(animated: true, completion: nil)
    }
    func displaySignUp(testResult: PersonalityTestResult) {
        let view = SignUpRouter.presentation(testResult: testResult)
        view.modalPresentationStyle = (DeviceInfo.deviceType == .iPad ? .overFullScreen : .automatic)
        currentController().present(view, animated: true, completion: nil)
    }
}
extension PersonalityTestResultRouter {
    static func presentation(testResult: PersonalityTestResult) -> UIViewController {
        PersonalityTestResultView(presenter: PersonalityTestResultPresenter(interactor: PersonalityTestResultInteractor(testResult: testResult)))
    }
}
