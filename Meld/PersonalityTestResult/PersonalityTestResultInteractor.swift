//
//  PersonalityTestInteractor.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol PersonalityTestResultInteractorIdentity: PersonalityTypeInteractorIdentity {
    func observeTestResult() -> PersonalityTestResult
}

final class PersonalityTestResultInteractor {
    
    private let testResult: PersonalityTestResult
    
    init(testResult: PersonalityTestResult) {
        self.testResult = testResult
    }
}
extension PersonalityTestResultInteractor: PersonalityTestResultInteractorIdentity {
    func observeTitle() -> Observable<String> {
        .just(testResult.personalityType().title)
    }
    
    func observePersonalityType() -> PersonalityType {
        testResult.personalityType()
    }
    
    func observeProCons() -> [PersonalityProCon] {
        let strengths: [PersonalityProCon] = observePersonalityType().strengths
        let weaknesses: [PersonalityProCon] = observePersonalityType().weakness
        return strengths.appendingAll(weaknesses)
    }
    
    func observePersonalityIcon() ->String {
        observePersonalityType().title
    }
    
    func observeTestResult() -> PersonalityTestResult {
        testResult
    }
}
