//
//  PersonalityTestResultView.swift
//  Meld
//
//  Created by Blake Rogers on 8/19/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//
import Foundation
import UIKit
import UtilitiesPackage

final class PersonalityTestResultView: UIViewController {
    
    private var presenter: PersonalityTestResultPresenter!
   
    private func setViews() {
        let view = UIView()
            |> whiteBackground
            |> fill
            |> appendSubView(
                UIView()
                |> named("image")
                |>  alignedToCenterX
                >>> alignToTop(50)
                >>> width_Height(300, 250)
                >>> diamondShadowed
                >>> appendSubView(
                    presenter.image
                    |> scaledToFill
                    >>> rounded(10)
                    >>> isClipping
                    >>> fill
                )
            )
            |> appendSubView(UILabel()
                                |> named("heading")
                                |> textColored(appBlack)
                                |> titled("Your Results")
                                |> backgroundColored(.clear)
                                |> textFont(appFontFormal(20))
                                |> textCenterAligned
                                |> alignToTop(16)
                                |> alignedToCenterX)
            |> appendSubView(presenter.titleLabel
                                |> named("title")
                                |> textColored(appBlack)
                                |> backgroundColored(.clear)
                                |> textFont(appFontFormal(20))
                                |> textCenterAligned
                                |> alignToSibling("image", constraint: alignTopToSiblingBottom(20))
                                |> alignedToCenterX)
            >>> appendSubView(
                presenter.backButton
                |> named("back_button")
                >>> setImage(UIImage(named: "BackButton"))
                >>> alignToLeft(10)
                >>> alignToSibling("title", constraint: alignToSiblingCenterY())
            )
            |> appendSubView(presenter.summary
                                |> named("summary")
                                |> alignToSibling("title", constraint: alignTopToSiblingBottom(10))
                                |> width_Height(appFrame.width, 130))
            |> appendSubView(presenter.compatibleTypes
                                |> named("types")
                                |> alignToSibling("summary", constraint: alignTopToSiblingBottom(0))
                                |> width_Height(appFrame.width, 100))
            |> appendSubView(presenter.joinButton
                                |> whiteBackground
                                |> styledTitle(blackAttributedString("Create an Account", 20))
                                |> alignToSibling("types", constraint: alignTopToSiblingBottom(0))
                                |> alignedToCenterX
                                |> width_Height(200, 40))
        self.view = view
    }
}
extension PersonalityTestResultView {
    convenience init(presenter: PersonalityTestResultPresenter) {
        self.init()
        self.presenter = presenter
        self.modalPresentationStyle = ( DeviceInfo.deviceType == .iPad ? .overCurrentContext : .automatic)
        self.setViews()
    }
}


