//
//  PersonalityTestResultPresenter.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import UtilitiesPackage
import UIKit

final class PersonalityTestResultPresenter {
    private let bag = DisposeBag()
    private let interactor: PersonalityTestResultInteractorIdentity
    private let router: PersonalityTestResultRouter
    
    let titleLabel = UILabel()
    let summary: UIView
    let compatibleTypes: UIView
    let image = UIImageView()
    let joinButton = UIButton()
    let backButton = UIButton()
    |> visibility( DeviceInfo.deviceType == .iPad ? 1.0 : 0.0 )
    |> asButton
    init(interactor: PersonalityTestResultInteractorIdentity) {
        self.interactor = interactor
        self.router = PersonalityTestResultRouter()
        
        self.summary = PersonalitySummaryItemRouter.presentation(personalityType: interactor.observePersonalityType())
        
        self.compatibleTypes = CompatiblePersonalityTypesRouter.presentation(type: interactor.observePersonalityType())
        
        self.image.image = UIImage(named: interactor.observePersonalityIcon())
        
        bag.insert(
            interactor.observeTitle()
                .map {
                    "You're an \($0)!"
                }
                .asDriver(onErrorJustReturn: .empty)
                .drive(titleLabel.rx.text),
            joinButton.rx.tap.subscribe(onNext: { [weak self] _ in
                self?.router.displaySignUp(testResult: interactor.observeTestResult())
            }),
            backButton.rx.tap.subscribe(onNext: { [weak self] _ in
                self?.router.dismiss()
            })
        )
    }
}
