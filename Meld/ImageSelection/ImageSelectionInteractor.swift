//
//  ImageSelectionInteractor.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import Photos
import UIKit

protocol ImageSelectionDelegate {
    func didAddImage(image: UIImage)
}
protocol ImageSelectionInteractorIdentity {
    func didSelectAddImage(image: UIImage)
    func observeImages() -> Observable<[PHAsset]>
}
final class ImageSelectionInteractor {
    
    private let bag = DisposeBag()
    private let imageSelectionDelegate: ImageSelectionDelegate
    private let images = PublishSubject<[PHAsset]>()
    
    init(imageSelectionDelegate: ImageSelectionDelegate) {
        
        self.imageSelectionDelegate = imageSelectionDelegate
        
        bag.insert(
            MediaImageService.getImageAssets().bind(to: images)
        )
    }
}
extension ImageSelectionInteractor: ImageSelectionInteractorIdentity {
    func didSelectAddImage(image: UIImage) {
        imageSelectionDelegate.didAddImage(image: image)
    }
    
    func observeImages() -> Observable<[PHAsset]> {
        images.asObservable()
    }
}
