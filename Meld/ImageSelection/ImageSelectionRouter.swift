//
//  ImageSelectionRouter.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import UIKit
import UtilitiesPackage

final class ImageSelectionRouter {
    func dismiss() {
        let controller = currentController()
        UIView.animate(withDuration: 0.24, animations: {
            controller.view.alpha = 0.0
        }, completion: { _ in
            controller.dismiss(animated: false, completion: nil)
        })
    }
}
extension ImageSelectionRouter {
    static func presentation(imageSelectionDelegate: ImageSelectionDelegate) -> UIViewController {
        ImageSelectionView(presenter: ImageSelectionPresenter(interactor: ImageSelectionInteractor(imageSelectionDelegate: imageSelectionDelegate)))
    }
}
