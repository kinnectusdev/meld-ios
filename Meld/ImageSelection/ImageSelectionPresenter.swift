//
//  ImageSelectionPresenter.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Photos
import UtilitiesPackage
import UIKit
import YPImagePicker

final class ImageSelectionPresenter {
    
    private let bag = DisposeBag()
    private let interactor: ImageSelectionInteractorIdentity
    private let router: ImageSelectionRouter
    let picker = YPImagePicker()


    init(interactor: ImageSelectionInteractorIdentity) {
        
        self.interactor = interactor
        self.router = ImageSelectionRouter()
        
        self.picker.didFinishPicking { [weak self] items, _ in
            if let photo = items.singlePhoto {
                print(photo.fromCamera) // Image source (camera or library)
                print(photo.image) // Final image selected by the user
                print(photo.originalImage) // original image selected by the user, unfiltered
                print(photo.modifiedImage) // Transformed image, can be nil
                print(photo.exifMeta) // Print exif meta data of original image.
                if let image = photo.modifiedImage {
                    self?.interactor.didSelectAddImage(image: image)
                } else {
                    self?.interactor.didSelectAddImage(image: photo.image)
                }
            }
            self?.picker.dismiss(animated: true, completion: nil)
            self?.router.dismiss()
        }
    }
}
