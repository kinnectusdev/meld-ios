//
//  ImageCollectionManager.swift
//  Meld
//
//  Created by blakerogers on 11/29/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import UIKit
import RxSwift
import UtilitiesPackage

public class ImageCollectionManager: UIView {
    let collection = UICollectionView.collectionView(width: 300, height: 300, backgroundColor: .white, itemSpacing: 0, lineSpacing: 0, selectionAllowed: true, direction: .horizontal, paging: true)
    let didSelectImage = PublishSubject<UIImage>()
}
extension ImageCollectionManager {
    public override func layoutSubviews() {
        super.layoutSubviews()
        establishCollection()
        setViews()
    }
    private func establishCollection() {
        collection.register(DeviceImageCell.self, forCellWithReuseIdentifier: "Cell")
        collection.delegate = self
    }
    private func setViews() {
        backgroundColor = .clear
        add(views: collection)
        collection.constrainInView(view: self, top: 0, left: 0, right: 0, bottom: 0)
    }
}
extension ImageCollectionManager: UICollectionViewDelegate {
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? DeviceImageCell, let image = cell.image.image else { return }
        didSelectImage.onNext(image)
    }
}
extension ImageCollectionManager: UICollectionViewDelegateFlowLayout {
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 200, height: 200)
    }
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
