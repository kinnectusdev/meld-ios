//
//  ImageSelectionService.swift
//  Meld
//
//  Created by blakerogers on 12/30/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift

class ImageSelectionService {
    static func selectedImage() -> Observable<Data?> {
        return UserDefaults.standard.rx.observe(Data.self, "selected_image")
    }
    static func storeImage(data: Data?) {
        UserDefaults.standard.setValue(data, forKey: "selected_image")
    }
}
