//
//  ModalImageSelection.swift
//  Meld
//
//  Created by blakerogers on 11/28/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import UIKit
import UtilitiesPackage
import YPImagePicker

final class ImageSelectionView: UIViewController {
    private var presenter: ImageSelectionPresenter!
    
    private func setViews() {
        addChild(presenter.picker)
        view.addSubview(presenter.picker.view)
    }
}
extension ImageSelectionView {
    convenience init(presenter: ImageSelectionPresenter) {
        self.init()
        self.modalPresentationStyle = ( DeviceInfo.deviceType == .iPad ? .overCurrentContext : .automatic)
        self.presenter = presenter
        self.setViews()
    }
}
