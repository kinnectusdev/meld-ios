//
//  ImageSelectionViewModel.swift
//  Meld
//
//  Created by blakerogers on 11/29/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Photos
import UIKit

func image_selection_viewmodel(
    _ selectedImage: Observable<UIImage>)
    ->
    (image: Observable<[PHAsset]>,
    coordination: Observable<Data?>) {
        
        let images: Observable<[PHAsset]> = MediaImageService.getImageAssets()
        
    let selectedImageData = selectedImage.map { $0.pngData() }
    return (images, selectedImageData)
}
