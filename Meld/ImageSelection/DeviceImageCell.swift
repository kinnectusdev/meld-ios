//
//  DeviceImageCell.swift
//  Meld
//
//  Created by blakerogers on 11/29/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import UIKit
import RxSwift
import UtilitiesPackage

class DeviceImageCell: UICollectionViewCell {
    let bag = DisposeBag()
    let image = UIImageView()
                        |> scaled(.scaleAspectFill)
                        >>> rounded(50)
                        >>> isClipping
                        >>> utilizeConstraints
                        >>> asImage
    let imageContainer = UIView()
                        |> greyShadowed
                        >>> utilizeConstraints
                        >>> width_Height(100, 100)
                        >>> isInvisible
    override func layoutSubviews() {
        super.layoutSubviews()
        setViews()
    }
    private func setViews() {
        contentView.add(views: imageContainer)
        imageContainer.constrainCenterToCenter(of: contentView)
        imageContainer.add(views: image)
        image.backgroundColor = UIColor.gray
        image.constrainInView(view: imageContainer, top: 0, left: 0, right: 0, bottom: 0)
    }
}

