//
//  PageControlFx.swift
//  Meld
//
//  Created by blakerogers on 3/25/20.
//  Copyright © 2020 com.meld.kinnectus. All rights reserved.
//

import UIKit
import CHIPageControl
public typealias PageControlCHI = (CHIBasePageControl) -> CHIBasePageControl
//pageControl.numberOfPages = 4
//pageControl.radius = 4
//pageControl.tintColor = .red
//pageControl.currentPageTintColor = .green
//pageControl.padding = 6
public let numberOfPages: (Int) -> PageControlCHI = { number in
    return { pageControl in
        pageControl.numberOfPages = number
        return pageControl
    }
}

public let pageRadius: (CGFloat) -> PageControlCHI = { radius in
    return { pageControl in
        pageControl.radius = radius
        return pageControl
    }
}
public let pageTint: (UIColor) -> PageControlCHI = { tint in
    return { pageControl in
        pageControl.tintColor = tint
        return pageControl
    }
}
public let pageCurrentTint: (UIColor) -> PageControlCHI = { tint in
    return { pageControl in
        pageControl.currentPageTintColor = tint
        return pageControl
    }
}
public let pagePadding: (CGFloat) -> PageControlCHI = { padding in
    return { pageControl in
        pageControl.padding = padding
        return pageControl
    }
}

