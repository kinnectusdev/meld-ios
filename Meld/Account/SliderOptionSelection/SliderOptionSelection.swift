//
//  SliderOptionSelection.swift
//  Meld
//
//  Created by Blake Rogers on 8/6/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import UIKit
import UtilitiesPackage

final class SliderOptionSelectionView: UIViewController {
    private var presenter: SliderOptionSelectionPresenter!
    
    private func setViews() {
        let view = UIView()
        |> blurredWhiteBackground
        |> rounded(10)
        |> appendSubView(presenter.titleLabel
            |> named("title")
            >>> textFont(appFontFormal(18))
            >>> alignToTop(20)
            >>> alignedToCenterX
            >>> textColored(appBlack))
        |> appendSubView(presenter.saveButton
            |> styledTitle(blackAttributedString("Save", 18))
            >>> alignToLeft(10)
            >>> alignToTop(20))
        |> appendSubView(presenter.detailLabel
            |> textFont(appFontFormal(30))
            >>> alignBottomToCenter(-30)
            >>> alignedToCenterX
            >>> textColored(appBlack))
        |> appendSubView(UIView()
                    |> backgroundColored(appDarkGrey)
                    |> width_Height(200, 1)
                    |> alignedToCenterX
                    |> alignToSibling("title", constraint: alignTopToSiblingBottom(4)))
        |> appendSubView(presenter.slider |>  alignedToCenter |> width_Height(300, 50))
        
        self.view = view
    }
}

extension SliderOptionSelectionView {
    convenience init(presenter: SliderOptionSelectionPresenter) {
        self.init()
        self.presenter = presenter
        self.setViews()
    }
}
