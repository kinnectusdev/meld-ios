//
//  SliderOptionSelectionPresenter.swift
//  Meld
//
//  Created by Blake Rogers on 8/6/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import RxSwift
import RxCocoa
import UtilitiesPackage
import UIKit

final class SliderOptionSelectionPresenter {
    private let bag = DisposeBag()
    private let interactor: SliderOptionSelectionInteractorIdentity
    private let router: SliderOptionSelectionRouter
    
    let titleLabel = UILabel()
    let saveButton = UIButton()
    let detailLabel = UILabel()
    let slider = UISlider()
        |> tintColor(.black)
        |> thumbTintColor(.white)
        |> utilizeConstraints
        |> { $0 as! UISlider }
    
    init(interactor: SliderOptionSelectionInteractorIdentity) {
        
        self.interactor = interactor
        self.router = SliderOptionSelectionRouter()
        
        slider.minimumValue = interactor.observeMinValue()
        slider.maximumValue = interactor.observeMaxValue()
        
        bag.insert(
            interactor.observeCurrentOptionDescription().asDriver(onErrorJustReturn: .empty).drive(detailLabel.rx.text),
            interactor.observeCurrentOption().asDriver(onErrorJustReturn: 0).drive(slider.rx.value),
            interactor.observeTitle().asDriver(onErrorJustReturn: .empty).drive(titleLabel.rx.text),
            slider.rx.value.skip(1).subscribe(onNext: { [weak self] value in
                self?.interactor.didChangeValue(value: value)
            }),
            saveButton.rx.tap.subscribe(onNext: { [weak self] _ in
                guard let value = self?.slider.value else { return }
                self?.interactor.didSetValue(value: value)
                self?.router.dismiss()
            })
        )
    }
}
