//
//  SliderOptionSelectionRouter.swift
//  Meld
//
//  Created by Blake Rogers on 8/6/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import UIKit
import UtilitiesPackage

final class SliderOptionSelectionRouter {
    func dismiss() {
        currentController().dismiss(animated: true, completion: nil)
    }
}
extension SliderOptionSelectionRouter {
    static func presentation(attribute: Attributes, manager: AttributeEditManager) -> UIViewController {
        SliderOptionSelectionView(presenter: SliderOptionSelectionPresenter(interactor: SliderOptionSelectionInteractor(attribute: attribute, editManager: manager)))
    }
}
