//
//  SliderOptionSelectionInteractor.swift
//  Meld
//
//  Created by Blake Rogers on 8/6/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import RxSwift
import RxCocoa

protocol SliderOptionSelectionInteractorIdentity {
    func observeMaxValue() -> Float
    func observeMinValue() -> Float
    func observeTitle() -> Observable<String>
    func observeCurrentOption() -> Observable<Float>
    func observeCurrentOptionDescription() -> Observable<String>
    
    func didSetValue(value: Float)
    func didChangeValue(value: Float)
}
final class SliderOptionSelectionInteractor {
    
    private let bag = DisposeBag()
    private let attribute: Attributes
    private let manager: AttributeEditManager
    
    private let currentOption = BehaviorRelay<AttributeOption?>(value: nil)
    
    init(attribute: Attributes, editManager: AttributeEditManager) {
        
        self.attribute = attribute
        self.manager = editManager
        
        bag.insert(
            UserService.observeAttribute(attribute: attribute).bind(to: currentOption)
        )
        
    }
}

extension SliderOptionSelectionInteractor: SliderOptionSelectionInteractorIdentity {
    
    func didChangeValue(value: Float) {
        switch attribute {
        case .age:
            currentOption.accept(Age(rawValue: Int(value)))
        case .height:
            currentOption.accept(Height(rawValue: Int(value)))
        default:
            break
        }
    }
    
    func observeCurrentOptionDescription() -> Observable<String> {
        currentOption.map { $0?.description ?? .empty }
    }
    
    func observeCurrentOption() -> Observable<Float> {
        currentOption.asObservable().map { attribute -> Float in
            if let attribute = attribute as? Age {
                return attribute.value.float()
            }
            
            if let attribute = attribute as? Height {
                return attribute.height.float()
            }
            return 0
        }
    }
    
    func observeTitle() -> Observable<String> {
        Observable.just(attribute.description)
    }
    
    func observeMaxValue() -> Float {
        switch attribute {
        case .age: return 99
        case.height: return 84
        default:
            return 0
        }
    }
    
    func observeMinValue() -> Float {
        switch attribute {
        case .age: return 18
        case.height: return 48
        default:
            return 0
        }
    }
    
    func didSetValue(value: Float) {
        switch attribute {
        case .age:
            manager.onDidEditAttribute(option: Age.age(Int(value)))
        case .height:
            manager.onDidEditAttribute(option: Height.height(Int(value)))
        default:
            break
        }
    }
}
