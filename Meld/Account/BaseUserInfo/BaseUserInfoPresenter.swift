//
//  BaseUserInfoPresenter.swift
//  Meld
//
//  Created by Blake Rogers on 8/6/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import RxSwift
import RxCocoa
import UtilitiesPackage
import UIKit

final class BaseUserInfoPresenter {
    
    //MARK: Stored Properties
    private let bag = DisposeBag()
    let interactor: BaseUserInfoInteractorIdentity
    let router: BaseUserInfoRouter
    
    //MARK: UI Elements

    let userNameLabel = UILabel()
        |> isInterActive
        >>> textFont(appFont(BaseUserInfoView.Style.nameFontSize))
        >>> textColored(appBlack)
        >>> utilizeConstraints
        >>> asLabel
    
    let editNameField = UITextField()
        |> addGesture(UITapGestureRecognizer())
        >>> isInvisible
        >>> utilizesDoneReturnType
        >>> textFont(appFont(15))
        >>> textColored(appDarkGrey)
        >>> utilizeConstraints
        >>> asField
    
    let userEmailLabel = UILabel()
        |> isInterActive
        >>> textFont(appFont(BaseUserInfoView.Style.emailFontSize))
        >>> textColored(appBlack)
        >>> utilizeConstraints
        >>> asLabel
    
    let editEmailField = UITextField()
        |> addGesture(UITapGestureRecognizer())
        >>> isInvisible
        >>> utilizesDoneReturnType
        >>> textFont(appFont(15))
        >>> textColored(appDarkGrey)
        >>> utilizeConstraints
        >>> asField
    
    let userPersonalityTypeLabel = UILabel()
        |> textFont(appFont(BaseUserInfoView.Style.personalityFontSize))
        >>> textColored(appBlack)
        >>> utilizeConstraints
        >>> asLabel
    
    let pushNotificationSwitch = UISwitch()
            |> switchColor(appBlack)
            >>> utilizeConstraints
            >>> asSwitch
    
    let profileVisibleSwitch = UISwitch()
        |> switchColor(appBlack)
        >>> utilizeConstraints
        >>> asSwitch
    
    init(interactor: BaseUserInfoInteractorIdentity) {
        self.interactor = interactor
        self.router = BaseUserInfoRouter()
        
        bag.insert(
            interactor.observePushNotificationEnabled().asDriver(onErrorJustReturn: false).drive(pushNotificationSwitch.rx.isOn),
            interactor.observeProfileVisibility().asDriver(onErrorJustReturn: false).drive(profileVisibleSwitch.rx.isOn),
            interactor.observeEmail().asDriver(onErrorJustReturn: .empty).drive(onNext:{ [weak self] email in
                print("email: \(email)")
                self?.userEmailLabel.text = email
            }),
            interactor.observeName().asDriver(onErrorJustReturn: .empty).drive(userNameLabel.rx.text),
            interactor.observePersonalityType().asDriver(onErrorJustReturn: .empty).drive(userPersonalityTypeLabel.rx.text),
            editNameField.rx.controlEvent(.editingDidEnd).subscribe(onNext: { [weak self] _ in
                UIView.animate(withDuration: 0.25) {
                    self?.editNameField.alpha = 0.0
                    self?.userNameLabel.alpha = 1.0
                }
            }),
            editNameField.rx.controlEvent(.editingDidEnd)
                .withLatestFrom(editNameField.rx.value)
                .map { $0 ?? "" }.subscribe(onNext:{ edit in
                    interactor.didEditName(name: edit)
                }),
            editNameField.rx.value.subscribe(onNext: { [weak self] text in
                if (text ?? "").contains("\n") {
                    self?.editNameField.text = (text ?? "").replacingOccurrences(of: "\n", with: "")
                    self?.editNameField.resignFirstResponder()
                }
            }),
            editEmailField.rx.controlEvent(.editingDidEnd).subscribe(onNext: {_ in
                UIView.animate(withDuration: 0.25) { [weak self] in
                    self?.editEmailField.alpha = 0.0
                    self?.userEmailLabel.alpha = 1.0
                }
            }),
            editEmailField.rx.controlEvent(.editingDidEnd)
                .withLatestFrom(editEmailField.rx.value)
                .map { $0 ?? "" }.subscribe(onNext: { edit in
                    interactor.didEditEmail(email: edit)
                }),
            editEmailField.rx.value.subscribe(onNext: { [weak self] text in
                if (text ?? "").contains("\n") {
                    self?.editEmailField.text = (text ?? "").replacingOccurrences(of: "\n", with: "")
                    self?.editEmailField.resignFirstResponder()
                }
            })
        )
        userNameLabel.addTapGestureRecognizer { [weak self] in
            UIView.animate(withDuration: 0.24) {
                self?.editNameField.alpha = 1.0
                self?.userNameLabel.alpha = 0.0
            }
            self?.editNameField.becomeFirstResponder()
        }
        userEmailLabel.addTapGestureRecognizer { [weak self] in
            UIView.animate(withDuration: 0.25) {
                self?.editEmailField.alpha = 1.0
                self?.userEmailLabel.alpha = 0.0
            }
            self?.editEmailField.becomeFirstResponder()
        }
        userPersonalityTypeLabel.addTapGestureRecognizer { [weak self] in
            guard let this = self else { return }
            self?.router.displayEditPersonalityType(manager: this.interactor)
        }
    }
}
