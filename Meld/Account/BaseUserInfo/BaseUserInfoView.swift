//
//  BaseUserInfoView.swift
//  Meld
//
//  Created by Blake Rogers on 5/18/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import UIKit
import UtilitiesPackage

final class BaseUserInfoView: UIView {
    
    private var presenter: BaseUserInfoPresenter!
    
    private let titleLabel = UILabel()
        |> titled("Settings")
        >>> utilizeConstraints
        >>> textFont(appFontFormal(18))
        >>> textColored(appBlack)
        >>> onTap { _ in
            print("did select Settings")
        }
    
   private let underline = UIView()
        |> backgroundColored(appGrey)
        >>> utilizeConstraints
    
    private let nameLabel = UILabel()
        |> named("name_label")
        >>> utilizeConstraints
        >>> styledTitle(darkGreyAttributedString("Name", Style.nameFontSize))
       
    private let emailLabel = UILabel()
        |> utilizeConstraints
        >>> styledTitle(darkGreyAttributedString("Email", Style.emailFontSize))

    private let personalityLabel = UILabel()
        |> textForLabel("Personality Type")
        >>> utilizeConstraints
        >>> textFont(appFont(Style.profileFontSize))
        >>> textColored(appDarkGrey)
                              
    private let pushLabel = UILabel()
        |> utilizeConstraints
        >>> styledTitle(darkGreyAttributedString("Push Notifications", Style.pushFontSize))
      
    private let profileLabel = UILabel()
        |> textForLabel("Profile Visible")
        >>> utilizeConstraints
        >>> textFont(appFont(Style.profileFontSize))
        >>> textColored(appDarkGrey)
    
    
    private func setViews() {

        
        self.add(views: titleLabel, underline, nameLabel,
                             presenter.userNameLabel,
                             presenter.editNameField,
                             emailLabel,
                             presenter.userEmailLabel,
                             presenter.editEmailField,
                             personalityLabel,
                             presenter.userPersonalityTypeLabel,
                             pushLabel,
                             presenter.pushNotificationSwitch,
                             profileLabel,
                             presenter.profileVisibleSwitch)
        
        titleLabel.constrainInView(view: self, top: 20, left: 20)
        
        underline.constrainViewHeight(to: 1)
        underline.constrainInView(view: self, left: 20, right: -20)
        underline.constrainTopToBottom(of: titleLabel, constant: 4)
        
        nameLabel.constrainInView(view: self, left: 20)
        nameLabel.constrainTopToBottom(of: underline, constant: Style.nameTopOffset)
         
        presenter.userNameLabel.constrainTopToTop(of: nameLabel, constant: 0)
        presenter.userNameLabel.constrainRightToRight(of: self, constant: -20)

        presenter.editNameField.constrainLeftToRight(of: nameLabel, constant: 20)
        presenter.editNameField.constrainTopToTop(of: nameLabel, constant: -8)
        presenter.editNameField.constrainWidth_Height(width: 200, height: 40)
        
        emailLabel.constrainLeftToLeft(of: nameLabel, constant: 0)
        emailLabel.constrainTopToBottom(of: nameLabel, constant: Style.emailTopOffset)
        
        presenter.userEmailLabel.constrainRightToRight(of: self, constant: -20)
        presenter.userEmailLabel.constrainTopToTop(of: emailLabel, constant: 0)

        presenter.editEmailField.constrainLeftToRight(of: emailLabel, constant: 20)
        presenter.editEmailField.constrainTopToTop(of: emailLabel, constant: -8)
        presenter.editEmailField.constrainWidth_Height(width: 200, height: 40)
        
        personalityLabel.constrainLeftToLeft(of: emailLabel, constant: 0)
        personalityLabel.constrainTopToBottom(of: emailLabel, constant: Style.personalityTopOffset)
        
        presenter.userPersonalityTypeLabel.constrainRightToRight(of: self, constant: -20)
        presenter.userPersonalityTypeLabel.constrainTopToTop(of: personalityLabel, constant: 0)

        pushLabel.constrainLeftToLeft(of: personalityLabel, constant: 0)
        pushLabel.constrainTopToBottom(of: personalityLabel, constant: Style.pushTopOffset)

        presenter.pushNotificationSwitch.constrainRightToRight(of: self, constant: -20)
        presenter.pushNotificationSwitch.constrainTopToTop(of: pushLabel, constant: 0)

        profileLabel.constrainLeftToLeft(of: pushLabel, constant: 0)
        profileLabel.constrainTopToBottom(of: pushLabel, constant: Style.profileTopOffset)

        presenter.profileVisibleSwitch.constrainRightToRight(of: self, constant: -20)
        presenter.profileVisibleSwitch.constrainTopToTop(of: profileLabel, constant: 0)
    }
}

extension BaseUserInfoView {
    convenience init(presenter: BaseUserInfoPresenter) {
        self.init()
        self.presenter = presenter
        self.setViews()
    }
    func configure(presenter: BaseUserInfoPresenter) {
        self.presenter = presenter
        self.setViews()
    }
}

extension BaseUserInfoView {
    struct Style {
        static let nameTopOffset: CGFloat = 30
        static let nameFontSize: CGFloat = 18
        static let emailFontSize: CGFloat = nameFontSize
        static let emailTopOffset: CGFloat = 30
        static let pushTopOffset: CGFloat = 30
        static let pushFontSize: CGFloat = nameFontSize
        static let profileFontSize: CGFloat = nameFontSize
        static let profileTopOffset: CGFloat = 30
        static let personalityFontSize: CGFloat = nameFontSize
        static let personalityTopOffset: CGFloat = 30
    }
}
