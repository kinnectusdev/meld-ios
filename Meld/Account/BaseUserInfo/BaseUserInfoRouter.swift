//
//  BaseUserInfoRouter.swift
//  Meld
//
//  Created by Blake Rogers on 8/6/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UtilitiesPackage

final class BaseUserInfoRouter {
    
    func displayEditPersonalityType(manager: PersonalitySelectionManager) {
        let view = PersonalitySelectionRouter.presentation(manager: manager)
        currentController().present(view, animated: true, completion: nil)
    }
}
