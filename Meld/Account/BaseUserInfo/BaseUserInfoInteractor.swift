//
//  BaseUserInfoInteractor.swift
//  Meld
//
//  Created by Blake Rogers on 8/6/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import RxSwift
import RxCocoa
import UtilitiesPackage

protocol BaseUserInfoInteractorIdentity: PersonalitySelectionManager {
    
    //MARK: Inputs
    func didTogglePushNotifications(isOn: Bool)
    func didToggleProfileVisibility(isOn: Bool)
    func didEditEmail(email: String)
    func didEditName(name: String)
    
    //MARK: Observed properties
    func observeEmail() -> Observable<String>
    func observeName() -> Observable<String>
    func observePushNotificationEnabled() -> Observable<Bool>
    func observeProfileVisibility() -> Observable<Bool>
    func observePersonalityType() -> Observable<String>
}

final class BaseUserInfoInteractor {
    private let bag = DisposeBag()
    private let name = BehaviorRelay(value: "Test")
    private let email = BehaviorRelay(value: "test@mail.com")
    private let personalityType = BehaviorRelay(value: PersonalityType.intj)
    private let isPushNotificationsEnabled = BehaviorRelay(value: false)
    private let isProfileVisible = BehaviorRelay(value: false)
    
    init() {
        let user = UserService.currentUser().compactMap({ $0.user })
        
        bag.insert(
            user.map { $0.name }.bind(to: name),
            user.map { $0.email }.bind(to: email),
            user.map { $0.personalityType }.compactMap { $0 }.bind(to: personalityType),
            user.map { $0.profileVisible }.bind(to: isProfileVisible ),
            user.map { $0.fcmToken.notEmpty() && $0.apnsToken.notEmpty() && PushNotificationService.isRegisteredForNotifications }.bind(to: isPushNotificationsEnabled)
        )
    }
}

extension BaseUserInfoInteractor: BaseUserInfoInteractorIdentity {
    
    func didTogglePushNotifications(isOn: Bool) {
        Observable.just(isOn).flatMap { isEnabled -> Observable<Bool> in
            isEnabled ? PushNotificationService.registerForNotifications() : UserService.unregisterForNotifications()
        }.subscribe(onNext: { [weak self] result in
            if !result {
                
            }
        }).disposed(by: bag)
    }
    
    func didToggleProfileVisibility(isOn: Bool) {
        Observable.just(isOn).flatMap { isEnabled -> Observable<Bool> in
            isEnabled ? UserService.enableProfileVisibility() : UserService.disableProfileVisibility()
        }.subscribe(onNext: { [weak self] result in
            if !result {
                
            }
        }).disposed(by: bag)
    }
    
    func didEditEmail(email: String) {
        UserService.updateUserEmail(email).subscribe(onNext: { [weak self] result in
            if !result {
                
            }
        }).disposed(by: bag)
    }
    
    func didEditName(name: String) {
        UserService.updateUserName(name).subscribe(onNext: { [weak self] result in
            if !result {
                
            }
        }).disposed(by: bag)
    }
    
    func observeEmail() -> Observable<String> {
        email.asObservable()
    }
    
    func observeName() -> Observable<String> {
        name.asObservable()
    }
    
    func observePushNotificationEnabled() -> Observable<Bool> {
        isPushNotificationsEnabled.asObservable()
    }
    
    func observeProfileVisibility() -> Observable<Bool> {
        isProfileVisible.asObservable()
    }
    
    func observePersonalityType() -> Observable<String> {
        personalityType.map { $0.title }
    }
    func didSelectPersonalityType(type: PersonalityType) {
        UserService.updateUserPersonality(type).subscribe(onNext: { [weak self] result in
            if !result {
                //Display alert
            } else {
                self?.personalityType.accept(type)
            }
        }).dispose()
    }
}
