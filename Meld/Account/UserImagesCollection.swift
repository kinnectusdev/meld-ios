//
//  UserImagesCollection.swift
//  Meld
//
//  Created by blakerogers on 1/23/20.
//  Copyright © 2020 com.meld.kinnectus. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import UtilitiesPackage

class UserImagesCollection: UIView {
    
    private let bag = DisposeBag()
    
    let collection = UICollectionView.collectionView(width: UIScreen.main.bounds.width, height: 300, direction: .horizontal)
    
    private var imageURLs: [String] = [] {
        didSet {
            collection.reloadData()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setViews()
    }
    
    private func setViews() {
        add(views: collection)
        collection.constrainInView(view: self, top: 0, left: 0, right: 0, bottom: 0)
        collection.register(UserImageItem.self, forCellWithReuseIdentifier: UserImageItem.identifier)
        collection.delegate = self
    }
}

extension UserImagesCollection {
    static let layoutHeight: CGFloat = 300
}

extension UserImagesCollection: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UserImageItem.imageItemWidth, height: UserImageItem.imageItemHeight)
    }
}

class UserImageItem: UICollectionViewCell {
    
    static let identifier: String = "UserImageItem"
    
    private let bag = DisposeBag()
    
    private let image = UIImageView()
        |> rounded(10)
        |> width_Height(imageWidth, imageHeight)
        |> diamondBackground
        |> utilizeConstraints
        |> asImage
    
    private let deleteImageButton = UIButton()
        |> styledTitle(deleteFontIcon(size: 10))
        |> width_Height(buttonSize, buttonSize)
        |> silverBackground
        |> greyShadowed
        |> rounded(buttonRadius)
        |> utilizeConstraints
        |> asButton
    
    private let addImageButton = UIButton()
        |> styledTitle(addFontIcon(size: 10))
        |> width_Height(buttonSize, buttonSize)
        |> silverBackground
        |> greyShadowed
        |> rounded(buttonRadius)
        |> utilizeConstraints
        |> asButton
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setViews()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        image.image = nil
    }
    
    private func setViews() {
        contentView.add(views: image, deleteImageButton, addImageButton)
        image.constrainInView(view: contentView, top: 8, left: 8, right: -8, bottom: -8)
        deleteImageButton.setXTo(con: image.x(), by: 0)
        deleteImageButton.setYTo(con: image.bottom(), by: -4)
        addImageButton.constrainCenterToCenter(of: deleteImageButton)
    }
    
    func setImage(url: String, delete_listener: PublishSubject<String>, add_listener: PublishSubject<Void>) {
        guard url.notEmpty() else {
            image.image = nil
            addImageButton.isHidden = false
            deleteImageButton.isHidden = true
            addImageButton.rx.tap
                       .do(onNext: { _ in
                           print("should be adding things...")
                       }).bind(to: add_listener).disposed(by: bag)
            return
        }
        let data = url |> MediaImageService.fetchImageData
        data.asDriver(onErrorJustReturn: nil).drive(onNext: { [weak self] data in
            if let data = data {
                self?.image.image = UIImage(data: data)
                self?.deleteImageButton.isHidden = false
                self?.addImageButton.isHidden = true
            }
        }).disposed(by: bag)
        
        deleteImageButton.rx.tap.map { _ in url }.bind(to: delete_listener).disposed(by: bag)
    }
}

extension UserImageItem {
    static let imageItemWidth: CGFloat = UIScreen.main.bounds.width*0.33
    static let imageItemHeight: CGFloat = 150
    static let imageWidth: CGFloat = imageItemWidth*0.9
    static let imageHeight: CGFloat = imageItemHeight*0.9
    static let buttonSize: CGFloat = 20
    static let buttonRadius: CGFloat = buttonSize*0.5
}
