//
//  TextUserInfoPresenter.swift
//  Meld
//
//  Created by Blake Rogers on 8/6/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//
import UIKit
import RxSwift
import UtilitiesPackage
import UIKit

final class TextUserInfoPresenter {
    
    private let bag = DisposeBag()
    private let interactor: TextUserInfoInteractorIdentity
    private let router: TextUserInfoRouter
    
    let titleLabel = UILabel()
    let messageField = UITextView()
        |> utilizesDoneReturnType
        |> asTextView
    let doneButton = UIButton()
    
    init(interactor: TextUserInfoInteractorIdentity) {
        
        self.interactor = interactor
        self.router = TextUserInfoRouter()
        self.messageField.text = interactor.defaultText()
        
        bag.insert(
            interactor.observeTitle().asDriver(onErrorJustReturn: .empty).drive(titleLabel.rx.text),
            Observable.merge(doneButton.rx.tap.asObservable(), messageField.rx.didEndEditing.map {_ in  ()}).subscribe(onNext: { [weak self] _ in
                guard let this = self else { return }
                guard let text = this.messageField.text else {
                    this.messageField.text = this.interactor.defaultText()
                    return
                }
                this.interactor.didSetAttribute(text: text)
            }),
            interactor.observeCurrentOption()
                .asDriver(onErrorJustReturn: .empty)
                .drive(messageField.rx.text),
            messageField.rx.didChange.subscribe(onNext: { [weak self] _ in
                guard let this = self else { return }
                if (this.messageField.text ?? "").contains("\n") {
                    this.messageField.text = this.messageField.text.replacingOccurrences(of: "\n", with: "")
                    this.messageField.resignFirstResponder()
                }
            }),
            messageField.rx.didEndEditing.subscribe(onNext: { [weak self] _ in
                self?.router.dismiss()
            }),
            messageField.rx.didBeginEditing.asDriver().drive(onNext: { [weak self] _ in
                guard let this = self else { return }
                if this.messageField.text.contains(this.interactor.defaultText()) {
                    this.messageField.text = ""
                }
            })
        )
    }
}
