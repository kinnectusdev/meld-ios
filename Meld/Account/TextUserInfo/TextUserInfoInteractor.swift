//
//  TextUserInfoInteractor.swift
//  Meld
//
//  Created by Blake Rogers on 8/6/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import RxSwift
import RxCocoa

protocol TextUserInfoInteractorIdentity {
    
    func observeTitle() -> Observable<String>
    func observeCurrentOption() -> Observable<String>
    
    func didSetAttribute(text: String)
    func defaultText() -> String
}
final class TextUserInfoInteractor {
    private let bag = DisposeBag()
    private let attribute: Attributes
    private let manager: AttributeEditManager
    private let attributeOption = BehaviorRelay<AttributeOption?>(value: nil)
    init(attribute: Attributes, manager: AttributeEditManager) {
        self.attribute = attribute
        self.manager = manager
        
        bag.insert(
            UserService.observeAttribute(attribute: attribute).bind(to: attributeOption)
        )
    }
}
extension TextUserInfoInteractor: TextUserInfoInteractorIdentity {
    func observeTitle() -> Observable<String> {
        Observable.just(attribute.description)
    }
    func observeCurrentOption() -> Observable<String> {
        attributeOption.compactMap { $0?.description }
    }
    func defaultText() -> String {
        switch attribute {
        case .hometown:
            return "Your hometown..."
        default:
            return ""
        }
    }
    func didSetAttribute(text: String) {
        switch attribute {
        case .hometown:
            manager.onDidEditAttribute(option: Hometown(description: text))
        default:
            break
        }
    }
}
