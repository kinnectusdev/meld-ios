//
//  TextUserInfoSelectionView.swift
//  Meld
//
//  Created by blakerogers on 4/9/20.
//  Copyright © 2020 com.meld.kinnectus. All rights reserved.
//
import UIKit
import UtilitiesPackage

final class TextUserInfoView: UIViewController {
    private var presenter: TextUserInfoPresenter!
    private func setViews() {
        let view = UIView()
        |> blurredWhiteBackground
            >>> appendSubView(presenter.titleLabel
            |> named("title")
            >>> textFont(appFontFormal(18))
            >>> textColored(appBlack)
            >>> utilizeConstraints
            >>> alignedToCenterX
            >>> alignToTop(100))
            |> appendSubView(UIView()
                    |> backgroundColored(appDarkGrey)
                    |> width_Height(200, 1)
                    |> alignedToCenterX
                    |> alignToSibling("title", constraint: alignTopToSiblingBottom(4)))
            >>> appendSubView(presenter.messageField
            |> named("message_field")
            >>> tagged(999)
            >>> clearBackground
            >>> textFont(appFont(18))
            >>> textColored(appDarkGrey)
            >>> width_Height(appFrame.width*0.5, 100)
            >>> alignedToCenterX
            >>> alignToSibling("title", constraint: alignTopToSiblingBottom(20)))
            >>> appendSubView(presenter.doneButton
            |> alignedToCenterX
            >>> alignToBottom(-100)
            >>> width_Height(200, 60)
            >>> styledTitle(darkGreyAttributedString("Done", 20))
            >>> roundedGreyShadowed(30))
        self.view = view
    }
}
extension TextUserInfoView {
    convenience init(presenter: TextUserInfoPresenter) {
        self.init()
        self.presenter = presenter
        self.setViews()
    }
}
