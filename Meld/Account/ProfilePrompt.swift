//
//  ProfilePrompt.swift
//  Meld
//
//  Created by blakerogers on 3/14/20.
//  Copyright © 2020 com.meld.kinnectus. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import UtilitiesPackage

public let profilePrompt: () -> (UILabel, UILabel, Observable<HomeCoordination>, Observable<Void>, UIView) = {
    let displayPromptGesture = UITapGestureRecognizer()
    let didSelectDisplayPrompts = displayPromptGesture.rx.event.map { _ in HomeCoordination.displayPrompts }
    let editAnswerGesture = UITapGestureRecognizer()
    let didSelectEditAnswer = editAnswerGesture.rx.event.map { _ in ()}
    let promptTypeLabel = UILabel()
                            |> isInterActive
                            >>> textFont(appFont(14))
                            >>> textColored(appDarkGrey)
                            >>> addGesture(displayPromptGesture)
                            >>> asLabel
    
    let promptAnswerLabel = UILabel()
                            |> isInterActive
                            >>> textFont(appFont(18))
                            >>> textColored(appBlack)
                            >>> addGesture(editAnswerGesture)
                            >>> asLabel
    
    let view = UIView()
                    |> utilizeConstraints
                    >>> appendSubView(promptTypeLabel
                                            |> named("prompt")
                                            >>> utilizeConstraints
                                            >>> titled("Select A Prompt For Your Profile")
                                            >>> alignToLeft(10)
                                            >>> alignToTop(10))
                    >>> appendSubView(promptAnswerLabel
                                        |> utilizeConstraints
                                        >>> alignToLeft(10)
                                        >>> alignToSibling("prompt", constraint: alignTopToSiblingBottom(10)))
    return (promptTypeLabel, promptAnswerLabel, didSelectDisplayPrompts, didSelectEditAnswer, view)
}
   
