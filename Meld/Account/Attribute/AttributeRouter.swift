//
//  AttributeRouter.swift
//  Meld
//
//  Created by Blake Rogers on 8/6/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UtilitiesPackage

final class AttributeRouter {
    func displayEditAttribute(attribute: Attributes, manager: AttributeEditManager) {
        switch attribute {
        case .age, .height:
            let view = SliderOptionSelectionRouter.presentation(attribute: attribute, manager: manager)
            currentController().present(view, animated: true, completion: nil)
        case .hometown:
            let view = TextUserInfoRouter.presentation(attribute: attribute, manager: manager)
            currentController().present(view, animated: true, completion: nil)
        default:
            let view = ScrollOptionSelectionRouter.presentation(attribute: attribute, manager: manager)
            currentController().present(view, animated: true, completion: nil)
        }
    }
}

