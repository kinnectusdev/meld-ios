//
//  AttributePresenter.swift
//  Meld
//
//  Created by Blake Rogers on 8/6/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import RxSwift
import RxCocoa
import UtilitiesPackage
import UIKit

final class AttributePresenter {
    
    private let bag = DisposeBag()
    private let interactor: AttributeInteractorIdentity
    private let router: AttributeRouter
    
    let typeLabel = UILabel()
        |> named("typeLabel")
        >>> textFont(appFontBold(18))
        >>> textColored(appBlack)
        >>> utilizeConstraints
        >>> asLabel
    
    let infoLabel = UILabel()
        |> textFont(appFont(16))
        >>> textColored(appBlack)
        >>> titled("None")
        >>> unlimitedLines
        >>> utilizeConstraints
        >>> asLabel
    
    init(interactor: AttributeInteractorIdentity) {
        
        self.router = AttributeRouter()
        self.interactor = interactor
        
        typeLabel.addTapGestureRecognizer { [weak self] in
            self?.router.displayEditAttribute(attribute: interactor.currentAttribute(), manager: interactor)
        }
        
        infoLabel.addTapGestureRecognizer { [weak self] in
            self?.router.displayEditAttribute(attribute: interactor.currentAttribute(), manager: interactor)
        }
        
        bag.insert(
            interactor.observeAttributeTitle()
                .asDriver(onErrorJustReturn: .empty)
                .drive(typeLabel.rx.text),
            interactor.observeAttributeDescription().asDriver(onErrorJustReturn: .empty)
                .drive(infoLabel.rx.text)
        )
    }
}
