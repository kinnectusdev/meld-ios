//
//  Age.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UtilitiesPackage
public enum Age: AttributeOption, Codable {
    case age(Int)
    
    public var description: String {
        switch self {
        case .age(let value): return "\(value)"
        }
    }
    public var defaultOption: AttributeOption {
        Age.age(18)
    }
    var value: Int {
        if case let .age(value) = self {
            return value
        } else {
            return 0
        }
    }
    var presentation: String {
        "\(value) y/0"
    }
    static var allCases: [Age] {
        (18...60).map { Age.age($0)}
    }
    public static func optionFromDescription(description: String) -> AttributeOption {
        return allCases
            |> filter { $0.description == description }
            |> first
            |> otherwise( .age(18))
    }
}
extension Age {
    static func >= (lhs: Age, rhs: Age) -> Bool {
        lhs.value >= rhs.value
    }
    static func <= (lhs: Age, rhs: Age) -> Bool {
        lhs.value <= rhs.value
    }
}
extension Age: RawRepresentable {
    public typealias RawValue = Int
    public init?(rawValue: Int) {
        self = Age.age(rawValue)
    }
    public var rawValue: Int {
        if case let .age(age) = self {
            return age
        } else {
            return 0
        }
    }
}
