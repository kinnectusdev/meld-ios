//
//  Ethnicity.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UtilitiesPackage

public enum Ethnicity: String, CaseIterable, AttributeOption, Codable {
    case native_American
    case african_Descent
    case east_Asian
    case middle_Eastern
    case hispanic_Latino
    case pacific_Islander
    case caucasian
    case prefer_Not_To_Say
    case south_Asian
    public var description: String {
        return "\(self)".replacingOccurrences(of: "_", with: " ").formalString()
    }
    public var defaultOption: AttributeOption {
        Ethnicity.prefer_Not_To_Say
    }
    public static func optionFromDescription(description: String) -> AttributeOption {
        return AllCases() |> filter { $0.description == description } |> first |> otherwise( .prefer_Not_To_Say)
    }
}
