//
//  Outlook.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UtilitiesPackage

public enum Outlook: String, CaseIterable, AttributeOption, Codable {
    case looking_For_Friend
    case looking_For__Relationship
    case looking_To_Hookup
    case looking_For_FWB
    case looking_For_Threesome
    case looking_To_Marry
    case looking_To_Date_Only
    case just_Curious
    public var description: String {
        return "\(self)".replacingOccurrences(of: "_", with: " ").formalString()
    }
    public var defaultOption: AttributeOption {
        Outlook.just_Curious
    }
    public static func optionFromDescription(description: String) -> AttributeOption {
        return AllCases() |> filter { $0.description == description } |> first |> otherwise( .looking_For_Friend)
    }
}
