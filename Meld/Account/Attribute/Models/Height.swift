//
//  Height.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UtilitiesPackage
//height
public enum Height: AttributeOption, Codable  {
    case fourFoot(Int)
    case fiveFoot(Int)
    case sixFoot(Int)
    case sevenFoot(Int)
    public static var all: [Height] {
        return (48...96).map { Height.height($0)}
    }
    public var defaultOption: AttributeOption {
        Height.fiveFoot(6)
    }
    public var baseHeight: Int {
        switch self {
        case .fourFoot: return 48
        case .fiveFoot: return 60
        case .sixFoot: return 72
        case .sevenFoot: return 84
        }
    }
    public var inches: Int {
        switch self {
        case .fourFoot(let inches), .fiveFoot(let inches), .sixFoot(let inches),.sevenFoot(let inches):
        return inches
        }
    }
    public var height: Int {
        switch self {
        case .fourFoot(let inches), .fiveFoot(let inches), .sixFoot(let inches), .sevenFoot(let inches):
            return baseHeight + inches
        }
    }
    public var description: String {
         switch self {
         case let .fourFoot(inches):
             return "4 ft \(inches) inches"
         case let .fiveFoot(inches):
                return "5 ft \(inches) inches"
         case let .sixFoot(inches):
            return "6 ft \(inches) inches"
         case let .sevenFoot(inches):
            return "7 ft \(inches) inches"
        }
    }
    public static func height(_ height: Int) -> Height {
        let inches = height % 12
        switch height - inches {
        case (48...59):
            return Height.fourFoot(inches)
        case (60...71):
            return Height.fiveFoot(inches)
        case (72...83):
            return Height.sixFoot(inches)
        case (84...96):
            return Height.sevenFoot(inches)
        default:
            return Height.fourFoot(0)
        }
    }
    public static func optionFromDescription(description: String) -> AttributeOption {
        return Height.all
            |> filter { $0.description == description }
            |> first
            |> otherwise( .fiveFoot(6))
    }
}
extension Height {
    static func <= (lhs: Height, rhs: Height) -> Bool  {
        lhs.height <= rhs.height
    }
    static func >= (lhs: Height, rhs: Height) -> Bool  {
        lhs.height >= rhs.height
    }
}
extension Height: RawRepresentable {
    public typealias RawValue = Int
    public init?(rawValue: Int) {
        self = Height.height(rawValue)
    }
    public var rawValue: Int {
       height
    }
}
