//
//  EducationLevel.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UtilitiesPackage

//educationLevel
public enum EducationLevel: String, CaseIterable, AttributeOption, Codable {
    case someHighSchool
    case ged
    case highschoolDiploma
    case someCollege
    case associatesDegree
    case mastersDegree
    case doctoralDegree
    public var description: String {
        switch self {
        case .someHighSchool: return "Some High School"
        case .ged: return "G.E.D"
        case .highschoolDiploma: return "High School Graduate"
        case .someCollege: return "Some College"
        case .associatesDegree: return "Associate's Degree"
        case .mastersDegree: return "Master's Degree"
        case .doctoralDegree: return "Doctoral Degree"
        }
    }
    public var defaultOption: AttributeOption {
        EducationLevel.highschoolDiploma
    }
    public static func optionFromDescription(description: String) -> AttributeOption {
        return AllCases() |> filter { $0.description == description } |> first |> otherwise( .someCollege)
    }
}
