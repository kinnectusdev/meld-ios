//
//  Physique.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UtilitiesPackage
//physique
public enum Physique: String, CaseIterable, AttributeOption, Codable {
    case skinny
    case slim
    case thick
    case curvy
    case lean
    case athletic
    case plump
    case slightly_Overweight
    case overweight
    case stocky
    case obese
    public var description: String {
         switch self {
         default:
            return "\(self)".replacingOccurrences(of: "_", with: " ").formalString()
        }
    }
    public var defaultOption: AttributeOption {
        Physique.slim
    }
    public static func optionFromDescription(description: String) -> AttributeOption {
        return AllCases() |> filter { $0.description == description } |> first |> otherwise( .slim)
    }
}
