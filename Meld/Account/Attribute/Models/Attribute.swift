//
//  Attribute.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
public protocol AttributeOption {
    var description: String { get }
    var defaultOption: AttributeOption { get }
    static func optionFromDescription(description: String) -> AttributeOption
}
extension AttributeOption {
    func category() -> Attributes? {
        return Attributes.allCases.filter { $0.containsOption(option: self) }.first
    }
    var typeAndValue: (String, String) {
        return (category()?.description ?? .empty, description)
    }
    var listDescription: [String] {
        if self is Vices {
            return Vices.Vice.allCases.filter { $0 != .prefer_Not_To_Say }.map { $0.description }
        } else if self is CoreValues {
            return CoreValues.CoreValue.allCases.filter { $0 != .prefer_Not_To_Say }.map { $0.description }
        } else {
            return [self.description]
        }
    }
    var isListable: Bool {
        listDescription.filter { $0 != "Prefer Not To Say" }.notEmpty()
    }
}

public enum Attributes: CaseIterable {
    case age
    case iq
    case height
    case ethnicity
    case hometown
    case gender
    case sexuality
    case politics
    case vices
    case core_Values
    case parental_Status
    case family_Plans
    case outlook
    case education_Level
    case religion
    case physique
    public var description: String {
        return "\(self)".replacingOccurrences(of: "_", with: " ").formalString()
    }
    var options: [AttributeOption] {
        switch self {
        case .ethnicity: return Ethnicity.allCases
        case .gender: return Gender.allCases
        case .sexuality: return Sexuality.allCases
        case .politics: return Politics.allCases
        case .vices: return [Vices(vices: Vices.Vice.allCases)]
        case .core_Values: return [CoreValues(values: CoreValues.CoreValue.allCases)]
        case .parental_Status: return ParentalStatus.allCases
        case .family_Plans: return FamilyPlans.allCases
        case .outlook: return Outlook.allCases
        case .education_Level: return EducationLevel.allCases
        case .religion: return Religion.allCases
        case .physique: return Physique.allCases
        case .iq: return IQ.allCases
        case .age, .height, .hometown: fatalError("dont use these")
        }
    }
    
    func containsOption(option: AttributeOption) -> Bool {
        switch self {
        case .ethnicity where option is Ethnicity: return true
        case .gender where option is Gender: return true
        case .sexuality where option is Sexuality: return true
        case .politics where option is Politics: return true
        case .vices where option is Vices: return true
        case .core_Values where option is CoreValues: return true
        case .parental_Status where option is ParentalStatus: return true
        case .family_Plans where option is FamilyPlans: return true
        case .outlook where option is Outlook: return true
        case .education_Level where option is EducationLevel: return true
        case .religion where option is Religion: return true
        case .physique where option is Physique: return true
        case .iq where option is IQ: return true
        case .age where option is Age: return true
        case .height where option is Height: return true
        case .hometown where option is Hometown: return true
        default: return false
        }
    }
}
