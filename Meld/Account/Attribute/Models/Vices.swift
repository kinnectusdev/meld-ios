//
//  Vices.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UtilitiesPackage


public struct Vices: AttributeOption, Codable {
    enum Vice: String, CaseIterable, Codable, CodingKey {
     
        case drinking
        case smoking
        case marijuana
        case drugs
        case prefer_Not_To_Say
        var description: String {
            return "\(self.rawValue)".formalString()
        }
        var isListable: Bool {
            return description != "Prefer_not_to_say"
        }
    }
    var vices: [Vice]
    public var defaultOption: AttributeOption {
        Vices(vices: [])
    }
    public var description: String {
        let validVices = vices.filter { $0 != .prefer_Not_To_Say }
        let vice = validVices.first?.description ?? .empty
        if validVices.count > 1  {
            return "\(vice) +\(validVices.count - 1) more"
        } else {
            return vice
        }
    }
    public static func optionFromDescription(description: String) -> AttributeOption {
        fatalError("Not Applicable")
    }
    public static func random() -> Vices {
        let vices = (0...([0,1,2,3].randomElement() ?? 0))
            .map { _ in  Vices.Vice.allCases.randomElement() }
            .compactMap  { $0  } |> uniqueElements
        return Vices(vices: vices)
    }
    static let empty: Vices = Vices(vices: [])
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encode(vices)
    }
    public init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        let values = try container.decode([Vice].self)
        vices = values
    }
    init(vices: [Vice]) {
        self.vices = vices
    }
}
