//
//  IQ.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UtilitiesPackage
//iq
public enum IQ: String, CaseIterable, AttributeOption, Codable {
    case between_80_to_100
    case between_100_to_120
    case between_120_to_140
    case over_140
    case over_160
    case over_180
    case dont_Know
   
    public var description: String {
         switch self {
         default:
            return "\(self)".replacingOccurrences(of: "_", with: " ").formalString()
        }
    }
    
    public var defaultOption: AttributeOption {
        IQ.dont_Know
    }
    
    public static func optionFromDescription(description: String) -> AttributeOption {
        return AllCases() |> filter { $0.description == description } |> first |> otherwise( .dont_Know)
    }
    
    var score: Int {
        switch self {
        case .between_80_to_100, .dont_Know: return 80
        case .between_100_to_120: return 100
        case .between_120_to_140: return 120
        case .over_140: return 140
        case .over_160: return 160
        case .over_180: return 180
        }
    }
    
    static func iq(value: Int) -> IQ {
        switch value {
        case (80...100): return .between_80_to_100
        case (100...120): return .between_120_to_140
        case (120...140): return .between_120_to_140
        case (140...160): return .over_140
        case (160...180): return .over_160
        case (180...(Int.max)): return .over_180
        default: return .dont_Know
        }
    }
}
