//
//  Religion.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UtilitiesPackage

//religion
public enum Religion: String, CaseIterable, AttributeOption, Codable {
    case atheist
    case agnostic
    case bahái
    case buddhism
    case christianity
    case confucianism
    case druze
    case gnosticism
    case hinduism
    case indigenous_American_Religion
    case islam
    case jainism
    case judaism
    case rastafarianism
    case shinto
    case sikhism
    case traditional_African_Religion
    case zoroastrianism
    case prefer_Not_To_Say
    public var description: String {
        switch self {
        default:
            return "\(self)".replacingOccurrences(of: "_", with: " ").formalString()
        }
    }
    public var defaultOption: AttributeOption {
        Religion.prefer_Not_To_Say
    }
    public static func optionFromDescription(description: String) -> AttributeOption {
        return AllCases() |> filter { $0.description == description } |> first |> otherwise( .prefer_Not_To_Say)
    }
}
