//
//  Hobbies.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UtilitiesPackage
//hobbies
public enum Hobbies: String, CaseIterable, AttributeOption, Codable {
    case reading
    case exercising
    case music
    case dancing
    case swimming
    case walking
    case hiking
    case biking
    case poetry
    case writing
    case martial_Arts
    case football
    case basketball
    case soccer
    case cricket
    case baseball
    case softball
    case board_games
    case traveling
    case sky_Diving
    case none
    public var description: String {
         switch self {
         default:
            return "\(self)".replacingOccurrences(of: "_", with: " ").formalString()
        }
    }
    public var defaultOption: AttributeOption {
        Hobbies.none
    }
    public static func optionFromDescription(description: String) -> AttributeOption {
        return AllCases() |> filter { $0.description == description } |> first |> otherwise( .none)
    }
}
