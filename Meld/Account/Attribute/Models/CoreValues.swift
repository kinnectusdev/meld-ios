//
//  CoreValues.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UtilitiesPackage

struct CoreValues: AttributeOption, Codable {
   
    enum CoreValue: String, CaseIterable, Codable, CodingKey  {
        case accomplishment
        case accountability
        case accuracy
        case acknowledgement
        case adventure
        case all_For_One_And_One_For_All
        case authenticity
        case beauty
        case calm_Quietude_And_Peace
        case challenge
        case change
        case cleanliness_And_Orderliness
        case collaboration
        case commitment
        case common_Sense
        case communication
        case community
        case competence
        case competition
        case concern_For_Others
        case connection
        case content_Over_Form
        case continuous_Improvement
        case cooperation
        case coordination
        case courage
        case creativity
        case customer_Satisfaction
        case decisiveness
        case delight_Of_Being_Or_Joy
        case democracy
        case discipline
        case discovery
        case diversity
        case ease_Of_Use
        case efficiency
        case equality
        case excellence
        case expertise
        case fairness
        case faith
        case faithfulness
        case family
        case family_Feeling
        case flair
        case freedom
        case friendship
        case fun
        case genius
        case global_View
        case good_Will
        case goodness
        case gratitude
        case hard_Work
        case harmony
        case health
        case honesty
        case honor
        case improvement
        case independence
        case individuality
        case inner_Peace_Calm_Quietude
        case innovation
        case integrity
        case intensity
        case intimacy
        case ingenuity
        case justice
        case knowledge
        case leadership
        case love_Or_Romance
        case loyalty
        case mastery
        case meaning
        case merit
        case methodical
        case money
        case openness
        case order
        case originality
        case patriotism
        case peace_And_NonViolence
        case perfection
        case personal_Growth
        case pleasure
        case power
        case practicality
        case preservation
        case privacy
        case progress
        case prosperity_And_Wealth
        case punctuality
        case quality_Of_Work
        case regularity
        case reliability
        case resourcefulness
        case respect_For_Others
        case responsiveness
        case results_Oriented
        case rule_Of_Law
        case safety
        case satisfying_Others
        case security
        case self_Giving
        case self_Reliance
        case self_Thinking
        case service_To_Others
        case seeing_Clearly
        case simplicity
        case skill
        case solving_Problems
        case speed
        case spirit_In_Life
        case spirituality
        case stability
        case standardization
        case status
        case stimulation
        case strength
        case succeeding//( a will toSuccess)
        case systemization
        case teamwork
        case timeliness
        case tolerance
        case tradition
        case tranquility
        case trust
        case truth
        case unity
        case variety
        case wisdom
        case prefer_Not_To_Say
        public var description: String {
             switch self {
             default:
                return "\(self.rawValue)".replacingOccurrences(of: "_", with: " ").formalString()
            }
        }
    }
    var values: [CoreValue]
    
    static let fromList: ([String]) -> CoreValues = { list in
        let allValues = CoreValue.allCases
        return CoreValues(values: list |> map { valueString in
            return allValues |> filter { $0.description  == valueString } |> first |> otherwise(.prefer_Not_To_Say)
        })
    }
    public var description: String {
        let validValues = values.filter { $0 != .prefer_Not_To_Say }
        let value = validValues.first?.description ?? .empty
        if validValues.count > 1  {
            return "\(value) +\(validValues.count - 1) more"
        } else {
            return value
        }
    }
    public var defaultOption: AttributeOption {
        CoreValues(values: [])
    }
    public static let model: CoreValues = CoreValues(values: CoreValue.allCases)
    public static func optionFromDescription(description: String) -> AttributeOption {
        fatalError("Not Applicable")
    }
    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encode(values)
    }
    public init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        let values = try container.decode([CoreValue].self)
        self.values = values
    }
    static func random() -> CoreValues {
        let values = (0...([0,1,2,3].randomElement() ?? 0))
            .map { _ in  CoreValues.CoreValue.allCases.randomElement() }
            .compactMap  { $0  } |> uniqueElements
        return CoreValues(values: values)
    }
    static let empty: CoreValues = CoreValues(values: [])
    init(values: [CoreValue]) {
        self.values = values
    }
}
