//
//  PaternalStatus.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UtilitiesPackage

public enum ParentalStatus: String, CaseIterable, AttributeOption, Codable {
    case no_Children
    case coparenting
    case single_Parent
    case couple_Of_Children
    case few_Children
    case several_Children
    case want_Children
    case want_No_Children
    public var description: String {
        return "\(self)".replacingOccurrences(of: "_", with: " ").formalString()
    }
    public var defaultOption: AttributeOption {
        ParentalStatus.no_Children
    }
    public static func optionFromDescription(description: String) -> AttributeOption {
        return AllCases() |> filter { $0.description == description } |> first |> otherwise( .no_Children)
    }
}
