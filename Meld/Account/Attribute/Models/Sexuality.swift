//
//  Sexuality.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UtilitiesPackage

public enum Sexuality: String, CaseIterable, AttributeOption, Codable {
    case androgynous
    case bisexual
    case gay
    case heterosexual
    case lesbian
    case pansexual
    case questioning
    case prefer_Not_To_Say
    public var description: String {
        return "\(self)".replacingOccurrences(of: "_", with: " ").formalString()
    }
    public var defaultOption: AttributeOption {
        Sexuality.prefer_Not_To_Say
    }
    public static func optionFromDescription(description: String) -> AttributeOption {
        return AllCases() |> filter { $0.description == description } |> first |> otherwise( .prefer_Not_To_Say)
    }
}
