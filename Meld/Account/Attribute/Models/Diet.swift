//
//  Diet.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UtilitiesPackage
//diet
public enum Diet: String, CaseIterable, AttributeOption, Codable {
    case standard_American
    case vegetarian
    case pescatarian
    case vegan
    case raw_Vegan
    case gluten_Free
    case paleo
    case ketogenic
    case determined_by_religious_restrictions
    public var description: String {
         switch self {
         default:
            return "\(self)".replacingOccurrences(of: "_", with: " ").formalString()
        }
    }
    public var defaultOption: AttributeOption {
        Diet.standard_American
    }
    public static func optionFromDescription(description: String) -> AttributeOption {
        return AllCases()
            |> filter { $0.description == description }
            |> first
            |> otherwise( .standard_American)
    }
}
