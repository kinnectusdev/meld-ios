//
//  AttributeView.swift
//  Meld
//
//  Created by Blake Rogers on 8/6/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import UIKit

final class AttributeView: UIView {
    
    private var presenter: AttributePresenter!
    
    private func setViews() {
        add(views: presenter.infoLabel, presenter.typeLabel)
        presenter.typeLabel.constrainInView(view: self, top:0, left: 0)
        presenter.infoLabel.constrainLeftToLeft(of: presenter.typeLabel, constant: 0)
        presenter.infoLabel.constrainTopToBottom(of: presenter.typeLabel, constant: 4)
    }
}

extension AttributeView {
    
    convenience init(presenter: AttributePresenter) {
        self.init()
        self.presenter = presenter
        self.setViews()
    }
}

