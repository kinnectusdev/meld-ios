//
//  AttributeInteractor.swift
//  Meld
//
//  Created by Blake Rogers on 8/6/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import RxSwift
import RxCocoa

protocol AttributeEditManager {
    func onDidEditAttribute(option: AttributeOption)
}


protocol AttributeInteractorIdentity: AttributeEditManager {
    
    func observeAttributeTitle() -> Observable<String>
    func observeAttributeDescription() -> Observable<String>
    func observeAttribute() -> Observable<Attributes>
    
    func currentAttribute() -> Attributes
    
}


final class AttributeInteractor {
    
    private let bag = DisposeBag()
    private let attribute: Attributes
    
    private let title = BehaviorRelay<String>(value: .empty)
    private let description = BehaviorRelay<String>(value: .empty)
    
    init(attribute: Attributes) {
        
        self.attribute = attribute
        
        bag.insert(
            Observable.just(attribute.description)
                .bind(to: title),
            UserService.currentUser()
                .map { $0.user }
                .compactMap { $0 }
                .map { user in
                    user.attributes
                        .filter { $0?.category() == attribute }
                        .first??.description ?? .empty
            }.bind(to: description)
        )
    }
}

extension AttributeInteractor: AttributeInteractorIdentity {
    
    func onDidEditAttribute(option: AttributeOption) {
        guard let attribute = option.category() else { return }
        UserService.updateAttribute(attribute: attribute, option: option).subscribe(onNext: { [weak self] _ in
            self?.description.accept(option.description)
        }).dispose()
    }
    
    func currentAttribute() -> Attributes {
        attribute
    }
    
    func observeAttributeTitle() -> Observable<String> {
        title.asObservable()
    }
    
    func observeAttributeDescription() -> Observable<String> {
        description.asObservable()
    }
    
    func observeAttribute() -> Observable<Attributes> {
        Observable.just(attribute)
    }
}
