//
//  ScrollOptionSelectionPresenter.swift
//  Meld
//
//  Created by Blake Rogers on 8/6/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import RxSwift
import RxCocoa
import UtilitiesPackage
import UIKit

final class ScrollOptionSelectionPresenter {
    
    private let bag = DisposeBag()
    private let interactor: ScrollOptionSelectionInteractorIdentity
    private let router: ScrollOptionSelectionRouter
    
    let titleLabel = UILabel()
    let table = UITableView()
        |> registerTableCell(OptionCell.self, OptionCell.identifier)
        |> tableWithoutSeparators
        |> asTable
    
    init(interactor: ScrollOptionSelectionInteractorIdentity) {
        
        self.interactor = interactor
        self.router = ScrollOptionSelectionRouter()
        self.table.allowsMultipleSelection = interactor.observeAllowsMultipleSelection()
        bag.insert(
            interactor.observeTitle().asDriver(onErrorJustReturn: .empty).drive(titleLabel.rx.text),
            interactor.observeAttributeOptions()
                .bind(to: table.rx.items(cellIdentifier: OptionCell.identifier, cellType: OptionCell.self))
                { index, item, cell in
                    cell.configure(text: item, isSelected: interactor.observeIsSelectedOption(value: item))
                },
            table.rx.itemSelected.map { $0.row }.subscribe(onNext: { [weak self] index in
                self?.interactor.didSelectOption(index: index)
                self?.table.reloadData()
            })
        )
    }
}
