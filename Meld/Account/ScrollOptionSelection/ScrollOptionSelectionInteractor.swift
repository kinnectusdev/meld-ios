//
//  ScrollOptionSelectionInteractor.swift
//  Meld
//
//  Created by Blake Rogers on 8/6/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import RxSwift
import RxCocoa
import UtilitiesPackage

protocol ScrollOptionSelectionInteractorIdentity {
    
    func observeAttributeOptions() -> Observable<[String]>
    func observeTitle() -> Observable<String>
    
    func observeIsSelectedOption(value: String) -> Bool
    func observeAllowsMultipleSelection() -> Bool
    
    func didSelectOption(index: Int)
}

final class ScrollOptionSelectionInteractor {
    private let bag = DisposeBag()
    private let editManager: AttributeEditManager
    private let attributeOptions: [String]
    private let attribute: Attributes
    private var selectedOption = BehaviorRelay<AttributeOption?>(value: nil)
    private var _selectedOption: AttributeOption?
    
    init(attribute: Attributes, manager: AttributeEditManager) {
        self.attribute = attribute
        self.editManager = manager
        switch attribute {
        case .vices:
            self.attributeOptions = Vices.empty.listDescription
        case .core_Values:
            self.attributeOptions = CoreValues.empty.listDescription
        default:
            self.attributeOptions = attribute.options.map { $0.description }
        }
        
        bag.insert(
            UserService.observeAttribute(attribute: attribute).do(onNext: { [weak self] option in
                self?._selectedOption = option
            }).bind(to: selectedOption)
        )
    }
}

extension ScrollOptionSelectionInteractor: ScrollOptionSelectionInteractorIdentity {
    func observeAllowsMultipleSelection() -> Bool {
        attribute == .vices && attribute == .core_Values
    }
    func observeTitle() -> Observable<String> {
        Observable.just(attribute.description)
    }
    
    func observeAttributeOptions() -> Observable<[String]> {
        Observable.just(attributeOptions)
    }
    
    func didSelectOption(index: Int) {
        
        switch attribute {
        case .vices:
            guard let vice = Vices.Vice.allCases.itemAt(index) else { return }
            guard let option = _selectedOption as? Vices else {
                self._selectedOption = Vices(vices: [vice])
                return
            }
            if option.vices.contains(vice) {
                self._selectedOption = (option |> prop(\.vices)({ vices in vices |> removing(vice)}))
            } else {
                self._selectedOption = (option |> prop(\.vices)({ vices in vices.appending(vice)}))
            }
        case .core_Values:
            guard let value = CoreValues.CoreValue.allCases.itemAt(index) else { return }
            guard let option = _selectedOption as? CoreValues else {
                self._selectedOption = CoreValues(values: [value])
                return
            }
            if option.values.contains(value) {
                self._selectedOption = (option |> prop(\.values)({ values in values |> removing(value)}))
            } else {
                self._selectedOption = (option |> prop(\.values)({ values in
                        values.count == 10 ? values: values.appending(value)
                }))
            }
        default:
            guard let option = attribute.options.itemAt(index) else { return }
            self._selectedOption = option
        }
        
        guard let option = _selectedOption else { return }
        editManager.onDidEditAttribute(option: option)
    }
    
    func observeIsSelectedOption(value: String) -> Bool {
        guard let option = _selectedOption else { return false }
        if let option = option as? CoreValues {
            return option.values.map { $0.description }.contains(value)
        } else if let option = option as? Vices {
            return option.vices.map { $0.description }.contains(value)
        } else {
            return option.listDescription.contains(value)
        }
    }
}
