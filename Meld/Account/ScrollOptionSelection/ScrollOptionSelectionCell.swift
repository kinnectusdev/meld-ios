//
//  ScrollOptionSelectionCell.swift
//  Meld
//
//  Created by Blake Rogers on 8/6/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import UIKit
import UtilitiesPackage

class OptionCell: UITableViewCell {

static let identifier: String = "OptionCell"

private let label = UILabel()
   |> textFont(appFont(16))
   >>> textColored(appBlack)
   >>> utilizeConstraints
   >>> asLabel

override func layoutSubviews() {
   super.layoutSubviews()
   setViews()
   self.selectionStyle = .none
}

private func setViews() {
   contentView.addSubview(label)
   label.constrainCenterToCenter(of: contentView)
}

func configure(text: String, isSelected: Bool) {
   label.text = text
   label.font = isSelected ? boldAppFont(16) : appFont(16)
}
}
