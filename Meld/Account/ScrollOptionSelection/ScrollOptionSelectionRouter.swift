//
//  ScrollOptionSelectionRouter.swift
//  Meld
//
//  Created by Blake Rogers on 8/6/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import UIKit
import UtilitiesPackage

final class ScrollOptionSelectionRouter {
    func dismiss() {
        currentController().dismiss(animated: true, completion: nil)
    }
}
extension ScrollOptionSelectionRouter {
    static func presentation(attribute: Attributes, manager: AttributeEditManager) -> UIViewController {
        ScrollOptionSelectionView(presenter: ScrollOptionSelectionPresenter(interactor: ScrollOptionSelectionInteractor(attribute: attribute, manager: manager)))
    }
}
