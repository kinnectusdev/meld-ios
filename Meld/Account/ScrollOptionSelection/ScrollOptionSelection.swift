//
//  ScrollOptionSelection.swift
//  Meld
//
//  Created by Blake Rogers on 8/6/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import UIKit
import UtilitiesPackage

final class ScrollOptionSelectionView: UIViewController {
    
    private var presenter: ScrollOptionSelectionPresenter!
    
    private func setViews() {
        let view = UIView()
        |> rounded(10)
        |> whiteBackground
        |> appendSubView(presenter.titleLabel
            |> named("title")
            >>> textFont(appFontFormal(18))
            >>> alignToTop(20)
            >>> alignedToCenterX
            >>> textColored(appBlack))
        |> appendSubView(UIView()
            |> backgroundColored(appDarkGrey)
            |> width_Height(200, 1)
            |> alignedToCenterX
            |> alignToSibling("title", constraint: alignTopToSiblingBottom(4)))
        |> appendSubView(presenter.table |> alignToTop(50) >>> alignedToLeftBottomRight)
        
        self.view = view
    }
}
extension ScrollOptionSelectionView {
    convenience init(presenter: ScrollOptionSelectionPresenter) {
        self.init()
        self.presenter = presenter
        self.setViews()
    }
}
  
