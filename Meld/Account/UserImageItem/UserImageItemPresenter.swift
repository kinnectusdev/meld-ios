//
//  UserImageItemPresenter.swift
//  Meld
//
//  Created by Blake Rogers on 8/6/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import RxSwift
import RxCocoa
import NVActivityIndicatorView
import UtilitiesPackage
import UIKit

final class UserImageItemPresenter {
    private let bag = DisposeBag()
    private let interactor: UserImageInteractorIdentity
    private let router: UserImageItemRouter
    
    let image = UIImageView()
        |> rounded(10)
        |> diamondBackground
        |> utilizeConstraints
        |> scaledToFill
        |> asImage
        
    
    let loadingIndicator = NVActivityIndicatorView(
        frame: CGRect(origin: .zero, size: CGSize(width: 50, height: 50)),
        type: .ballClipRotate,
        color: appBlack,
        padding: nil)
    
    let deleteImageButton = UIButton()
        |> styledTitle(deleteFontIcon(size: 10))
        |> silverBackground
        |> greyShadowed
        |> utilizeConstraints
        |> asButton
    
    let addImageButton = UIButton()
        |> styledTitle(addFontIcon(size: 10))
        |> silverBackground
        |> greyShadowed
        |> utilizeConstraints
        |> asButton
    
    init(interactor: UserImageInteractorIdentity) {
        
        self.interactor = interactor
        self.router = UserImageItemRouter()
        
        bag.insert(
            interactor.observeUserImage()
                .do(onNext: { [weak self ] _ in
                    self?.loadingIndicator.isHidden = false
                    self?.loadingIndicator.startAnimating()
                })
                .subscribe(onNext: { [weak self] data in
                        if let data = data {
                            self?.image.image = UIImage(data: data)
                            self?.addImageButton.isHidden = true
                            self?.deleteImageButton.isHidden = false
                        } else {
                            self?.image.image = nil
                            self?.addImageButton.isHidden = false
                            self?.deleteImageButton.isHidden = true
                        }
                    self?.loadingIndicator.stopAnimating()
                    self?.loadingIndicator.isHidden = true
                }),
            addImageButton.rx.tap.subscribe(onNext: { [weak self] _ in
                self?.router.displayImageSelection(imageSelectionDelegate: interactor)
            }),
            deleteImageButton.rx.tap.subscribe(onNext: { [weak self] in
                self?.interactor.didSelectDeleteImage()
            }),
            interactor.observeDidAddImage().subscribe(onNext: { [weak self] image in
                self?.image.image = image
                self?.router.dismissImageSelection()
            })
        )
    }
}
