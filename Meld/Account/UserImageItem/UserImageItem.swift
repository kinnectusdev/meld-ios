//
//  UserImageItem.swift
//  Meld
//
//  Created by Blake Rogers on 6/5/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import UIKit
import UtilitiesPackage

final class UserImageItemView: UIView {
    
    private var presenter: UserImageItemPresenter!
    
    private func setViews() {
        
        let view = UIView()
            |> fill
            |> appendSubView(presenter.image
            |> named("image")
            |> width_Height(Style.imageWidth, Style.imageHeight)
             >>> alignToLeft(8)
             >>> alignToTop(8)
             >>> alignToRight(-8)
             >>> alignToBottom(-8)
            >>> appendSubView(presenter.loadingIndicator |> alignedToCenter))
            |> appendSubView(presenter.deleteImageButton
            |> named("deleteImage")
            >>> alignedToCenterX
            >>> alignToSibling("image", constraint: alignCenterYToSiblingBottom(-4)))
            |> appendSubView(presenter.addImageButton
            |> width_Height(Style.buttonSize, Style.buttonSize)
            |> rounded(Style.buttonRadius)
            |> alignToSibling("deleteImage", constraint: alignedToSiblingCenter))
        
        add(views: view)
    }
}

extension UserImageItemView {
    
    convenience init(presenter: UserImageItemPresenter) {
        self.init()
        self.presenter = presenter
        self.setViews()
    }
}

extension UserImageItemView {
    
    struct Style {
        static let imageItemWidth: CGFloat = (UIScreen.main.bounds.width - 20)*0.33
        static let imageItemHeight: CGFloat = (imageItemWidth * 16) / 9
        static let imageWidth: CGFloat = imageItemWidth*0.9
        static let imageHeight: CGFloat = imageItemHeight*0.9
        static let buttonSize: CGFloat = 20
        static let buttonRadius: CGFloat = buttonSize*0.5
    }
    
}
