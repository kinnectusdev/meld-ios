//
//  UserImageItemRouter.swift
//  Meld
//
//  Created by Blake Rogers on 8/6/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import UIKit
import UtilitiesPackage

final class UserImageItemRouter {
    
    func displayImageSelection(imageSelectionDelegate: ImageSelectionDelegate) {
        let controller = ImageSelectionRouter.presentation(imageSelectionDelegate: imageSelectionDelegate)
        currentController().present(controller, animated: true)
    }
    
    func dismissImageSelection() {
        currentController().dismiss(animated: true, completion: nil)
    }
}
extension UserImageItemRouter {
    static func presentation(index: Int, imageManager: ImageManagerIdentity) -> UIView {
        UserImageItemView(presenter: UserImageItemPresenter(interactor: UserImageInteractor(index: index, imageManager: imageManager)))
    }
}
