//
//  UserImageItemInteractor.swift
//  Meld
//
//  Created by Blake Rogers on 8/6/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import RxSwift
import RxCocoa
import UIKit

protocol ImageManagerIdentity {
    func image(index: Int) -> Observable<String>
}

protocol UserImageInteractorIdentity: ImageSelectionDelegate {
    func observeUserImage() -> Observable<Data?>
    func didSelectDeleteImage()
    func observeDidAddImage() -> Observable<UIImage>
}

final class UserImageInteractor {
    
    private let bag = DisposeBag()
    private let imageManager: ImageManagerIdentity
    
    private var rawURL: String = .empty
    private let imageURL = BehaviorRelay<String>(value: .empty)
    private let userImage = BehaviorRelay<Data?>(value: nil)
    private let didAddImage = PublishSubject<UIImage>()
    
    init(index: Int, imageManager: ImageManagerIdentity) {
        
        self.imageManager = imageManager
        
        bag.insert (
            imageManager.image(index: index)
                .filter { !$0.isEmpty }
                .flatMap { url -> Observable<Data?> in
                MediaImageService.fetchImageData(from: url)
            }.bind(to: userImage)
        )
    }
}

extension UserImageInteractor: UserImageInteractorIdentity {
    
    func observeUserImage() -> Observable<Data?> {
        userImage.asObservable()
    }
    
    func didSelectDeleteImage() {
        userImage.accept(nil)
        UserService.deleteUserImage(url: rawURL).subscribe(onNext: { [weak self] result in
            if !result {
                
            }
        }).disposed(by: bag)
    }
    
    func didAddImage(image: UIImage) {
        didAddImage.onNext(image)
        UserService.addUserImage(image: image.pngData()).subscribe(onNext: { [weak self] result in
            if !result {
                
            }
        }).disposed(by: bag)
    }
    
    func observeDidAddImage() -> Observable<UIImage> {
        didAddImage.asObservable()
    }
}
