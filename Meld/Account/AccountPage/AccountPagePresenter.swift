//
//  AccountPagePresenter.swift
//  Meld
//
//  Created by Blake Rogers on 7/24/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import RxSwift
import RxCocoa
import UtilitiesPackage
import UIKit

final class AccountPagePresenter {
    private let bag = DisposeBag()
    private let interactor: AccountPageInteractorIdentity
    private let router: AccountPageRouter
    
    let logoutButton = UIButton()
        |> named("logout_buton")
        >>> styledTitle(darkGreyAttributedString("Sign Out", 12))
        >>> asButton
    
    let list = UIScrollView()
    let baseUserInfoView: UIView
    let userImagesView: UIView
    let profileInfoView: UIView
    let backButton = UIButton()
    |> visibility( DeviceInfo.deviceType == .iPad ? 1.0 : 0.0 )
    |> asButton
    init(interactor: AccountPageInteractorIdentity) {
        self.interactor = interactor
        self.router = AccountPageRouter()
        
        self.baseUserInfoView = BaseUserInfoView(presenter: BaseUserInfoPresenter(interactor: BaseUserInfoInteractor()))
        self.userImagesView = UserImagesView(presenter: UserImagesPresenter(interactor: UserImagesInteractor()))
        self.profileInfoView = UserProfileInfoView(presenter: UserProfileInfoPresenter(interactor: UserProfileInfoInteractor()))
        
        bag.insert(
            logoutButton.rx.tap.subscribe(onNext: { [weak self] _ in
                self?.interactor.didSelectLogout()
                self?.router.displayOnboarding()
            }),
            backButton.rx.tap.subscribe(onNext: { [weak self] _ in
                    self?.router.dismiss()
            })
        )
        
    }
}
