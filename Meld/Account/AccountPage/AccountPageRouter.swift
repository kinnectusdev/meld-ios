//
//  AccountPageRouter.swift
//  Meld
//
//  Created by Blake Rogers on 8/6/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import UIKit
import UtilitiesPackage

final class AccountPageRouter {
    func dismiss(){
        currentController().dismiss(animated: true, completion: nil)
    }
    func displayOnboarding() {
        let view = OnboardingRouter.presentation()
        currentWindow().rootViewController = view
        currentWindow().makeKeyAndVisible()
    }
}
extension AccountPageRouter {
    static func presentation() -> UIViewController {
        AccountPage(presenter: AccountPagePresenter(interactor: AccountPageInteractor()))
    }
}
