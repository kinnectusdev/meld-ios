//
//  AccountPageInteractor.swift
//  Meld
//
//  Created by Blake Rogers on 8/6/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation

protocol AccountPageInteractorIdentity {
    func didSelectLogout()
}

final class AccountPageInteractor {
    
}
extension AccountPageInteractor: AccountPageInteractorIdentity {
    func didSelectLogout() {
        UserService.logout()
    }
}
