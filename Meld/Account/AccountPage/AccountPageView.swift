//
//  AccountPage.swift
//  Meld
//
//  Created by blakerogers on 11/28/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import UIKit
import UtilitiesPackage

final class AccountPage: UIViewController {
    
    private var presenter: AccountPagePresenter!
        
    private func setViews() {
        
        let view =
            UIView()
            |> whiteBackground
            |> appendSubView(
                presenter.list
                |> contentSize(appFrame.width, 1950)
                |> framed(CGRect(x: 0, y: 0, width: appFrame.width, height: appFrame.height))
                |> appendSubView(
                    presenter.baseUserInfoView
                    |> framed(CGRect(x: 0, y: 0, width: appFrame.width, height: 350)))
                |> appendSubView(
                    presenter.userImagesView
                    |> framed(CGRect(x: 0, y: 350, width: appFrame.width, height: 525))
                )
                |> appendSubView(
                    presenter.profileInfoView
                    |> framed(CGRect(x: 0, y: 875, width: appFrame.width, height: 900)))
                |> appendSubView(
                    presenter.logoutButton
                    |> framed(CGRect(x: appFrame.width * 0.5 - 100, y: 1800, width: 200, height: 50))
                )
            )
            >>> appendSubView(
                    presenter.backButton
                    |> named("back_button")
                    >>> setImage(UIImage(named: "BackButton"))
                    >>> alignToLeft(10)
                    >>> alignToTop(70)
            )
        self.view = view
    }
}

extension AccountPage {
    convenience init(presenter: AccountPagePresenter) {
        self.init()
        self.presenter = presenter
        self.setViews()
        self.modalPresentationStyle = ( DeviceInfo.deviceType == .iPad ? .overCurrentContext : .automatic)
    }
}


