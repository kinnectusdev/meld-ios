//
//  UserImagesInteractor.swift
//  Meld
//
//  Created by Blake Rogers on 8/6/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import RxSwift
import RxCocoa

protocol UserImagesInteractorIdentity: ImageManagerIdentity {
}

final class UserImagesInteractor {
    
    private let bag = DisposeBag()
    
    private let userImages = BehaviorRelay<[String]>(value: [])
    
    init() {
        
        bag.insert(
            UserService.currentUser().compactMap { $0.user }.map { $0.imageURLs }.bind(to: userImages)
        )
    }
}
extension UserImagesInteractor: UserImagesInteractorIdentity {
    func image(index: Int) -> Observable<String> {
        userImages.asObservable().map { $0.itemAt(index) }.map { $0 ?? .empty }
    }
    func observeUserImages() -> Observable<[String]> {
        userImages.asObservable()
    }
}

struct AssignRandomImages {
    static func random(isMale: Bool) -> String {
        (0...(isMale ? 65 : 60)).map { isMale ? "male\($0)" : "female\($0)"}.randomElement() ?? .empty
    }
}
