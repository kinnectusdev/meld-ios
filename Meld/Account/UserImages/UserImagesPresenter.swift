//
//  UserImagesPresenter.swift
//  Meld
//
//  Created by Blake Rogers on 8/6/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import RxSwift
import UIKit

final class UserImagesPresenter {
    
    private let bag = DisposeBag()
    private let interactor: UserImagesInteractorIdentity
    
    let userImageOne: UIView
    let userImageTwo: UIView
    let userImageThree: UIView
    let userImageFour: UIView
    let userImageFive: UIView
    let userImageSix: UIView
    
    init(interactor: UserImagesInteractorIdentity) {
        self.interactor = interactor
        userImageOne = UserImageItemRouter.presentation(index: 0, imageManager: interactor)
        userImageTwo = UserImageItemRouter.presentation(index: 1, imageManager: interactor)
        userImageThree = UserImageItemRouter.presentation(index: 2, imageManager: interactor)
        userImageFour = UserImageItemRouter.presentation(index: 3, imageManager: interactor)
        userImageFive = UserImageItemRouter.presentation(index: 4, imageManager: interactor)
        userImageSix = UserImageItemRouter.presentation(index: 5, imageManager: interactor)
    }
}
