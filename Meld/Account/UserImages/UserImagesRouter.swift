//
//  UserImagesRouter.swift
//  Meld
//
//  Created by Blake Rogers on 8/6/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//


import UIKit

final class UserImagesRouter {
    static func presentation() -> UIView {
        UserImagesView(presenter: UserImagesPresenter(interactor: UserImagesInteractor()))
    }
}
