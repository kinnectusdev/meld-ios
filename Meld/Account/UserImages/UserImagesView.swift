//
//  UserImagesCollectionView.swift
//  Meld
//
//  Created by Blake Rogers on 5/18/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import UIKit
import UtilitiesPackage

final class UserImagesView: UIView {
    private var presenter: UserImagesPresenter!
    
    private let title: UIView = {
        return UILabel()
        |> titled("Images")
        >>> named("title")
        >>> textFont(appFontFormal(18))
        >>> textColored(appBlack)
        >>> utilizeConstraints
    }()
    
    let underline: UIView = {
       return UIView()
        |> backgroundColored(appGrey)
        |> alignToSibling("title", constraint: alignedToSiblingLeft)
        |> viewHeight(1)
        |> utilizeConstraints
    }()
    
    private func setViews() {
        self.add(views: title, underline, presenter.userImageOne, presenter.userImageTwo, presenter.userImageThree, presenter.userImageFour, presenter.userImageFive, presenter.userImageSix)
        
        title.constrainInView(view: self, top: 20, left: 20)
        
        underline.constrainTopToBottom(of: title , constant: 4)
        underline.constrainInView(view: self, left: 0, right: 0)
        
        [presenter.userImageOne, presenter.userImageTwo, presenter.userImageThree, presenter.userImageFour, presenter.userImageFive, presenter.userImageSix].forEach { $0.translatesAutoresizingMaskIntoConstraints = false}
        presenter.userImageTwo.constrainCenterXTo(view: underline, constant: 0)
        presenter.userImageTwo.constrainTopToBottom(of: underline, constant: 8)
    
        presenter.userImageOne.constrainTopToTop(of: presenter.userImageTwo, constant: 0)
        presenter.userImageOne.constrainRightToLeft(of: presenter.userImageTwo, constant: -4)
    
        presenter.userImageThree.constrainTopToTop(of: presenter.userImageTwo, constant: 0)
        presenter.userImageThree.constrainLeftToRight(of: presenter.userImageTwo, constant: 4)
   
        presenter.userImageFive.constrainCenterXTo(view: underline, constant: 0)
        presenter.userImageFive.constrainTopToBottom(of: presenter.userImageTwo, constant: 4)
        
        presenter.userImageFour.constrainTopToTop(of: presenter.userImageFive, constant: 0)
        presenter.userImageFour.constrainRightToLeft(of: presenter.userImageFive, constant: -4)
        
        presenter.userImageSix.constrainTopToTop(of: presenter.userImageFive, constant: 0)
        presenter.userImageSix.constrainLeftToRight(of: presenter.userImageFive, constant: 4)
    }
}

extension UserImagesView {
    
    convenience init(presenter: UserImagesPresenter) {
        self.init()
        self.presenter = presenter
        self.setViews()
    }
    
    func configure(presenter: UserImagesPresenter) {
        self.presenter = presenter
        self.setViews()
    }
}
