//
//  UserProfileInfoPresenter.swift
//  Meld
//
//  Created by Blake Rogers on 8/6/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//
import UIKit
import RxSwift
import UtilitiesPackage

final class UserProfileInfoPresenter {
    private let bag = DisposeBag()
    private let interactor: UserProfileInfoInteractorIdentity
    private let router: UserProfileInfoRouter
    let attributes: [UIView]
    
    init(interactor: UserProfileInfoInteractorIdentity) {
        self.interactor = interactor
        self.router = UserProfileInfoRouter()
        
        self.attributes = Attributes.allCases.map { attribute -> UIView in
                AttributeView(presenter: AttributePresenter(interactor: AttributeInteractor(attribute: attribute)))
                    |> width_Height(appFrame.width*0.4, 100)
        }
    }
}
