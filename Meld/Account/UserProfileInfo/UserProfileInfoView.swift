//
//  UserProfileInfoView.swift
//  Meld
//
//  Created by blakerogers on 3/31/20.
//  Copyright © 2020 com.meld.kinnectus. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import UtilitiesPackage

final class UserProfileInfoView: UIView {
    private var presenter: UserProfileInfoPresenter!
    
    private func setViews() {
        let view = UIView()
        |> whiteBackground
        |> fill
        >>> appendSubView(UILabel()
            |> titled("About Me")
            >>> named("title")
            >>> textFont(appFontFormal(18))
            >>> alignedToTop
            >>> alignToLeft(20)
            >>> textColored(appBlack))
        >>> appendSubView(UIView()
            |> viewHeight(1)
            >>> backgroundColored(appGrey)
            >>> alignToLeft(20)
            >>> alignToRight(-20)
            >>> alignToSibling("title", constraint: alignTopToSiblingBottom(4)))
        >>> appendSubView(UIView()
            |> named("underline")
            |> alignToLeft(20)
            >>> alignedToRight
            >>> alignedToBottom
            >>> alignToSibling("title", constraint: alignTopToSiblingBottom(20))
            >>> appendSubviewsVertically(presenter.attributes.groupBy(limit: 2).map { views -> UIView in
                UIView()
                |> appendSubviewsHorizontally(views, 16)
                |> width_Height(appFrame.width*0.8, 100)
            }))
        
        add(views: view)
    }
}

extension UserProfileInfoView {
    convenience init(presenter: UserProfileInfoPresenter) {
        self.init()
        self.presenter = presenter
        self.setViews()
    }
    func configure(presenter: UserProfileInfoPresenter) {
        self.presenter = presenter
        self.setViews()
    }
}
