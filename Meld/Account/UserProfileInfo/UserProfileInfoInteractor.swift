//
//  UserProfileInfoInteractor.swift
//  Meld
//
//  Created by Blake Rogers on 8/6/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol UserProfileInfoInteractorIdentity {
    
}

final class UserProfileInfoInteractor {
    
    
}

extension UserProfileInfoInteractor: UserProfileInfoInteractorIdentity {
  
}
