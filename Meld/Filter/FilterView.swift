//
//  FilterView.swift
//  Meld
//
//  Created by Blake Rogers on 8/23/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import UtilitiesPackage
import UIKit

final class FilterView: UIViewController {
    private var presenter: FilterPresenter!
    private func setViews() {
        let view = UIView()
        |> whiteBackground
        |> appendSubView(
            UILabel()
            |> named("title")
            |> styledTitle(formalBlackAttributedString("Filter", 20))
            |> alignToTop(60)
            |> alignedToCenterX
        )
        |> appendSubView(
            presenter.list
            |> alignToSibling("title", constraint: alignTopToSiblingBottom(20))
            |> alignedToLeftBottomRight
            |> appendSubView(presenter.filterAppliedFilter)
            |> appendSubView(presenter.searchRadiusFilter)
            |> appendSubView(presenter.sexualityFilter)
            |> appendSubView(presenter.genderFilter)
            |> appendSubView(presenter.ethnicityFilter)
            |> appendSubView(presenter.vicesFilter)
            |> appendSubView(presenter.ageRangeFilter)
            |> appendSubView(presenter.heightRangeFilter)
        )
        >>> appendSubView(
            presenter.backButton
            |> named("back_button")
            >>> setImage(UIImage(named: "BackButton"))
            >>> alignToLeft(10)
            >>> alignToSibling("title", constraint: alignToSiblingCenterY())
        )
        self.view = view
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        presenter.onDismiss()
    }
}
extension FilterView {
    convenience init(presenter: FilterPresenter) {
        self.init()
        self.presenter = presenter
        self.modalPresentationStyle = ( DeviceInfo.deviceType == .iPad ? .overCurrentContext : .automatic)
        self.setViews()
    }
}

