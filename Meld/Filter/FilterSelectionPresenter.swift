//
//  FilterSelectionPresenter.swift
//  Meld
//
//  Created by Blake Rogers on 8/24/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import UIKit
import UtilitiesPackage

protocol FilterSelectionPresenterIdentity {
    
    var titleLabel: UILabel { get }
    var filterSwitch: UISwitch { get }
    var list: UIScrollView { get }
    var options: [UIView] { get }
}

final class FilterAttributeSelectionPresenter: FilterSelectionPresenterIdentity {
    
    private let bag = DisposeBag()
    var titleLabel: UILabel = UILabel()
    var filterSwitch: UISwitch = UISwitch()
    var list: UIScrollView = UIScrollView()
    var options: [UIView] = []
    
    init(attribute: Attributes,
         observeSelectedOption: Observable<AttributeOption>,
         onSelectAttribute: @escaping (AttributeOption) -> Void) {
        var title: String {
            switch attribute {
            case .gender:
                return "Partner's Gender"
            case .sexuality:
                return "Partner's Sexuality"
            case .ethnicity:
                return "Partner's Ethnicity"
            default: return .empty
            }
        }
        self.titleLabel.text = title
        
        self.list.contentSize = CGSize(width: attribute.options.count.cgFloat() * appFrame.width * 0.33,
                                       height: 30)
        self.filterSwitch.setOn(false, animated: false)
        self.options = attribute.options.filter { $0.isListable }.map { option in
            UILabel()
            |> named(option.description)
            |> titled(option.description)
            |> textFont(appFont(12))
            |> textColored(appDarkGrey)
            |> { view in
                view.addTapGestureRecognizer {
                    onSelectAttribute(option)
                    self.filterSwitch.setOn(true, animated: true)
                }
               return view
            }
            |> framed(CGRect(x: (attribute.options.filter { $0.isListable }.map { $0.description } |> index(of: option.description)).cgFloat() * appFrame.width * 0.33,
                             y: 0,
                             width: appFrame.width * 0.33,
                             height: 30))
        }
        
        bag.insert(
            observeSelectedOption.subscribe(onNext: { [weak self] option in
                self?.options.map { $0 as? UILabel }.compactMap { $0 }.forEach { label in
                    if label.name == option.description {
                        label.font = appFontBold(12)
                    } else {
                        label.font = appFont(12)
                    }
                }
                guard option.isListable else { return }
                self?.filterSwitch.setOn(true, animated: false)
            }),
            filterSwitch.rx.isOn.skip(1).distinctUntilChanged().subscribe(onNext: { [weak self] _ in
                guard let filter = self?.filterSwitch, !filter.isOn else { return }
                var preferNeither: AttributeOption? {
                    switch attribute {
                    case .ethnicity:
                        return Ethnicity.prefer_Not_To_Say
                    case .gender:
                        return Gender.prefer_Not_To_Say
                    case .sexuality:
                        return Sexuality.prefer_Not_To_Say
                    default:
                        return nil
                    }
                }
                guard let option = preferNeither else {
                    return
                }
                onSelectAttribute(option)
            })
        )
    }
}


