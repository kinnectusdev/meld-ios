//
//  FilterInteractor.swift
//  Meld
//
//  Created by Blake Rogers on 8/24/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol FilterUpdateInteractorIdentity {
    func updateIsFilterApplied(isApplied: Bool)
    func updateSearchRadius()
    func updatePreferredOption(option: AttributeOption)
    func updateIntolerableVices(vice: Vices.Vice)
    func updatePreferredMinAge(age: Age)
    func updatePreferredMaxAge(age: Age)
    func updatePreferredMinHeight(height: Height)
    func updatePreferredMaxHeight(height: Height)
}

protocol FilterObservableInteractorIdentity {
    func observeDidUpdateFilter() -> Bool
    func observeIsFilterApplied() -> Observable<Bool>
    func observeSearchRadius() -> Observable<Int>
    func observePreferredOption(attribute: Attributes) -> Observable<AttributeOption>
    func observeIntolerableVices() -> Observable<[Vices.Vice]>
    func observePreferredMinAge() -> Observable<Age>
    func observePreferredMaxAge() -> Observable<Age>
    func observePreferredMinHeight() -> Observable<Height>
    func observePreferredMaxHeight() -> Observable<Height>
    func observePreferredEthnicity() -> Observable<Ethnicity>
}

protocol FilterInteractorIdentity: FilterUpdateInteractorIdentity, FilterObservableInteractorIdentity {
}

final class FilterInteractor: FilterInteractorIdentity {
    private let bag = DisposeBag()
    private let settings = BehaviorRelay<FilterSettings?>(value: nil)
    let initialFilter: FilterSettings
    init() {
        initialFilter = FilterService.fetchFilterSettings()
        let filter = FilterService.observeFilterSettings().share()
        bag.insert(
            filter.bind(to: settings)
        )
        
    }
}

extension FilterInteractor {
    func observeDidUpdateFilter() -> Bool {
        guard let filter = settings.value else {
            return true
        }
        return filter != initialFilter
    }
    func observePreferredOption(attribute: Attributes) -> Observable<AttributeOption> {
        switch attribute {
        case .ethnicity:
            return settings.compactMap { $0?.preferredEthnicity }
        case .gender:
            return settings.compactMap { $0?.preferredGender }
        case .sexuality:
            return settings.compactMap { $0?.preferredSexuality }
        default: return .never()
        }
    }
    
    func observeSearchRadius() -> Observable<Int> {
        settings.compactMap { $0?.searchRadius }
    }
    
    func observeIsFilterApplied() -> Observable<Bool> {
        settings.compactMap { $0?.isFilterApplied }
    }
    
    func observePreferredMinAge() -> Observable<Age> {
        settings.compactMap { $0?.preferredMinAge }
    }
    
    func observePreferredMaxAge() -> Observable<Age> {
        settings.compactMap { $0?.preferredMaxAge }
    }
    
    func observeIntolerableVices() -> Observable<[Vices.Vice]> {
        settings.compactMap { $0?.intolerableVices.vices }
    }
    
    func observePreferredMinHeight() -> Observable<Height> {
        settings.compactMap { $0?.preferredMinHeight}
    }
    
    func observePreferredMaxHeight() -> Observable<Height> {
        settings.compactMap { $0?.preferredMaxHeight}
    }
    func observePreferredEthnicity() -> Observable<Ethnicity> {
        settings.compactMap { $0?.preferredEthnicity }
    }
}
extension FilterInteractor {
    func updatePreferredOption(option: AttributeOption) {
        switch option.category() {
        case .age:
            break
        case .height:
            break
        case .ethnicity:
            guard let ethnicity = option as? Ethnicity else { return }
            FilterService.updateSettings(preferredEthnicity: ethnicity)
        case .gender:
            guard let gender = option as? Gender else { return }
            FilterService.updateSettings(preferredGender: gender)
        case .sexuality:
            guard let sexuality = option as? Sexuality else { return }
            FilterService.updateSettings(preferredSexuality: sexuality)
        default: break
        }
    }
    
    func updateSearchRadius() {
        settings.take(1).compactMap { $0?.searchRadius }.subscribe(onNext: { radius in
            switch radius {
            case 5:
                FilterService.updateSettings(searchRadius: 25)
            case 25:
                FilterService.updateSettings(searchRadius: 50)
            case 50:
                FilterService.updateSettings(searchRadius: 100)
            case 100:
                FilterService.updateSettings(searchRadius: 5)
            default:
                FilterService.updateSettings(searchRadius: 5)
            }
        }).disposed(by: bag)
    }
    
    func updateIsFilterApplied(isApplied: Bool) {
        FilterService.updateSettings(isFilterApplied: isApplied)
    }
    
    func updatePreferredMinAge(age: Age) {
        FilterService.updateSettings(preferredMinAge: age)
    }
    
    func updatePreferredMaxAge(age: Age) {
        FilterService.updateSettings(preferredMaxAge: age)
    }
    
    func updateIntolerableVices(vice: Vices.Vice) {
        FilterService.updateSettings(intolerableVice: vice)
    }
    
    func updatePreferredMinHeight(height: Height) {
        FilterService.updateSettings(preferredMinHeight: height)
    }
    
    func updatePreferredMaxHeight(height: Height) {
        FilterService.updateSettings(preferredMaxHeight: height)
    }
}
