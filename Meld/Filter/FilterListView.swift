//
//  FilterListView.swift
//  Meld
//
//  Created by Blake Rogers on 8/24/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import UIKit
import UtilitiesPackage

final class FilterListView: UIView {
    private var presenter: FilterSelectionPresenterIdentity!
    private func setViews() {
        let view = UIView()
        |> fill
        |> appendSubView(
            presenter.titleLabel
            |> named("title")
            |> textFont(appFont(16))
            |> textColored(appDarkGrey)
            |> alignToLeft(20)
            |> alignedToTop
        )
        |> appendSubView(
            UIView()
            |> backgroundColored(appDiamond)
            |> alignToSibling("title", constraint: alignTopToSiblingBottom(8))
            |> viewHeight(1)
            |> alignToLeft(20)
            |> alignToRight(-20)
        )
        |> appendSubView(
            presenter.filterSwitch
            |> switchColor(appDarkGrey)
            |> alignToRight(-20)
            |> alignToSibling("title", constraint: alignedToSiblingCenterY)
        )
        |> appendSubView(
            presenter.list
                |> utilizeConstraints
                |> alignToLeft(20)
                |> alignToRight(-20)
                |> alignToSibling("title", constraint: alignTopToSiblingBottom(16))
                |> alignedToBottom
                |> appendSubviews(presenter.options)
        )
        
        self.add(views: view)
    }
}
extension FilterListView {
    convenience init(presenter: FilterSelectionPresenterIdentity) {
        self.init()
        self.presenter = presenter
        self.setViews()
    }
}
