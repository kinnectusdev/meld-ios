//
//  FilterPresenter.swift
//  Meld
//
//  Created by Blake Rogers on 8/24/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import UtilitiesPackage
import UIKit

final class FilterPresenter {
    
    private let bag = DisposeBag()
    private let interactor: FilterInteractorIdentity
    private let router: FilterRouter
    
    let backButton = UIButton()
    |> visibility( DeviceInfo.deviceType == .iPad ? 1.0 : 0.0 )
    |> asButton
    let filterAppliedFilter: UIView
    let searchRadiusFilter: UIView
    let sexualityFilter: UIView
    let genderFilter: UIView
    let ethnicityFilter: UIView
    let vicesFilter: UIView
    let ageRangeFilter: UIView
    let heightRangeFilter: UIView
    let list = UIScrollView()
    
    init(interactor: FilterInteractorIdentity) {
        
        self.interactor = interactor
        self.router = FilterRouter()
        self.list.contentSize = CGSize(width: appFrame.width, height: 800)
        
        let filterAppliedPresenter = FilterAppliedPresenter(observeIsFilterApplied: interactor.observeIsFilterApplied(), onToggleApplyFilter: interactor.updateIsFilterApplied)
        
        self.filterAppliedFilter = (FilterAppliedView(presenter: filterAppliedPresenter)
                                        |> framed(CGRect(x: 0, y: 0, width: appFrame.width, height: 50)))
        
        let searchRadiusFilterPresenter = SearchRadiusFilterPresenter(observeSearchRadius: interactor.observeSearchRadius(), onToggleRadius: interactor.updateSearchRadius)
        
        self.searchRadiusFilter = (SearchRadiusFilterView(presenter: searchRadiusFilterPresenter)
                                    |> framed(CGRect(x: 0, y: 70, width: appFrame.width, height: 30))
        )
        
        let sexualityPresenter = FilterAttributeSelectionPresenter(attribute: .sexuality,
                                                                   observeSelectedOption: interactor.observePreferredOption(attribute: .sexuality),
                                                                   onSelectAttribute: interactor.updatePreferredOption)
        
        self.sexualityFilter = (FilterListView(presenter: sexualityPresenter)
                                    |> framed(CGRect(x: 0, y: 120, width: appFrame.width, height: 80)))
        
        let genderPresenter = FilterAttributeSelectionPresenter(attribute: .gender,
                                                                observeSelectedOption: interactor.observePreferredOption(attribute: .gender),
                                                                onSelectAttribute: interactor.updatePreferredOption)
        
        self.genderFilter = (FilterListView(presenter: genderPresenter)
                                |> framed(CGRect(x: 0, y: 220, width: appFrame.width, height: 80)))
        
        let ethnicityPresenter = FilterAttributeSelectionPresenter(attribute: .ethnicity,
                                                                observeSelectedOption: interactor.observePreferredOption(attribute: .ethnicity),
                                                                onSelectAttribute: interactor.updatePreferredOption)
        
        self.ethnicityFilter = (FilterListView(presenter: ethnicityPresenter)
                                |> framed(CGRect(x: 0, y: 320, width: appFrame.width, height: 80)))
        
        let vicesPresenter = FilterVicesSelectionPresenter(observeSelectedOption: interactor.observeIntolerableVices(),
                                                           onSelectVice: interactor.updateIntolerableVices)
        
        self.vicesFilter = (FilterListView(presenter: vicesPresenter)
                                |> framed(CGRect(x: 0, y: 420, width: appFrame.width, height: 80)))
        
        let filterAgePresenter = FilterAgePresenter(observePreferredMinAge: interactor.observePreferredMinAge(),
                                                    observePreferredMaxAge: interactor.observePreferredMaxAge(),
                                                    onUpdateMinAge: interactor.updatePreferredMinAge,
                                                    onUpdateMaxAge: interactor.updatePreferredMaxAge)
        
        self.ageRangeFilter = (FilterRangedAttributeView(presenter: filterAgePresenter)
                                |> framed(CGRect(x: 0, y: 520, width: appFrame.width, height: 100)))

        let filterHeightPresenter = FilterHeightPresenter(observePreferredMinHeight: interactor.observePreferredMinHeight(),
                                                          observePreferredMaxHeight: interactor.observePreferredMaxHeight(),
                                                          onUpdateMinHeight: interactor.updatePreferredMinHeight,
                                                          onUpdateMaxHeight: interactor.updatePreferredMaxHeight)
        
        self.heightRangeFilter = (FilterRangedAttributeView(presenter: filterHeightPresenter)
                                    |> framed(CGRect(x: 0, y: 640, width: appFrame.width, height: 100)))
        bag.insert(
            backButton.rx.tap.subscribe(onNext: { [weak self] _ in
                self?.router.dismiss()
            })
        )
    }
    func onDismiss() {
        if interactor.observeDidUpdateFilter() {
            GlobalEventNotifier.shared.emit(event: .didUpdateFilter)
        }
    }
}

