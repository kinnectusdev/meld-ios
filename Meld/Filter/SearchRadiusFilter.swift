//
//  SearchRadiusFilter.swift
//  Meld
//
//  Created by Blake Rogers on 8/24/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UIKit
import UtilitiesPackage

final class SearchRadiusFilterView: UIView {
    private var presenter: SearchRadiusFilterPresenter!
    private func setViews() {
        let view = UIView()
        |> whiteBackground
        |> fill
        |> appendSubView(
            UILabel()
            |> named("title")
            |> styledTitle(darkGreyAttributedString("Search Radius", 16))
            |> alignedToCenterY
            |> alignToLeft(20)
        )
        |> appendSubView(
            presenter.radiusButton
            |> width_Height(80, 30)
            |> rounded(15)
            |> bordered(appDiamond, 1)
            |> alignToRight(-20)
            |> alignedToCenterY
        )
        
        self.add(views: view)
    }
}
extension SearchRadiusFilterView {
    convenience init(presenter: SearchRadiusFilterPresenter) {
        self.init()
        self.presenter = presenter
        self.setViews()
    }
}
