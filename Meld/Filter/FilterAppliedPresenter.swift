//
//  FilterAppliedPresenter.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import UIKit

final class FilterAppliedPresenter {
    
    private let bag = DisposeBag()
    let filterSwitch = UISwitch()
    
    init(observeIsFilterApplied: Observable<Bool>, onToggleApplyFilter: @escaping (Bool) -> Void) {
        
        bag.insert(
            observeIsFilterApplied.asDriver(onErrorJustReturn: false).drive(filterSwitch.rx.isOn),
            filterSwitch.rx.isOn.skip(1).distinctUntilChanged().subscribe(onNext: { [weak self] _ in
                guard let filter = self?.filterSwitch else { return }
                onToggleApplyFilter(filter.isOn)
            })
        )
    }
}
