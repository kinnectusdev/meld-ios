//
//  FilterVicesSelectionPresenter.swift
//  Meld
//
//  Created by Blake Rogers on 8/24/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import UIKit
import UtilitiesPackage

final class FilterVicesSelectionPresenter: FilterSelectionPresenterIdentity {
    private let bag = DisposeBag()
    var titleLabel: UILabel = UILabel()
    var filterSwitch: UISwitch = UISwitch()
    var list: UIScrollView = UIScrollView()
    var options: [UIView] = []
    
    init(observeSelectedOption: Observable<[Vices.Vice]>, onSelectVice: @escaping (Vices.Vice) -> Void) {
        self.titleLabel.text = "Vices I can't Tolerate"
        self.list.contentSize = CGSize(width: Vices.Vice.allCases.count.cgFloat() * appFrame.width * 0.33,
                                       height: 30)
        self.filterSwitch.setOn(false, animated: false)
        self.options = Vices.Vice.allCases.filter { $0.isListable }.map { vice in
            UILabel()
            |> named(vice.description)
            |> titled(vice.description)
            |> textFont(appFont(12))
            |> textColored(appDarkGrey)
            |> { view in
                view.addTapGestureRecognizer {
                    onSelectVice(vice)
                    self.filterSwitch.setOn(true, animated: true)
                }
               return view
            }
            |> framed(CGRect(x: (Vices.Vice.allCases.filter { $0.isListable } |> index(of: vice)).cgFloat() * appFrame.width * 0.33,
                             y: 0, width: appFrame.width * 0.33, height: 30))
        }
        bag.insert(
            observeSelectedOption.subscribe(onNext: { [weak self] vices in
                self?.options.map { $0 as? UILabel }.compactMap { $0 }.forEach { label in
                    if vices.map { $0.description }.contains(label.name) {
                        label.font = appFontBold(12)
                    } else {
                        label.font = appFont(12)
                    }
                }
                guard vices.notEmpty() else { return }
                self?.filterSwitch.setOn(true, animated: true)
            }),
            filterSwitch.rx.isOn.skip(1).distinctUntilChanged().subscribe(onNext: { [weak self] _ in
                guard let filter = self?.filterSwitch, !filter.isOn else { return }
                onSelectVice(Vices.Vice.prefer_Not_To_Say)
            })

        )
    }
}

