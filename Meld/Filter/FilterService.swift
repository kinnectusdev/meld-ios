//
//  FilterService.swift
//  Meld
//
//  Created by Blake Rogers on 8/23/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import UtilitiesPackage

class FilterService {
    static func fetchFilterSettings() -> FilterSettings {
        do {
            guard let data = UserDefaults.standard.data(forKey: "filterSettings") else {
                return FilterSettings.empty
            }
            let settings = try JSONDecoder().decode(FilterSettings.self, from: data)
            return settings
        } catch {
            setFilterSettings(settings: FilterSettings.empty)
            return FilterSettings.empty
        }
    }
    static func observeFilterSettings() -> Observable<FilterSettings> {
        UserDefaults.standard.rx.observe(Data.self, "filterSettings").map { data -> FilterSettings in
            do {
                guard let data = data else {
                    setFilterSettings(settings: FilterSettings.empty)
                    return FilterSettings.empty
                }
                let settings = try JSONDecoder().decode(FilterSettings.self, from: data)
                return settings
            } catch {
                return FilterSettings.empty
            }
        }.compactMap { $0 }
    }
    static func setFilterSettings(settings: FilterSettings) {
        do {
            let data = try JSONEncoder().encode(settings)
            UserDefaults.standard.setValue(data, forKey: "filterSettings")
        } catch {
            assertionFailure(error.localizedDescription)
        }
    }
    
    static func updateSettings(preferredSexuality: Sexuality){
        let settings = fetchFilterSettings() |> prop(\.preferredSexuality)({_ in
            preferredSexuality
        })
        setFilterSettings(settings: settings)
    }
    
    static func updateSettings(preferredGender: Gender){
        let settings = fetchFilterSettings() |> prop(\.preferredGender)({_ in
            preferredGender
        })
        setFilterSettings(settings: settings)
    }
    
    static func updateSettings(searchRadius: Int){
        let settings = fetchFilterSettings() |> prop(\.searchRadius)({_ in
            searchRadius
        })
        setFilterSettings(settings: settings)
    }
    
    static func updateSettings(isFilterApplied: Bool){
        let settings = fetchFilterSettings() |> prop(\.isFilterApplied)({_ in
            isFilterApplied
        })
        setFilterSettings(settings: settings)
    }
    static func updateSettings(preferredMinAge: Age){
        let settings = fetchFilterSettings() |> prop(\.preferredMinAge)({_ in
            preferredMinAge
        })
        setFilterSettings(settings: settings)
    }
    static func updateSettings(preferredMaxAge: Age){
        let settings = fetchFilterSettings() |> prop(\.preferredMaxAge)({_ in
            preferredMaxAge
        })
        setFilterSettings(settings: settings)
    }
    static func updateSettings(intolerableVice: Vices.Vice){
        let settings = fetchFilterSettings() |> prop(\.intolerableVices)({ vices in
            if intolerableVice == .prefer_Not_To_Say {
                return Vices.empty
            }
            if vices.vices.contains(intolerableVice) {
                return vices |> prop(\.vices)({ $0 |> removing(intolerableVice)})
            } else {
                return vices |> prop(\.vices)({ $0 |> appending(intolerableVice)})
            }
        })
        setFilterSettings(settings: settings)
    }
    static func updateSettings(preferredEthnicity: Ethnicity){
        let settings = fetchFilterSettings() |> prop(\.preferredEthnicity)({_ in
            preferredEthnicity
        })
        
        setFilterSettings(settings: settings)
    }
    static func updateSettings(preferredMinHeight: Height){
        let settings = fetchFilterSettings() |> prop(\.preferredMinHeight)({_ in
            preferredMinHeight
        })
        setFilterSettings(settings: settings)
    }
    static func updateSettings(preferredMaxHeight: Height){
        let settings = fetchFilterSettings() |> prop(\.preferredMaxHeight)({_ in
            preferredMaxHeight
        })
        setFilterSettings(settings: settings)
    }
    static func validate(_ user: MeldUser) -> Bool {
        let settings = fetchFilterSettings()
        guard settings.isFilterApplied else  { return true }
        let a = user.sexuality == settings.preferredSexuality || settings.preferredSexuality == Sexuality.prefer_Not_To_Say
        let b = user.gender == settings.preferredGender || user.gender == Gender.prefer_Not_To_Say || settings.preferredGender == Gender.prefer_Not_To_Say
        let c = user.age ?? .age(18) >= settings.preferredMinAge
        let d = user.age ?? .age(18) <= settings.preferredMaxAge
        let e = (user.vices?.vices ?? []).reduce(true, { $0 && settings.intolerableVices.vices.contains($1)}) || settings.intolerableVices.vices.isEmpty
        let f = user.ethnicity == settings.preferredEthnicity || user.ethnicity == Ethnicity.prefer_Not_To_Say || settings.preferredEthnicity == Ethnicity.prefer_Not_To_Say
        let g = user.height ?? .fourFoot(0) >= settings.preferredMinHeight
        let h = user.height ?? .fourFoot(0) <= settings.preferredMaxHeight
        if !a {
            print("user \(user.name) sexuality of \(user.sexuality) is not \(settings.preferredSexuality) \n")
        }
        if !b {
            print("user \(user.name) gender of \(user.gender?.description) is not \(settings.preferredGender.description) \n")
        }
        if !c {
            print("user \(user.name) age of \(user.age.value) is less than \(settings.preferredMinAge.value) \n")
        }
        if !d {
            print("user \(user.name) age of \(user.age.value) is greater than \(settings.preferredMaxAge.value) \n")
        }
        if !e {
            print("user \(user.name) vices of \(user.vices?.listDescription) is not compatible with \(settings.intolerableVices.vices.description) \n")
        }
        if !f {
            print("user \(user.name) ethnicity of \(user.ethnicity) is not \(settings.preferredEthnicity) \n")
        }
        if !g {
            print("user \(user.name) height of \(user.height?.height) is less than \(settings.preferredMinHeight.height) \n ")
        }
        if !h {
            print("user \(user.name) height of \(user.height?.height) is greater than \(settings.preferredMaxHeight.height) \n")
        }
        return a && b && c && d && e && f && g && h
    }
}
