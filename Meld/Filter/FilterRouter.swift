//
//  FilterRouter.swift
//  Meld
//
//  Created by Blake Rogers on 8/24/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UIKit
import UtilitiesPackage

final class FilterRouter {
    func dismiss() {
        currentController().dismiss(animated: true, completion: nil)
    }
}

extension FilterRouter {
    static func presentation() -> UIViewController {
        FilterView(presenter: FilterPresenter(interactor: FilterInteractor()))
    }
}
