//
//  IsFilterAppliedView.swift
//  Meld
//
//  Created by Blake Rogers on 8/24/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UIKit
import UtilitiesPackage

final class FilterAppliedView: UIView {
    private var presenter: FilterAppliedPresenter!
    private func setViews() {
        let view = UIView()
        |> whiteBackground
        |> fill
        |> appendSubView(
            UILabel()
            |> named("title")
            |> styledTitle(darkGreyAttributedString("Enable Filter", 16))
            |> alignedToCenterY
            |> alignToLeft(20)
        )
        |> appendSubView(
            presenter.filterSwitch
            |> switchColor(appDarkGrey)
            |> alignToRight(-20)
            |> alignedToCenterY
        )
        
        self.add(views: view)
    }
}

extension FilterAppliedView {
    convenience init(presenter: FilterAppliedPresenter) {
        self.init()
        self.presenter = presenter
        self.setViews()
    }
}
