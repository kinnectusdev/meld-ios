//
//  FilterModels.swift
//  Meld
//
//  Created by Blake Rogers on 8/24/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation

struct FilterSettings: Codable {
    
    var preferredSexuality: Sexuality
    var preferredGender: Gender
    var searchRadius: Int
    var isFilterApplied: Bool
    var preferredMinAge: Age
    var preferredMaxAge: Age
    var intolerableVices: Vices
    var preferredEthnicity: Ethnicity
    var preferredMinHeight: Height
    var preferredMaxHeight: Height
    
}
extension FilterSettings {
    
    static let empty: FilterSettings = FilterSettings(preferredSexuality: .heterosexual, preferredGender: .male, searchRadius: 0, isFilterApplied: false, preferredMinAge: .age(18), preferredMaxAge: .age(18), intolerableVices: Vices(vices: []), preferredEthnicity: .african_Descent, preferredMinHeight: .fiveFoot(6), preferredMaxHeight: .fiveFoot(6))
    
    static func !=(lhs: FilterSettings, rhs: FilterSettings) -> Bool {
        let a = lhs.preferredSexuality != rhs.preferredSexuality
        let b = lhs.preferredGender != rhs.preferredGender
        let c = lhs.searchRadius != rhs.searchRadius
        let d = lhs.isFilterApplied != rhs.isFilterApplied
        let e = lhs.preferredMinAge != rhs.preferredMinAge
        let f = lhs.preferredMaxAge != rhs.preferredMaxAge
        let g = lhs.intolerableVices.vices != rhs.intolerableVices.vices
        let h = lhs.preferredEthnicity != rhs.preferredEthnicity
        let i = lhs.preferredMinHeight != rhs.preferredMinHeight
        let j = lhs.preferredMaxHeight != rhs.preferredMaxHeight
        return a || b || c || d || e || f || f || g || h || i || j
    }
}
