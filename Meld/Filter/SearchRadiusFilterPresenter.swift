//
//  SearchRadiusFilterPresenter.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import UtilitiesPackage
import UIKit

final class SearchRadiusFilterPresenter {
    
    private let bag = DisposeBag()
    
    let radiusButton = UIButton()
    init(observeSearchRadius: Observable<Int>, onToggleRadius: @escaping () -> Void) {
        
        
        bag.insert(
            observeSearchRadius.subscribe(onNext: { [weak self] radius in
                self?.radiusButton.setAttributedTitle(blackAttributedString("\(radius).0 mi", 16), for: .normal)
            }),
            radiusButton.rx.tap.subscribe(onNext: { onToggleRadius() })
        )
    }
}
