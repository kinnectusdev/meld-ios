//
//  FilterRangedAttributePresenter.swift
//  Meld
//
//  Created by Blake Rogers on 8/24/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import MultiSlider
import UtilitiesPackage
import UIKit

protocol FilterRangedAttributePresenterIdentity {
    var title: UILabel { get }
    var minLabel: UILabel { get }
    var maxLabel: UILabel { get }
    var slider: MultiSlider { get }
}

final class FilterAgePresenter: FilterRangedAttributePresenterIdentity {
    
    private let bag = DisposeBag()
    var title: UILabel = UILabel()
    var slider: MultiSlider = MultiSlider()
    var minLabel: UILabel = UILabel()
    var maxLabel: UILabel = UILabel()
    
    init(observePreferredMinAge: Observable<Age>,
         observePreferredMaxAge: Observable<Age>,
         onUpdateMinAge: @escaping (Age) -> Void,
         onUpdateMaxAge: @escaping (Age) -> Void) {
        
        title.text = "Partner's Age"
        slider.tintColor = appDarkGrey
        slider.outerTrackColor = appDiamond
        
        slider.minimumValue = (Age.allCases |> first |> { $0!.value } |> asCGFloat)
        slider.maximumValue = (Age.allCases |> last |>  { $0!.value } |> asCGFloat)
        slider.orientation = .horizontal
        
        bag.insert(
            Observable
                .combineLatest(observePreferredMinAge, observePreferredMaxAge, resultSelector: { ($0, $1)})
                .subscribe(onNext: { [weak self] min, max in
                    
                    self?.slider.value = [min.value.cgFloat(), max.value.cgFloat()]
                    self?.minLabel.text = "\(min.rawValue) years old"
                    self?.maxLabel.text = "\(max.rawValue) years old"
            }),
            slider.rx.controlEvent(.valueChanged).subscribe(onNext: { [weak self] _ in
                guard let slider = self?.slider else { return }
                guard let value = slider.value.itemAt(slider.draggedThumbIndex) else { return }
                guard let age = Age(rawValue: Int(value)) else { return }
                switch slider.draggedThumbIndex {
                case 0:
                    self?.minLabel.text = "\(age.rawValue) years old"
                case 1:
                    self?.maxLabel.text = "\(age.rawValue) years old"
                default: break
                }
            }),
            slider.rx.controlEvent(.touchUpInside).subscribe(onNext: { [weak self] _ in
                guard let slider = self?.slider else { return }
                guard let value = slider.value.itemAt(slider.draggedThumbIndex) else { return }
                guard let age = Age(rawValue: Int(value)) else { return }
                switch slider.draggedThumbIndex {
                case 0:
                    onUpdateMinAge(age)
                case 1:
                    onUpdateMaxAge(age)
                default: break
                }
            })
        )
    }
}

final class FilterHeightPresenter: FilterRangedAttributePresenterIdentity {
    
    private let bag = DisposeBag()
    var title: UILabel = UILabel()
    var slider: MultiSlider = MultiSlider()
    var minLabel: UILabel = UILabel()
    var maxLabel: UILabel = UILabel()
    
    init(observePreferredMinHeight: Observable<Height>,
         observePreferredMaxHeight: Observable<Height>,
         onUpdateMinHeight: @escaping (Height) -> Void,
         onUpdateMaxHeight: @escaping (Height) -> Void) {
        
        title.text = "Partner's Height"
        slider.tintColor = appDarkGrey
        slider.outerTrackColor = appDiamond

        slider.minimumValue = (Height.all |> first |> { $0!.height } |> asCGFloat)
        slider.maximumValue = (Height.sevenFoot(11) |>  { $0.height } |> asCGFloat)
        slider.orientation = .horizontal
    
        bag.insert(
            Observable
                .combineLatest(observePreferredMinHeight, observePreferredMaxHeight, resultSelector: { ($0, $1)})
                .subscribe(onNext: { [weak self] min, max in
                    self?.slider.value = [min.height.cgFloat(), max.height.cgFloat()]
                    self?.minLabel.text = min.description
                    self?.maxLabel.text = max.description
            }),
            slider.rx.controlEvent(.valueChanged).subscribe(onNext: { [weak self] _ in
                guard let slider = self?.slider else { return }
                guard let value = slider.value.itemAt(slider.draggedThumbIndex) else { return }
                guard let height = Height(rawValue: Int(value)) else { return }
                switch slider.draggedThumbIndex {
                case 0:
                    self?.minLabel.text = height.description
                case 1:
                    self?.maxLabel.text = height.description
                default: break
                }
            }),
            slider.rx.controlEvent(.touchUpInside).subscribe(onNext: { [weak self] _ in
                guard let slider = self?.slider else { return }
                guard let value = slider.value.itemAt(slider.draggedThumbIndex) else { return }
                guard let height = Height(rawValue: Int(value)) else { return }
                switch slider.draggedThumbIndex {
                case 0:
                    onUpdateMinHeight(height)
                case 1:
                    onUpdateMaxHeight(height)
                default: break
                }
            })
        )
    }
}
