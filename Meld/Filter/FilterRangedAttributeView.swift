//
//  FilterRangedAttributeView.swift
//  Meld
//
//  Created by Blake Rogers on 8/24/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import UIKit
import UtilitiesPackage

final class FilterRangedAttributeView: UIView {
    private var presenter: FilterRangedAttributePresenterIdentity!
    private func setViews() {
        let view = UIView()
        |> whiteBackground
        |> fill
        |> appendSubView(
            presenter.title
            |> named("title")
            |> textFont(appFont(16))
            |> textColored(appDarkGrey)
            |> alignedToTop
            |> alignToLeft(20)
        )
        |> appendSubView(
            UIView()
            |> named("underline")
            |> backgroundColored(appDiamond)
            |> viewHeight(1)
            |> alignToLeft(20)
            |> alignToRight(-20)
            |> alignToSibling("title", constraint: alignTopToSiblingBottom(4))
        )
        |> appendSubView(
            presenter.slider
            |> named("slider")
            |> alignToRight(-20)
            |> alignToLeft(20)
            |> alignToSibling("underline", constraint: alignTopToSiblingBottom(30))
        )
        |> appendSubView(
            presenter.minLabel
            |> textFont(appFont(12))
            |> textColored(appDarkGrey)
            |> textCenterAligned
            |> alignToSibling("slider", constraint: alignedToSiblingLeft)
            |> alignToSibling("slider", constraint: alignBottomToSiblingTop(4))
        )
        |> appendSubView(
            presenter.maxLabel
                |> textFont(appFont(12))
                |> textColored(appDarkGrey)
                |> textCenterAligned
                |> alignToSibling("slider", constraint: alignedToSiblingRight)
                |> alignToSibling("slider", constraint: alignBottomToSiblingTop(4))
            )
        self.add(views: view)
    }
}

extension FilterRangedAttributeView {
    convenience init(presenter: FilterRangedAttributePresenterIdentity) {
        self.init()
        self.presenter = presenter
        self.setViews()
    }
}


