//
//  OnboardingPresenter.swift
//  Meld
//
//  Created by Blake Rogers on 7/14/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import UIKit
import RxSwift

final class OnboardingPresenter {
    
    //MARK: Stored Properties
    private let interactor: OnboardingInteractorIdentity
    private let router: OnboardingRouter
    private let bag = DisposeBag()
    
    //MARK: UI Elements
    let signInButton = UIButton()
    let signUpButton = UIButton()
    let (list, _): (UIView, Observable<PersonalityType>) = personality_selectionList()
    
    init(interactor: OnboardingInteractorIdentity) {
        self.interactor = interactor
        self.router = OnboardingRouter()
        
        bag.insert(
            signInButton.rx.tap.subscribe(onNext: { [weak self] _ in
                self?.router.displaySignIn()
            }),
            signUpButton.rx.tap.subscribe(onNext: { [weak self] personality in
                self?.router.displayPersonalityTest()
            }),
            interactor.observeIsCurrentUser().subscribe(onNext: { [weak self] isCurrentUser in
                if isCurrentUser {
                    self?.router.displayHome()
                }
            })
        )
        
    }
}
