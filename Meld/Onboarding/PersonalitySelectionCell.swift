//
//  PersonalitySelectionCell.swift
//  Personality Selection
//
//  Created by blakerogers on 10/26/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import UtilitiesPackage

public let personalitySelectionCellSize: CGSize = CGSize(width: UIScreen.main.bounds.width, height: 410)
public let selectionListIsHorizontal: Bool = true
public let personalitySelectionCellFrame: (PersonalityType) -> CGRect = { type in
    let index = PersonalityType.allCases.firstIndex(of: type) ?? 0
    return selectionListIsHorizontal ?
    CGRect(origin: CGPoint(x: personalitySelectionCellSize.width*CGFloat(index), y: 0), size: personalitySelectionCellSize)
    : CGRect(origin: CGPoint(x: 0, y: personalitySelectionCellSize.height*CGFloat(index)), size: personalitySelectionCellSize)
}
let personalityImage: (PersonalityType) -> UIView = { type in
    return UIImageView(image: UIImage(named: type.background))
                |> scaled(.scaleAspectFill)
                >>> rounded(10)
                >>> isClipping
                >>> isInterActive
                >>> fill
                >>> alignedToCenterX
}
public let personality_selectionCell: (PersonalityType) -> (UIView, Observable<PersonalityType>)  = { type in
    
    let selectionGesture = UITapGestureRecognizer()
    
    let selection = selectionGesture.rx.event.flatMap { _  -> Observable<PersonalityType> in
        return .just(type)
    }
    
    let image = personalityImage(type) |> addGesture(selectionGesture)
///TODO: Reduce Build Time
    let view = UIView()
        |> named("personality_selection_cell")
        >>> whiteBackground
        >>> framed(personalitySelectionCellFrame(type))
        >>> appendSubView(UIView()
            |>  alignedToCenterX
            >>> alignToTop(5)
            >>> width_Height(300, 300)
            >>> diamondShadowed
            >>> appendSubView(image))
        >>> appendSubView(UILabel()
            |> named("title")
            >>> textCenterAligned
            >>> textColored(appBlack)
            >>> textForLabel(type.title)
            >>> textFont(appFont(18))
            >>> alignedToCenterX
            >>> alignToBottom(-70))
        >>> appendSubView(UILabel()
            |> named("description")
            >>> textColored(appBlack)
            >>> textForLabel(type.description)
            >>> textCenterAligned
            >>> textFont(appFont(12))
            >>> unlimitedLines
            >>> alignedToCenterX
            >>> width_Height(300, 100)
            >>> alignedToBottom)
        
    return (view, selection)
}

