//
//  PersonalitySelectionService.swift
//  Meld
//
//  Created by blakerogers on 11/15/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift

class PersonalitySelectionService {
    
    static func storePersonalitySelection(type: PersonalityType) {
        UserDefaults.standard.set(type.title, forKey: "personality_type")
    }
    
    static func selectedPersonalityType() -> Observable<PersonalityType> {
        return UserDefaults.standard.rx.observe(String.self, "personality_type").map { string -> PersonalityType? in
            PersonalityType.typeForTitle(string ?? "")
        }.compactMap { $0 }
    }
    
}
