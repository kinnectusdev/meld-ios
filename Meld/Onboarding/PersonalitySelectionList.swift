//
//  PersonalitySelectionList.swift
//  Personality Selection
//
//  Created by blakerogers on 10/26/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import UIKit
import RxSwift
import UtilitiesPackage

public let selectionListHeight: CGFloat = selectionListIsHorizontal ?
    personalitySelectionCellSize.height
    : personalitySelectionCellSize.height*CGFloat(PersonalityType.allCases.count)
public let selectionListWidth: CGFloat = selectionListIsHorizontal ?
personalitySelectionCellSize.width*CGFloat(PersonalityType.allCases.count)
    : appFrame.width
public let personality_selectionList: ()-> (UIView, Observable<PersonalityType>) = {
    let cells: [(UIView, Observable<PersonalityType>)] = PersonalityType.sortedList.map { (question) in
           return personality_selectionCell(question)
       }
       let selection = Observable.merge(cells.map { $0.1})
       // render view
       let view = UIView()
           |> named("personality_test")
           >>> clearBackground
           >>> appendSubView(UIScrollView()
                                |> fill
                                >>> isPaging
                                >>> named("test")
                                >>> scrollSize(CGSize(width: selectionListWidth, height: selectionListHeight))
                                >>> appendSubView(UIView()
                                               |> framed(CGRect(origin: .zero, size: CGSize(width: selectionListWidth, height: selectionListHeight))))
                                               >>> appendSubviews(cells.map { $0.0}))
       return (view, selection)
}
