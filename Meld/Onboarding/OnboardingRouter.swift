//
//  OnboardingRouter.swift
//  Meld
//
//  Created by Blake Rogers on 7/14/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import UIKit
import UtilitiesPackage

final class OnboardingRouter {
    
    func displayPersonalityTest() {
        let view = PersonalityTestRouter.presentation()
        currentController().present(view, animated: true, completion: nil)
    }
    
    func displaySignIn() {
        currentController().present(SignInRouter.presentation(), animated: true, completion: nil)
    }
    func displayHome() {
        currentController().present(HomeRouter.presentation(), animated: true, completion: nil)
    }
}

extension OnboardingRouter {
    static func presentation() -> UIViewController {
        OnboardingView(presenter: OnboardingPresenter(interactor: OnboardingInteractor()))
    }
}
