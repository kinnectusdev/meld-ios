//
//  SplashController.swift
//  Onboarding
//
//  Created by blakerogers on 9/26/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import UtilitiesPackage

final class OnboardingView: UIViewController {
    private var presenter: OnboardingPresenter!
    private func setViews() {
        
        let view = UIView()
            |> named("onboarding")
            ///TODO: Reduce Build Time
            >>>  whiteBackground
            >>> framed(view.frame)
            >>> appendSubView(UIImageView(image: UIImage(named: "logoDark"))
                                    |> width_Height(50, 50)
                                    >>> alignedToCenterX
                                    >>> alignToTop(50)
                                    >>> named("logo"))
            >>> appendSubView(UILabel()
                |> textAligned(.center)
                >>> named("onboarding_label")
                >>> unlimitedLines
                >>> alignToLeft(10)
                >>> alignToRight(-10)
                >>> textFont(appFont(14))
                >>> textColored(appBlack)
                >>> textForLabel("A Great Relationship is the Meeting of Two Complementary Personalities.")
                >>> alignToSibling("logo", constraint: alignTopToSiblingBottom(20))
                >>> alignedToCenterX
                >>> viewWidth(250))
            >>> appendSubView(presenter.list
                |> alignedToCenterX
                |> alignToSibling("onboarding_label", constraint: alignTopToSiblingBottom(20))
                >>> width_Height(appFrame.width, selectionListHeight)
                >>> maskedToBounds)
            >>> appendSubView(presenter.signInButton
                |> styledTitle(darkGreyAttributedString("Already a User?, Sign In", 12))
                >>> alignToBottom(-30)
                >>> alignToLeft(16))
            >>> appendSubView(presenter.signUpButton
                |> styledTitle(darkGreyAttributedString("Sign Up", 12))
                >>> alignToBottom(-30)
                >>> alignToRight(-16))
        
        self.view = view
    }
}

extension OnboardingView {
    convenience init(presenter: OnboardingPresenter) {
        self.init()
        self.presenter = presenter
        self.setViews()
    }
}

public let appFrame = UIScreen.main.bounds//CGRect(origin: .zero, size: CGSize(width: 375, height: 667))
