//
//  OnboardingViewModel.swift
//  Onboarding
//
//  Created by blakerogers on 9/22/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol OnboardingInteractorIdentity {
    func observeIsCurrentUser() -> Observable<Bool>
}

final class OnboardingInteractor {
   
    private let bag = DisposeBag()
    private let isCurrentUser = PublishSubject<Bool>()
    init() {
        bag.insert(
            UserService.currentUser()
                .map { $0.user }
                .compactMap { $0 }.map { _ in true }.bind(to: isCurrentUser)
        )
    }
}

extension OnboardingInteractor: OnboardingInteractorIdentity {
    func observeIsCurrentUser() -> Observable<Bool> {
        isCurrentUser.asObservable()
    }
}
