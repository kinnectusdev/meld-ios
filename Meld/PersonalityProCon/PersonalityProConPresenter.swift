//
//  PersonalityProConPresenter.swift
//  Meld
//
//  Created by Blake Rogers on 8/11/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import RxSwift
import UIKit

final class PersonalityProConPresenter {
    private let bag = DisposeBag()
    private let interactor: PersonalityProConInteractorIdentity
    
    let titleLabel = UILabel()
    let summaryLabel = UILabel()
    
    init(interactor: PersonalityProConInteractorIdentity) {
        
        self.interactor = interactor
        
        bag.insert(
            interactor.observeTitle().asDriver(onErrorJustReturn: .empty).drive(titleLabel.rx.text),
            interactor.observeSummary().asDriver(onErrorJustReturn: .empty).drive(summaryLabel.rx.text)
        )
    }
}
