//
//  PersonalityProConInteractor.swift
//  Meld
//
//  Created by Blake Rogers on 8/11/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import RxSwift


protocol PersonalityProConInteractorIdentity {
    func observeTitle() -> Observable<String>
    func observeSummary() -> Observable<String>
}

final class PersonalityProConInteractor {

    private let proCon: PersonalityProCon
    
    init(proCon: PersonalityProCon) {
        self.proCon = proCon
    }
}
extension PersonalityProConInteractor: PersonalityProConInteractorIdentity {
    func observeTitle() -> Observable<String> {
        .just(proCon.title)
    }
    
    func observeSummary() -> Observable<String> {
        .just(proCon.description)
    }
}
