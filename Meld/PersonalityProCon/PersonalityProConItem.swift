//
//  PersonalityProConItem.swift
//  Meld
//
//  Created by Blake Rogers on 8/11/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import UIKit
import UtilitiesPackage

final class PersonalityProConItem: UIView {
    
    private var presenter: PersonalityProConPresenter!
    
    private func setViews() {
        let view = UIView()
            |> fill
            |> whiteBackground
            |> isClipping
            >>> appendSubView(presenter.titleLabel
                                |> named("title")
                                >>> textFont(appFontFormal(16))
                                |> alignToTop(10)
                                |> alignToLeft(20))
            >>> appendSubView(UIView() |> backgroundColored(appSilver) |> alignToLeft(20) |> viewWidth(100) |> viewHeight(1) |> alignToSibling("title", constraint: alignTopToSiblingBottom(4)))
            >>> appendSubView(presenter.summaryLabel
                                |> unlimitedLines
                                |> textFont(appFont(14))
                                |> alignToRight(-20)
                                |> alignToSibling("title", constraint: alignedToSiblingLeft)
                                |> alignToSibling("title", constraint: alignTopToSiblingBottom(10)))
        
        self.add(views: view)
    }

}
extension PersonalityProConItem {
    convenience init(presenter: PersonalityProConPresenter) {
        self.init()
        self.presenter = presenter
        self.setViews()
    }
    func configure(presenter: PersonalityProConPresenter) {
        self.presenter = presenter
        self.setViews()
    }
}
