//
//  CompatiblePersonalityTypesInteractor.swift
//  Meld
//
//  Created by Blake Rogers on 8/31/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation

protocol CompatiblePersonalityTypesInteractorIdentity {
    func observeComptabileTypes() -> [PersonalityType]
}
final class CompatiblePersonalityTypesInteractor {
    private let personalityType: PersonalityType
    
    init(personalityType: PersonalityType) {
        self.personalityType = personalityType
    }
}
extension CompatiblePersonalityTypesInteractor: CompatiblePersonalityTypesInteractorIdentity {
    func observeComptabileTypes() -> [PersonalityType] {
        personalityType.complimentaryTypes
    }
}
