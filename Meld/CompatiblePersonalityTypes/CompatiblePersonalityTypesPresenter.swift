//
//  CompatiblePersonalityTypesPresenter.swift
//  Meld
//
//  Created by Blake Rogers on 8/31/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import RxSwift
import UtilitiesPackage
import UIKit

final class CompatiblePersonalityTypesPresenter {
    private let bag = DisposeBag()
    private let interactor: CompatiblePersonalityTypesInteractorIdentity
    private let router: CompatiblePersonalityTypesRouter
    
    let types: [UIView]
    let list = UIScrollView()
    
    init(interactor: CompatiblePersonalityTypesInteractorIdentity) {
        self.interactor = interactor
        let router = CompatiblePersonalityTypesRouter()
        self.router = router
        let contentSize = CGSize(width: interactor.observeComptabileTypes().count * 110, height: 50)
        self.list.contentSize = contentSize
        self.types = interactor.observeComptabileTypes().map { type -> UIView in
                UILabel()
                |> rounded(15)
                >>> backgroundColored(type.personalityGroup.color)
                >>> width_Height(100, 30)
                >>> textColored(.white)
                >>> textCenterAligned
                >>> titled(type.title)
                >>> textFont(appFont(18))
                >>> onTap { _ in
                    router.displayPersonalityType(type: type)
                }
        }
    }
}
