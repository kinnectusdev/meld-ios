//
//  CompatiblePersonalityTypesRouter.swift
//  Meld
//
//  Created by Blake Rogers on 8/31/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import UIKit
import UtilitiesPackage

final class CompatiblePersonalityTypesRouter {
    
    func displayPersonalityType(type: PersonalityType) {
        currentController().present(PersonalityTypeRouter.presentation(personalityType: type), animated: true, completion: nil)
    }
}
extension CompatiblePersonalityTypesRouter {
    static func presentation(type: PersonalityType) -> UIView {
        CompatiblePersonalityTypeItem(presenter: CompatiblePersonalityTypesPresenter(interactor: CompatiblePersonalityTypesInteractor(personalityType: type)))
    }
}
