//
//  MeldLocation.swift
//  Meld
//
//  Created by Blake Rogers on 5/25/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit

struct MeldLocation: Codable {
    let latitude: Double
    let longitude: Double
    
    var coordinate: CLLocationCoordinate2D {
        CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    static let empty: MeldLocation = MeldLocation(latitude: 0, longitude: 0)
}
extension MeldLocation: Equatable {
    static func ==(lhs: MeldLocation, rhs: MeldLocation) -> Bool {
        return lhs.latitude == rhs.latitude && lhs.longitude == rhs.longitude
    }
    static let centerLatitude: Double = 36.850800
    static let centerLongitude: Double = 76.285900
    static let testCoordinate = CLLocationCoordinate2D(latitude: centerLatitude, longitude: centerLongitude)
    static func random(miles: Double? = nil, coordinate: CLLocationCoordinate2D = testCoordinate) -> MeldLocation {
        guard let miles = miles else {
            let milesCollection: [Double] = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
            let randomMiles = milesCollection.randomElement()!
            return MeldLocation.random(miles: randomMiles)
        }
        let region = MKCoordinateRegion(center: coordinate, latitudinalMeters: miles*1600, longitudinalMeters: miles*1600)
        let latDelta = region.span.latitudeDelta
        let longDelta = region.span.longitudeDelta
        let latitudeRange = ((region.center.latitude - latDelta)...(region.center.latitude + latDelta))
        let latitude = Double.random(in: latitudeRange)
        let longitudeRange = ((region.center.longitude - longDelta)...(region.center.longitude + longDelta))
        let longitude = Double.random(in: longitudeRange)
        return MeldLocation(latitude: latitude, longitude: longitude)
    }
}
