//
//  UserService.swift
//  Meld
//
//  Created by blakerogers on 11/12/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import RxSwift
import RxCocoa
import Foundation
import Firebase
import FirebaseFirestore
import FirebaseStorage
import FirebaseFirestoreSwift
import UtilitiesPackage

public enum UserFetchServiceResult {
    case success(MeldUser)
    case failure(String)
    var user: MeldUser? {
        switch self {
        case .success(let user): return user
        default: return nil
        }
    }
}
public enum UsersFetchServiceResult {
    case success([MeldUser])
    case failure(String)
}
public enum LocalUserServiceResult {
    case success
    case failure(String)
}
public enum CurrentUserServiceResult {
    case success(MeldUser)
    case noCurrentUser
    var user: MeldUser? {
        switch self {
        case .success(let user):
            return user
        default: return nil
        }
    }
}
public enum UserUpdateServiceResult {
    case success
    case failure(String)
}
public class UserService {
    static func fetchUserByEmail(_ email: String) -> Observable<UserFetchServiceResult> {
        guard email.notEmpty() else { return .empty() }
        return Observable.create { observer in
            Firestore.firestore().collection("Users")
                .whereField("email", in: [email, email.lowercased()])
                .getDocuments { (snapshot, error) in
                if let error = error {
                    observer.onNext(.failure(error.localizedDescription))
                } else if let query = snapshot, let user = try? query.documents.first?.data(as: WireMeldUser.self) {
                    observer.onNext(.success(MeldUser(user: user)))
                } else {
                    observer.onNext(.failure("No User Found"))
                }
            }
            return Disposables.create()
        }
    }
    static func fetchUser(_ id: String) -> Observable<UserFetchServiceResult> {
        guard id.notEmpty() else { return .empty() }
        return Observable.create { observer in
            Firestore.firestore().collection("Users").document(id).getDocument { (snapshot, error) in
                if let error = error {
                    observer.onNext(.failure(error.localizedDescription))
                }
                do {
                    if let user = try snapshot?.data(as: WireMeldUser.self) {
                        observer.onNext(.success(MeldUser(user: user)))
                    } else {
                        observer.onNext(.failure("No User Found"))
                    }
                } catch {
                    print(error.localizedDescription)
                }
                    
            }
            return Disposables.create()
        }
    }
    static func userWithCredentialIsCurrentUser(id: String) -> Single<MeldUser?> {
        return Single.create { observer in
            let query = Firestore.firestore().collection("Users").whereField("id", isEqualTo: id).limit(to: 1)
            query.getDocuments { (snapshot, error) in
                if let error = error {
                    observer(.failure(error))
                } else {
                    if let documents = snapshot?.documents {
                        let users = documents.compactMap { document -> MeldUser? in
                            try? document.data(as: MeldUser.self)
                        }.compactMap { $0 }
                         observer(.success(users.first))
                    } else {
                        observer(.success(nil))
                    }
                }
            }
            return Disposables.create()
        }
    }
    static func updateUserSeenStatus(id: String) {
        if let seenUsers = UserDefaults.standard.array(forKey: "seenUsers") as? [String] {
            guard !seenUsers.contains(id) else { return }
                UserDefaults.standard.set( seenUsers.appending(id), forKey: "seenUsers")
        } else {
            UserDefaults.standard.set( [id], forKey: "seenUsers")
        }
    }
    static func fetchSeenUserIds() -> [String] {
        guard let ids = UserDefaults.standard.array(forKey: "seenUsers") as? [String] else {
            return []
        }
        return ids
    }
    static func observeSeenUsers() -> Observable<[String]> {
        return UserDefaults.standard.rx.observe([String].self, "seenUsers").map { $0 ?? [] }
    }
    static func observeLikedUsers() -> Observable<[String]> {
        return currentUser().map { $0.user }.compactMap { $0 }.map { $0.likedUsers }
    }
    static func updateUserLikedStatus(id: String, isLiked: Bool) -> Observable<Bool> {
        return currentUser().take(1).map { $0.user }.compactMap { $0 }.flatMap { user -> Observable<Bool> in
            let updatedUser = user |> prop(\.likedUsers)({ users in
                if !isLiked {
                    return users |> removing(id)
                } else {
                    return users |> appending(id)
                }
            })
            return Observable.zip(setCurrentUser(user: updatedUser), updateRemoteCurrentUser(user: updatedUser)).map { $0 && $1 }
        }
    }
    static func updateUserLikedStatus(id: String) -> Observable<Bool> {        
        return currentUser().take(1).map { $0.user }.compactMap { $0 }.flatMap { user -> Observable<Bool> in
            let updatedUser = user |> prop(\.likedUsers)({ users in
                let isLiked = users.contains(id)
                if isLiked {
                    return users |> removing(id)
                } else {
                    return users |> appending(id)
                }
            })
            return Observable.zip(setCurrentUser(user: updatedUser), updateRemoteCurrentUser(user: updatedUser)).map { $0 && $1 }
        }
    }
    static func observeUserLikedStatus(id: String) -> Observable<Bool> {
        return currentUser().map { $0.user }.compactMap { $0 }.map { $0.likedUsers }.map { $0.contains(id)}
    }
    
    static func setCurrentUser(user: MeldUser) -> Observable<Bool> {
        return Observable.create { observer in
            do {
                let data = try JSONEncoder().encode(user)
                UserDefaults.standard.set(data, forKey: "current_user")
                observer.onNext(true)
            } catch {
                observer.onNext(false)
            }
            return Disposables.create()
        }
    }
    static func observeAttribute(attribute: Attributes) -> Observable<AttributeOption?>  {
        currentUser()
            .map { $0.user }
            .compactMap { $0 }
            .map { $0.attributes }
            .map { attributes -> AttributeOption? in
                
                if let value = attributes.filter { $0?.category() == attribute }.first {
                    return value
                } else {
                    return attributes.filter { $0?.category() == attribute }.first??.defaultOption
                }
        }
    }
    static func updateAttribute(attribute: Attributes, option: AttributeOption) -> Observable<Bool>  {
        return currentUser().take(1).map { $0.user }.compactMap { $0 }.map { user -> MeldUser in
            switch attribute {
            case .age:
                return user |> prop(\.age)({_ in option as? Age })
            case .iq:
                return user |> prop(\.iq)({_ in option as? IQ })
            case .height:
                return user |> prop(\.height)({_ in option as? Height })
            case .ethnicity:
                return user |> prop(\.ethnicity)({_ in option as? Ethnicity })
            case .hometown:
                return user |> prop(\.hometown)({_ in option as? Hometown })
            case .gender:
                return user |> prop(\.gender)({_ in option as? Gender })
            case .sexuality:
                return user |> prop(\.sexuality)({_ in option as? Sexuality })
            case .politics:
                return user |> prop(\.politics)({_ in option as? Politics })
            case .vices:
                return user |> prop(\.vices)({ _ in option as? Vices })
            case .core_Values:
                return user |> prop(\.coreValues)({ _ in option as? CoreValues })
            case .parental_Status:
                return user |> prop(\.parentalStatus)({_ in option as? ParentalStatus })
            case .family_Plans:
                return user |> prop(\.familyPlans)({_ in option as? FamilyPlans })
            case .outlook:
                return user |> prop(\.outlook)({_ in option as? Outlook })
            case .education_Level:
                return user |> prop(\.educationLevel)({_ in option as? EducationLevel })
            case .religion:
                return user |> prop(\.religion)({_ in option as? Religion })
            case .physique:
                return user |> prop(\.physique)({_ in option as? Physique })
            }
        }.flatMap { user -> Observable<Bool> in
            return Observable.zip(updateRemoteCurrentUser(user: user), setCurrentUser(user: user)).map { remoteResult, localResult in
                return remoteResult && localResult
            }
        }
    }
    static func updateUserName(_ name: String) -> Observable<Bool> {
        return currentUser().take(1).map { $0.user }.compactMap { $0 }.flatMap { user -> Observable<Bool> in
            guard user.name != name else { return .never() }
            let update = user |> prop(\.name)({_ in return name})
            return Observable.zip(updateRemoteCurrentUser(user: update), setCurrentUser(user: update)).map { remoteResult, localResult in
                return remoteResult && localResult
            }
        }
    }
    static func updateUserEmail(_ email: String) -> Observable<Bool> {
        return currentUser().take(1).map { $0.user }.compactMap { $0 }.flatMap { user -> Observable<Bool> in
            guard user.email != email else { return .never() }
            let update = user |> prop(\.email)({_ in return email})
            return Observable.zip(updateRemoteCurrentUser(user: update), setCurrentUser(user: update)).map { remoteResult, localResult in
                return remoteResult && localResult
            }
        }
    }
    static func updateUserPersonality(_ personality: PersonalityType) -> Observable<Bool> {
        return currentUser().take(1).map { $0.user }.compactMap { $0 }.flatMap { user -> Observable<Bool> in
            guard user.personalityType != personality else { return .never() }
            let update = user |> prop(\.personalityType)({_ in return Optional(personality) })
            return Observable.zip(updateRemoteCurrentUser(user: update), setCurrentUser(user: update)).map { remoteResult, localResult in
                return remoteResult && localResult
            }
        }
    }
    static func updateRemoteCurrentUser(user: MeldUser) -> Observable<Bool> {
        let user = WireMeldUser(user: user)
        return Observable.create { observer in
            do {
                try Firestore.firestore().collection("Users").document(user.identifier).setData(from: user)
                observer.onNext(true)
            } catch {
                print(error.localizedDescription)
                observer.onNext(false)
            }
            return Disposables.create()
        }
    }
    static func updateLocalAndRemoteCurrentUser(user: MeldUser) -> Observable<Bool> {
        return Observable.zip(updateRemoteCurrentUser(user: user), setCurrentUser(user: user)).map { local, remote -> Bool in
            return local && remote
        }
    }
    static func setLocalUserPersonalityType(personality: PersonalityType) -> Observable<Bool>  {
        return currentUser().take(1).flatMap { result  -> Observable<Bool> in
            switch result {
            case let .success(user):
                let update = user |> prop(\.personalityType)({_ in return Optional(personality)})
                return setCurrentUser(user: update)
            default:
                UserDefaults.standard.setValue(personality, forKey: "personality_type")
                return .just(true)
            }
        }
    }
    static func updatePersonalityTestResult(result: PersonalityTestResult) -> Observable<Bool> {
        return currentUser().take(1).map { $0.user }.compactMap { $0 }.flatMap { user -> Observable<Bool> in
            let update = user |> prop(\.personalityTestResult)({_ in return Optional(result) })
            return Observable.zip(updateRemoteCurrentUser(user: update), setCurrentUser(user: update)).map { remoteResult, localResult in
                return remoteResult && localResult
            }
        }
    }
    static func observeUserPersonalityType() -> Observable<PersonalityType?> {
        return currentUser().flatMap { result  -> Observable<PersonalityType?> in
            switch result {
            case let .success(user):
                return .just(user.personalityType)
            default:
                return UserDefaults.standard.rx.observe(PersonalityType.self, "personality_type").map { type -> PersonalityType?  in
                    return type
                }
            }
        }
    }

    static func setUserPricePlan(plan: PricePlan) -> Observable<Bool> {
        return .never()
//        return currentUser().flatMap { userResult -> Observable<Bool> in
//            switch userResult {
//            case .success(let user):
//                let update = user |> prop(\.pricePlan)({_ in return plan.productIdentifier})
//                return setCurrentUser(user: update).concat(updateRemoteCurrentUser(user: update))
//            default: return .just(false)
//            }
//        }
    }
    static func logout() {
        UserDefaults.standard.setValue(nil, forKey: "current_user")
        UserDefaults.standard.setValue(nil, forKey: "previous_test_answers")
        UserDefaults.standard.setValue(nil, forKey: "personality_type")
        UserDefaults.standard.setValue(nil, forKey: "fcmToken")
        UserDefaults.standard.setValue(nil, forKey: "device_pushkit_token")
        UserDefaults.standard.setValue(nil, forKey: "deviceToken")
        let firebaseAuth = Auth.auth()
        do {
          try firebaseAuth.signOut()
        } catch let signOutError as NSError {
          print ("Error signing out: %@", signOutError)
        }
    }
    static func currentUser() -> Observable<CurrentUserServiceResult> {
        return UserDefaults.standard.rx.observe(Data.self, "current_user").map { data -> CurrentUserServiceResult in
            if let data = data {
                do {
                    let user = try JSONDecoder().decode(MeldUser.self, from: data)
                    return .success(user)
                } catch {
                    return .noCurrentUser
                }
            } else {
                return .noCurrentUser
            }
        }
    }
    static func immediateCurrentUser() -> MeldUser? {
        let data = UserDefaults.standard.data(forKey: "current_user")
            if let data = data {
                do {
                    let user = try JSONDecoder().decode(MeldUser.self, from: data)
                    return user
                } catch {
                    return nil
                }
            } else {
                return nil
        }
    }
    static func updateUserNotificationTokens(fcmToken: String, apnsToken: String, pushKitToken: String) -> Observable<Bool> {
        return currentUser().take(1).compactMap { $0.user }.flatMap { user -> Observable<Bool> in
            let update = user
                |> prop(\.fcmToken)({token in
                    return fcmToken.notEmpty() ? fcmToken : token
                })
                |> prop(\.apnsToken)({_ in return apnsToken})
                |> prop(\.pushKitToken)({_ in return pushKitToken})
            return updateLocalAndRemoteCurrentUser(user: update)
        }
    }
    static func updateUserFCMToken(fcmToken: String) -> Observable<Bool> {
        return currentUser().take(1).compactMap { $0.user }.flatMap { user -> Observable<Bool> in
            let update = user
                |> prop(\.fcmToken)({token in
                    return fcmToken.notEmpty() ? fcmToken : token
                })
            return updateLocalAndRemoteCurrentUser(user: update)
        }
    }
    static func updateUserAPNSToken(apnsToken: String) -> Observable<Bool> {
        return currentUser().take(1).compactMap { $0.user }.flatMap { user -> Observable<Bool> in
            let update = user
                |> prop(\.apnsToken)({_ in return apnsToken})
            return updateLocalAndRemoteCurrentUser(user: update)
        }
    }
    static func updateUserPushKitNotificationToken(token: String) -> Observable<Bool> {
        return currentUser().take(1).compactMap { $0.user }.flatMap { user -> Observable<Bool> in
            let update = user
                |> prop(\.pushKitToken)({_ in return token})
            return Observable.zip(setCurrentUser(user: update), updateRemoteCurrentUser(user: update)).map { local, remote -> Bool in
                return local && remote
            }
        }
    }
    static func setProfileVisible(isVisible: Bool) -> Observable<UserUpdateServiceResult> {
        return currentUser().take(1).compactMap { $0.user }.flatMap { user -> Observable<UserUpdateServiceResult> in
            let updatedUser = user |> prop(\.profileVisible)({_ in isVisible})
            return Observable.zip(setCurrentUser(user: updatedUser), updateRemoteCurrentUser(user: updatedUser)).map  { didUpdateLocally, didUpdateRemote -> UserUpdateServiceResult in
                if didUpdateLocally && didUpdateRemote {
                    return .success
                } else {
                    return .failure("User Update Failed")
                }
            }
        }
    }
    ///TODO: implement a user image storage function
    static func setUserImage(image: Data?) -> Observable<UserUpdateServiceResult> {
        guard let image = image else { return .just(.failure("No Image Data"))}
        return currentUser().take(1).compactMap { $0.user }.flatMap { user -> Observable<UserUpdateServiceResult> in
            return Observable<(UserUpdateServiceResult, String)>.create { observer in
                let storageRef = Storage.storage().reference()
                    .child("UserImages")
                    .child(user.id)
                    .child("\(NSUUID().uuidString).png")
                let path = storageRef.fullPath
                storageRef.putData(image, metadata: nil) { (_, error) in
                    if let error = error {
                        observer.onNext((.failure(error.localizedDescription), ""))
                    } else {
                        observer.onNext((.success, path))
                    }
                }
                return Disposables.create()
            }.flatMap { (updateResult, imageURL) -> Observable<UserUpdateServiceResult> in
                switch updateResult {
                case .failure:
                    return .just(updateResult)
                case .success:
                    let update = user |> prop(\.imageURLs)({$0 |> appending(imageURL)})
                    return Observable.zip(updateRemoteCurrentUser(user: update), setCurrentUser(user: update)).map { remoteUpdate, localUpdate -> UserUpdateServiceResult in
                        if (remoteUpdate && localUpdate) {
                            return .success
                        } else {
                            return .failure("User Info Update Failed")
                        }
                    }
                }
            }
        }
    }
    static func deleteUserImage(url: String) -> Observable<Bool> {
        return currentUser()
            .take(1)
            .map { $0.user }
            .compactMap { $0 }
            .do(onNext: { user in
                Storage.storage().reference(withPath: url).delete { (error) in
                    print(error)
                }
            })
            .flatMap { user -> Observable<Bool> in
                let update = user |> prop(\.imageURLs)({ urls in
                    return urls |> removing(url)
                })
                return Observable.zip(updateRemoteCurrentUser(user: update), setCurrentUser(user: update)).map { remoteResult, localResult in
                    return remoteResult && localResult
                }
        }
    }
    static func addUserImage(image: Data?) -> Observable<Bool> {
        guard let data = image else { return .empty() }
        
        return currentUser()
            .take(1)
            .map { $0.user }
            .compactMap { $0 }
            .flatMap { user -> Observable<(Bool, String)> in
                return Observable<(Bool, String)>.create { observer in
                    let path = "Images/\(user.id)/\(NSUUID().uuidString).png"
                Storage.storage().reference(withPath: path).putData(data, metadata: nil) { (_, error) in
                    if let error = error {
                        observer.onNext((false, error.localizedDescription))
                    } else {
                        observer.onNext((true, path))
                    }
                }
                return Disposables.create()
            }
        }.do(onNext: { args in
            let (isSuccess, _) = args
            if isSuccess {
                UserDefaults.standard.setValue(nil, forKey: "selected_image")
            }
        }).flatMap { args -> Observable<Bool> in
            let (isSuccess, imageURL) = args
            if isSuccess {
                return currentUser().take(1).map { $0.user }.compactMap { $0 }.flatMap { user -> Observable<Bool> in
                    let update = user
                                |> prop(\.imageURLs)({ $0 |> appending(imageURL) })
                    return Observable.zip(updateRemoteCurrentUser(user: update), setCurrentUser(user: update)).map { remoteResult, localResult in
                        return remoteResult && localResult
                    }
                }
            } else {
                return .just(false)
            }
        }
    }
    static func updateUserLocation(latitude: Double, longitude: Double) -> Observable<Bool> {
        return currentUser().take(1).map { $0.user }.compactMap { $0 }.flatMap { user -> Observable<Bool> in
            let update = user
                |> prop(\.location)({_ in return Optional(MeldLocation(latitude: latitude, longitude: longitude)) })
            return Observable.zip(updateRemoteCurrentUser(user: update), setCurrentUser(user: update)).map { remoteResult, localResult in
                return remoteResult && localResult
            }
        }
    }
    static func enableProfileVisibility() -> Observable<Bool> {
        return currentUser().take(1).map { $0.user }.compactMap { $0 }.flatMap { user -> Observable<Bool> in
            let update = user
                |> prop(\.profileVisible)({_ in return true})
            return Observable.zip(updateRemoteCurrentUser(user: update), setCurrentUser(user: update)).map { remoteResult, localResult in
                return remoteResult && localResult
            }
        }
    }
    static func disableProfileVisibility() -> Observable<Bool> {
        return currentUser().take(1).map { $0.user }.compactMap { $0 }.flatMap { user -> Observable<Bool> in
            let update = user
                |> prop(\.profileVisible)({_ in return false})
            return Observable.zip(updateRemoteCurrentUser(user: update), setCurrentUser(user: update)).map { remoteResult, localResult in
                return remoteResult && localResult
            }
        }
    }
    static func unregisterForNotifications() -> Observable<Bool> {
        return Observable<Void>.create { observer in
            PushNotificationService.unregisterForNotifications()
            observer.onNext(())
            return Disposables.create()
        }.flatMap { _ -> Observable<Bool> in
            return currentUser().take(1).map { $0.user }.compactMap { $0 }.flatMap { user -> Observable<Bool> in
                let update = user
                                |> prop(\.fcmToken)({_ in return ""})
                                |> prop(\.apnsToken)({_ in return ""})
                return Observable.zip(updateRemoteCurrentUser(user: update), setCurrentUser(user: update)).map { remoteResult, localResult in
                    return remoteResult && localResult
                }
            }
        }
    }
    
    static func makeFakeProfiles() {
        let fakeProfiles = (0...100).map { _ in MeldUser.mockUser() }.map { WireMeldUser(user: $0)}
        let batch = Firestore.firestore().batch()
        fakeProfiles.forEach { profile in
            let document = Firestore.firestore().collection("Users").document()
            let finalProfile = profile
                |> prop(\.id)({_ in Optional(document.documentID) })
            do {
                print("did make user: \(finalProfile.name)")
               try batch.setData(from: finalProfile, forDocument: document)
            } catch {
                print(error.localizedDescription)
            }
        }
        batch.commit()
    }
    static func updateUserLocations() {
        Firestore.firestore().collection("Users").getDocuments { (snapshot, error) in
            if let documents = snapshot?.documents {
                let ids = documents.compactMap { try? $0.data(as: WireMeldUser.self) }.map { $0.id!}
                ids.forEach { id in
                    let randomLocation = MeldLocation.random()
                    let location = ["location": ["latitude": randomLocation.latitude, "longitude": randomLocation.longitude]]
                    Firestore.firestore().collection("Users").document(id).updateData(location) { (error) in
                        if let error = error {
                            print("error: \(error.localizedDescription)")
                        }
                    }
                }
            }
        }
    }
}
