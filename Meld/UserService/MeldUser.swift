//
//  MeldUser.swift
//  Meld
//
//  Created by blakerogers on 11/12/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import Foundation
import FirebaseFirestoreSwift
public struct WireMeldUser: Identifiable, Codable {
    @DocumentID public var id: String?
    var email: String
    var fcmToken: String
    var apnsToken: String
    var pushKitToken: String
    var imageURLs: [String]
    var name: String
    var age: Age?
    var iq: IQ?
    var height: Height?
    var ethnicity: Ethnicity?
    var hometown: Hometown?
    var gender: Gender?
    var sexuality: Sexuality?
    var politics: Politics?
    var vices: Vices?
    var coreValues: CoreValues?
    var parentalStatus: ParentalStatus?
    var familyPlans: FamilyPlans?
    var outlook: Outlook?
    var educationLevel: EducationLevel?
    var religion: Religion?
    var physique: Physique?
    var personalityType: PersonalityType?
    var personalityTestResult: PersonalityTestResult?
    var location: MeldLocation?
    var lastSignIn: Double
    var profileVisible: Bool
    var currentConversations: [String]
    var currentMatch: String
    var likedUsers: [String]
    var blockList: [String]
    
    var identifier: String {
        id ?? .empty
    }
    enum Keys: CodingKey {
        case id
        case email
        case fcmToken
        case apnsToken
        case pushKitToken
        case imageURLs
        case name
        case age
        case iq
        case height
        case ethnicity
        case hometown
        case gender
        case sexuality
        case politics
        case vices
        case coreValues
        case parentalStatus
        case familyPlans
        case outlook
        case educationLevel
        case religion
        case physique
        case personalityType
        case location
        case lastSignIn
        case profileVisible
        case currentConversations
        case currentMatch
        case likedUsers
        case blockList
    }
    
    init(user: MeldUser) {
        self.id = user.id
        self.email = user.email
        self.fcmToken = user.fcmToken
        self.apnsToken = user.apnsToken
        self.pushKitToken = user.pushKitToken
        self.imageURLs = user.imageURLs
        self.name = user.name
        self.age = user.age
        self.iq = user.iq
        self.height = user.height
        self.ethnicity = user.ethnicity
        self.hometown = user.hometown
        self.gender = user.gender
        self.sexuality = user.sexuality
        self.politics = user.politics
        self.vices = user.vices
        self.coreValues = user.coreValues
        self.parentalStatus = user.parentalStatus
        self.familyPlans = user.familyPlans
        self.outlook = user.outlook
        self.educationLevel = user.educationLevel
        self.religion = user.religion
        self.physique = user.physique
        self.personalityType = user.personalityType
        self.location = user.location
        self.lastSignIn = user.lastSignIn
        self.profileVisible = user.profileVisible
        self.currentConversations = user.currentConversations
        self.currentMatch = user.currentMatch
        self.likedUsers = user.likedUsers
        self.blockList = user.blockList
    }
    
}
public struct MeldUser: Codable {
    var id: String
    var email: String
    var fcmToken: String
    var apnsToken: String
    var pushKitToken: String
    var imageURLs: [String]
    var name: String
    var age: Age?
    var iq: IQ?
    var height: Height?
    var ethnicity: Ethnicity?
    var hometown: Hometown?
    var gender: Gender?
    var sexuality: Sexuality?
    var politics: Politics?
    var vices: Vices?
    var coreValues: CoreValues?
    var parentalStatus: ParentalStatus?
    var familyPlans: FamilyPlans?
    var outlook: Outlook?
    var educationLevel: EducationLevel?
    var religion: Religion?
    var physique: Physique?
    var personalityType: PersonalityType?
    var personalityTestResult: PersonalityTestResult?
    var location: MeldLocation?
    var lastSignIn: Double
    var profileVisible: Bool
    var currentConversations: [String]
    var currentMatch: String
    var likedUsers: [String]
    var blockList: [String]
    
    var attributes: [AttributeOption?] {
        [age,
        iq,
        height,
        ethnicity,
        hometown,
        gender,
        sexuality,
        politics,
        vices,
        coreValues,
        parentalStatus,
        familyPlans,
        outlook,
        educationLevel,
        religion,
        physique]
    }
    var publicAttributes: [AttributeOption?] {
        [age,
        iq,
        height,
        hometown,
        vices,
        coreValues,
        outlook,
        educationLevel,
        physique]
    }
    
    enum Keys: CodingKey {
        case id
        case email
        case fcmToken
        case apnsToken
        case pushKitToken
        case imageURLs
        case name
        case age
        case iq
        case height
        case ethnicity
        case hometown
        case gender
        case sexuality
        case politics
        case vices
        case coreValues
        case parentalStatus
        case familyPlans
        case outlook
        case educationLevel
        case religion
        case physique
        case personalityType
        case personalityTestResult
        case location
        case lastSignIn
        case profileVisible
        case currentConversations
        case currentMatch
        case likedUsers
        case blockList
    }
    
    static var empty: MeldUser {
        return MeldUser(id: "", email: "", fcmToken: "", apnsToken: "", pushKitToken: "", imageURLs: [], name: "", age: nil, iq: nil, height: nil, ethnicity: nil, hometown: nil, gender: nil, sexuality: nil, politics: nil, vices: nil, coreValues: nil, parentalStatus: nil, familyPlans: nil, outlook: nil, educationLevel: nil, religion: nil, physique: nil, personalityType: nil, personalityTestResult: nil, location: nil, lastSignIn: 0, profileVisible: false, currentConversations: [], currentMatch: "", likedUsers: [], blockList: [])
    }
    static func mockUser() -> MeldUser {
        let gender = [Gender.female, Gender.male].randomElement()
        let name = Names.random()
        return MeldUser(id: .empty,
                 email: "\(name)@mail.com",
                 fcmToken: .empty,
                 apnsToken: .empty,
                 pushKitToken: .empty,
                 imageURLs: ["Images/Users/".appending(AssignRandomImages.random(isMale: gender == .male)).appending(".jpg")],
                 name: name,
                 age: Age.allCases.randomElement(),
                 iq: IQ.allCases.randomElement(),
                 height: Height.all.randomElement(),
                        ethnicity: Ethnicity.african_Descent,
                 hometown: Hometown.random(),
                 gender: gender,
                        sexuality: Sexuality.heterosexual,
                 politics: Politics.allCases.randomElement(),
                 vices: Vices.random(),
                 coreValues: CoreValues.random(),
                 parentalStatus: ParentalStatus.allCases.randomElement(),
                 familyPlans: FamilyPlans.allCases.randomElement(),
                 outlook: Outlook.allCases.randomElement(),
                 educationLevel: EducationLevel.allCases.randomElement(),
                 religion: Religion.allCases.randomElement(),
                 physique: Physique.allCases.randomElement(),
                 personalityType: PersonalityType.allCases.randomElement(),
                 personalityTestResult: PersonalityTestResult.empty,
                 location: MeldLocation.random(),
                 lastSignIn: Date().timeIntervalSince1970,
                 profileVisible: true,
                 currentConversations: [],
                 currentMatch: .empty,
                 likedUsers: [],
                 blockList: [])
    }
   
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: Keys.self)
        do {
            try container.encode(id, forKey: .id)
            try container.encode(email, forKey: .email)
            try container.encode(fcmToken, forKey: .fcmToken)
            try container.encode(apnsToken, forKey: .apnsToken)
            try container.encode(pushKitToken, forKey: .pushKitToken)
            try container.encode(imageURLs, forKey: .imageURLs)
            try container.encode(name, forKey: .name)
            try container.encode(age, forKey: .age)
            try container.encode(iq, forKey: .iq)
            try container.encode(height, forKey: .height)
            try container.encode(ethnicity, forKey: .ethnicity)
            try container.encode(hometown, forKey: .hometown)
            try container.encode(gender, forKey: .gender)
            try container.encode(sexuality, forKey: .sexuality)
            try container.encode(politics, forKey: .politics)
            try container.encode(vices, forKey: .vices)
            try container.encode(coreValues, forKey: .coreValues)
            try container.encode(parentalStatus, forKey: .parentalStatus)
            try container.encode(familyPlans, forKey: .familyPlans)
            try container.encode(outlook, forKey: .outlook)
            try container.encode(educationLevel, forKey: .educationLevel)
            try container.encode(religion, forKey: .religion)
            try container.encode(physique, forKey: .physique)
            try container.encode(personalityType, forKey: .personalityType)
            try container.encode(personalityTestResult, forKey: .personalityTestResult)
            try container.encode(location, forKey: .location)
            try container.encode(lastSignIn, forKey: .lastSignIn)
            try container.encode(profileVisible, forKey: .profileVisible)
            try container.encode(currentConversations, forKey: .currentConversations)
            try container.encode(currentMatch, forKey: .currentMatch)
            try container.encode(likedUsers, forKey: .likedUsers)
            try container.encode(blockList, forKey: .blockList)
        } catch {
            print(error.localizedDescription)
        }
    }
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: Keys.self)
        id = try container.decode(String.self, forKey: .id)
        email = try container.decode(String.self, forKey: .email)
        fcmToken = try container.decode(String.self, forKey: .fcmToken)
        apnsToken = try container.decode(String.self, forKey: .apnsToken)
        pushKitToken = try container.decode(String.self, forKey: .pushKitToken)
        imageURLs = try container.decode([String].self, forKey: .imageURLs)
        name = try container.decode(String.self, forKey: .name)
        age = try? container.decode(Age.self, forKey: .age)
        iq = try? container.decode(IQ.self, forKey: .iq)
        height = try? container.decode(Height.self, forKey: .height)
        ethnicity = try? container.decode(Ethnicity.self, forKey: .ethnicity)
        hometown = try? container.decode(Hometown.self, forKey: .hometown)
        gender = try? container.decode(Gender.self, forKey: .gender)
        sexuality = try? container.decode(Sexuality.self, forKey: .sexuality)
        politics = try? container.decode(Politics.self, forKey: .politics)
        vices = try? container.decode(Vices.self, forKey: .vices)
        coreValues = try? container.decode(CoreValues.self, forKey: .coreValues)
        parentalStatus = try? container.decode(ParentalStatus.self, forKey: .parentalStatus)
        familyPlans = try? container.decode(FamilyPlans.self, forKey: .familyPlans)
        outlook = try? container.decode(Outlook.self, forKey: .outlook)
        educationLevel = try? container.decode(EducationLevel.self, forKey: .educationLevel)
        religion = try? container.decode(Religion.self, forKey: .religion)
        physique = try? container.decode(Physique.self, forKey: .physique)
        personalityType = try? container.decode(PersonalityType.self, forKey: .personalityType)
        personalityTestResult = try? container.decode(PersonalityTestResult.self, forKey: .personalityTestResult)
        location = try? container.decode(MeldLocation.self, forKey: .location)
        lastSignIn = try container.decode(Double.self, forKey: .lastSignIn)
        profileVisible = try container.decode(Bool.self, forKey: .profileVisible)
        currentConversations = try container.decode([String].self, forKey: .currentConversations)
        currentMatch = try container.decode(String.self, forKey: .currentMatch)
        likedUsers = try container.decode([String].self, forKey: .likedUsers)
        blockList = try container.decode([String].self, forKey: .blockList)
    }
    
    
    init(id: String,
             email: String,
             fcmToken: String,
             apnsToken: String,
             pushKitToken: String,
             imageURLs: [String],
             name:  String,
             age: Age?,
             iq: IQ?,
             height: Height?,
             ethnicity: Ethnicity?,
             hometown: Hometown?,
             gender: Gender?,
             sexuality: Sexuality?,
             politics: Politics?,
             vices: Vices?,
             coreValues: CoreValues?,
             parentalStatus: ParentalStatus?,
             familyPlans: FamilyPlans?,
             outlook: Outlook?,
             educationLevel: EducationLevel?,
             religion: Religion?,
             physique: Physique?,
             personalityType: PersonalityType?,
             personalityTestResult: PersonalityTestResult?,
             location: MeldLocation?,
             lastSignIn: Double,
             profileVisible: Bool,
             currentConversations: [ String],
             currentMatch:  String,
             likedUsers: [ String],
             blockList: [ String]) {
        
        
        self.id = id
        self.email = email
        self.fcmToken = fcmToken
        self.apnsToken = apnsToken
        self.pushKitToken = pushKitToken
        self.imageURLs = imageURLs
        self.name = name
        self.age = age
        self.iq = iq
        self.height = height
        self.ethnicity = ethnicity
        self.hometown = hometown
        self.gender = gender
        self.sexuality = sexuality
        self.politics = politics
        self.vices = vices
        self.coreValues = coreValues
        self.parentalStatus = parentalStatus
        self.familyPlans = familyPlans
        self.outlook = outlook
        self.educationLevel = educationLevel
        self.religion = religion
        self.physique = physique
        self.personalityType = personalityType
        self.personalityTestResult = personalityTestResult
        self.location = location
        self.lastSignIn = lastSignIn
        self.profileVisible = profileVisible
        self.currentConversations = currentConversations
        self.currentMatch = currentMatch
        self.likedUsers = likedUsers
        self.blockList = blockList
    }
    init(user: WireMeldUser) {
        self.id = user.id ?? .empty
        self.email = user.email
        self.fcmToken = user.fcmToken
        self.apnsToken = user.apnsToken
        self.pushKitToken = user.pushKitToken
        self.imageURLs = user.imageURLs
        self.name = user.name
        self.age = user.age
        self.iq = user.iq
        self.height = user.height
        self.ethnicity = user.ethnicity
        self.hometown = user.hometown
        self.gender = user.gender
        self.sexuality = user.sexuality
        self.politics = user.politics
        self.vices = user.vices
        self.coreValues = user.coreValues
        self.parentalStatus = user.parentalStatus
        self.familyPlans = user.familyPlans
        self.outlook = user.outlook
        self.educationLevel = user.educationLevel
        self.religion = user.religion
        self.physique = user.physique
        self.personalityType = user.personalityType
        self.personalityTestResult = user.personalityTestResult
        self.location = user.location
        self.lastSignIn = user.lastSignIn
        self.profileVisible = user.profileVisible
        self.currentConversations = user.currentConversations
        self.currentMatch = user.currentMatch
        self.likedUsers = user.likedUsers
        self.blockList = user.blockList
    }
}
extension MeldUser: Equatable {
    public static func == (lhs: MeldUser, rhs: MeldUser) -> Bool {
        lhs.id == rhs.id
    }
}
extension MeldUser: Hashable {
    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}
